package alcatel.services;

import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.Build;
import android.os.IBinder;
import android.os.PowerManager;
import android.util.Log;

import androidx.core.app.NotificationCompat;

import com.arowana.houdini.byod.R;
import com.arowana.houdini.byod.view.activity.HomeActivity;

import alcatel.Constants;

public class AlcatelService extends Service {

    private android.os.PowerManager.WakeLock wakeLock;
    int ONGOING_NOTIFICATION_ID = 1;

    @Override
    public IBinder onBind(Intent arg0) {
        return null;
    }

    @Override
    public void onCreate() {
        super.onCreate();


        if (Build.VERSION.SDK_INT > Build.VERSION_CODES.O)
            startMyOwnForeground();
        else {
            // startForeground(1, new Notification());
            Intent intent = new Intent(this.getBaseContext(), HomeActivity.class);
            PendingIntent pendingIntent = PendingIntent.getActivity(this, 0, intent, 0);

            Notification.Builder builder = new Notification.Builder(this.getBaseContext());
            Notification notification = builder
                    .setContentTitle("EmiratePalace")
                    .setContentText("Emrt.Palace started")
                    .setSmallIcon(R.mipmap.ic_launcher)
                    .setContentIntent(pendingIntent)
                    .setWhen(System.currentTimeMillis()).build();

            startForeground(ONGOING_NOTIFICATION_ID, notification);
        }
    }

    @TargetApi(26)
    private void startMyOwnForeground() {
        String NOTIFICATION_CHANNEL_ID = "com.arowana.houdini.byod";
        String channelName = "EmiratePalace Service";
        NotificationChannel chan = new NotificationChannel(NOTIFICATION_CHANNEL_ID, channelName, NotificationManager.IMPORTANCE_NONE);
        chan.setLightColor(Color.BLUE);
        chan.setLockscreenVisibility(Notification.VISIBILITY_PRIVATE);
        NotificationManager manager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
        assert manager != null;
        manager.createNotificationChannel(chan);
        Intent intent = new Intent(this.getBaseContext(), HomeActivity.class);
        PendingIntent pendingIntent = PendingIntent.getActivity(this, 0, intent, 0);

        NotificationCompat.Builder notificationBuilder = new NotificationCompat.Builder(this, NOTIFICATION_CHANNEL_ID);
        Notification notification = notificationBuilder.setOngoing(true)
                .setContentTitle("EmiratePalace")
                .setContentText("Emrt.Palace started")
                .setSmallIcon(R.mipmap.ic_launcher)
                .setPriority(NotificationManager.IMPORTANCE_MIN)
                .setCategory(Notification.CATEGORY_SERVICE)
                .setContentIntent(pendingIntent)
                .build();
        startForeground(ONGOING_NOTIFICATION_ID, notification);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        Log.i(Constants.TAG, "Stopping ByodDemo service");
        wakeLock.release();
    }

    @Override
    public void onLowMemory() {
        super.onLowMemory();
    }

    @SuppressLint({"WakelockTimeout", "InvalidWakeLockTag"})
    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        Log.i(Constants.TAG, "Starting ByodDemo service");
        if (wakeLock == null) {
            PowerManager pm = (PowerManager) getSystemService(Context.POWER_SERVICE);
            if (pm != null) {
                wakeLock = pm.newWakeLock(PowerManager.PARTIAL_WAKE_LOCK, "ByodService");
                wakeLock.acquire();
            }
        }
        return START_STICKY;

    }



}
