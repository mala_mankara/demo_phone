package alcatel.model;

import android.content.Context;
import android.util.Log;

import com.arowana.houdini.byod.model.sipProfile.SipProfileConfigObj;
import com.arowana.houdini.byod.utils.RealmUtil;

import io.realm.Realm;

class ByodConfiguration {

    String 	userId ;
    String 	displayName ;
    String 	loginName ;
    String 	password ;
    String 	domain ;
    String  domainPort ;
    boolean enableOutboundProxy ;
    String  outboundProxy ;
    String 	outboundProxyPort ;
    String 	directProtocol ;
    String 	mgsApiUrl ;
    String 	firebaseProjectId ;
    String 	deviceId ;

    String SbcPublicIpAddress;
    String SbcPort;


    public String getDirectProtocol() {
        return directProtocol;
    }

    public String getUserName() {
        return userId;
    }

    public String getDisplayName() {
        return displayName;
    }

    public String getLoginName() {
        return loginName;
    }

    public String getPassword() {
        return password;
    }

    public String getDomain() {
        return domain;
    }

    public String getDomainPort() {
        return domainPort;
    }

    public boolean isEnableOutboundProxy() {
        return enableOutboundProxy;
    }

    public String getOutboundProxy() {
        return outboundProxy;
    }

    public String getOutboundProxyPort() {
        return outboundProxyPort;
    }

    public String getMgsApiUrl() {
        return mgsApiUrl;
    }

    public String getFirebaseProjectId() {
        return firebaseProjectId;
    }

    public String getDeviceId() {
        return deviceId;
    }

    public String getSbcPublicIpAddress() {
        return SbcPublicIpAddress;
    }

    public String getSbcPort() {
        return SbcPort;
    }



    public static ByodConfiguration readConfiguration(Context context) {
        Log.d("ByodConfiguration","readConfiguration.....1111111111");

            ByodConfiguration configuration = new ByodConfiguration();
        try {
            // getData from Realm Object..
            Realm realm = RealmUtil.getInstance();
            //    QR_DeviceRegisterConfigObj qr_configObj=realm.where(QR_DeviceRegisterConfigObj.class).findFirst();
            SipProfileConfigObj alcatelSipProfileObj = realm.where(SipProfileConfigObj.class).findFirst();
            Log.d("ByodConfiguration", "the SIPPRofileData...." + realm.where(SipProfileConfigObj.class).findFirst());
            configuration.userId = alcatelSipProfileObj.getLoginName();
            configuration.displayName = alcatelSipProfileObj.getDisplayName();
            configuration.loginName = alcatelSipProfileObj.getLoginName();
            configuration.password = alcatelSipProfileObj.getPassword();
            configuration.domain = alcatelSipProfileObj.getDomain();
            configuration.domainPort = alcatelSipProfileObj.getDomainPort();
            configuration.outboundProxyPort = alcatelSipProfileObj.getOutboundProxyPort();

            configuration.directProtocol = alcatelSipProfileObj.getProtocol();
            configuration.outboundProxy = alcatelSipProfileObj.getOutboundProxy();
            configuration.deviceId = alcatelSipProfileObj.getDeviceId();
            configuration.SbcPublicIpAddress = alcatelSipProfileObj.getSbcPublicIpAddress();
            configuration.SbcPort = alcatelSipProfileObj.getSbcPort();


        }catch (Exception e){
            e.printStackTrace();
        }

        return configuration;

//        if(qr_configObj!= null) {
//            QR_RegisterResponseData responseObj = qr_configObj.getResponseData();
//            ActivityResponse alcatelResponseObj = responseObj.getActivityResponse();
//            SipProfile alcatelSipProfileObj = alcatelResponseObj.getSipProfile();
//
//            Log.d("ByodConfiguration", "readConfiguration..datas...1111111111--" + alcatelSipProfileObj.toString());
//            configuration.userName = alcatelResponseObj.getUserName();
//            configuration.displayName = alcatelSipProfileObj.getDisplayName();
//            configuration.loginName = alcatelSipProfileObj.getLoginName();
//            configuration.password = alcatelSipProfileObj.getPassword();
//            configuration.domain = alcatelSipProfileObj.getDomain();
//            configuration.domainPort = Integer.parseInt(alcatelSipProfileObj.getDomainPort());
//            configuration.directProtocol = alcatelSipProfileObj.getProtocol();
//            //configuration.enableOutboundProxy =
//            configuration.outboundProxy = alcatelSipProfileObj.getOutboundProxy();
//            configuration.outboundProxyPort = (Integer.parseInt(alcatelSipProfileObj.getOutboundProxyPort()));
//            //configuration.mgsApiUrl 		  =
//            //configuration.firebaseProjectId   =
//            configuration.deviceId = alcatelResponseObj.getDeviceId();
//        }

        //  SharedPreferenceUtil.putSipProfileIntoStore(StoreKeys.APPLICATION_STATE, state);
       /* SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(context);
        configuration.userName = prefs.getString("PREF_USERNAME", null) ;
        configuration.displayName = prefs.getString("PREF_DISPLAYNAME", null) ;
        configuration.loginName = prefs.getString("PREF_LOGINNAME", null) ;
        configuration.password = prefs.getString("PREF_PASSWORD", null) ;
        configuration.domain = prefs.getString("PREF_SIPDOMAIN", null) ;
        configuration.domainPort = Integer.parseInt(prefs.getString("PREF_SIPDOMAINPORT", "5060")) ;
        configuration.directProtocol = prefs.getString("PREF_SIPDIRECTPROTOCOL", "udp") ;

        configuration.enableOutboundProxy = prefs.getBoolean("PREF_SIPENABLE_OUBOUNDPROXY", false) ;
        configuration.outboundProxy = prefs.getString("PREF_SIPOUTBOUNDPROXY_SERVER", "") ;
		configuration.outboundProxyPort = Integer.parseInt(prefs.getString("PREF_SIPOUTBOUNDPROXY_PORT", "5060")) ;
		configuration.mgsApiUrl = prefs.getString("PREF_MGS_APÏ_URL", null) ;
		configuration.firebaseProjectId = prefs.getString("PREF_FIREBASE_PROJECTID", null) ;
		configuration.deviceId = prefs.getString("PREF_MGS_DEVICEID", null) ;*/


    }

}
