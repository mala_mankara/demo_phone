package alcatel.model;

import android.annotation.SuppressLint;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.os.PowerManager;
import android.os.PowerManager.WakeLock;
import android.util.Log;

import com.alcatel.servpro.byod.api.CallState;
import com.alcatel.servpro.byod.api.IEventCall;
import com.alcatel.servpro.byod.api.IEventCallCreatedModified;
import com.alcatel.servpro.byod.api.IEventCallRemoved;
import com.alcatel.servpro.byod.api.IEventInfo;
import com.alcatel.servpro.byod.api.ISqualeCallback;
import com.alcatel.servpro.byod.api.ISqualeConfiguration;
import com.alcatel.servpro.byod.api.ISqualeService;
import com.alcatel.servpro.byod.api.SqualeServiceFactory;
import com.alcatel.servpro.byod.api.StackOriginValue;
import com.arowana.houdini.byod.view.activity.HomeActivity;

import java.util.ArrayList;
import java.util.Hashtable;

import alcatel.Constants;

public class ByodManager implements ISqualeCallback {

	public static class CallContext {
		public String callId;
		public CallState callState;
		public String participantNumber;
	}

	static ByodManager _instance = null;

	public static ByodManager getInstance() {
		if (_instance == null) {
			_instance = new ByodManager();
		}
		return _instance;
	}

	Context context;
	ISqualeService squaleService;
	ArrayList<ISqualeCallback> callbacks = new ArrayList<ISqualeCallback>();
	public Hashtable<String, CallContext> callContexts = new Hashtable<String, CallContext>();
	String stackStatus = "";
	public boolean isRegistered=false;

	public String getStackStatus() {
		return stackStatus;
	}

	String logsToDisplay = "";

	public String getLogsToDisplay() {
		return logsToDisplay;
	}

	public void clearLogs() {
		logsToDisplay = "";
	}

	public void storeLogs(String txt) {
		logsToDisplay += txt + '\n';
	}

	public void setContext(Context context) {
		this.context = context;
	}

	public ISqualeService getSqualeService() {
		return squaleService;
	}

	public void init() {
		squaleService = SqualeServiceFactory.createSqualeService(context, this);

		start();
	}

	public void start() {
		if (squaleService != null) {
			ISqualeConfiguration configuration = squaleService
					.createConfiguration();
			ByodConfiguration conf = ByodConfiguration
					.readConfiguration(context);
			/*if (!conf.getUserName().isEmpty() )
				configuration.setCfgUserName(conf.getLoginName());
			if (!conf.getDisplayName().isEmpty())
				configuration.setCfgDisplayName(conf.getDisplayName());
			if (!conf.getLoginName().isEmpty())
				configuration.setCfgLoginName(conf.getLoginName());
			if (!conf.getPassword().isEmpty())
				configuration.setCfgUserPassword(conf.getPassword());
			if (!conf.getDomain().isEmpty())
				configuration.setCfgUserDomain(conf.getDomain());*/

			if (!conf.getUserName().isEmpty() ){
				configuration.setCfgUserName(conf.getUserName());
			}
			if (!conf.getDisplayName().isEmpty()) {
				configuration.setCfgDisplayName(conf.getDisplayName());
			}
			if (!conf.getLoginName().isEmpty()) {
				configuration.setCfgLoginName(conf.getLoginName());
			}
			if (!conf.getPassword().isEmpty()) {
				configuration.setCfgUserPassword(conf.getPassword());
			}
			configuration.setCfgUserDomain("10.140.0.53");
			configuration.setCfgUserDomainPort("5060");
			configuration.setCfgDirectProtocol("tls");
			configuration.setCfgOutboundProxy("auhmgs.kempinski.com");
			configuration.setCfgOutboundProxyPort("5060");
			configuration.setCfgSBCAddress("10.251.8.90");
			configuration.setCfgSBCPort("5260");
			configuration.setCfgSBCProtocol("tls");

			configuration.applyConfiguration();
		}


	}

	public void addCallback(ISqualeCallback cb) {
		callbacks.add(cb);
	}

	public void removeCallback(ISqualeCallback cb) {
		callbacks.remove(cb);
	}

	@Override
	public void onCallEvent(IEventCall evt) {
		Log.d(Constants.TAG, "onCallEvent received" + evt);
		storeLogs(evt.getRawEvent());
		handleCallEvent(evt);
		for (ISqualeCallback cb : callbacks) {
			try {
				cb.onCallEvent(evt);
			} catch (Throwable th) {
				Log.e(Constants.TAG, "onCallEvent received failed ", th);
			}
		}
	}

	@Override
	public void onSipphoneInformation(IEventInfo evt) {
		Log.d(Constants.TAG, "onSipphoneInformation received" + evt);
		storeLogs(evt.getRawEvent());
		handleStackStatus(evt);
		for (ISqualeCallback cb : callbacks) {
			try {
				cb.onSipphoneInformation(evt);
			} catch (Throwable th) {
				Log.e(Constants.TAG, "onSipphoneInformation received failed ",
						th);
			}
		}
	}

	public void handleStackStatus(IEventInfo stackEvt) {
		if (stackEvt.getOriginValue() == StackOriginValue.SOV_STACK
				|| stackEvt.getOriginValue() == StackOriginValue.SOV_USER)
			stackStatus = stackEvt.getSourceAsString() + " "
					+ stackEvt.getOriginStateAsString();
		Log.i("MANAGERRRRR","stackStatus............."+stackStatus);
	}


	public IEventCall handleCallEvent(IEventCall callEvent) {
		switch (callEvent.getCallEventType()) {
			case CET_MODIFIED:
			case CET_CREATED:
				IEventCallCreatedModified callEventCreatedModified = (IEventCallCreatedModified) callEvent;
				Log.d(Constants.TAG, "EventCallCreatedModified received callId="
						+ callEventCreatedModified.getCallId() + ",state="
						+ callEventCreatedModified.getCallState());
				CallContext ctx = new CallContext();
				ctx.callId = callEventCreatedModified.getCallId();
				ctx.callState = callEventCreatedModified.getCallState();
				ctx.participantNumber = callEventCreatedModified
						.getParticipantNumber();
				callContexts.put(ctx.callId, ctx);
				if (CallState.CS_RINGING_INCOMING == ctx.callState) {
					sendNotification(context, "Incoming Call", "Call from "
							+ ctx.participantNumber);
				}
				break;
			case CET_REMOVED:
				IEventCallRemoved callEventRemoved = (IEventCallRemoved) callEvent;
				Log.d(Constants.TAG, "EventCallRemoved received  callId="
						+ callEventRemoved.getCallId());
				callContexts.remove(callEventRemoved.getCallId());
				break;
			default:
				;
		}
		return callEvent;
	}

	public CallContext getActiveCallContext() {
		@SuppressWarnings("unchecked")
		Hashtable<String, CallContext> mCallContexts = (Hashtable<String, CallContext>) callContexts
				.clone();
		for (CallContext ctx : mCallContexts.values()) {
			if (CallState.CS_ACTIVE == ctx.callState)
				return ctx;
		}
		return null;
	}

	public CallContext getHeldCallContext() {
		@SuppressWarnings("unchecked")
		Hashtable<String, CallContext> mCallContexts = (Hashtable<String, CallContext>) callContexts
				.clone();
		for (CallContext ctx : mCallContexts.values()) {
			if (CallState.CS_HELD.equals(ctx.callState))
				return ctx;
		}
		return null;
	}

	public void sendNotification(Context context, String title, String content) {
		try {
			PowerManager pm = (PowerManager) context
					.getSystemService(Context.POWER_SERVICE);
			@SuppressLint("InvalidWakeLockTag") WakeLock wakeLock = pm.newWakeLock(
					PowerManager.ACQUIRE_CAUSES_WAKEUP
							| PowerManager.FULL_WAKE_LOCK, "ByodService");
			wakeLock.acquire();
			Intent it = new Intent("android.intent.action.MAIN");
			it.setComponent(new ComponentName(context.getPackageName(),
					HomeActivity.class.getName()));
			it.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
			context.getApplicationContext().startActivity(it);
			wakeLock.release();
		} catch (Exception ex) {
			Log.e(Constants.TAG, "sendNotification failed", ex);
		}
	}
	public boolean isSipConnected(){

		Log.d("call Status", getStackStatus());

		if(getStackStatus().equals(Constants.STAKE_STATUS_OK))
			isRegistered=true;
		else if(getStackStatus().equals(Constants.STAKE_STATUS_REGISTERING))
			isRegistered=false;
		else if(getStackStatus().equals(Constants.STAKE_STATUS_NOT_OK))
			isRegistered=false;
		return isRegistered;
	}

}