package alcatel.alcatel_statusBR;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.util.Log;
import android.widget.Toast;

import alcatel.services.AlcatelDeviceStatusService;

import static android.content.Context.MODE_PRIVATE;

public class Alcatel_StatusBootUpReceiver extends BroadcastReceiver {

    SharedPreferences statusPref;
    String stack_status;
    @Override
    public void onReceive(Context context, Intent intent) {
        statusPref  =context.getSharedPreferences("Stack_Status", MODE_PRIVATE);
        stack_status = statusPref.getString("device_stack_status", null);
        if(stack_status!=null) {
            Toast.makeText(context, "on receiver......status   " + stack_status, Toast.LENGTH_SHORT).show();
            Log.i("RECEIVER....", "RECEIVER................check Status...."+stack_status);
            /***** For start Service  ****/

            Intent myIntent = new Intent(context, AlcatelDeviceStatusService.class);
            myIntent.putExtra("stack_status", stack_status);
            context.startService(myIntent);

        }else
            Toast.makeText(context,"Device is not registered",Toast.LENGTH_SHORT).show();

        // an Intent broadcast.
        throw new UnsupportedOperationException("Not yet implemented");
    }
}
