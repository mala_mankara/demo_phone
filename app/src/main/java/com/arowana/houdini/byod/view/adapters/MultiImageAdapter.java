package com.arowana.houdini.byod.view.adapters;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import androidx.annotation.NonNull;
import androidx.viewpager.widget.PagerAdapter;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.arowana.houdini.byod.R;
import com.arowana.houdini.byod.interfaces.listeners.FragmentNavigationHandler;
import com.arowana.houdini.byod.model.offers.OfferDetails;
import com.arowana.houdini.byod.utils.FactoryUtil;
import com.arowana.houdini.byod.view.fragments.offers.OfferDetailFragment;

import java.util.ArrayList;

import io.realm.RealmList;


public class MultiImageAdapter extends PagerAdapter {
    private Context mContext;
    private LayoutInflater mLayoutInflater;
    private ArrayList<String> imageList;
    private RealmList<OfferDetails> dataObj;
    private boolean isClickable = false;
    private FragmentNavigationHandler mHandler;
    private FrameLayout mHolder;


    public MultiImageAdapter(Context context, ArrayList<String> mImages) {
        this.mContext = context;
        this.imageList = mImages;
        mLayoutInflater = ((Activity) mContext).getLayoutInflater();
    }

    public MultiImageAdapter(Context context, FragmentNavigationHandler handler, FrameLayout fragmentHolder,
                             ArrayList<String> mImages,RealmList<OfferDetails> details, boolean value) {
        this.mContext = context;
        mHandler = handler;
        mHolder = fragmentHolder;
        this.imageList = mImages;
        dataObj=details;
        mLayoutInflater = ((Activity) mContext).getLayoutInflater();
        isClickable = value;
    }

    @Override
    public int getCount() {
        return imageList.size();
    }

    @Override
    public boolean isViewFromObject(@NonNull View view, Object object) {
        return view == object;
    }

    @NonNull
    @SuppressLint("InflateParams")
    @Override
    public Object instantiateItem(@NonNull ViewGroup container, int position) {
        View view = mLayoutInflater.inflate(R.layout.item_image_holder, null);
        ImageView imageHolder = view.findViewById(R.id.placeImage);
        String image = imageList.get(position);
        FactoryUtil.loadImage(mContext, image, imageHolder);
        container.addView(view);
        if (isClickable) {
            imageHolder.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    OfferDetailFragment fragment = new OfferDetailFragment();
                    fragment.setDetailsObj(dataObj);
                    mHandler.showFragment(mHolder.getId(), fragment);
                }
            });
        }
        return view;
    }

    @Override
    public void destroyItem(@NonNull ViewGroup container, int position, @NonNull Object object) {
        container.removeView((LinearLayout) object);
    }

}
