package com.arowana.houdini.byod.model.offers;

import io.realm.RealmList;
import io.realm.RealmObject;

public class OfferDetails extends RealmObject {

    private String Name;

    private String Description;

    private String OfferId;

    private String ToDate;

    private String CategoryId;

    private String FromDate;

    private RealmList<OfferImages> Images;

    public String getName ()
    {
        return Name;
    }

    public void setName (String Name)
    {
        this.Name = Name;
    }

    public String getDescription ()
    {
        return Description;
    }

    public void setDescription (String Description)
    {
        this.Description = Description;
    }

    public String getOfferId ()
    {
        return OfferId;
    }

    public void setOfferId (String OfferId)
    {
        this.OfferId = OfferId;
    }

    public String getToDate ()
    {
        return ToDate;
    }

    public void setToDate (String ToDate)
    {
        this.ToDate = ToDate;
    }

    public String getCategoryId ()
    {
        return CategoryId;
    }

    public void setCategoryId (String CategoryId)
    {
        this.CategoryId = CategoryId;
    }

    public String getFromDate ()
    {
        return FromDate;
    }

    public void setFromDate (String FromDate)
    {
        this.FromDate = FromDate;
    }

    public RealmList<OfferImages> getImages ()
    {
        return Images;
    }

    public void setImages (RealmList<OfferImages> Images)
    {
        this.Images = Images;
    }

}
