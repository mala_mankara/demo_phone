package com.arowana.houdini.byod.view.activity;

import android.os.Bundle;
import android.util.Log;
import android.widget.FrameLayout;

import androidx.fragment.app.Fragment;

import com.arowana.houdini.byod.MainApplication;
import com.arowana.houdini.byod.R;
import com.arowana.houdini.byod.view.fragments.WebViewFragment;
import com.arowana.houdini.byod.view.fragments.facilityservice.FacilityServiceFragment;

public class FacilityServiceActivity extends BaseActivity  {

    public static FrameLayout fragmentHolder;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_module_layout);
        MainApplication.setCurrentActivity(this);
        initComponenets();
    }

    private void initComponenets() {
        fragmentHolder = findViewById(R.id.fragment_holder);
        loadFragments();
    }

    private void loadFragments() {
        showFragment(fragmentHolder.getId(), new FacilityServiceFragment());
    }


    public void onModuleURLSelected(String link, String title) {
        Fragment flightWebView = new WebViewFragment();
        Bundle bundle = new Bundle();
        Log.d("FacilityServiceActivity", "loadWebView link...." + link+"/title.."+title);
        bundle.putString("title", title);
        bundle.putString("url",link);
        flightWebView.setArguments(bundle);
        showFragment(fragmentHolder.getId(),flightWebView);
    }

   /* private BroadcastReceiver mReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            //do something based on the intent's action
            //This piece of code will be executed when you click on your item
            // Call your fragment...
            String title=intent.getStringExtra("title");
            String link=intent.getStringExtra("url");
            Fragment flightWebView = new WebViewFragment();
            Bundle bundle = new Bundle();
            Log.d("FacilityServiceActivity", "RECEIVER...." + link);
            bundle.putString("title", title);
            bundle.putString("url",link);
            flightWebView.setArguments(bundle);
            showFragment(fragmentHolder.getId(),flightWebView);

        }
    };*/

    @Override
    protected void onResume() {
        super.onResume();
       /* LocalBroadcastManager.getInstance(this).registerReceiver(mReceiver,
                new IntentFilter("start.fragment.action"));*/
    }


}
