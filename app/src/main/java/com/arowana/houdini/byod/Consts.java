package com.arowana.houdini.byod;


import com.arowana.houdini.byod.utils.DeviceOSFeatureUtils;

public class Consts {
    public static final String PARSE_FLIGHT_DATA ;
    public static final String PARSE_SOCIAL_MEDIA_DATA ;
    public static final String WEATHER_TEMP_CELUNIT;
    public static final String WEATHER_TEMP_FAHRENUNIT;
    public static final String PARSE_BOOK_AROOM_DATA;
    public static final String PARSE_WHATSAPP_DATA;
    public static final String PARSE_PRESSREADER_DATA ;

    public static final String HB_MENU_FLIGHT;
    public static final String HB_MENU_WEATHER;


    public static final String UTC = "UTC";
    public static final int MAX_STACK_COUNT = 50;
    public static final String NO_CONNECTION_ERROR = "com.android.volley.NoConnectionError";
    public static boolean IS_PRIMARYUSER_CHECKIN = false;




    //Application state
    public static int APP_CURRENT_STATE = -1;
    public static final int APP_DEFAULT__STATE = 0;
    public static final int APP_LOGIN_STATE = 1;
    public static final int APP_CHECKIN_STATE = 2;

    public static final int APP_REGISTER_TOALCATEL_STATE = 2;

    public static final String LOGIN_TYPE_HOUDINI = "Houdini";

    public static final String HOTEL_CHECKIN_TIME = "02:00 PM";

    public static final String DEVICE_TYPE = "Android";

    public static final String DEVICE_MAC_ID = DeviceOSFeatureUtils.getMacAddress();
    public static final String DEVICE_DENSITY = DeviceOSFeatureUtils.getDeviceDensity(MainApplication.getInstance().getGlobalContext());

    public static final String DEVICE_ACTIVATIONTYPE_QR ;
    public static final String DEVICE_ACTIVATIONTYPE_ID ;




    static {
        PARSE_FLIGHT_DATA           = "airLines";
        WEATHER_TEMP_CELUNIT        = "C";
        WEATHER_TEMP_FAHRENUNIT     = "F";

        PARSE_BOOK_AROOM_DATA       = "bookARoom";

        HB_MENU_FLIGHT              = "FLIGHTS";
        HB_MENU_WEATHER             = "WEATHER";
        DEVICE_ACTIVATIONTYPE_QR    = "QR";
        DEVICE_ACTIVATIONTYPE_ID    = "Manual";

        PARSE_SOCIAL_MEDIA_DATA     = "socialMedia";
        PARSE_WHATSAPP_DATA         = "Whatsapp";
        PARSE_PRESSREADER_DATA      = "pressreader";
    }

    // firbase
    public static final String HOUDINI_PUSH_NOTIFICATION_ID = "push_notification_id";
    public static final String ACC_FEEDBACK = "feedback";
}
