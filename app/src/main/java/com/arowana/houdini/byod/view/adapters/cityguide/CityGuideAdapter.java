package com.arowana.houdini.byod.view.adapters.cityguide;

import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;
import androidx.viewpager.widget.ViewPager;

import com.arowana.houdini.byod.R;
import com.arowana.houdini.byod.interfaces.listeners.FragmentNavigationHandler;
import com.arowana.houdini.byod.model.cityguide.CityGuideDetailsObj;
import com.arowana.houdini.byod.model.cityguide.CityGuideImagesObj;
import com.arowana.houdini.byod.model.cityguide.CityguideResponseDataObj;
import com.arowana.houdini.byod.view.adapters.MultiImageAdapter;
import com.arowana.houdini.byod.view.fragments.cityguide.MapFragment;

import java.util.ArrayList;

public class CityGuideAdapter extends RecyclerView.Adapter<CityGuideAdapter.MyViewHolder> {

    private Context context;
    private CityguideResponseDataObj mObj;
    FragmentNavigationHandler mhandler;
    FrameLayout mholder;


    public CityGuideAdapter(Context mContext, CityguideResponseDataObj obj, FragmentNavigationHandler handler, FrameLayout holder) {
        this.context = mContext;
        this.mObj = obj;
        mhandler = handler;
        mholder = holder;

    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        ViewPager imgHolder;
        public TextView title, distance, description, address, kilometer;
        private LinearLayout ll_dots;
        private ImageView mLocation;


        public MyViewHolder(View view) {
            super(view);
            imgHolder = view.findViewById(R.id.cityguideImageHolder);

            title = view.findViewById(R.id.cityGuideTitle);
            description = view.findViewById(R.id.cityGuideDescription);
            address = view.findViewById(R.id.cityGuideAddress);
            distance = view.findViewById(R.id.cityGuideDistance);
            kilometer = view.findViewById(R.id.cityGuideDistanceUnit);

            mLocation = view.findViewById(R.id.cityGuideImgLocation);

            ll_dots = view.findViewById(R.id.cityguide_ll_dots);


        }


    }


    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_city_guide, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, int position) {
        final CityGuideDetailsObj detailsObj = mObj.getDetails().get(position);
        if (detailsObj.getDistanceFromHotel()!=null){
            String[] str = detailsObj.getDistanceFromHotel().split(" ");
            holder.distance.setText(str[0]);
            if(str.length>1)
                holder.kilometer.setText(str[1] != null ? str[1] : "KM");
        }




        holder.title.setText(detailsObj.getTitle());
        holder.description.setText(detailsObj.getOverview());

        holder.address.setText(detailsObj.getAddress()!=null?detailsObj.getAddress():"");

        holder.mLocation.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                MapFragment fragment = new MapFragment();
                Bundle bundle = new Bundle();

                bundle.putString("name", detailsObj.getTitle());
                bundle.putString("address", detailsObj.getAddress());
                bundle.putString("lat", detailsObj.getLatitude());
                bundle.putString("lng", detailsObj.getLongitude());
                fragment.setArguments(bundle);
                mhandler.showFragment(mholder.getId(), fragment);
            }
        });


        if (detailsObj.getImages().size() > 0) {
            ArrayList<String> imageList = new ArrayList<>();
            for (CityGuideImagesObj image : detailsObj.getImages()) {
                String imgPath = image.getImageUrl();
                imageList.add(imgPath);

            }


            holder.imgHolder.setVisibility(View.VISIBLE);
            holder.imgHolder.setAdapter(new MultiImageAdapter(context, imageList));

            if (imageList.size() > 1) {
                holder.ll_dots.removeAllViews();
                final ImageView[] ivArrayDotsPager = new ImageView[imageList.size()];
                for (int i = 0; i < ivArrayDotsPager.length; i++) {
                    ivArrayDotsPager[i] = new ImageView(context);
                    LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
                    params.setMargins(5, 0, 5, 0);
                    ivArrayDotsPager[i].setLayoutParams(params);
                    ivArrayDotsPager[i].setImageResource(R.drawable.slider_inactive);

                    ivArrayDotsPager[i].setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            view.setAlpha(1);
                        }
                    });
                    holder.ll_dots.addView(ivArrayDotsPager[i]);
                    holder.ll_dots.bringToFront();
                }


                ivArrayDotsPager[0].setImageResource(R.drawable.slider_active);

                holder.imgHolder.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
                    @Override
                    public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

                    }

                    @Override
                    public void onPageSelected(int position) {
                        for (int i = 0; i < ivArrayDotsPager.length; i++) {
                            ivArrayDotsPager[i].setImageResource(R.drawable.slider_inactive);
                        }
                        ivArrayDotsPager[position].setImageResource(R.drawable.slider_active);
                    }

                    @Override
                    public void onPageScrollStateChanged(int state) {

                    }
                });
            } else
                holder.ll_dots.setVisibility(View.GONE);

        } else {
            holder.imgHolder.setVisibility(View.GONE);
        }


    }


    @Override
    public int getItemCount() {
        return mObj.getDetails().size();
    }


}
