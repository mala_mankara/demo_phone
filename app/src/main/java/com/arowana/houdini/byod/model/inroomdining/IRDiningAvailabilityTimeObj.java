package com.arowana.houdini.byod.model.inroomdining;

import io.realm.RealmObject;

public class IRDiningAvailabilityTimeObj extends RealmObject {

    private String OpeningTime;

    private String ClosingTime;

    public String getOpeningTime() {
        return OpeningTime;
    }

    public void setOpeningTime(String openingTime) {
        OpeningTime = openingTime;
    }

    public String getClosingTime() {
        return ClosingTime;
    }

    public void setClosingTime(String closingTime) {
        ClosingTime = closingTime;
    }
}
