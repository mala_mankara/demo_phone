package com.arowana.houdini.byod.model.deviceactivation_QR;

import io.realm.RealmObject;

public class ActivityResponse extends RealmObject{

    private String DeviceLabel;

    private String SipPhoneNumber;

    private String AlcatelDatabaseDeviceId;

    private String UserName;

    private SipProfile SipProfile;

    private String DeviceId;

    private String GatewayPublicIpAddress;

    private String Password;

    public String getDeviceLabel ()
    {
        return DeviceLabel;
    }

    public void setDeviceLabel (String DeviceLabel)
    {
        this.DeviceLabel = DeviceLabel;
    }

    public String getSipPhoneNumber ()
    {
        return SipPhoneNumber;
    }

    public void setSipPhoneNumber (String SipPhoneNumber)
    {
        this.SipPhoneNumber = SipPhoneNumber;
    }

    public String getAlcatelDatabaseDeviceId ()
    {
        return AlcatelDatabaseDeviceId;
    }

    public void setAlcatelDatabaseDeviceId (String AlcatelDatabaseDeviceId)
    {
        this.AlcatelDatabaseDeviceId = AlcatelDatabaseDeviceId;
    }

    public String getUserName ()
    {
        return UserName;
    }

    public void setUserName (String UserName)
    {
        this.UserName = UserName;
    }

    public SipProfile getSipProfile ()
    {
        return SipProfile;
    }

    public void setSipProfile (SipProfile SipProfile)
    {
        this.SipProfile = SipProfile;
    }

    public String getDeviceId ()
    {
        return DeviceId;
    }

    public void setDeviceId (String DeviceId)
    {
        this.DeviceId = DeviceId;
    }

    public String getGatewayPublicIpAddress ()
    {
        return GatewayPublicIpAddress;
    }

    public void setGatewayPublicIpAddress (String GatewayPublicIpAddress)
    {
        this.GatewayPublicIpAddress = GatewayPublicIpAddress;
    }

    public String getPassword ()
    {
        return Password;
    }

    public void setPassword (String Password)
    {
        this.Password = Password;
    }

    @Override
    public String toString()
    {
        return "ClassPojo [DeviceLabel = "+DeviceLabel+", SipPhoneNumber = "+SipPhoneNumber+", AlcatelDatabaseDeviceId = "+AlcatelDatabaseDeviceId+", UserName = "+UserName+", SipProfile = "+SipProfile+", DeviceId = "+DeviceId+", GatewayPublicIpAddress = "+GatewayPublicIpAddress+", Password = "+Password+"]";
    }
}
