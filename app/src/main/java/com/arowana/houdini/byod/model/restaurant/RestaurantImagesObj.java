package com.arowana.houdini.byod.model.restaurant;

import io.realm.RealmObject;

public class RestaurantImagesObj extends RealmObject {

    private String ImageSize;

    private String ImageId;

    private String ImageName;

    private String ImageUrl;

    private String ImageWidth;

    private String ImageHeight;

    public String getImageSize() {
        return ImageSize;
    }

    public void setImageSize(String imageSize) {
        ImageSize = imageSize;
    }

    public String getImageId() {
        return ImageId;
    }

    public void setImageId(String imageId) {
        ImageId = imageId;
    }

    public String getImageName() {
        return ImageName;
    }

    public void setImageName(String imageName) {
        ImageName = imageName;
    }

    public String getImageUrl() {
        return ImageUrl;
    }

    public void setImageUrl(String imageUrl) {
        ImageUrl = imageUrl;
    }

    public String getImageWidth() {
        return ImageWidth;
    }

    public void setImageWidth(String imageWidth) {
        ImageWidth = imageWidth;
    }

    public String getImageHeight() {
        return ImageHeight;
    }

    public void setImageHeight(String imageHeight) {
        ImageHeight = imageHeight;
    }
}
