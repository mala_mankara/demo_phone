package com.arowana.houdini.byod.view.activity;

import android.os.Bundle;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import android.widget.FrameLayout;

import com.arowana.houdini.byod.MainApplication;
import com.arowana.houdini.byod.R;
import com.arowana.houdini.byod.interfaces.listeners.ProvideBundleListener;
import com.arowana.houdini.byod.interfaces.listeners.UpdateBundleListener;
import com.arowana.houdini.byod.interfaces.listeners.UpdatePreviousScreenListener;
import com.arowana.houdini.byod.view.fragments.serviceRequest.ServiceRequestFragment;

public class ServiceRequestActivity extends BaseActivity implements UpdatePreviousScreenListener {

    public static FrameLayout fragmentHolder;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_module_layout);
        MainApplication.setCurrentActivity(this);
        initComponents();
    }

    public void initComponents() {
        fragmentHolder = findViewById(R.id.fragment_holder);
        loadFragments();
        Fragment showingFragment = getSupportFragmentManager().findFragmentById(R.id.fragment_holder);
        if (showingFragment instanceof ProvideBundleListener) {
            ((ProvideBundleListener) showingFragment).registerBundleListener(this);
        }
    }

    public void loadFragments() {
        showFragment(fragmentHolder.getId(), new ServiceRequestFragment());
    }

    @Override
    public void updatePreviousScreen(Bundle bundle) {
        super.onBackPressed();
        FragmentManager mgr = getSupportFragmentManager();
        Fragment fragment = mgr.findFragmentById(R.id.fragment_holder);
        if (fragment instanceof UpdateBundleListener) {
            ((UpdateBundleListener) fragment).onBundleUpdate(bundle);
        }
    }



}
