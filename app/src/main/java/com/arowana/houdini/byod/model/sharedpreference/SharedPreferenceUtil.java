package com.arowana.houdini.byod.model.sharedpreference;

import android.content.Context;
import android.content.SharedPreferences;

import com.arowana.houdini.byod.MainApplication;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;


public class SharedPreferenceUtil {

    private static Context STORE_CONTEXT;
    private static SharedPreferences preferences;
    private static SharedPreferences.Editor editor;

    public static void setStoreContext(Context context) {
        STORE_CONTEXT = context;
    }

    public static String getFromStore(Context context, String key) {
        SharedPreferences prefs = context.getSharedPreferences(MainApplication.TAG, Context.MODE_PRIVATE);
        return prefs.getString(key, null);
    }

    public static String getFromStore(String key) {
        SharedPreferences prefs = STORE_CONTEXT.getSharedPreferences(MainApplication.TAG, Context.MODE_PRIVATE);
        return prefs.getString(key, null);
    }

    public static boolean getBooleanFromStore(Context context, String key) {

        SharedPreferences prefs = context.getSharedPreferences(MainApplication.TAG, Context.MODE_PRIVATE);
        return prefs.getBoolean(key, false);

    }

    public static boolean getBooleanFromStore(String key) {

        SharedPreferences prefs = STORE_CONTEXT.getSharedPreferences(MainApplication.TAG, Context.MODE_PRIVATE);
        return prefs.getBoolean(key, false);

    }

    public static boolean putBooleanIntoStore(String key, boolean value) {

        SharedPreferences prefs = STORE_CONTEXT.getSharedPreferences(MainApplication.TAG, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = prefs.edit();
        return editor.putBoolean(key, value).commit();


    }

    public static boolean putBooleanIntoStore(Context context, String key, boolean value) {

        SharedPreferences prefs = context.getSharedPreferences(MainApplication.TAG, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = prefs.edit();
        return editor.putBoolean(key, value).commit();


    }

    public static ArrayList<String> getFromStore(Context context, String key, String s) {

        SharedPreferences prefs = context.getSharedPreferences(MainApplication.TAG, Context.MODE_PRIVATE);
        ArrayList<String> ar = new ArrayList<String>();
        Set<String> newset = prefs.getStringSet(key, null);


        Iterator iterator = newset.iterator();
        while (iterator.hasNext()) {
            ar.add(iterator.next().toString());
        }

        return ar;

    }

    public static Map<String, ?> getAllFromStore(Context context) {

        SharedPreferences prefs = context.getSharedPreferences(MainApplication.TAG, Context.MODE_PRIVATE);
        Map<String, ?> keys = prefs.getAll();
        return keys;

    }

    public static boolean putIntoStore(Context context, String key, String value) {
        SharedPreferences prefs = context.getSharedPreferences(MainApplication.TAG, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = prefs.edit();
        return editor.putString(key, value).commit();

    }

    public static boolean putIntoStore(String key, String value) {

        SharedPreferences prefs = STORE_CONTEXT.getSharedPreferences(MainApplication.TAG, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = prefs.edit();
        return editor.putString(key, value).commit();


    }

    public static boolean checkKeyInStore(String key) {
        SharedPreferences prefs = STORE_CONTEXT.getSharedPreferences(MainApplication.TAG, Context.MODE_PRIVATE);

        return prefs.contains(key);

    }

    public static boolean checkKeyInStore(Context context, String key) {
        SharedPreferences prefs = context.getSharedPreferences(MainApplication.TAG, Context.MODE_PRIVATE);

        return prefs.contains(key);

    }

    public static void updateValesInStore(Context context,
                                          String key, String value) {
        SharedPreferences prefs = context.getSharedPreferences(MainApplication.TAG, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = prefs.edit();
        editor.putString(key, value);
        editor.commit();
    }


    public static boolean putIntegerIntoStore(String key, int value) {
        SharedPreferences prefs = STORE_CONTEXT.getSharedPreferences(MainApplication.TAG, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = prefs.edit();
        return editor.putInt(key, value).commit();
    }

    public static int getIntegerFromStore(String key) {
        SharedPreferences prefs = STORE_CONTEXT.getSharedPreferences(MainApplication.TAG, Context.MODE_PRIVATE);
        return prefs.getInt(key, -1);
    }

    private static SharedPreferences getSharedPreferences(Context context) {
        preferences = context.getSharedPreferences(MainApplication.TAG, Context.MODE_PRIVATE);
        return preferences;
    }

    private static SharedPreferences.Editor getEditor(Context context) {
        editor = getSharedPreferences(context).edit();
        return editor;
    }

    public static void clearAll(Context context) {
        SharedPreferences prefs = context.getSharedPreferences(MainApplication.TAG, Context.MODE_PRIVATE);
        prefs.edit().clear().commit();

    }

    public static void setConfigStatus(Context context, String configName, boolean isConfig) {
        editor = getEditor(context);
        editor.putBoolean(configName, isConfig);
        editor.commit();
    }

    public static boolean getConfigStatus(Context context, String configName) {
        return getSharedPreferences(context).getBoolean(configName, false);
    }

}
