package com.arowana.houdini.byod.model.deviceactivation_QR;

import io.realm.RealmObject;

public class SipProfile extends RealmObject {


    private String SbcPublicIpAddress;

    private String LoginName;

    private String ContactUri;

    private String SbcPort;

    private String DomainPort;

    private String OutboundProxyPort;

    private String SbcUseTls;

    private String UserId;

    private String DisplayName;

    private String Domain;

    private String OutboundProxy;

    private String Protocol;

    private String Password;

    public String getSbcPublicIpAddress ()
    {
        return SbcPublicIpAddress;
    }

    public void setSbcPublicIpAddress (String SbcPublicIpAddress)
    {
        this.SbcPublicIpAddress = SbcPublicIpAddress;
    }

    public String getLoginName ()
    {
        return LoginName;
    }

    public void setLoginName (String LoginName)
    {
        this.LoginName = LoginName;
    }

    public String getContactUri ()
    {
        return ContactUri;
    }

    public void setContactUri (String ContactUri)
    {
        this.ContactUri = ContactUri;
    }

    public String getSbcPort ()
    {
        return SbcPort;
    }

    public void setSbcPort (String SbcPort)
    {
        this.SbcPort = SbcPort;
    }

    public String getDomainPort ()
    {
        return DomainPort;
    }

    public void setDomainPort (String DomainPort)
    {
        this.DomainPort = DomainPort;
    }

    public String getOutboundProxyPort ()
    {
        return OutboundProxyPort;
    }

    public void setOutboundProxyPort (String OutboundProxyPort)
    {
        this.OutboundProxyPort = OutboundProxyPort;
    }

    public String getSbcUseTls ()
    {
        return SbcUseTls;
    }

    public void setSbcUseTls (String SbcUseTls)
    {
        this.SbcUseTls = SbcUseTls;
    }

    public String getUserId ()
    {
        return UserId;
    }

    public void setUserId (String UserId)
    {
        this.UserId = UserId;
    }

    public String getDisplayName ()
    {
        return DisplayName;
    }

    public void setDisplayName (String DisplayName)
    {
        this.DisplayName = DisplayName;
    }

    public String getDomain ()
    {
        return Domain;
    }

    public void setDomain (String Domain)
    {
        this.Domain = Domain;
    }

    public String getOutboundProxy ()
    {
        return OutboundProxy;
    }

    public void setOutboundProxy (String OutboundProxy)
    {
        this.OutboundProxy = OutboundProxy;
    }

    public String getProtocol ()
    {
        return Protocol;
    }

    public void setProtocol (String Protocol)
    {
        this.Protocol = Protocol;
    }

    public String getPassword ()
    {
        return Password;
    }

    public void setPassword (String Password)
    {
        this.Password = Password;
    }

    @Override
    public String toString()
    {
        return "ClassPojo [SbcPublicIpAddress = "+SbcPublicIpAddress+", LoginName = "+LoginName+", ContactUri = "+ContactUri+", SbcPort = "+SbcPort+", DomainPort = "+DomainPort+", OutboundProxyPort = "+OutboundProxyPort+", SbcUseTls = "+SbcUseTls+", UserId = "+UserId+", DisplayName = "+DisplayName+", Domain = "+Domain+", OutboundProxy = "+OutboundProxy+", Protocol = "+Protocol+", Password = "+Password+"]";
    }
}
