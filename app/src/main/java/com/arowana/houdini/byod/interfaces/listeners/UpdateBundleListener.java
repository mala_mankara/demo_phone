package com.arowana.houdini.byod.interfaces.listeners;

import android.os.Bundle;

public interface UpdateBundleListener {
    void onBundleUpdate(Bundle bundle);
}
