package com.arowana.houdini.byod.utils;

import android.app.ProgressDialog;
import android.content.Context;

public class DialogUtil {

    public static ProgressDialog showProgressDialog(Context context, String message) {

        ProgressDialog m_Dialog = new ProgressDialog(context);
        m_Dialog.setMessage("Please wait..");
        m_Dialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
       // m_Dialog.setCancelable(false);
        // m_Dialog.show();
        return m_Dialog;
    }
}
