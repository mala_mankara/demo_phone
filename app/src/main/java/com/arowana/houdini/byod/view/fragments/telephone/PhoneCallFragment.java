package com.arowana.houdini.byod.view.fragments.telephone;

import android.annotation.SuppressLint;
import android.content.Context;
import android.os.Bundle;
import android.os.Handler;
import android.os.SystemClock;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentActivity;
import androidx.fragment.app.FragmentManager;

import com.alcatel.servpro.byod.api.IEventCall;
import com.alcatel.servpro.byod.api.IEventInfo;
import com.alcatel.servpro.byod.api.ISqualeCallback;
import com.alcatel.servpro.byod.api.ISqualeService;
import com.alcatel.servpro.byod.api.impl.EventCallParser;
import com.arowana.houdini.byod.R;
import com.arowana.houdini.byod.view.fragments.BaseFragment;

import java.util.ArrayList;
import java.util.Hashtable;
import java.util.Objects;

import alcatel.model.ByodManager;
import alcatel.model.ByodManager.CallContext;

/**
 * A simple {@link Fragment} subclass.
 */
public class PhoneCallFragment extends BaseFragment implements ISqualeCallback , View.OnClickListener{

    private static final String TAG = "PhoneCallFragment";
    Context mContext;
    View mRootView;
    public PhoneCallFragment() { // Required empty public constructor
        isMute = false;
    }
    String dialNo ;
    String dialerName ;
    TextView dialName;
    TextView dialStatus;
    TextView dialTimer;
    ImageView callReceive;
    ImageView callReject;
    ImageView callMute;
    ImageView callSpeaker;
    LinearLayout receiveOptionLayout;
    CallContext ctx;
    private boolean isMakingCall=false;
    ISqualeService squaleService;
    private boolean isMute, isSpeakerON;


    private long startTime = 0L;
    private Handler customHandler = new Handler();
    long timeInMilliseconds = 0L;
    long timeSwapBuff = 0L;
    long updatedTime = 0L;

    @Override
    protected int getFragmentLayout() {
        return R.layout.speedial_callestablish;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ByodManager.getInstance().addCallback(this);
        mContext=getActivity();
        dialNo = getArguments().getString("phoneNo");
        dialerName= getArguments().getString("name");
        isMakingCall = getArguments().getBoolean("isMakingCall");;
        Log.d("PhoneCallFrag","phno.."+dialNo);
        setTitle(getResources().getString(R.string.title_activity_telephone));
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        mRootView   = view;
        initComponents();
        displayCallContext() ;
        if(isMakingCall)
            makeCall();


    }

    public void makeCall() {
        Log.d( TAG,"makeCall");

        if (!dialNo.isEmpty()) {
            squaleService = ByodManager.getInstance().getSqualeService() ;
            Log.d("PhoneCallFragment","makeCallAudio dest=" + dialNo) ;
            if (squaleService  != null)
                squaleService.makeCallAudio(dialNo);
        }
    }

    private void initComponents() {
        dialStatus  = mRootView.findViewById(R.id.sd_callstatus);
        dialName    = mRootView.findViewById(R.id.sd_callername);
        dialTimer   = mRootView.findViewById(R.id.sd_calltime);
        callReceive = mRootView.findViewById(R.id.sd_callreceive);
        callReject  = mRootView.findViewById(R.id.sd_callreject);

        receiveOptionLayout = mRootView.findViewById(R.id.sd_optionlayout);
        callMute            = mRootView.findViewById(R.id.sd_callmute);
        callSpeaker         = mRootView.findViewById(R.id.sd_callspeaker);
        if(isMakingCall)
            callReceive.setVisibility(View.GONE);
        dialStatus.setText("");
        dialName.setText("");
        callReceive.setOnClickListener(this);
        callReject.setOnClickListener(this);
        callSpeaker.setOnClickListener(this);
        callMute.setOnClickListener(this);
    }

    public void displayCallContext() {
        if (dialStatus != null) {
            ctx = getCurrentCallContext() ;
            if (ctx == null) {
                dialStatus.setText("idle");
            } else {
                dialStatus.setText(""+ctx.callState);
                dialName.setText(ctx.participantNumber);
            }
        }

        if(dialStatus.getText().equals("CS_ACTIVE")){
            starttimer();
        }
    }



    private Runnable updateTimerThread = new Runnable() {
        @SuppressLint({"SetTextI18n", "DefaultLocale"})
        public void run() {
            timeInMilliseconds = SystemClock.uptimeMillis() - startTime;
            updatedTime = timeSwapBuff + timeInMilliseconds;
            int secs = (int) (updatedTime / 1000);
            int mins = secs / 60;
            secs = secs % 60;
            int milliseconds = (int) (updatedTime % 1000);
            dialTimer.setText("" + mins + ":"+String.format("%02d", secs) + ":"+String.format("%03d", milliseconds));
            customHandler.postDelayed(this, 0);
        }
    };


    private void starttimer() {
        startTime = SystemClock.uptimeMillis();
        customHandler.postDelayed(updateTimerThread, 0);
    //    customHandler.removeCallbacks(updateTimerThread);

    }

    public CallContext getCurrentCallContext() {
        Hashtable<String, CallContext> ctxs = ByodManager.getInstance().callContexts ;
        String callId = null ;
      /*  if (radioGroup != null) {
            int selectedId = radioGroup.getCheckedRadioButtonId();
            if (selectedId != -1) {
                RadioButton rb = (RadioButton)radioGroup.findViewById(selectedId);
                if (rb !=null)
                    callId = (String)rb.getTag() ;
            }
        }*/
        if (ctxs.isEmpty()) {
            return null ;
        } else if (callId != null) {
            return ctxs.get(callId) ;
        } else {
            ArrayList<CallContext> lst = new ArrayList<CallContext>(ctxs.values()) ;
            return lst.get(0) ;
        }

    }

    @Override
    public void onSipphoneInformation(IEventInfo iEventInfo) {
        FragmentActivity activity = this.getActivity() ;
        if (activity == null) return ;
        activity.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                Log.d(TAG,"onSipphoneInformation.. phone status.."+ByodManager.getInstance().getStackStatus());
                //  stackStatus.setText(ByodManager.getInstance().getStackStatus()) ;
            }
        });
    }

    @Override
    public void onCallEvent(final IEventCall evtCall) {
        FragmentActivity activity = this.getActivity() ;
        if (activity == null) return ;
        activity.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                switch (evtCall.getCallEventType()) {
                    case CET_CREATED :
                    case CET_MODIFIED :
                        EventCallParser.EventCallCreatedModified callEventCreatedModified = (EventCallParser.EventCallCreatedModified)evtCall ;
                        break;
                    case CET_REMOVED :
                        EventCallParser.EventCallRemoved callEventRemoved = (EventCallParser.EventCallRemoved)evtCall ;
                        break;
                    default:
                }
                displayCallContext() ;
            }
        });
    }

    @Override
    public void onDestroyView() {
        ByodManager.getInstance().removeCallback(this);
        super.onDestroyView();
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.sd_callmute:
                squaleService = ByodManager.getInstance().getSqualeService() ;
                if(!isMute)isMute = true;
                else isMute= false;
//                if (squaleService  != null) {
//                    squaleService.muteAudioCall("", isMute);
//                    Log.d(TAG,"buttonMuteMicro value = " + isMute);
//                }
                break;
            case R.id.sd_callreceive:
                Log.d( TAG,"callreceive");
                ctx = getCurrentCallContext() ;
                if (ctx != null) {
                    squaleService = ByodManager.getInstance().getSqualeService() ;
                    Log.d( TAG,"takeCallAudio callId=" + ctx.callId) ;
                    if (squaleService  != null)
                        squaleService.takeCallAudio(ctx.callId);
                }
                break;
            case R.id.sd_callreject:
                Log.d( TAG,"callreject");
                ctx = getCurrentCallContext() ;
                if (ctx != null) {
                    squaleService = ByodManager.getInstance().getSqualeService() ;
                    Log.d(TAG,"rejectCall callId=" + ctx.callId) ;
                    if (squaleService  != null)
                        squaleService.rejectCall(ctx.callId);
                }
                customHandler.removeCallbacks(updateTimerThread);
                getBackTo();
                break;


            case R.id.sd_callspeaker:
                squaleService = ByodManager.getInstance().getSqualeService() ;
                if(!isSpeakerON)isSpeakerON = true;
                else isSpeakerON= false;
                if (squaleService  != null) {
                    squaleService.loudSpeaker(isSpeakerON);
                    Log.d(TAG,"buttonMuteMicro value = " + isSpeakerON);
                }
                break;


        }

    }

    private void getBackTo() {
        FragmentManager.BackStackEntry backEntry;
        if (getFragmentManager() != null) {
            backEntry = getFragmentManager().getBackStackEntryAt(0);
            String tag = backEntry.getName();
            getFragmentManager().findFragmentByTag(tag);
        }
        Objects.requireNonNull(getActivity()).onBackPressed();
    }


}
