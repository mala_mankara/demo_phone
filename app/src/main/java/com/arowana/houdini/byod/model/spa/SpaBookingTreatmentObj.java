package com.arowana.houdini.byod.model.spa;

import io.realm.RealmObject;

public class SpaBookingTreatmentObj extends RealmObject{

    private String Description;

    private String RequestDate;

    private String CouplesCount;

    private String TreatmentName;

    private String RequestTime;

    private String TreatmentId;

    private String MalePax;

    private String Time;

    private String FemalePax;

    private String Price;

    private String Gender;

    private String CouplesTreatment;

    private String Currency;

    public String getDescription() {
        return Description;
    }

    public void setDescription(String description) {
        Description = description;
    }

    public String getRequestDate() {
        return RequestDate;
    }

    public void setRequestDate(String requestDate) {
        RequestDate = requestDate;
    }

    public String getCouplesCount() {
        return CouplesCount;
    }

    public void setCouplesCount(String couplesCount) {
        CouplesCount = couplesCount;
    }

    public String getTreatmentName() {
        return TreatmentName;
    }

    public void setTreatmentName(String treatmentName) {
        TreatmentName = treatmentName;
    }

    public String getRequestTime() {
        return RequestTime;
    }

    public void setRequestTime(String requestTime) {
        RequestTime = requestTime;
    }

    public String getTreatmentId() {
        return TreatmentId;
    }

    public void setTreatmentId(String treatmentId) {
        TreatmentId = treatmentId;
    }

    public String getMalePax() {
        return MalePax;
    }

    public void setMalePax(String malePax) {
        MalePax = malePax;
    }

    public String getTime() {
        return Time;
    }

    public void setTime(String time) {
        Time = time;
    }

    public String getFemalePax() {
        return FemalePax;
    }

    public void setFemalePax(String femalePax) {
        FemalePax = femalePax;
    }

    public String getPrice() {
        return Price;
    }

    public void setPrice(String price) {
        Price = price;
    }

    public String getGender() {
        return Gender;
    }

    public void setGender(String gender) {
        Gender = gender;
    }

    public String getCouplesTreatment() {
        return CouplesTreatment;
    }

    public void setCouplesTreatment(String couplesTreatment) {
        CouplesTreatment = couplesTreatment;
    }

    public String getCurrency() {
        return Currency;
    }

    public void setCurrency(String currency) {
        Currency = currency;
    }
}
