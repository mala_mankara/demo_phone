package com.arowana.houdini.byod.utils;

import android.util.Log;
import android.widget.TimePicker;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.TimeZone;

public class DateUtil {
    public static String getCurrentDateInSpecificFormat(Calendar currentCalDate) {
        DateFormat dateFormat = new SimpleDateFormat("d MMM yyyy");
        dateFormat.setTimeZone(TimeZone.getTimeZone("Asia/Dubai"));
        return dateFormat.format(currentCalDate.getTime());
    }

    public static Date getDate(String date) throws ParseException{
        Date mDate = new SimpleDateFormat("d MMM yyyy").parse(date);
        return mDate;
    }

    public static String getUTCDatetime(String date, String time) throws ParseException {
        String fromTZ="Asia/ Dubai";
        String toTZ="UTC";
        TimeZone fromTimeZone = TimeZone.getTimeZone(fromTZ);
        TimeZone toTimeZone = TimeZone.getTimeZone(toTZ);
        Calendar caltest = Calendar.getInstance();

        String datetime = date + " " + time;
        datetime = datetime.replace(",", "");
        Log.d("datetime", "" + datetime);

        SimpleDateFormat sdf = new SimpleDateFormat("dd MMM yyyy hh:mm a");
        sdf.setTimeZone(TimeZone.getTimeZone("Asia/Dubai")); // better than using IST
      //  Date d1 =sdf.parse("2018/12/05 07:55 PM");
        Date zonalDate = sdf.parse(datetime);
        caltest.setTime(zonalDate);
        System.out.println(zonalDate);
        System.out.println(caltest.getTime());

        System.out.println("============After UTC========");
        //Here you say to java the initial timezone. This is the secret
        sdf.setTimeZone(TimeZone.getTimeZone("UTC"));
        //Will print in UTC
        Date utcDate = caltest.getTime();
        System.out.println(sdf.format(zonalDate));
        System.out.println(sdf.format(caltest.getTime()));

        SimpleDateFormat optFmt = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'");
        optFmt.setTimeZone(TimeZone.getTimeZone("UTC"));
        Log.d("optFmt", "return date.."+ optFmt.format(utcDate));

        return optFmt.format(utcDate);
//        System.out.println("============After CONVERTED TO LOCAL TIME========");
//        //Here you set to your timezone
//        sdf.setTimeZone(TimeZone.getDefault());
//        //Will print on your default Timezone
//        System.out.println(sdf.format(caltest.getTime()));
//        System.out.println(caltest.getTime());
//        SimpleDateFormat optFmt = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'");
//        optFmt.setTimeZone(TimeZone.getTimeZone("UTC"));
//        Log.d("optFmt", "return date.."+ optFmt.format(optFmt));
//        // setUTCTime(output);
//        return  sdf.format(zonalDate);

    }

    public static String getUTCTime(String date, String time) throws ParseException {
        String fromTZ="Asia/ Dubai";
        String toTZ="UTC";
        TimeZone fromTimeZone = TimeZone.getTimeZone(fromTZ);
        TimeZone toTimeZone = TimeZone.getTimeZone(toTZ);
        Calendar caltest = Calendar.getInstance();

        String datetime = date + " " + time;
        datetime = datetime.replace(",", "");
        Log.d("datetime", "" + datetime);

        SimpleDateFormat sdf = new SimpleDateFormat("dd MMM yyyy hh:mm a");
        sdf.setTimeZone(TimeZone.getTimeZone("Asia/Dubai")); // better than using IST
        //  Date d1 =sdf.parse("2018/12/05 07:55 PM");
        Date zonalDate = sdf.parse(datetime);
        caltest.setTime(zonalDate);
        System.out.println(zonalDate);
        System.out.println(caltest.getTime());

        System.out.println("============saved After UTC========");
        //Here you say to java the initial timezone. This is the secret
        sdf.setTimeZone(TimeZone.getTimeZone("UTC"));
        //Will print in UTC
        Date utcDate = caltest.getTime();
        System.out.println(sdf.format(zonalDate));
        System.out.println(sdf.format(caltest.getTime()));


        SimpleDateFormat dateFormat = new SimpleDateFormat("hh:mm a");
        dateFormat.setTimeZone(TimeZone.getTimeZone("UTC"));
        Log.d("optFmt", "return date.."+ dateFormat.format(utcDate));

        return dateFormat.format(utcDate);
    }

    public static String getTimepickerTime(TimePicker timePicker) {
        String timeStamp = "AM";
        int hour = timePicker.getCurrentHour();
        String sHour = "00";
        if (hour > 12) {
            hour = hour - 12;
            timeStamp = "PM";
        }else if(hour == 12){
            hour =12;
            timeStamp = "PM";
        }else if (hour == 0) {
            hour = hour + 12;
            timeStamp = "AM";
        }else
            timeStamp = "AM";
        if (hour < 10) {
            sHour = "0" + hour;
        } else {
            sHour = String.valueOf(hour);
        }
        int minute = timePicker.getCurrentMinute();
        String sMinute = "00";
        if (minute < 10) {
            sMinute = "0" + minute;
        } else {
            sMinute = String.valueOf(minute);
        }
        String time = sHour + ":" + sMinute + " " + timeStamp;
        return time;
    }

    public static String getLocalDateFromUTC(String dateStr, boolean isDate) {

        Calendar calendar = Calendar.getInstance();
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'");
        System.out.println("============Sample date set as asia dubai TIME========");
        //Will print in UTC
        sdf.setTimeZone(TimeZone.getTimeZone("UTC"));
        try {
            Date d1 = sdf.parse(dateStr);
            calendar.setTime(d1);
            System.out.println(d1);
            System.out.println(calendar.getTime());
            System.out.println("============get After UTC========");
            //Here you say to java the initial timezone. This is the secret
            System.out.println(sdf.format(d1));
            System.out.println(sdf.format(calendar.getTime()));
            System.out.println("============ get After CONVERTED TO ZONAL TIME========");
            //Here you set to your timezone
            sdf.setTimeZone(TimeZone.getTimeZone("Asia/Dubai"));
            //Will print on your default Timezone
            System.out.println(sdf.format(calendar.getTime()));
            System.out.println(calendar.getTime());


            sdf.applyPattern("dd MMM yyyy");
            String finalDate= sdf.format(calendar.getTime());
            System.out.println("============ get After CONVERTED TO formatted.========");
            System.out.println(sdf.format(calendar.getTime()));
            sdf.applyPattern("hh:mm a");
            String finalTime=sdf.format(calendar.getTime());
            Log.d("DateTime", sdf.format(calendar.getTime()));

            if (isDate) {
                return finalDate;
            } else {
                return finalTime;
            }
        }catch (ParseException ex){
            ex.printStackTrace();
        }
        return "";
    }

    public static Date getTimepickerDate(String time) throws ParseException{

        Date date = new SimpleDateFormat("hh:mm a").parse(time);

        return date;
    }

    public static int getHr_in24Hr_Timeformat(String timeValue) {
        String[] splitByColon = timeValue.split(":");
        int hoursValue = Integer.parseInt(splitByColon[0]);
        String[] splitForMins = splitByColon[1].split(" ");
        if (splitForMins[1].equals("PM")) {
            if(hoursValue == 12)
                return hoursValue ;
            else
                return ( hoursValue + 12);
        }else {
            if(hoursValue == 12)
                return 0;
            else
                return hoursValue;
        }

    }

    public static int getMin_24Hr_Timeformat(String timeValue) {
        String[] splitByColon = timeValue.split(":");

        String[] splitForMins = splitByColon[1].split(" ");
        int minutesValue = Integer.parseInt(splitForMins[0]);
        return minutesValue;
    }
}
