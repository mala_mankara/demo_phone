package com.arowana.houdini.byod.model.feedback;

import io.realm.RealmList;
import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

public class FeedBackConfigObject extends RealmObject{

    @PrimaryKey
    private String appVersion;

    private String RequestData;

    private FeedbackResponseStatus ResponseStatus;

    private RealmList<FeedBackResponseData> ResponseData;

    public String getAppVersion() {
        return appVersion;
    }

    public void setAppVersion(String appVersion) {
        this.appVersion = appVersion;
    }

    public String getRequestData ()
    {
        return RequestData;
    }

    public void setRequestData (String RequestData)
    {
        this.RequestData = RequestData;
    }

    public FeedbackResponseStatus getResponseStatus ()
    {
        return ResponseStatus;
    }

    public void setResponseStatus (FeedbackResponseStatus ResponseStatus)
    {
        this.ResponseStatus = ResponseStatus;
    }

    public RealmList<FeedBackResponseData> getResponseData ()
    {
        return ResponseData;
    }

    public void setResponseData (RealmList<FeedBackResponseData> ResponseData)
    {
        this.ResponseData = ResponseData;
    }

    @Override
    public String toString()
    {
        return "ClassPojo [RequestData = "+RequestData+", ResponseStatus = "+ResponseStatus+", ResponseData = "+ResponseData+"]";
    }

}
