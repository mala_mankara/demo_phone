package com.arowana.houdini.byod.view.adapters;

import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.arowana.houdini.byod.MainApplication;
import com.arowana.houdini.byod.R;
import com.arowana.houdini.byod.interfaces.listeners.FragmentNavigationHandler;
import com.arowana.houdini.byod.model.palaceInformation.PalaceInfoHotels;
import com.arowana.houdini.byod.model.palaceInformation.PalaceInfoInformation;
import com.arowana.houdini.byod.utils.FactoryUtil;
import com.arowana.houdini.byod.utils.ViewUtils;
import com.arowana.houdini.byod.view.activity.BaseActivity;
import com.arowana.houdini.byod.view.fragments.WebViewFragment;
import com.arowana.houdini.byod.view.fragments.palaceInfo.PalaceMapFragment;

import java.util.Objects;

import io.realm.RealmList;

import static com.arowana.houdini.byod.view.activity.PalaceInfoActivity.fragmentHolder;

public class PalaceInfoAdapter extends RecyclerView.Adapter<PalaceInfoAdapter.CustomViewHolder> {

    private  PalaceInfoHotels hotelObj;
    private RealmList<PalaceInfoInformation> infoList;
    private Context cntxt;
    private FragmentNavigationHandler mHandler;
    private boolean isDisplayDefault;

    public PalaceInfoAdapter(Context context, PalaceInfoHotels information, FragmentNavigationHandler handler) {
        cntxt          = context;
        hotelObj       = information;
        mHandler       = handler;
        infoList       = hotelObj.getInformation();
    }

    public PalaceInfoAdapter(Context context, PalaceInfoHotels information, FragmentNavigationHandler handler, boolean b) {
        cntxt            = context;
        hotelObj         = information;
        mHandler         = handler;
        isDisplayDefault = b;
        infoList         = hotelObj.getInformation();
    }

    @NonNull
    @Override
    public CustomViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.palaceinfo_item, parent, false);
        return new PalaceInfoAdapter.CustomViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull CustomViewHolder holder, int position) {

        PalaceInfoInformation infoData = infoList.get(position);
        holder.info_field.setText(Objects.requireNonNull(infoData).getField());
        holder.info_value.setTag(position);

        if (isDisplayDefault) {
            holder.navigation.setBackgroundResource(R.drawable.navigation_icon);
        } else
            holder.navigation.setBackgroundResource(R.drawable.location_icon);
        holder.navigation.setTag(position);
        if (infoData.getField().equalsIgnoreCase("address")) {
            holder.info_value.setTextColor(cntxt.getResources().getColor(R.color.color_charcoal));
            holder.navigation.setVisibility(View.VISIBLE);
        }else if(infoData.getField().equalsIgnoreCase("Phone")||infoData.getField().equalsIgnoreCase("Telephone")
                ||infoData.getField().equalsIgnoreCase("Email")||infoData.getField().equalsIgnoreCase("Whatsapp")||
                (!infoData.getValue().isEmpty() && FactoryUtil.isValidURL(infoData.getValue()))){
            holder.info_value.setLinkTextColor(cntxt.getResources().getColor(R.color.autolink_text_color));
            holder.info_value.setTextColor(cntxt.getResources().getColor(R.color.autolink_text_color));
            holder.navigation.setVisibility(View.GONE);
            //holder.info_value.setAutoLinkMask(position);
        }else {
            //holder.info_value.setLinkTextColor();
            holder.navigation.setVisibility(View.GONE);
        }

        holder.info_value.setText(infoData.getValue());
        holder.navigation.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if(isDisplayDefault){
                    displayDefaultMap();

                }else {
                    if (infoList != null)
                        loadMapView();
                }
            }
        });
    }


    private void displayDefaultMap() {
        String uri = "http://maps.google.com/maps?daddr=" + hotelObj.getLatitude() + "," + hotelObj.getLongitude() + "(" + hotelObj.getName() + ")";
        Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(uri));
        intent.setPackage("com.google.android.apps.maps");
        cntxt.startActivity(intent);

    }

    private void loadMapView() {
        PalaceMapFragment mapFragment=new PalaceMapFragment();
        mapFragment.setObj(hotelObj);
        mHandler.showFragment(fragmentHolder.getId(),mapFragment);
    }

    @Override
    public int getItemCount() {
        return infoList.size();
    }

    class CustomViewHolder extends RecyclerView.ViewHolder  implements  View.OnClickListener {
        private TextView info_field;
        private TextView info_value;
        private ImageView navigation;

        CustomViewHolder(View itemView) {
            super(itemView);
            info_field    = itemView.findViewById(R.id.field);
            info_value    = itemView.findViewById(R.id.value);
            navigation    = itemView.findViewById(R.id.palace_location_image);
            info_value.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            PalaceInfoInformation rowData = infoList.get((Integer) v.getTag());

            String value     = rowData.getValue();
            String fieldName = rowData.getField();
            Intent actionIntent ;
            if(fieldName.equalsIgnoreCase("Phone")|| fieldName.equalsIgnoreCase("TelePhone")){

                actionIntent = new Intent(Intent.ACTION_DIAL);
                String p = "tel:" + (value);
                actionIntent.setData(Uri.parse(p));
                cntxt.startActivity(actionIntent);

            }if(fieldName.equalsIgnoreCase("Email")){

                actionIntent = new Intent(Intent.ACTION_SENDTO);
                actionIntent.setType("text/plain");
                actionIntent.setData(Uri.fromParts("mailto",value, null));
                cntxt.startActivity(Intent.createChooser(actionIntent, "Choose an Email client :"));

            }if(fieldName.equalsIgnoreCase("Whatsapp")){

                PackageManager pm = cntxt.getPackageManager();

                if (!MainApplication.appInstalledOrNot("com.whatsapp")) {
                    ViewUtils.showSnackBarMessage("Feature unavailable. WhatsApp needs to be installed",(BaseActivity)cntxt);
                } else {
                    String whatsAppNo = value;
                    if (!(whatsAppNo == null && whatsAppNo.isEmpty())) {
                        FactoryUtil.openWhatsApp(cntxt, whatsAppNo);
                    } else
                        ViewUtils.showSnackBarMessage("WhatsApp no. is not valid.");
                }

            }if(!value.isEmpty() && FactoryUtil.isValidURL(value)){
              // FactoryUtil.openUrlinBrowser(value,cntxt);
                WebViewFragment webview = new WebViewFragment();
                Bundle bundle = new Bundle();
                bundle.putString("title", fieldName);
                bundle.putString("url", value);
                webview.setArguments(bundle);
                mHandler.showFragment(fragmentHolder.getId(), webview);

            }
        }



    }
}
