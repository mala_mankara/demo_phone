package com.arowana.houdini.byod.model.inroomdining;

import io.realm.RealmObject;

public class IRBookingCustomType extends RealmObject {

    private String CustomTypeId;

    private String CustomTypeName;

    public String getCustomTypeId ()
    {
        return CustomTypeId;
    }

    public void setCustomTypeId (String CustomTypeId)
    {
        this.CustomTypeId = CustomTypeId;
    }

    public String getCustomTypeName ()
    {
        return CustomTypeName;
    }

    public void setCustomTypeName (String CustomTypeName)
    {
        this.CustomTypeName = CustomTypeName;
    }

    @Override
    public String toString()
    {
        return "ClassPojo [CustomTypeId = "+CustomTypeId+", CustomTypeName = "+CustomTypeName+"]";
    }
}
