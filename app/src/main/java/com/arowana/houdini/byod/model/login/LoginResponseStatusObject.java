package com.arowana.houdini.byod.model.login;

import io.realm.RealmObject;

public class LoginResponseStatusObject extends RealmObject {

    private String ResponseFlag;

    private String ResponseCode;

    private String ResponseMessage;

    private String ResponseId;

    public String getResponseFlag() {
        return ResponseFlag;
    }

    public void setResponseFlag(String responseFlag) {
        ResponseFlag = responseFlag;
    }

    public String getResponseCode() {
        return ResponseCode;
    }

    public void setResponseCode(String responseCode) {
        ResponseCode = responseCode;
    }

    public String getResponseMessage() {
        return ResponseMessage;
    }

    public void setResponseMessage(String responseMessage) {
        ResponseMessage = responseMessage;
    }

    public String getResponseId() {
        return ResponseId;
    }

    public void setResponseId(String responseId) {
        ResponseId = responseId;
    }
}
