package com.arowana.houdini.byod.view.fragments;


import android.Manifest;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.ImageView;

import androidx.annotation.NonNull;
import androidx.core.app.ActivityCompat;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentActivity;

import com.arowana.houdini.byod.Consts;
import com.arowana.houdini.byod.MainApplication;
import com.arowana.houdini.byod.R;
import com.arowana.houdini.byod.interfaces.ApiClient;
import com.arowana.houdini.byod.interfaces.api_interface.DeviceActivationApiInterface;
import com.arowana.houdini.byod.model.deviceactivation_QR.ActivityResponse;
import com.arowana.houdini.byod.model.deviceactivation_QR.QR_DeviceRegisterConfigObj;
import com.arowana.houdini.byod.model.deviceactivation_QR.QR_RegisterResponseData;
import com.arowana.houdini.byod.model.deviceactivation_QR.QR_RegisterResponseStatus;
import com.arowana.houdini.byod.model.deviceactivation_QR.SipProfile;
import com.arowana.houdini.byod.model.sipProfile.SipProfileConfigObj;
import com.arowana.houdini.byod.utils.DeviceOSFeatureUtils;
import com.arowana.houdini.byod.utils.DialogUtil;
import com.arowana.houdini.byod.utils.NetworkUtils;
import com.arowana.houdini.byod.utils.RealmUtil;
import com.arowana.houdini.byod.utils.TextUtil;
import com.arowana.houdini.byod.utils.ViewUtils;
import com.arowana.houdini.byod.view.activity.HomeActivity;
import com.google.android.material.textfield.TextInputLayout;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

import org.json.JSONObject;

import java.util.Objects;

import io.realm.Realm;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.arowana.houdini.byod.view.activity.DeviceActivationActivity.fragmentHolder;

/**
 * A simple {@link Fragment} subclass.
 */
public class DeviceActivationFragment extends BaseFragment implements  View.OnClickListener {

    FragmentActivity mContext;
    private View mRootView;
    private TextInputLayout mName;
    private TextInputLayout mRoomNo;
    private TextInputLayout mReservationId;
    protected ProgressDialog mDialog;
    Realm realm;
    private static final int REQUEST_CODE = 100;

    //  private boolean checkInStatus;

    @Override
    protected int getFragmentLayout() {
        return R.layout.fragment_device_activation;
    }

    public DeviceActivationFragment() {
        // Required empty public constructor
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        mRootView = view;
        mDialog = DialogUtil.showProgressDialog(mContext, "");
        // mDialog.setCancelable(false);
        initComponents();
        if (ActivityCompat.checkSelfPermission(mContext, Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED) {
            if(getActivity()!= null)
                ActivityCompat.requestPermissions(getActivity(), new String[]{Manifest.permission.CAMERA},
                    100);
            // return;
        }

    }


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mContext = getActivity();
        setTitle(getResources().getString(R.string.device_activation));
    }

    @Override
    public void onResume() {
        super.onResume();

    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        if (mDialog.isShowing())
            mDialog.dismiss();
    }

    public void initComponents() {

        Button mCheckin = mRootView.findViewById(R.id.device_activation_checkin);
        mName = mRootView.findViewById(R.id.device_activation_name);
        mRoomNo = mRootView.findViewById(R.id.device_activation_roomno);
        mReservationId = mRootView.findViewById(R.id.device_activation_reservationid);
        ImageView mScan = mRootView.findViewById(R.id.scan_qr);

        mCheckin.setOnClickListener(this);
        mRootView.setOnClickListener(this);
        mScan.setOnClickListener(this);
    }


    @Override
    public void onClick(View v) {
        InputMethodManager imm =  (InputMethodManager) mContext.getSystemService(Context.INPUT_METHOD_SERVICE);
        if (imm != null) {
            imm.hideSoftInputFromWindow(v.getWindowToken(), 0);
        }

        switch (v.getId()) {
            case R.id.device_activation_checkin:
                validate(Consts.DEVICE_ACTIVATIONTYPE_ID);
                break;
            case R.id.scan_qr:
                DeviceActivationWithQR qrFragment = new DeviceActivationWithQR();
                qrFragment.setTargetFragment(DeviceActivationFragment.this, 100);
                mHandler.showFragment(fragmentHolder.getId(), qrFragment);
                break;

        }
    }

    private void validate(String deviceActivationtypeId) {
        if (!TextUtil.isEmpty(mName, "LastName is required")) {
            if (!TextUtil.isEmpty(mRoomNo, "Enter valid RoomNo.")) {
                if (!TextUtil.isEmpty(mReservationId, "Enter Valid reservation Id")) {
                    createJSonObject(deviceActivationtypeId);
                }
            }
        }
    }

    private void createJSonObject(String deviceActivationtypeId) {
        try {
            Log.d("createJSonObject ", "------" + Objects.requireNonNull(mReservationId.getEditText()).getText());
            if (NetworkUtils.checkConnectivityStatus(mContext)) {
                JSONObject jsonObject = new JSONObject();
                jsonObject.put("FirstName", RealmUtil.getUserName());
                jsonObject.put("LastName", mName.getEditText().getText().toString());
                jsonObject.put("RoomNo", mRoomNo.getEditText().getText().toString());
                jsonObject.put("ReservationNo", mReservationId.getEditText().getText().toString());

                jsonObject.put("ActionType", deviceActivationtypeId);
                jsonObject.put("MembershipId", RealmUtil.getMembershipId());
                jsonObject.put("MacId", DeviceOSFeatureUtils.getMacAddress());
                jsonObject.put("PrimaryConnectionId", RealmUtil.getPrimaryConnectionId());

                Log.d("DeviceActiavtion ", "---activation fetchQRCode---" + jsonObject.toString());
                JsonParser jsonParser = new JsonParser();
                JsonObject gsonObject = (JsonObject) jsonParser.parse(jsonObject.toString());
                callRegistrationApi(gsonObject);

                //}
            }
        } catch (Exception e) {
            Log.d("123", "in the Error");
            e.printStackTrace();
        }
    }

    private String registrationMessage;

    // 19-07-18... call API for device register with OTP
    private void callRegistrationApi(JsonObject gsonObject) {

        if (mDialog.isShowing())
            mDialog.dismiss();

        Log.d("CALL API", "data.." + gsonObject.toString());
        Call<QR_DeviceRegisterConfigObj> qrConfigObj = ApiClient.getApiClient(DeviceActivationApiInterface.class).setQRDevRegistration(gsonObject);
        Log.d("CALL API", "configObj.." + qrConfigObj.isExecuted());

        qrConfigObj.enqueue(new Callback<QR_DeviceRegisterConfigObj>() {
            @Override
            public void onResponse(@NonNull Call<QR_DeviceRegisterConfigObj> call, @NonNull Response<QR_DeviceRegisterConfigObj> response) {
                Log.d("DeviceActiavtion ", "--- activation status---" + response.message() + ",call..." + call.toString());
                if (response.body() != null) {
                    QR_DeviceRegisterConfigObj configBody = response.body();
                    if (configBody != null) {
                        QR_RegisterResponseStatus responseStatus = configBody.getResponseStatus();
                        if (responseStatus != null) {
                            String registerFlag = responseStatus.getResponseFlag();
                            registrationMessage = responseStatus.getResponseMessage();
                            Context appContext = MainApplication.getmContext();

                            if (registerFlag.equalsIgnoreCase(appContext.getString(R.string.registration_status))) {
                                QR_RegisterResponseData responseData = configBody.getResponseData();
                                if (responseData != null) {
                                    Log.d("DeviceActiavtion ", "---save responseData to REALM---" + responseData.toString());
                                    realm = RealmUtil.getInstance();
                                    realm.beginTransaction();
                                    realm.delete(QR_RegisterResponseData.class);
                                    realm.copyToRealmOrUpdate(configBody);
                                    realm.commitTransaction();

                                    mListener.changeAppState(Consts.APP_CHECKIN_STATE);
                                    saveProfile(responseData);

                                }
                            }
                            Intent intent = new Intent(appContext, HomeActivity.class);
                            intent.putExtra("login_message", registrationMessage);
                            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                            intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                            mContext.startActivity(intent);
                        }
                    }
                } else
                    ViewUtils.showSnackBarMessage("Server error occurred.");
            }

            @Override
            public void onFailure(@NonNull Call<QR_DeviceRegisterConfigObj> call, @NonNull Throwable t) {
                t.printStackTrace();
                if (mDialog.isShowing())
                    mDialog.dismiss();

                Log.d("QRDeviceActiavtion ", "--- activation status---" + t);
                ViewUtils.showSnackBarMessage("Error occurred: " + t.getMessage());
            }
        });

    }

    SipProfileConfigObj sipPRofileObj;

    public void saveProfile(QR_RegisterResponseData responseData) {
        realm.beginTransaction();
      //  realm.delete(SipProfileConfigObj.class);
        sipPRofileObj = realm.createObject(SipProfileConfigObj.class);
        ActivityResponse alcatelResponseObj	= responseData.getActivityResponse();
        SipProfile alcatelSipProfileObj	= alcatelResponseObj.getSipProfile();
        sipPRofileObj.setUserName(alcatelSipProfileObj.getLoginName());
        sipPRofileObj.setDisplayName(alcatelSipProfileObj.getDisplayName());
        sipPRofileObj.setLoginName(alcatelSipProfileObj.getLoginName());
        sipPRofileObj.setPassword(alcatelSipProfileObj.getPassword());
        sipPRofileObj.setDomain(alcatelSipProfileObj.getDomain());
        sipPRofileObj.setDomainPort(alcatelSipProfileObj.getDomainPort());
        sipPRofileObj.setProtocol(alcatelSipProfileObj.getProtocol());
        sipPRofileObj.setOutboundProxy(alcatelSipProfileObj.getOutboundProxy());
        sipPRofileObj.setOutboundProxyPort(alcatelSipProfileObj.getOutboundProxyPort());
        sipPRofileObj.setDeviceId(alcatelResponseObj.getDeviceId());
        sipPRofileObj.setSbcPort(alcatelSipProfileObj.getSbcPort());
        sipPRofileObj.setSbcPublicIpAddress(alcatelSipProfileObj.getSbcPublicIpAddress());
       // sipPRofileObj.setSbcUseTls(alcatelSipProfileObj.getSbcUseTls());
        realm.commitTransaction();
    }



    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == REQUEST_CODE && resultCode == getActivity().RESULT_OK) {
            String jsonObj = data.getStringExtra("objData");
            JsonParser jsonParser = new JsonParser();
            JsonObject gsonObject = (JsonObject) jsonParser.parse(jsonObj.toString());
            Log.d("onActivityResult", "datas Are.." + gsonObject + ",/string.." + gsonObject.toString());
            callRegistrationApi(gsonObject);
        }
    }
}
