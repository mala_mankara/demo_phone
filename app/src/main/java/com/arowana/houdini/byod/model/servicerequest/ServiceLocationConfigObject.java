package com.arowana.houdini.byod.model.servicerequest;

import io.realm.RealmList;
import io.realm.RealmObject;

public class ServiceLocationConfigObject  extends RealmObject {

    private String RequestData;

    private ServiceResponseStatusObject ResponseStatus;

    private RealmList<ServiceLocationResponseData> ResponseData;

    public String getRequestData ()
    {
        return RequestData;
    }

    public void setRequestData (String RequestData)
    {
        this.RequestData = RequestData;
    }

    public ServiceResponseStatusObject getResponseStatus ()
    {
        return ResponseStatus;
    }

    public void setResponseStatus (ServiceResponseStatusObject ResponseStatus)
    {
        this.ResponseStatus = ResponseStatus;
    }

    public RealmList<ServiceLocationResponseData> getResponseData()
    {
        return ResponseData;
    }

    public void setResponseData (RealmList<ServiceLocationResponseData> ResponseData)
    {
        this.ResponseData = ResponseData;
    }
}
