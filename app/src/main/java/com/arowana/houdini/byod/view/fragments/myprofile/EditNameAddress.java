package com.arowana.houdini.byod.view.fragments.myprofile;

import android.content.Context;
import android.os.Bundle;
import com.google.android.material.textfield.TextInputLayout;
import android.text.Html;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.TextView;

import com.arowana.houdini.byod.R;
import com.arowana.houdini.byod.model.login.LoginResponseDataObject;
import com.arowana.houdini.byod.utils.TextUtil;
import com.arowana.houdini.byod.utils.ViewUtils;


public class EditNameAddress implements View.OnClickListener {

    private View mRootView;
    private TextInputLayout mGuestAddressTxt;
    private TextInputLayout mGuestCityTxt;
    private TextInputLayout mGuestCountryTxt;
    private TextInputLayout mGuestPincodeTxt;
    private TextView mConfirmBtn;
    private Context mContext;
    private LoginResponseDataObject myProfileObject;
    private UpdateProfileDataListener mListener;

    public EditNameAddress(View mRootView, Context mContext, LoginResponseDataObject profileObject, UpdateProfileDataListener listener) {
        Log.d("test", "---EditNameAddress----");
        this.mRootView = mRootView;
        this.mContext = mContext;
        this.myProfileObject = profileObject;
        this.mListener = listener;

        init();
    }

    private void init() {

        View view = LayoutInflater.from(mContext).inflate(R.layout.layout_edit_name_address, null);
        FrameLayout layout = mRootView.findViewById(R.id.inflaterView);
        layout.addView(view);

        mGuestAddressTxt = view.findViewById(R.id.address_txt);
        mGuestAddressTxt.getEditText().setHint(TextUtils.concat(mContext.getString(R.string.guest_address), Html.fromHtml(mContext.getString(R.string.required_asterisk))));
        if (myProfileObject.getAddress() != null && !myProfileObject.getAddress().isEmpty()) {
            mGuestAddressTxt.getEditText().setText(myProfileObject.getAddress());
        }

        mGuestCityTxt = view.findViewById(R.id.city_txt);
        mGuestCityTxt.getEditText().setHint(TextUtils.concat(mContext.getString(R.string.guest_city), Html.fromHtml(mContext.getString(R.string.required_asterisk))));
        if (myProfileObject.getCity() != null && !myProfileObject.getCity().isEmpty()) {
            mGuestCityTxt.getEditText().setText(myProfileObject.getCity());
        }

        mGuestCountryTxt = view.findViewById(R.id.country_txt);
        mGuestCountryTxt.getEditText().setHint(TextUtils.concat(mContext.getString(R.string.guest_country), Html.fromHtml(mContext.getString(R.string.required_asterisk))));
        if (myProfileObject.getCountry() != null && !myProfileObject.getCountry().isEmpty()) {
            mGuestCountryTxt.getEditText().setText(myProfileObject.getCountry());
        }

        mGuestPincodeTxt = view.findViewById(R.id.pincode_txt);
        mGuestPincodeTxt.getEditText().setHint(TextUtils.concat(mContext.getString(R.string.guest_pincode), Html.fromHtml(mContext.getString(R.string.required_asterisk))));
        if (myProfileObject.getPinCode() != null && !myProfileObject.getPinCode().isEmpty()) {
            mGuestPincodeTxt.getEditText().setText(myProfileObject.getPinCode());
        }


        mConfirmBtn = view.findViewById(R.id.confirmButton);
        mConfirmBtn.setOnClickListener(this);

    }


    @Override
    public void onClick(View v) {
        int id = v.getId();

        switch (id) {
            case R.id.confirmButton:
                ViewUtils.hideSoftKeyboard(v);
                Log.d("test", "-----onClick----------");
                validateField();
                break;
        }


    }

    private void validateField() {
        Log.d("test", "-----validateField----------");
        /*if (!TextUtil.isEmpty()(mFirstNameTxt, "Please enter Guest First Name")) {
            return;
        }*/
        Boolean hasAddress = false;
        if (TextUtil.isEmpty(mGuestAddressTxt) && TextUtil.isEmpty(mGuestCityTxt) &&
                TextUtil.isEmpty(mGuestCountryTxt) && TextUtil.isEmpty(mGuestPincodeTxt)) {
            hasAddress = true;
        }
        if (!hasAddress) {
            if (TextUtil.isEmpty(mGuestAddressTxt, "Please enter valid address")) {
                return;
            }
            if (TextUtil.isEmpty(mGuestCityTxt, "Please enter valid city")) {
                return;
            }
            if (TextUtil.isEmpty(mGuestCountryTxt, "Please enter valid country")) {
                return;
            }
            if (TextUtil.isEmpty(mGuestPincodeTxt, "Please enter valid pincode")) {
                return;
            }
            if(!TextUtil.validatePincode(mGuestPincodeTxt, "Enter Validpincode")){
                return;
            }
        }
        if (mListener != null) {
            Bundle data = new Bundle();

            if (hasAddress) {
                data.putString("guestAddress", mGuestAddressTxt.getEditText().getText().toString());
                data.putString("guestCity", mGuestCityTxt.getEditText().getText().toString());
                data.putString("guestCountry", mGuestCountryTxt.getEditText().getText().toString());
                data.putString("guestPincode", mGuestPincodeTxt.getEditText().getText().toString());
            }
            mListener.onUpdateProfileData(data);
        }
    }


}
