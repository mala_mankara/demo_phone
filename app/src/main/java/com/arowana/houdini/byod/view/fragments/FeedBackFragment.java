package com.arowana.houdini.byod.view.fragments;


import android.app.Fragment;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.arowana.houdini.byod.Consts;
import com.arowana.houdini.byod.R;
import com.arowana.houdini.byod.interfaces.ApiClient;
import com.arowana.houdini.byod.interfaces.api_interface.FeedbackApiInterface;
import com.arowana.houdini.byod.model.feedback.FeedBackConfigObject;
import com.arowana.houdini.byod.model.feedback.FeedBackResponseData;
import com.arowana.houdini.byod.model.feedback.FeedbackResponseStatus;
import com.arowana.houdini.byod.utils.RealmUtil;
import com.arowana.houdini.byod.utils.ViewUtils;
import com.arowana.houdini.byod.view.adapters.FeedBackAdapter;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

import org.json.JSONArray;
import org.json.JSONObject;

import io.realm.Realm;
import io.realm.RealmList;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


/**
 * A simple {@link Fragment} subclass.
 */
public class FeedBackFragment extends BaseFragment implements View.OnClickListener {

    private  View mRootView;

    private FeedBackConfigObject mConfigObject;
    private RealmList<FeedBackResponseData> mFeedbackDataObj;
    FeedBackConfigObject feedbackObject;
    private String mRequestData;
    private Realm mRealm;
    private LinearLayout mMsgLayout;
    private RecyclerView mRecyclerView;
    private EditText commentsView;

    public FeedBackFragment() { // Required empty public constructor
    }

    @Override
    protected int getFragmentLayout() {
        return R.layout.fragment_feedback;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setTitle(getResources().getString(R.string.feedBack_Title));
        mRealm = RealmUtil.getInstance();
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        mRootView=view;

        initView();
        fetchData();
    }
    String  message;
    private void fetchData() {
        final Call<FeedBackConfigObject> feedbackObj = ApiClient.getApiClient(FeedbackApiInterface.class).getFeedback(RealmUtil.getMembershipId(), RealmUtil.getBookingId());
        feedbackObj.enqueue(new Callback<FeedBackConfigObject>() {
            @Override
            public void onResponse(@NonNull Call<FeedBackConfigObject> call, @NonNull Response<FeedBackConfigObject> response) {
                if (response.isSuccessful() && response.body() != null) {
                    mConfigObject = response.body();
                    message = mConfigObject.getResponseStatus().getResponseMessage();
                    Realm realm = RealmUtil.getInstance();
                    realm.beginTransaction();
                    realm.copyToRealmOrUpdate(mConfigObject);
                    realm.commitTransaction();
                    feedbackObject = realm.where(FeedBackConfigObject.class).findFirst();
                    assert feedbackObject != null;
                    mFeedbackDataObj = feedbackObject.getResponseData();
                    mRequestData =feedbackObject.getRequestData();
                    if (mFeedbackDataObj != null) {
                        updateFeedBackList(mFeedbackDataObj,mRequestData);
                    } else {
                        ViewUtils.showSnackBarMessage(message);
                    }
                } else {
                    ViewUtils.showSnackBarMessage(message);
                }
            }

            @Override
            public void onFailure(@NonNull Call<FeedBackConfigObject> call, @NonNull Throwable t) {
                t.printStackTrace();
            }
        });
        //  }
    }

    private void updateFeedBackList(RealmList<FeedBackResponseData> results, String mRequestData) {
        populateAdapterView(results);
        if (mRequestData != null && !mRequestData.isEmpty()) {
            commentsView.setText(mRequestData);
        } else {
            commentsView.setHint("");
        }
        commentsView.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {}
            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {}
            @Override
            public void afterTextChanged(Editable s) {
                String commentText=commentsView.getText().toString().trim();
                if (!commentText.isEmpty()) {
                    if (!mRealm.isInTransaction()) {
                        mRealm.beginTransaction();
                        mConfigObject.setRequestData(commentsView.getText().toString().trim() + "");
                        mRealm.commitTransaction();
                    }
                } else {
                    if (!mRealm.isInTransaction()) {
                        mRealm.beginTransaction();
                        mConfigObject.setRequestData("");
                        mRealm.commitTransaction();
                    }
                }
            }
        });
    }

    private void populateAdapterView(RealmList<FeedBackResponseData> results) {
        LinearLayoutManager linearVertical = new LinearLayoutManager(getContext(), RecyclerView.VERTICAL, false);
        mRecyclerView.setLayoutManager(linearVertical);
        FeedBackAdapter adapter = new FeedBackAdapter(getContext(),results );
        mRecyclerView.setAdapter(adapter);
    }

    private void initView() {
        Button mSubmitButton    = mRootView.findViewById(R.id.submit_btn_feedback);
        Button mCheckoutSubmit  = mRootView.findViewById(R.id.submit_btn_checkout_feedback);
        Button mSkipButton      = mRootView.findViewById(R.id.skip_btn_feedback);
        LinearLayout layoutBtns = mRootView.findViewById(R.id.layout_checkout_btns);

        mMsgLayout      = mRootView.findViewById(R.id.layout_checkout_msg);
        mRecyclerView   = mRootView.findViewById(R.id.feedback_recyclerview);
        commentsView    = mRootView.findViewById(R.id.editTxtComment);

        commentsView.setHorizontallyScrolling(false);
        commentsView.setScrollContainer(true);
        commentsView.setMaxLines(3);
        commentsView.setImeOptions(EditorInfo.IME_ACTION_DONE);

        if(Consts.APP_CURRENT_STATE != Consts.APP_CHECKIN_STATE)    {
            mMsgLayout.setVisibility(View.VISIBLE);
            layoutBtns.setVisibility(View.VISIBLE);
            mSubmitButton.setVisibility(View.GONE);
            setTitle(getString(R.string.checkout));

        } else {
            setTitle(getString(R.string.feedBack_Title));
        }
        mSubmitButton.setOnClickListener(this);
        mCheckoutSubmit.setOnClickListener(this);
        mSkipButton.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        int id = v.getId();
        switch (id) {
            case R.id.submit_btn_feedback:
                //  populateSnackbar();
                submitFeedback();
                break;

            case R.id.submit_btn_checkout_feedback:
                //populateSnackbar();
                submitFeedback();
                break;

            case R.id.skip_btn_feedback:
                if(getActivity()!= null && !getActivity().isDestroyed())
                    getActivity().finish();
                break;
        }
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        mMsgLayout.setVisibility(View.GONE);
    }

    private void submitFeedback() {
        try {
            FeedBackResponseData dataObj;
            JSONArray feedBackData = new JSONArray();
            JSONObject json = null;
            for (int i = 0; i < mFeedbackDataObj.size(); i++) {
                dataObj=mFeedbackDataObj.get(i);
                if(dataObj != null && dataObj.isValid()) {
                    json = new JSONObject();
                    json.put("ID",dataObj.getID());
                    json.put("Name", dataObj.getName());
                    json.put("Rating", dataObj.getPreviousRating());
                }

                feedBackData.put(json);
            }
            JSONObject jsonObject = new JSONObject();
            jsonObject.put("Services", feedBackData);
            String comment=mConfigObject.getRequestData();
            if(comment != null && !comment.isEmpty())
                jsonObject.put("Comments", comment);
            jsonObject.put("MembershipID", RealmUtil.getMembershipId());
            jsonObject.put("BookingID", RealmUtil.getBookingId());

            JsonParser jsonParser = new JsonParser();
            JsonObject gsonObject = (JsonObject) jsonParser.parse(jsonObject.toString());
            submitFeedbackApi(gsonObject);


        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    private String flag;
    private void submitFeedbackApi(JsonObject gsonObject) {
        final Call<FeedBackConfigObject> feedbackObj=ApiClient.getApiClient(FeedbackApiInterface.class).sendFeedback(gsonObject);
        feedbackObj.enqueue(new Callback<FeedBackConfigObject>() {
            @Override
            public void onResponse(@NonNull Call<FeedBackConfigObject> call, @NonNull Response<FeedBackConfigObject> response) {

                if(response.body() != null ){
                    FeedBackConfigObject responseBody=response.body();
                    if(responseBody != null){
                        FeedbackResponseStatus responseState=responseBody.getResponseStatus();
                        if(responseState.isValid()) {
                            flag = responseState.getResponseFlag();
                            message=responseState.getResponseMessage();
                        }
                    }
                }
                if (flag.equalsIgnoreCase("SUCCESS")) {
                    ViewUtils.showSnackBarMessage(message);
                    new Handler(Looper.myLooper()).postDelayed(new Runnable() {
                        @Override
                        public void run() {
//                           Intent intent=new Intent(getActivity(), Activity.class);
//                           intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
//                           startActivity(intent);
                            if (getActivity() != null && !getActivity().isDestroyed()) {
                                getActivity().finish();
                            }

                        }
                    },3000);

                } else ViewUtils.showSnackBarMessage(message);
            }

            @Override
            public void onFailure(@NonNull Call<FeedBackConfigObject> call,@NonNull Throwable t) {
                t.printStackTrace();
            }
        });
    }
}
