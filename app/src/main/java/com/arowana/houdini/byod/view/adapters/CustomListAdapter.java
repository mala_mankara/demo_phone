package com.arowana.houdini.byod.view.adapters;

import android.content.Context;
import androidx.annotation.NonNull;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.arowana.houdini.byod.model.servicerequest.ServiceLocationResponseData;

import io.realm.RealmList;

public class CustomListAdapter extends ArrayAdapter  {

    private RealmList<ServiceLocationResponseData>locationData;

    public CustomListAdapter(@NonNull Context context, int resource,
                             RealmList<ServiceLocationResponseData> locationObj) {
        super(context, resource);
        locationData=locationObj;
    }

    @Override
    public int getCount() {
        return locationData.size();
    }

    @Override
    public String getItem(int position) {return locationData.get(position) != null ? locationData.get(position).getPlace() : null; }

    @NonNull
    @Override
    public View getView(int position, View view, @NonNull ViewGroup parent) {

        if (view == null) {
            view = LayoutInflater.from(parent.getContext()).inflate(android.R.layout.simple_spinner_dropdown_item, parent, false);
        }
        TextView locationName;
        locationName = view.findViewById(android.R.id.text1);
        locationName.setText(getItem(position));
        return view;
    }
}
