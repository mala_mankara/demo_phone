package com.arowana.houdini.byod.model.inroomdining;

import io.realm.RealmObject;

public class IRDiningGuestProfileObject extends RealmObject {

    private String Email;

    private String MembershipId;

    private String FullName;

    private String FirstName;

    private String PhoneNumber;

    private String LastName;

    public String getEmail() {
        return Email;
    }

    public void setEmail(String email) {
        Email = email;
    }

    public String getMembershipId() {
        return MembershipId;
    }

    public void setMembershipId(String membershipId) {
        MembershipId = membershipId;
    }

    public String getFullName() {
        return FullName;
    }

    public void setFullName(String fullName) {
        FullName = fullName;
    }

    public String getFirstName() {
        return FirstName;
    }

    public void setFirstName(String firstName) {
        FirstName = firstName;
    }

    public String getPhoneNumber() {
        return PhoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        PhoneNumber = phoneNumber;
    }

    public String getLastName() {
        return LastName;
    }

    public void setLastName(String lastName) {
        LastName = lastName;
    }
}
