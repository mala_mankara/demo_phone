package com.arowana.houdini.byod.model.weather;

public class WeatherResponseDataObject {

    private Weather Weather;
    private Location Location;
    private String Id;
    private String Unit;

    public Weather getWeather (){
        return Weather;
    }

    public void setWeather (Weather Weather){
        this.Weather = Weather;
    }

    public Location getLocation ()
    {
        return Location;
    }

    public void setLocation (Location Location)
    {
        this.Location = Location;
    }

    public String getId ()
    {
        return Id;
    }

    public void setId (String Id)
    {
        this.Id = Id;
    }

    public String getUnit ()
    {
        return Unit;
    }

    public void setUnit (String Unit)
    {
        this.Unit = Unit;
    }

}
