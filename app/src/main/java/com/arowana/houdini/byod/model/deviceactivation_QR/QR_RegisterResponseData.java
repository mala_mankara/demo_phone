package com.arowana.houdini.byod.model.deviceactivation_QR;

import io.realm.RealmObject;

public class QR_RegisterResponseData extends RealmObject {

    private String MacId;

    private String MobileNo;

    private String RoomNo;

    private String UniqueCode;

    private String ActionType;

    private String PrimaryConnectionId;

    private String FirstName;

    private String DeviceConfigurations;

    private String AdditionalDevices;

    private ActivityResponse ActivityResponse;

    private String IsCheckedIn;

    private String MembershipId;

    private String IsPrimary;

    private String Id;

    private String LastName;

    private String GuestName;

    private String ReservationNo;

    public String getMacId ()
    {
        return MacId;
    }

    public void setMacId (String MacId)
    {
        this.MacId = MacId;
    }

    public String getMobileNo ()
    {
        return MobileNo;
    }

    public void setMobileNo (String MobileNo)
    {
        this.MobileNo = MobileNo;
    }

    public String getRoomNo ()
    {
        return RoomNo;
    }

    public void setRoomNo (String RoomNo)
    {
        this.RoomNo = RoomNo;
    }

    public String getUniqueCode ()
    {
        return UniqueCode;
    }

    public void setUniqueCode (String UniqueCode)
    {
        this.UniqueCode = UniqueCode;
    }

    public String getActionType ()
    {
        return ActionType;
    }

    public void setActionType (String ActionType)
    {
        this.ActionType = ActionType;
    }

    public String getPrimaryConnectionId ()
    {
        return PrimaryConnectionId;
    }

    public void setPrimaryConnectionId (String PrimaryConnectionId)
    {
        this.PrimaryConnectionId = PrimaryConnectionId;
    }

    public String getFirstName ()
    {
        return FirstName;
    }

    public void setFirstName (String FirstName)
    {
        this.FirstName = FirstName;
    }

    public String getDeviceConfigurations ()
    {
        return DeviceConfigurations;
    }

    public void setDeviceConfigurations (String DeviceConfigurations)
    {
        this.DeviceConfigurations = DeviceConfigurations;
    }

    public String getAdditionalDevices ()
    {
        return AdditionalDevices;
    }

    public void setAdditionalDevices (String AdditionalDevices)
    {
        this.AdditionalDevices = AdditionalDevices;
    }

    public ActivityResponse getActivityResponse ()
    {
        return ActivityResponse;
    }

    public void setActivityResponse (ActivityResponse ActivityResponse)
    {
        this.ActivityResponse = ActivityResponse;
    }

    public String getIsCheckedIn ()
    {
        return IsCheckedIn;
    }

    public void setIsCheckedIn (String IsCheckedIn)
    {
        this.IsCheckedIn = IsCheckedIn;
    }

    public String getMembershipId ()
    {
        return MembershipId;
    }

    public void setMembershipId (String MembershipId)
    {
        this.MembershipId = MembershipId;
    }

    public String getIsPrimary ()
    {
        return IsPrimary;
    }

    public void setIsPrimary (String IsPrimary)
    {
        this.IsPrimary = IsPrimary;
    }

    public String getId ()
    {
        return Id;
    }

    public void setId (String Id)
    {
        this.Id = Id;
    }

    public String getLastName ()
    {
        return LastName;
    }

    public void setLastName (String LastName)
    {
        this.LastName = LastName;
    }

    public String getGuestName ()
    {
        return GuestName;
    }

    public void setGuestName (String GuestName)
    {
        this.GuestName = GuestName;
    }

    public String getReservationNo ()
    {
        return ReservationNo;
    }

    public void setReservationNo (String ReservationNo)
    {
        this.ReservationNo = ReservationNo;
    }

    @Override
    public String toString()
    {
        return "ClassPojo [MacId = "+MacId+", MobileNo = "+MobileNo+", RoomNo = "+RoomNo+", UniqueCode = "+UniqueCode+", ActionType = "+ActionType+", PrimaryConnectionId = "+PrimaryConnectionId+", FirstName = "+FirstName+", DeviceConfigurations = "+DeviceConfigurations+", AdditionalDevices = "+AdditionalDevices+", ActivityResponse = "+ActivityResponse+", IsCheckedIn = "+IsCheckedIn+", MembershipId = "+MembershipId+", IsPrimary = "+IsPrimary+", Id = "+Id+", LastName = "+LastName+", GuestName = "+GuestName+", ReservationNo = "+ReservationNo+"]";
    }
}
