package com.arowana.houdini.byod.view.fragments.myprofile;

import android.content.Context;
import android.os.Bundle;
import com.google.android.material.textfield.TextInputLayout;
import android.text.Html;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.TextView;

import com.arowana.houdini.byod.R;
import com.arowana.houdini.byod.model.login.LoginResponseDataObject;
import com.arowana.houdini.byod.utils.TextUtil;
import com.arowana.houdini.byod.utils.ViewUtils;


public class EditPhoneNumber implements View.OnClickListener {

    private View mRootView;
    private TextInputLayout mPhoneTxt, mPhoneCode;
    private TextView mConfirmBtn;
    private Context mContext;
    private LoginResponseDataObject myProfileObject;
    private UpdateProfileDataListener mListener;


    public EditPhoneNumber(View mRootView, Context mContext, LoginResponseDataObject profileObject, UpdateProfileDataListener listener) {
        Log.d("test", "---EditPhoneNumber----");
        this.mRootView = mRootView;
        this.mContext = mContext;
        this.myProfileObject = profileObject;
        this.mListener = listener;

        init();
    }

    private void init() {

        View view = LayoutInflater.from(mContext).inflate(R.layout.layout_edit_phone_no, null);
        FrameLayout layout = mRootView.findViewById(R.id.inflaterView);
        layout.addView(view);


        mPhoneTxt = view.findViewById(R.id.phone_txt);
        mPhoneCode = view.findViewById(R.id.phone_code);
        mPhoneTxt.getEditText().setHint(TextUtils.concat(mContext.getString(R.string.phone_number), Html.fromHtml(mContext.getString(R.string.required_asterisk))));
        mPhoneCode.getEditText().setHint(TextUtils.concat(mContext.getString(R.string.phone_code), Html.fromHtml(mContext.getString(R.string.required_asterisk))));


        if (myProfileObject.getCountryCode() != null && !myProfileObject.getCountryCode().isEmpty()) {
            mPhoneCode.getEditText().setText(myProfileObject.getCountryCode());
        }


        if (myProfileObject.getMobileNumber() != null && !myProfileObject.getMobileNumber().isEmpty()) {
            mPhoneTxt.getEditText().setText(myProfileObject.getMobileNumber());
        }


        mConfirmBtn = view.findViewById(R.id.confirmButton);
        mConfirmBtn.setOnClickListener(this);

    }


    @Override
    public void onClick(View v) {
        int id = v.getId();

        switch (id) {
            case R.id.confirmButton:
                ViewUtils.hideSoftKeyboard(v);
                validateField();
                break;
        }


    }

    private void validateField() {
        if (TextUtil.isEmpty(mPhoneCode, "Please enter the code")) {
            return;
        }
        if (TextUtil.isEmpty(mPhoneTxt, "Please enter the phone number")) {
            return;
        }
        if (!TextUtil.validatePhoneNo(mPhoneTxt, "Please enter valid phone number")) {
            return;
        }
        if (mListener != null) {
            Bundle data = new Bundle();
            data.putString("phoneCode", mPhoneCode.getEditText().getText().toString());
            data.putString("phoneNumber", mPhoneTxt.getEditText().getText().toString());
            mListener.onUpdateProfileData(data);
        }
    }

}
