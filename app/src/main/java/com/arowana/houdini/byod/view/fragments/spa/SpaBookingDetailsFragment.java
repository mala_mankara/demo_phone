package com.arowana.houdini.byod.view.fragments.spa;


import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.arowana.houdini.byod.R;
import com.arowana.houdini.byod.model.spa.SpaBookingConfigObject;
import com.arowana.houdini.byod.model.spa.SpaBookingObject;
import com.arowana.houdini.byod.model.spa.SpaBookingTreatmentObj;
import com.arowana.houdini.byod.model.spa.SpaBookingTreatmentTypeObj;
import com.arowana.houdini.byod.utils.RealmUtil;
import com.arowana.houdini.byod.view.adapters.spa.SpaBookingDetailsAdapter;
import com.arowana.houdini.byod.view.fragments.BaseFragment;

import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.text.ParseException;

import io.realm.Realm;
import io.realm.RealmResults;


public class SpaBookingDetailsFragment extends BaseFragment {


    public SpaBookingDetailsFragment() {

    }

    private View mRootView;
    private RecyclerView mRecyclerView;
    SpaBookingObject mBookingObj;
    TextView mInstructions,mCost;
    private Button mBtnOk;


    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        mRootView = view;
        setTitle(getResources().getString(R.string.booking_summary));
        initComponents();
        populateData();
        setTotalPrice();
    }

    private void populateData() {

        mInstructions.setText(mBookingObj.getAdditionalNote());
        LinearLayoutManager linearVertical = new LinearLayoutManager(getContext(), RecyclerView.VERTICAL, false);
        mRecyclerView.setLayoutManager(linearVertical);
        Log.d("SPA_BD_FRAG","call Adapter.."+ mBookingObj.getTreatmentType().toString());
        SpaBookingDetailsAdapter adapter = new SpaBookingDetailsAdapter(getContext(), mBookingObj.getTreatmentType());
        mRecyclerView.setAdapter(adapter);
    }

    public void initComponents() {

        mRecyclerView = mRootView.findViewById(R.id.spa_booking_details_recyclerview);
        mInstructions = mRootView.findViewById(R.id.spa_instruction_data);
        mCost = mRootView.findViewById(R.id.spa_total_cost);
        mBtnOk=mRootView.findViewById(R.id.spa_booking_btn_ok);
        ImageView mBtnBack = getActivity().findViewById(R.id.btn_back);
        mBtnBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                getActivity().finish();
                deleteSpaBooking();
            }
        });

        mBtnOk.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                getActivity().finish();
                deleteSpaBooking();
            }
        });

    }

    @Override
    protected int getFragmentLayout() {
        return R.layout.fragment_spa_booking_details;
    }

    public void setBookingObj(SpaBookingObject obj) {
        mBookingObj = obj;
        Log.d("","setBookingObj"+mBookingObj.toString());
    }


    public void setTotalPrice() {

        DecimalFormat format = new DecimalFormat();
        format.setDecimalSeparatorAlwaysShown(false);
        Double price = Double.valueOf(0);


        for (SpaBookingTreatmentTypeObj typeObj : mBookingObj.getTreatmentType()) {
            for(SpaBookingTreatmentObj bean:typeObj.getTreatment()) {
                try {
                    price += (Double.valueOf(bean.getFemalePax()) +Double.valueOf( bean.getMalePax())
                            + Double.valueOf(bean.getCouplesCount()) )* NumberFormat.getInstance().parse(bean.getPrice()).doubleValue();
                } catch (ParseException e) {
                    e.printStackTrace();
                }
            }


        }

        mCost.setText(RealmUtil.getSpaCurrency() + " " + String.valueOf(format.format(price)));
    }

    public void deleteSpaBooking(){
        RealmUtil.getInstance().executeTransaction(new Realm.Transaction() {
            @Override
            public void execute(Realm realm) {
                RealmResults<SpaBookingConfigObject> result = realm.where(SpaBookingConfigObject.class).findAll();
                result.deleteAllFromRealm();
            }
        });
    }






}
