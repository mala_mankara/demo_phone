package com.arowana.houdini.byod.view.adapters.spa;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.viewpager.widget.ViewPager;

import com.arowana.houdini.byod.R;
import com.arowana.houdini.byod.interfaces.listeners.FragmentNavigationHandler;
import com.arowana.houdini.byod.model.spa.SpaImagesObj;
import com.arowana.houdini.byod.model.spa.SpaObject;
import com.arowana.houdini.byod.model.spa.SpaResponseDataObj;
import com.arowana.houdini.byod.view.adapters.MultiImageAdapter;

import java.util.ArrayList;

import io.realm.RealmList;

public class SpaAdapter extends RecyclerView.Adapter<SpaAdapter.MyViewHolder> {

    private Context context;
    private RealmList<SpaObject> spaObj;
    FragmentNavigationHandler mHandler;
    FrameLayout mHolder;
    public String  HotelTimeZone;





    public class MyViewHolder extends RecyclerView.ViewHolder {
        ViewPager imgHolder;
        RecyclerView recyclerview;
        TextView title;
        LinearLayout ll_dots;


        public MyViewHolder(View view) {
            super(view);
            imgHolder = view.findViewById(R.id.spa_viewpager);
            recyclerview = view.findViewById(R.id.spa_treamenttype_list);
            title = view.findViewById(R.id.spa_title);
            ll_dots = view.findViewById(R.id.spa_ll_dots);
        }
    }

    public SpaAdapter(Context mContext, SpaResponseDataObj obj, FragmentNavigationHandler handler, FrameLayout holder) {
        this.context = mContext;
        this.spaObj = obj.getSpaInfo();
        HotelTimeZone = obj.getHotelTimeZone();
        mHandler=handler;
        mHolder=holder;

    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_spa_main, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, int position) {
        SpaObject bean = spaObj.get(position);


        holder.title.setText(bean != null ? bean.getSpaName() : null);

        LinearLayoutManager linearVertical = new LinearLayoutManager(context, RecyclerView.VERTICAL, false);
        holder.recyclerview.setLayoutManager(linearVertical);
        SpaTreatmentTypeAdapter adapter = new SpaTreatmentTypeAdapter(context,HotelTimeZone, bean,mHandler,mHolder);
        holder.recyclerview.setAdapter(adapter);


        if (bean.getImages().size() > 0) {
            ArrayList<String> imageList = new ArrayList<>();
            for (SpaImagesObj image : bean.getImages()) {
                String imgPath = image.getImageUrl();
                imageList.add(imgPath);
            }

            holder.imgHolder.setVisibility(View.VISIBLE);
            holder.imgHolder.setAdapter(new MultiImageAdapter(context, imageList));

            if (imageList.size() > 1) {
                holder.ll_dots.removeAllViews();
                final ImageView[] ivArrayDotsPager = new ImageView[imageList.size()];
                for (int i = 0; i < ivArrayDotsPager.length; i++) {
                    ivArrayDotsPager[i] = new ImageView(context);
                    LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
                    params.setMargins(5, 0, 5, 0);
                    ivArrayDotsPager[i].setLayoutParams(params);
                    ivArrayDotsPager[i].setImageResource(R.drawable.slider_inactive);

                    ivArrayDotsPager[i].setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            view.setAlpha(1);
                        }
                    });
                    holder.ll_dots.addView(ivArrayDotsPager[i]);
                    holder.ll_dots.bringToFront();
                }


                ivArrayDotsPager[0].setImageResource(R.drawable.slider_active);

                holder.imgHolder.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
                    @Override
                    public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

                    }

                    @Override
                    public void onPageSelected(int position) {

                        for (int i = 0; i < ivArrayDotsPager.length; i++) {
                            ivArrayDotsPager[i].setImageResource(R.drawable.slider_inactive);
                        }
                        ivArrayDotsPager[position].setImageResource(R.drawable.slider_active);
                    }

                    @Override
                    public void onPageScrollStateChanged(int state) {

                    }
                });
            } else
                holder.ll_dots.setVisibility(View.GONE);

        } else {
            holder.imgHolder.setVisibility(View.GONE);
        }


    }


    @Override
    public int getItemCount() {
        return spaObj.size();
    }


}
