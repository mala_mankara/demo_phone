package com.arowana.houdini.byod.view.adapters.offers;

import android.content.Context;
import android.text.Html;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.arowana.houdini.byod.R;
import com.arowana.houdini.byod.model.offers.OfferDetails;
import com.arowana.houdini.byod.utils.ExpandableTextView;
import com.arowana.houdini.byod.utils.FactoryUtil;

import java.util.Date;

import io.realm.RealmList;

public class OfferDetailsAdapter extends RecyclerView.Adapter <OfferDetailsAdapter.DetailsViewHolder>{
    private Context mContext;
    private RealmList<OfferDetails> dataObj;


    public OfferDetailsAdapter(Context context, RealmList<OfferDetails> detailsObj) {
        mContext = context;
        dataObj  = detailsObj;
    }

    @NonNull
    @Override
    public DetailsViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.offer_details, parent, false);
        return new OfferDetailsAdapter.DetailsViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull final DetailsViewHolder holder, int position) {
        OfferDetails rowData = dataObj.get(position);
        if (rowData != null) {
            if (rowData.getImages() == null || rowData.getImages().isEmpty()) {
                holder.imageHolder.setVisibility(View.GONE);

            } else {
                holder.imageHolder.setVisibility(View.VISIBLE);
                String image = rowData.getImages().first().getImageUrl();
                Log.d("Offer Details","the images are.."+image);
               // ArrayList<String> imageList = new ArrayList<>(rowData.getImages().size());
               // for (int i = 0; i < rowData.getImages().size(); i++)
                //    imageList.add(Objects.requireNonNull(rowData.getImages().get(i)).getImageUrl());
               // holder.imageHolder.setAdapter(new MultiImageAdapter(mContext, imageList));
                FactoryUtil.loadImage(mContext, image, holder.imageHolder);
            }
            holder.offerType.setText(rowData.getName());
            holder.offerDescription.setText(rowData.getDescription());
            Date fromDate = FactoryUtil.getDate(rowData.getFromDate());
            Date toDate = FactoryUtil.getDate(rowData.getToDate());
            String duration = "Offer valid from " + "<b>" + FactoryUtil.getCurrentDateInSpecificFormat(fromDate)
                    + "</b>" + " to " + "<b>" + FactoryUtil.getCurrentDateInSpecificFormat(toDate) + "</b>";
            holder.offervalidTo.setText(Html.fromHtml(duration));

            holder.offerDescription.post(new Runnable() {
                @Override
                public void run() {
                    // Perform any actions you want based on the line count here.
                    int count = holder.offerDescription.getLineCount();
                    Log.d("Offer Details","count........"+count);
                    holder.expandBtnTxt.setVisibility(count < 3 ? View.GONE : View.VISIBLE);
                }
            });
            holder.expandBtnTxt.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    holder.offerDescription.toggle();
                    holder.expandBtnTxt.setText(holder.offerDescription.isExpanded() ? "Read more" : "Read less");
                }
            });
        }
    }

    @Override
    public int getItemCount() {
        return dataObj.size();
    }

    class DetailsViewHolder extends RecyclerView.ViewHolder {

        TextView offerType;
        TextView offervalidTo;
       // ViewPager imageHolder;
        ImageView imageHolder;
        private ExpandableTextView offerDescription;
        private TextView expandBtnTxt;

        DetailsViewHolder(View itemView) {
            super(itemView);
            offerType         = itemView.findViewById(R.id.offer_title);
            offerDescription  = itemView.findViewById(R.id.detail_expand_txt);
            expandBtnTxt      = itemView.findViewById(R.id.offer_desc_read_more);
            offervalidTo      = itemView.findViewById(R.id.offer_duration_txt);
            imageHolder       = itemView.findViewById(R.id.imageHolder);
        }
    }
}
