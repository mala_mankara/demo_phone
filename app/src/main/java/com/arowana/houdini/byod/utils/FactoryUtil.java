package com.arowana.houdini.byod.utils;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.util.Log;
import android.util.Patterns;
import android.widget.ImageView;

import com.arowana.houdini.byod.R;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.RequestOptions;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

@SuppressLint("SimpleDateFormat")
public class FactoryUtil {



    public static void loadImage(Context context, String iconImage, ImageView thumbnail) {

        Glide.with(context)
                .load(iconImage)
                .apply(new RequestOptions()
                        .placeholder(R.drawable.place_holder_small_screen)
                        .diskCacheStrategy(DiskCacheStrategy.ALL))
                .into(thumbnail);


    }






    public static String DayConverter(String timeData) {
        try {
            String[] separated = timeData.split("T");
                    /*separated[0];
                    separated[1];*/
            return  getDay(separated[0]);
        }catch (ParseException e) {
            e.printStackTrace();
        }
        return null;
    }

    private static  String  getDay(String date) throws ParseException {
        // SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
        Date dt = format.parse(date);
        return  (String) android.text.format.DateFormat.format("EEE", dt);
    }

    public static String getCurrentDateInSpecificFormat(Date currentCalDate) {
        Calendar cal = Calendar.getInstance();
        cal.setTime(currentCalDate);
        return getCurrentDateInSpecificFormat(cal);
    }

    private static String getCurrentDateInSpecificFormat(Calendar currentCalDate) {

        DateFormat dateFormat = new SimpleDateFormat("d MMM yyyy");
        return dateFormat.format(currentCalDate.getTime());
    }

    public static Date getDate(String str) {
        Date date = null;
        try {
            DateFormat df = new  SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'");
            date = df.parse(str);

        } catch (Exception e) {
            e.printStackTrace();
        }
        return date;
    }

    public static boolean isValidURL(String url) {
        if(!((url == null)&& url.isEmpty()))
            return Patterns.WEB_URL.matcher(url).matches();
        else
            return false;
    }


    public static void openWhatsApp(Context context, String Number) {
        PackageManager pm = context.getPackageManager();
        //String toNumber = Number./*replace("+", "").*/replace(" ", "");
        //String toNumber = "+971508119041";
        String toNumber = Number;
        Log.i("openWhatsApp","whatsApp.."+toNumber);
        try {
            PackageInfo info = pm.getPackageInfo("com.whatsapp", PackageManager.GET_META_DATA);
            String url = "https://api.whatsapp.com/send?phone="+toNumber;
            Intent sendIntent = new Intent(Intent.ACTION_VIEW);
            sendIntent.setData(Uri.parse(url));
            sendIntent.setPackage("com.whatsapp");
            context.startActivity(sendIntent);
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
            ViewUtils.showSnackBarMessage("Feature unavailable. WhatsApp needs to be installed");
        }
//        PackageManager pm = context.getPackageManager();
//       // String toNumber = Number.replace("+", "").replace(" ", "");
//        String toNumber = "+971508119041";
//
//        try {
//            PackageInfo info = pm.getPackageInfo("com.whatsapp", PackageManager.GET_META_DATA);
//            Intent sendIntent = new Intent(Intent.ACTION_SENDTO,Uri.parse("smsto:" + "" + toNumber));
//            sendIntent.setPackage("com.whatsapp");
//            context.startActivity(sendIntent);
//        } catch (PackageManager.NameNotFoundException e) {
//            ViewUtils.showSnackBarMessage("Feature unavailable. WhatsApp needs to be installed");
//        }
    }

    public static void openUrlinBrowser(String link, Context context){

        Uri webpage = Uri.parse(link);
        if (!link.startsWith("http://") && !link.startsWith("https://")) {
            webpage = Uri.parse("http://" + link);
        }
        Intent intent = new Intent(Intent.ACTION_VIEW, webpage);
        if (intent.resolveActivity(context.getPackageManager()) != null) {
            context.startActivity(intent);
        }
    }
}
