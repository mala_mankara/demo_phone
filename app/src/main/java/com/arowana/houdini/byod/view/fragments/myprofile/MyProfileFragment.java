package com.arowana.houdini.byod.view.fragments.myprofile;


import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.arowana.houdini.byod.Consts;
import com.arowana.houdini.byod.R;
import com.arowana.houdini.byod.model.login.LoginConfigObject;
import com.arowana.houdini.byod.model.login.LoginResponseDataObject;
import com.arowana.houdini.byod.utils.RealmUtil;
import com.arowana.houdini.byod.view.fragments.BaseFragment;

import io.realm.Realm;

import static com.arowana.houdini.byod.view.activity.MyProfileActivity.fragmentHolder;


public class MyProfileFragment extends BaseFragment implements View.OnClickListener {


    public MyProfileFragment() {

    }

    private View mRootView;
    private TextView mUsername, mMembershipId, mUsernameEmail, mPassword, mEditPassword;
    private LinearLayout mEmailLayout, mNameAddressLayout, mPhoneNumberLayout;
    private ImageView mBtnAddEmail, mBtnAddAddress, mBtnAddPhoneno;
    private LoginResponseDataObject mProfileObject;
    private ProfileContainerFragment fragment;
    private Realm mRealm;


    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        mRootView = view;
        setTitle(getString(R.string.my_profile));
        LoginConfigObject results = RealmUtil.getInstance().where(LoginConfigObject.class).equalTo("RealmId", "1").findFirst();
        mProfileObject = results.getResponseData();
        mRealm = RealmUtil.getInstance();
        initComponents();
        initListeners();
        populateData();
    }

    public void initComponents() {
        mUsername = mRootView.findViewById(R.id.user_name);
        mMembershipId = mRootView.findViewById(R.id.membership_id);

        mUsernameEmail = mRootView.findViewById(R.id.pi_username);
        mPassword = mRootView.findViewById(R.id.pi_password);
        mEditPassword = mRootView.findViewById(R.id.pi_edit_password);

        mEmailLayout = mRootView.findViewById(R.id.layout_email_id);
        mBtnAddEmail = mRootView.findViewById(R.id.pi_add_mail_id);

        mBtnAddAddress = mRootView.findViewById(R.id.pi_add_name_address);
        mNameAddressLayout = mRootView.findViewById(R.id.layout_name_address);

        mBtnAddPhoneno = mRootView.findViewById(R.id.pi_add_phone_number);
        mPhoneNumberLayout = mRootView.findViewById(R.id.layout_phone_number);


    }

    public void initListeners() {
        mEditPassword.setOnClickListener(this);
        mBtnAddEmail.setOnClickListener(this);
        mBtnAddAddress.setOnClickListener(this);
        mBtnAddPhoneno.setOnClickListener(this);
    }

    public void populateData() {
        mUsername.setText(RealmUtil.getFullName());
        mMembershipId.setText(RealmUtil.getMembershipId());

        populateEmail();
        populateNameAddress();
        populatePhoneNumber();

        mUsernameEmail.setText(RealmUtil.getEmailId());
        if (mProfileObject.getLoginType().equalsIgnoreCase(Consts.LOGIN_TYPE_HOUDINI)) {
            mPassword.setVisibility(View.VISIBLE);
            mEditPassword.setVisibility(View.VISIBLE);
            mPassword.setText(mProfileObject.getPassword()==null ? "" : mProfileObject.getPassword());
        }

    }


    @Override
    public void onClick(View v) {

        int id = v.getId();
        fragment = new ProfileContainerFragment();
        switch (id) {


            case R.id.pi_edit_password:
                fragment.setObject(mProfileObject, mRealm, 2);
                mHandler.showFragment(fragmentHolder.getId(), fragment);

                break;

            case R.id.pi_add_mail_id:
                fragment.setObject(mProfileObject, mRealm, 3);
                mHandler.showFragment(fragmentHolder.getId(), fragment);
                break;

            case R.id.pi_add_name_address:
                fragment.setObject(mProfileObject, mRealm, 5);
                mHandler.showFragment(fragmentHolder.getId(), fragment);
                break;

            case R.id.pi_add_phone_number:
                fragment.setObject(mProfileObject, mRealm, 7);
                mHandler.showFragment(fragmentHolder.getId(), fragment);
                break;


        }

    }

    @Override
    protected int getFragmentLayout() {
        return R.layout.fragment_my_profile;
    }


    public void populateEmail() {
        if (mProfileObject.getEmailId() != null && !mProfileObject.getEmailId().isEmpty()) {
            View view = LayoutInflater.from(getContext()).inflate(R.layout.pi_email_component, null);
            final TextView text = view.findViewById(R.id.pi_mail_id);
            TextView edit = view.findViewById(R.id.pi_edit_mail_id);
            TextView delete = view.findViewById(R.id.pi_delete_mail_id);
            edit.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    fragment = new ProfileContainerFragment();
                    fragment.setObject(mProfileObject, mRealm, 4);
                    mHandler.showFragment(fragmentHolder.getId(), fragment);
                }
            });
            delete.setVisibility(View.GONE);
            mBtnAddEmail.setVisibility(View.GONE);
            if (mProfileObject.getUserId() != null && mProfileObject.getUserId().equalsIgnoreCase(mProfileObject.getEmailId())) {
                edit.setVisibility(View.GONE);
            }

            text.setText(mProfileObject.getEmailId());
            mEmailLayout.addView(view);
        }
    }


    public void populateNameAddress() {

        if (mProfileObject.getAddress() == null)
            mBtnAddAddress.setVisibility(View.VISIBLE);

        else {

            View view = LayoutInflater.from(getContext()).inflate(R.layout.pi_name_address_component, null);
            final TextView address = view.findViewById(R.id.pi_address);
            TextView edit = view.findViewById(R.id.pi_edit_name_address);
            TextView delete = view.findViewById(R.id.pi_delete_name_address);

            edit.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    fragment = new ProfileContainerFragment();
                    fragment.setObject(mProfileObject, mRealm, 6);
                    mHandler.showFragment(fragmentHolder.getId(), fragment);
                }
            });

            //Populating address
            int addressLine = 0;
            address.setSingleLine(false);
            String guestAddress = "";
            if (mProfileObject.getAddress() != null && !mProfileObject.getAddress().isEmpty()) {
                addressLine++;
                guestAddress = mProfileObject.getAddress();

            }
            if (mProfileObject.getCity() != null && !mProfileObject.getCity().isEmpty()) {
                if (addressLine > 0) {
                    guestAddress += "\n";
                }
                addressLine++;
                guestAddress += mProfileObject.getCity();
                if (mProfileObject.getPinCode() != null && !mProfileObject.getPinCode().isEmpty()) {
                    guestAddress += "-" + mProfileObject.getPinCode();
                }

            }
            if (mProfileObject.getCountry() != null && !mProfileObject.getCountry().isEmpty()) {
                if (addressLine > 0) {
                    guestAddress += "\n";
                }
                addressLine++;
                guestAddress += mProfileObject.getCountry();

            }

            address.setText(guestAddress);

            mNameAddressLayout.addView(view);
            delete.setVisibility(View.GONE);
            mBtnAddAddress.setVisibility(View.GONE);
        }

    }


    public void populatePhoneNumber() {

        if (mProfileObject.getMobileNumber() != null && !mProfileObject.getMobileNumber().isEmpty()) {

            View view = LayoutInflater.from(getContext()).inflate(R.layout.pi_phone_component, null);
            final TextView number = view.findViewById(R.id.pi_phone_number);
            final TextView type = view.findViewById(R.id.pi_phone_number_type);
            TextView edit = view.findViewById(R.id.pi_edit_phone_number);
            TextView delete = view.findViewById(R.id.pi_delete_phone_number);


            edit.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    fragment = new ProfileContainerFragment();
                    fragment.setObject(mProfileObject, mRealm, 8);
                    mHandler.showFragment(fragmentHolder.getId(), fragment);
                }
            });
            if (mProfileObject.getUserId() != null && mProfileObject.getUserId().equalsIgnoreCase(mProfileObject.getMobileNumber())) {
                edit.setVisibility(View.GONE);
            }
            number.setText("+" + mProfileObject.getCountryCode() + "  " + mProfileObject.getMobileNumber());
            delete.setVisibility(View.GONE);
            type.setVisibility(View.GONE);
            mBtnAddPhoneno.setVisibility(View.GONE);

            mPhoneNumberLayout.addView(view);
        }
    }


}
