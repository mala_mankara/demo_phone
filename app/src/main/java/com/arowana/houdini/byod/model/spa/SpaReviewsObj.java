package com.arowana.houdini.byod.model.spa;

import io.realm.RealmObject;

public class SpaReviewsObj extends RealmObject {

    private String Name;

    private String Url;

    public String getName() {
        return Name;
    }

    public void setName(String name) {
        Name = name;
    }

    public String getUrl() {
        return Url;
    }

    public void setUrl(String url) {
        Url = url;
    }
}
