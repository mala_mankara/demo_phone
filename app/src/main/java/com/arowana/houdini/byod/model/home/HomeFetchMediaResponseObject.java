package com.arowana.houdini.byod.model.home;

import java.util.List;

import io.realm.RealmList;

public class HomeFetchMediaResponseObject {

    private ResponseStatus ResponseStatus;
    private RealmList<ResponseData> ResponseData;
     private String RequestData;

    public HomeFetchMediaResponseObject.ResponseStatus getResponseStatus() {
        return ResponseStatus;
    }

    public void setResponseStatus(HomeFetchMediaResponseObject.ResponseStatus responseStatus) {
        ResponseStatus = responseStatus;
    }

    public RealmList<HomeFetchMediaResponseObject.ResponseData> getResponseData() {
        return ResponseData;
    }

    public void setResponseData(RealmList<HomeFetchMediaResponseObject.ResponseData> responseData) {
        ResponseData = responseData;
    }

    public static class ResponseStatus {
        private String ResponseFlag;

        private String ResponseCode;

        private String ResponseMessage;

        private String ResponseId;

        public String getResponseFlag() {
            return ResponseFlag;
        }

        public void setResponseFlag(String responseFlag) {
            ResponseFlag = responseFlag;
        }

        public String getResponseCode() {
            return ResponseCode;
        }

        public void setResponseCode(String responseCode) {
            ResponseCode = responseCode;
        }

        public String getResponseMessage() {
            return ResponseMessage;
        }

        public void setResponseMessage(String responseMessage) {
            ResponseMessage = responseMessage;
        }

        public String getResponseId() {
            return ResponseId;
        }

        public void setResponseId(String responseId) {
            ResponseId = responseId;
        }
    }

    public static class ResponseData{

        private String Key;

        private String ActivityId;

        private String HotelId;

        private String ModifiedBy;

        private String VideoUrl;

        private List<ImagesObject> Images;

        private String BrandCode;

        private String Module;

        private String GroupId;

        private String BrandId;

        private String ModifiedDate;

        private String HotelCode;

        private String GroupCode;

        private String CreatedDate;

        private String CreatedBy;

        private String Id;

        private String MessageCode;

        private String Version;

        private String Tenant;




        public String getKey() {
            return Key;
        }

        public void setKey(String key) {
            Key = key;
        }

        public String getActivityId() {
            return ActivityId;
        }

        public void setActivityId(String activityId) {
            ActivityId = activityId;
        }

        public String getHotelId() {
            return HotelId;
        }

        public void setHotelId(String hotelId) {
            HotelId = hotelId;
        }

        public String getModifiedBy() {
            return ModifiedBy;
        }

        public void setModifiedBy(String modifiedBy) {
            ModifiedBy = modifiedBy;
        }

        public String getVideoUrl() {
            return VideoUrl;
        }

        public void setVideoUrl(String videoUrl) {
            VideoUrl = videoUrl;
        }

        public List<ImagesObject> getImages() {
            return Images;
        }

        public void setImages(List<ImagesObject> images) {
            Images = images;
        }

        public String getBrandCode() {
            return BrandCode;
        }

        public void setBrandCode(String brandCode) {
            BrandCode = brandCode;
        }

        public String getModule() {
            return Module;
        }

        public void setModule(String module) {
            Module = module;
        }

        public String getGroupId() {
            return GroupId;
        }

        public void setGroupId(String groupId) {
            GroupId = groupId;
        }

        public String getBrandId() {
            return BrandId;
        }

        public void setBrandId(String brandId) {
            BrandId = brandId;
        }

        public String getModifiedDate() {
            return ModifiedDate;
        }

        public void setModifiedDate(String modifiedDate) {
            ModifiedDate = modifiedDate;
        }

        public String getHotelCode() {
            return HotelCode;
        }

        public void setHotelCode(String hotelCode) {
            HotelCode = hotelCode;
        }

        public String getGroupCode() {
            return GroupCode;
        }

        public void setGroupCode(String groupCode) {
            GroupCode = groupCode;
        }

        public String getCreatedDate() {
            return CreatedDate;
        }

        public void setCreatedDate(String createdDate) {
            CreatedDate = createdDate;
        }

        public String getCreatedBy() {
            return CreatedBy;
        }

        public void setCreatedBy(String createdBy) {
            CreatedBy = createdBy;
        }

        public String getId() {
            return Id;
        }

        public void setId(String id) {
            Id = id;
        }

        public String getMessageCode() {
            return MessageCode;
        }

        public void setMessageCode(String messageCode) {
            MessageCode = messageCode;
        }

        public String getVersion() {
            return Version;
        }

        public void setVersion(String version) {
            Version = version;
        }

        public String getTenant() {
            return Tenant;
        }

        public void setTenant(String tenant) {
            Tenant = tenant;
        }

    }


}
