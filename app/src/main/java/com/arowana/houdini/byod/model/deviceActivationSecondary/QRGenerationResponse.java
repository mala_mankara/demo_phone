package com.arowana.houdini.byod.model.deviceActivationSecondary;

import com.arowana.houdini.byod.utils.ApiResponseStatus;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;


public class QRGenerationResponse extends RealmObject {
    @PrimaryKey
    private String RealmId = "1";
    private ApiResponseStatus ResponseStatus;
    private QRGenerateData ResponseData;


    public String getRealmId() {
        return RealmId;
    }

    public void setRealmId(String realmId) {
        RealmId = realmId;
    }

    public ApiResponseStatus getResponseStatus() {
        return ResponseStatus;
    }

    public void setResponseStatus(ApiResponseStatus responseStatus) {
        ResponseStatus = responseStatus;
    }

    public QRGenerateData getResponseData() {
        return ResponseData;
    }

    public void setResponseData(QRGenerateData responseData) {
        ResponseData = responseData;
    }
}
