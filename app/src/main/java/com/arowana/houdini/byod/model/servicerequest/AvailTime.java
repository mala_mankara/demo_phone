package com.arowana.houdini.byod.model.servicerequest;

import io.realm.RealmObject;

public class AvailTime extends RealmObject {

    private String Time;

    private boolean isSelected;

    public String getTime ()
    {
        return Time;
    }

    public void setTime (String Time)
    {
        this.Time = Time;
    }


    public boolean isSelected() {
        return isSelected;
    }

    public void setSelected(boolean selected) {
        isSelected = selected;
    }
}
