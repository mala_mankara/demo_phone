package com.arowana.houdini.byod.model.inroomdining;

import io.realm.RealmObject;

public class IRDiningHotelPropertyObj extends RealmObject{

    private String HotelCode;

    private String GroupCode;

    private String BrandCode;

    public String getHotelCode() {
        return HotelCode;
    }

    public void setHotelCode(String hotelCode) {
        HotelCode = hotelCode;
    }

    public String getGroupCode() {
        return GroupCode;
    }

    public void setGroupCode(String groupCode) {
        GroupCode = groupCode;
    }

    public String getBrandCode() {
        return BrandCode;
    }

    public void setBrandCode(String brandCode) {
        BrandCode = brandCode;
    }
}
