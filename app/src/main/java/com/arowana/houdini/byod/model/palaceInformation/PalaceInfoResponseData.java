package com.arowana.houdini.byod.model.palaceInformation;

import io.realm.RealmList;
import io.realm.RealmObject;

public class PalaceInfoResponseData extends RealmObject{
    private String Name;

    private String GroupCode;

    private String GroupId;

    private RealmList<PalaceInfoBrands> Brands;

    public String getName ()
    {
        return Name;
    }

    public void setName (String Name)
    {
        this.Name = Name;
    }

    public String getGroupCode ()
    {
        return GroupCode;
    }

    public void setGroupCode (String GroupCode)
    {
        this.GroupCode = GroupCode;
    }

    public String getGroupId ()
    {
        return GroupId;
    }

    public void setGroupId (String GroupId)
    {
        this.GroupId = GroupId;
    }

    public RealmList<PalaceInfoBrands> getBrands ()
    {
        return Brands;
    }

    public void setBrands (RealmList<PalaceInfoBrands> Brands)
    {
        this.Brands = Brands;
    }

    @Override
    public String toString()
    {
        return "ClassPojo [Name = "+Name+", GroupCode = "+GroupCode+", GroupId = "+GroupId+", Brands = "+Brands+"]";
    }
}
