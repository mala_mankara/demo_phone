package com.arowana.houdini.byod.view.fragments.spa;


import android.app.ProgressDialog;
import android.os.Bundle;
import android.text.InputType;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.arowana.houdini.byod.Consts;
import com.arowana.houdini.byod.MainApplication;
import com.arowana.houdini.byod.R;
import com.arowana.houdini.byod.interfaces.ApiClient;
import com.arowana.houdini.byod.interfaces.api_interface.SpaApiInterface;
import com.arowana.houdini.byod.model.spa.SpaAvailabilityTimeObj;
import com.arowana.houdini.byod.model.spa.SpaBookingConfigObject;
import com.arowana.houdini.byod.model.spa.SpaBookingObject;
import com.arowana.houdini.byod.model.spa.SpaConfigObject;
import com.arowana.houdini.byod.model.spa.SpaObject;
import com.arowana.houdini.byod.model.spa.SpaTreatmentObj;
import com.arowana.houdini.byod.model.spa.SpaTreatmentTypeObj;
import com.arowana.houdini.byod.utils.DateUtil;
import com.arowana.houdini.byod.utils.NetworkUtils;
import com.arowana.houdini.byod.utils.RealmUtil;
import com.arowana.houdini.byod.utils.ViewUtils;
import com.arowana.houdini.byod.view.adapters.ICart;
import com.arowana.houdini.byod.view.adapters.spa.SpaCartAdapter;
import com.arowana.houdini.byod.view.fragments.BaseFragment;
import com.arowana.houdini.byod.view.fragments.telephone.PhoneCallFragment;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;

import alcatel.model.ByodManager;
import io.realm.Realm;
import io.realm.RealmResults;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.arowana.houdini.byod.view.activity.SpaActivity.fragmentHolder;

/**
 * A simple {@link Fragment} subclass.
 */
public class SpaCartFragment extends BaseFragment implements ICart {


    private View mRootView;
    private RecyclerView mRecyclerView;
    TextView mTotal;
    RelativeLayout mBtnSubmit;
    private ProgressDialog mDialog;
    private Realm realm;
    private View call_conceirge;
    private String speedDialNo;
    private EditText mSpecialInstructions;
    private SpaAvailabilityTimeObj spaTimeAvailabilityObj;
    private String hotelTimeZone;

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        mRootView = view;
        setTitle(getString(R.string.cart));
        realm = RealmUtil.getInstance();
        initDialog();
        initComponents();

        populateData();
    }

    private void initDialog() {
        mDialog = new ProgressDialog(getContext());
        mDialog.setMessage(getString(R.string.dialog_message));
        mDialog.setCancelable(false);
    }


    public void initComponents() {

        mRecyclerView = mRootView.findViewById(R.id.spa_cart_recyclerview);
        mTotal = mRootView.findViewById(R.id.spa_cart_total_price);
        mBtnSubmit = mRootView.findViewById(R.id.spa_cart_btn_submit);
        mSpecialInstructions = mRootView.findViewById(R.id.special_instructions);
        call_conceirge = mRootView.findViewById(R.id.btn_spa_conceirge);
        mTotal.setText(RealmUtil.getSpaCurrency() + " " + RealmUtil.getSpaTotalPrice());

        //mSpecialInstructions.setMaxLines(Integer.MAX_VALUE);
        mSpecialInstructions.setHorizontallyScrolling(false);
        mSpecialInstructions.setMaxLines(3);
        mSpecialInstructions.setImeOptions(EditorInfo.IME_ACTION_DONE);
        mSpecialInstructions.setRawInputType(InputType.TYPE_CLASS_TEXT);



        mBtnSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                RealmResults<SpaObject> spaObjList = realm.where(SpaObject.class).equalTo("TreatmentType.Treatment.isSelected", true).findAll();
                if(spaObjList.size()>0)
                   populateSnackbar();
                else {
                    Toast tost = Toast.makeText(getActivity(), "Please add packages to cart .", Toast.LENGTH_SHORT);
                    tost.setGravity(Gravity.BOTTOM,0,0);
                    tost.show();

                }
            }
        });

        if(MainApplication.isContainedSpeedDial("spa") && Consts.APP_CURRENT_STATE > 1){
            call_conceirge.setVisibility(View.VISIBLE);
            speedDialNo = MainApplication.getValueforKey();
            //speedDialData=EmiratesPalaceApplication.getSpeedDialValue();
        }else
            call_conceirge.setVisibility(View.GONE);

        call_conceirge.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(!ByodManager.getInstance().isSipConnected()) {
                    Toast.makeText(getActivity(),"Call service not working. Please try later..",Toast.LENGTH_SHORT).show();
                }else {

                    if (!speedDialNo.isEmpty()) {
                        PhoneCallFragment fragment = new PhoneCallFragment();
                        Bundle bundle = new Bundle();
                        bundle.putString("phoneNo", speedDialNo);
                        bundle.putString("name", "spa");
                        bundle.putBoolean("isMakingCall",true);
                        fragment.setArguments(bundle);
                        mHandler.showFragment(fragmentHolder.getId(), fragment);
                    }
                }
            }
        });


    }

    public void populateData() {

        LinearLayoutManager linearVertical = new LinearLayoutManager(getContext(), RecyclerView.VERTICAL, false);
        mRecyclerView.setLayoutManager(linearVertical);

        SpaCartAdapter adapter = new SpaCartAdapter(getContext(), this, mHandler, fragmentHolder,spaTimeAvailabilityObj,hotelTimeZone);
        mRecyclerView.setAdapter(adapter);


    }

    @Override
    public void updateCartInfo() {
        mTotal.setText(RealmUtil.getSpaCurrency() + " " + RealmUtil.getSpaTotalPrice());
    }

    @Override
    protected int getFragmentLayout() {
        return R.layout.fragment_spa_cart;
    }


    public void createSubmitJson() {
        try {
            RealmResults<SpaObject> spaObjList = realm.where(SpaObject.class).equalTo("TreatmentType.Treatment.isSelected", true).findAll();


            JsonObject submitJson = new JsonObject();

            submitJson.addProperty("RoomNo", RealmUtil.getRoomNo());
            submitJson.addProperty("BookingId", RealmUtil.getBookingId());
            submitJson.addProperty("AdditionalNote", "" + mSpecialInstructions.getText().toString());


            JsonArray treatmentTypeArray = new JsonArray();
            //for (int i=0;i<spaObjList.size();i++) {
//                if(spaObjList.first().isValid()){
//                 SpaObject spaObj=spaObjList.first();
           for (SpaObject spaObj : spaObjList) {
                submitJson.addProperty("SpaName", spaObj.getSpaName());
                submitJson.addProperty("SpaId", spaObj.getId());
                RealmResults<SpaTreatmentTypeObj> treatmentTypeResults = spaObj.getTreatmentType().where().equalTo("Treatment.isSelected", true).findAll();
                for (SpaTreatmentTypeObj treatmentTypeBean : treatmentTypeResults) {
                    JsonObject treatmentTypeObject = new JsonObject();
                    treatmentTypeObject.addProperty("TreatmentTypeId", treatmentTypeBean.getTypeId());
                    treatmentTypeObject.addProperty("TreatmentType", treatmentTypeBean.getTypeName());
                    RealmResults<SpaTreatmentObj> treatmentResults = treatmentTypeBean.getTreatment().where().equalTo("isSelected", true).findAll();

                    JsonArray treatmentArray = new JsonArray();
                    for (SpaTreatmentObj obj : treatmentResults) {
                        JsonObject treatmentObject = new JsonObject();
                        treatmentObject.addProperty("TreatmentId", obj.getTreatmentId());
                        treatmentObject.addProperty("TreatmentName", obj.getTreatmentName());
                        treatmentObject.addProperty("Time", obj.getBookingTimeAndPrice().getTime());
                        treatmentObject.addProperty("Price", obj.getBookingTimeAndPrice().getPrice());
                        treatmentObject.addProperty("Currency", RealmUtil.getSpaCurrency());
                        treatmentObject.addProperty("Description", obj.getDescription());
                        treatmentObject.addProperty("RequestDate", DateUtil.getUTCDatetime(obj.getBookingDate(), obj.getBookingTime()));
                        treatmentObject.addProperty("RequestTime", DateUtil.getUTCTime(obj.getBookingDate(), obj.getBookingTime()));
                        treatmentObject.addProperty("CouplesTreatment", Boolean.valueOf(obj.getCouplesTreatment()));
                        treatmentObject.addProperty("CouplesCount", String.valueOf(obj.getCoupleCount()));
                        treatmentObject.addProperty("MalePax", String.valueOf(obj.getMalePaxCount()));
                        treatmentObject.addProperty("FemalePax", String.valueOf(obj.getFemalePaxCount()));
                        treatmentArray.add(treatmentObject);
                    }
                    treatmentTypeObject.add("Treatment", treatmentArray);
                    treatmentTypeArray.add(treatmentTypeObject);

                }
            }
            submitJson.add("TreatmentType", treatmentTypeArray);

            JsonObject guestProfileObject = new JsonObject();
            guestProfileObject.addProperty("MembershipId", RealmUtil.getMembershipId());
            guestProfileObject.addProperty("FirstName", "" + RealmUtil.getUserName());
            submitJson.add("GuestProfile", guestProfileObject);

            JsonObject hotelPropertyObject = new JsonObject();
            hotelPropertyObject.addProperty("GroupCode", "");
            hotelPropertyObject.addProperty("BrandCode", "");
            hotelPropertyObject.addProperty("HotelCode", "");
            submitJson.add("HotelProperty", hotelPropertyObject);

            JsonObject deviceObject = new JsonObject();
            deviceObject.addProperty("DeviceType", "");
            deviceObject.addProperty("MacId", "");
            submitJson.add("Device", deviceObject);

            Log.d("spaJson", "spaJson is************" + submitJson);


            if (NetworkUtils.checkConnectivityStatus(getContext())) {
                mDialog.show();
                callSubmitAPI(submitJson);

            }

        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    public void callSubmitAPI(JsonObject json) {
        Call<SpaBookingConfigObject> responseObj;


        responseObj = ApiClient.getApiClient(SpaApiInterface.class).submitSpa(json);

        responseObj.enqueue(new Callback<SpaBookingConfigObject>() {
            @Override
            public void onResponse(Call<SpaBookingConfigObject> call, Response<SpaBookingConfigObject> response) {
                if (mDialog.isShowing())
                    mDialog.dismiss();

                String flag = response.body().getResponseStatus().getResponseFlag();
                final String message = response.body().getResponseStatus().getResponseMessage();
                if (flag.equals("SUCCESS")) {
                    SpaBookingObject obj = response.body().getResponseData();

                    SpaBookingDetailsFragment fragment = new SpaBookingDetailsFragment();
                    fragment.setBookingObj(obj);
                    mHandler.showFragment(fragmentHolder.getId(), fragment);
                   // ViewUtils.showSnackBarMessage(message);

                    realm.executeTransaction(new Realm.Transaction() {
                        @Override
                        public void execute(Realm realm) {
                            RealmResults<SpaConfigObject> result = realm.where(SpaConfigObject.class).findAll();
                            result.deleteAllFromRealm();
                        }
                    });


                } else ViewUtils.showSnackBarMessage(message);
            }

            @Override
            public void onFailure(Call<SpaBookingConfigObject> call, Throwable t) {

            }
        });

    }


    public void populateSnackbar() {
        final FrameLayout layout = getActivity().findViewById(R.id.snacbar_layout);
        layout.setVisibility(View.VISIBLE);
        View view = getActivity().getLayoutInflater().inflate(R.layout.custom_confirm_snackbar_layout, null);
        TextView mOkMessage = view.findViewById(R.id.snack_ok_message);
        TextView mCancelMessage = view.findViewById(R.id.snack_cancel_message);

        mOkMessage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                createSubmitJson();
                layout.setVisibility(View.GONE);
                layout.removeAllViews();

            }
        });

        mCancelMessage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                layout.setVisibility(View.GONE);
                layout.removeAllViews();

            }
        });

        layout.addView(view);
    }

    public void setBookingTimeDetails(SpaAvailabilityTimeObj mTimeAvailabilityObj, String spaHotelTimeZone) {
        spaTimeAvailabilityObj = mTimeAvailabilityObj;
        hotelTimeZone=spaHotelTimeZone;
    }
}
