package com.arowana.houdini.byod.model.facilitiesAndServices;

import io.realm.RealmList;
import io.realm.RealmObject;

public class FacilityServiceConfigObj extends RealmObject {

    private String RequestData;

    private String IsReadOnly;

    private FSResponseStatus ResponseStatus;

    private RealmList<FServiceResponseData> ResponseData;

    public String getRequestData ()
    {
        return RequestData;
    }

    public void setRequestData (String RequestData)
    {
        this.RequestData = RequestData;
    }

    public String getIsReadOnly ()
    {
        return IsReadOnly;
    }

    public void setIsReadOnly (String IsReadOnly)
    {
        this.IsReadOnly = IsReadOnly;
    }

    public FSResponseStatus getResponseStatus ()
    {
        return ResponseStatus;
    }

    public void setResponseStatus (FSResponseStatus ResponseStatus)
    {
        this.ResponseStatus = ResponseStatus;
    }

    public RealmList<FServiceResponseData> getResponseData ()
    {
        return ResponseData;
    }

    public void setResponseData (RealmList<FServiceResponseData> ResponseData)
    {
        this.ResponseData = ResponseData;
    }

    @Override
    public String toString()
    {
        return "ClassPojo [RequestData = "+RequestData+", IsReadOnly = "+IsReadOnly+", ResponseStatus = "+ResponseStatus+", ResponseData = "+ResponseData+"]";
    }
}
/* "ResponseData": [
        {
            "Id": "5b22730ecda8715710914f9d",
            "Type": "Restaurant & Bar",
            "GroupId": "5ab103a423d1621460512e8e",
            "GroupCode": "KMP",
            "BrandId": "5ab1081a23d1621460512e8f",
            "BrandCode": "EP",
            "HotelId": "5ab1119223d1621460512e90",
            "HotelCode": "EMP",
            "Module": "restaurantBar",
            "Url": "",
            "Sequence": 1,
            "Images": [
                {
                    "ImageUrl": "http://demo.arowanaconsulting.com:8000//Image/AmenitiesType/AmenitiesType_31c051a2-0c44-4181-9d42-12244ebf5ecc750x434.jpg",
                    "ImageName": "AmenitiesType_31c051a2-0c44-4181-9d42-12244ebf5ecc750x434.jpg",
                    "ImageHeight": "434",
                    "ImageWidth": "750",
                    "ImageSize": "2x"
                }
            ],
            "Amenities": []
        },
        {
            "Id": "5b31dd34cda8703348685245",
            "Type": "agfag",
            "GroupId": "5ab103a423d1621460512e8e",
            "GroupCode": "KMP",
            "BrandId": "5ab1081a23d1621460512e8f",
            "BrandCode": "EP",
            "HotelId": "5ab1119223d1621460512e90",
            "HotelCode": "EMP",
            "Module": null,
            "Url": null,
            "Sequence": 1,
            "Images": [],
            "Amenities": []
        },
        {
            "Id": "5b31dd8bcda8703348685246",
            "Type": "agak",
            "GroupId": "5ab103a423d1621460512e8e",
            "GroupCode": "KMP",
            "BrandId": "5ab1081a23d1621460512e8f",
            "BrandCode": "EP",
            "HotelId": "5ab1119223d1621460512e90",
            "HotelCode": "EMP",
            "Module": null,
            "Url": "",
            "Sequence": 1,
            "Images": [],
            "Amenities": [
                {
                    "Id": "5b39f04ecda870149012b1e7",
                    "TypeId": "5b31dd8bcda8703348685246",
                    "Name": "test",
                    "Description": "test",
                    "Images": [
                        {
                            "ImageUrl": "http://demo.arowanaconsulting.com:8000//Image/Amenities/Amenities_524636_750x434.png",
                            "ImageName": "Amenities_524636_750x434.png",
                            "ImageHeight": "434",
                            "ImageWidth": "750",
                            "ImageSize": "2x"
                        }
                    ],
                    "Information": [
                        {
                            "Sequence": 1,
                            "Field": "test",
                            "Value": "test"
                        }
                    ]
                }
            ]
        },*/