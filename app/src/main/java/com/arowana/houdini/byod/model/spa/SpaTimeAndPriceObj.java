package com.arowana.houdini.byod.model.spa;

import io.realm.RealmObject;

public class SpaTimeAndPriceObj extends RealmObject {

    private String Time;

    private String Price;

    public String getTime ()
    {
        return Time;
    }

    public void setTime (String Time)
    {
        this.Time = Time;
    }

    public String getPrice ()
    {
        return Price;
    }

    public void setPrice (String Price)
    {
        this.Price = Price;
    }

    @Override
    public String toString()
    {
        return "ClassPojo [Time = "+Time+", Price = "+Price+"]";
    }
}
