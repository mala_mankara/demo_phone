package com.arowana.houdini.byod.model.palaceInformation;

import io.realm.RealmList;
import io.realm.RealmObject;

public class PalaceInfoBrands extends RealmObject {

    private String Name;

    private String BrandId;

    private String Description;

    private String GroupCode;

    private RealmList<PalaceInfoHotels> Hotels;

    private RealmList<PalaceInfoImages> Images;

    private String BrandCode;

    public String getName ()
    {
        return Name;
    }

    public void setName (String Name)
    {
        this.Name = Name;
    }

    public String getBrandId ()
    {
        return BrandId;
    }

    public void setBrandId (String BrandId)
    {
        this.BrandId = BrandId;
    }

    public String getDescription ()
    {
        return Description;
    }

    public void setDescription (String Description)
    {
        this.Description = Description;
    }

    public String getGroupCode ()
    {
        return GroupCode;
    }

    public void setGroupCode (String GroupCode)
    {
        this.GroupCode = GroupCode;
    }

    public RealmList<PalaceInfoHotels> getHotels ()
    {
        return Hotels;
    }

    public void setHotels (RealmList<PalaceInfoHotels> Hotels)
    {
        this.Hotels = Hotels;
    }

    public RealmList<PalaceInfoImages> getImages ()
    {
        return Images;
    }

    public void setImages (RealmList<PalaceInfoImages> Images)
    {
        this.Images = Images;
    }

    public String getBrandCode ()
    {
        return BrandCode;
    }

    public void setBrandCode (String BrandCode)
    {
        this.BrandCode = BrandCode;
    }

    @Override
    public String toString()
    {
        return "ClassPojo [Name = "+Name+", BrandId = "+BrandId+", Description = "+Description+", GroupCode = "+GroupCode+", Hotels = "+Hotels+", Images = "+Images+", BrandCode = "+BrandCode+"]";
    }
}
