package com.arowana.houdini.byod.view.fragments;

import android.Manifest;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.util.Log;
import android.util.SparseArray;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.view.View;
import android.widget.TextView;

import androidx.core.app.ActivityCompat;
import androidx.fragment.app.Fragment;

import com.arowana.houdini.byod.Consts;
import com.arowana.houdini.byod.R;
import com.arowana.houdini.byod.utils.DeviceOSFeatureUtils;
import com.arowana.houdini.byod.utils.NetworkUtils;
import com.arowana.houdini.byod.utils.RealmUtil;
import com.google.android.gms.vision.CameraSource;
import com.google.android.gms.vision.Detector;
import com.google.android.gms.vision.barcode.Barcode;
import com.google.android.gms.vision.barcode.BarcodeDetector;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;

/**
 * A simple {@link Fragment} subclass.
 */
public class DeviceActivationWithQR extends BaseFragment {

    View mRootView;
    private Context mContext;
    private TextView scanResult;
    TextView txtBarcodeValue;
    private Fragment mFragment;

    public DeviceActivationWithQR() { // Required empty public constructor
    }


    private SurfaceView surfaceView;
    private BarcodeDetector barcodeDetector;
    private CameraSource cameraSource;
    private static final int REQUEST_CAMERA_PERMISSION = 201;
    String intentData = "";

    @Override
    protected int getFragmentLayout() {
        return R.layout.fragment_deviceactivationwith_qr;
    }

    @Override
    public void onCreate( Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mContext = getActivity();
        setTitle(getResources().getString(R.string.device_activation_QR));
       /* if (ActivityCompat.checkSelfPermission(mContext, Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(getActivity(), new String[]{Manifest.permission.CAMERA},
                    100);
            return;
        }*/
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        mRootView = view;
        scanResult = mRootView.findViewById(R.id.scanResult);
        surfaceView = mRootView.findViewById(R.id.surfaceView);
        txtBarcodeValue = mRootView.findViewById(R.id.txtBarcodeValue);
        initialiseDetectorsAndSources();
    }

    private void createJSonObject(String intentData)  {

        try {
            JSONObject obj     = new JSONObject(intentData);
            String roomNo = obj.getString("RoomNumber");
            String primaryConnectionId = null;
            if (obj.has("MembershipId"))
                primaryConnectionId = obj.getString("MembershipId");

            if (NetworkUtils.checkConnectivityStatus(mContext)) {
                JSONObject jsonObject = new JSONObject();
                jsonObject.put("FirstName", RealmUtil.getUserName());
                jsonObject.put("LastName",RealmUtil.getLastName());
                jsonObject.put("RoomNo", roomNo);
                jsonObject.put("ActionType", Consts.DEVICE_ACTIVATIONTYPE_QR);
                jsonObject.put("MembershipId", RealmUtil.getMembershipId());
                jsonObject.put("MacId", DeviceOSFeatureUtils.getMacAddress());
                if (primaryConnectionId != null && !primaryConnectionId.isEmpty())
                    jsonObject.put("PrimaryConnectionId", primaryConnectionId);
                getTargetFragment().onActivityResult(getTargetRequestCode(),Activity.RESULT_OK,
                        new Intent().putExtra("objData", jsonObject.toString())
                );
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    private void initialiseDetectorsAndSources(){
        //Toast.makeText(mContext, "Barcode scanner started", Toast.LENGTH_SHORT).show();
        barcodeDetector = new BarcodeDetector.Builder(mContext)
                .setBarcodeFormats(Barcode.ALL_FORMATS)
                .build();
        cameraSource = new CameraSource.Builder(mContext, barcodeDetector)
                .setRequestedPreviewSize(1920, 1080)
                .setAutoFocusEnabled(true) //you should add this feature
                .build();
        surfaceView.getHolder().addCallback(new SurfaceHolder.Callback() {
            @Override
            public void surfaceCreated(SurfaceHolder holder) {
                try {
                    if (ActivityCompat.checkSelfPermission(mContext, Manifest.permission.CAMERA) == PackageManager.PERMISSION_GRANTED) {
                        cameraSource.start(surfaceView.getHolder());
                    } else {
                        ActivityCompat.requestPermissions(getActivity(), new
                                String[]{Manifest.permission.CAMERA}, REQUEST_CAMERA_PERMISSION);
                    }

                } catch (IOException e) {
                    e.printStackTrace();
                }


            }
            @Override
            public void surfaceChanged(SurfaceHolder holder, int format, int width, int height) {
            }
            @Override
            public void surfaceDestroyed(SurfaceHolder holder) {
                cameraSource.stop();
            }
        });

        barcodeDetector.setProcessor(new Detector.Processor<Barcode>() {
            @Override
            public void release() {
                //Toast.makeText(mContext, "To prevent memory leaks barcode scanner has been stopped", Toast.LENGTH_SHORT).show();
            }
            @Override
            public void receiveDetections(Detector.Detections<Barcode> detections) {
                final SparseArray<Barcode> barcodes = detections.getDetectedItems();
                if (barcodes.size() != 0) {

                    txtBarcodeValue.post(new Runnable() {
                        @Override
                        public void run() {
                            cameraSource.stop();

                            intentData = barcodes.valueAt(0).displayValue;
//                            scanResult.setText( intentData);
                            Log.d("QRData", "QR JSON:\n" + intentData);
                            createJSonObject(intentData);

                            //  cameraSource.release();
                            if (getFragmentManager() != null) {
                                if ( getFragmentManager().getBackStackEntryCount() > 0){
                                    getFragmentManager().popBackStack();
                                    return;
                                }
                            }
                            txtBarcodeValue.setText("Scanned");
                        }
                    });
                }
            }
        });
    }

    @Override
    public  void onPause() {
        super.onPause();
        cameraSource.release();
    }

    @Override
    public void onResume() {
        super.onResume();
        initialiseDetectorsAndSources();
    }
}
