package com.arowana.houdini.byod.model.deviceactivation_QR;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

public class QR_DeviceRegisterConfigObj extends RealmObject {

    @PrimaryKey
    private String RealmId = "1";

    private QR_RegisterResponseStatus ResponseStatus;

    private QR_RegisterResponseData ResponseData;

    public String getRealmId() {
        return RealmId;
    }

    public void setRealmId(String realmId) {
        RealmId = realmId;
    }

    public QR_RegisterResponseStatus getResponseStatus() {
        return ResponseStatus;
    }

    public void setResponseStatus(QR_RegisterResponseStatus responseStatus) {
        ResponseStatus = responseStatus;
    }

    public QR_RegisterResponseData getResponseData() {
        return ResponseData;
    }

    public void setResponseData(QR_RegisterResponseData responseData) {
        ResponseData = responseData;
    }
}

/*{
        "ResponseData": {
        "Id": null,
        "UniqueCode": null,
        "MacId": null,
        "PrimaryConnectionId": null,
        "MembershipId": "EP0017201886757",
        "RoomNo": "103",
        "GuestName": "mahanta",
        "ReservationNo": "1032",
        "MobileNo": null,
        "IsCheckedIn": true,
        "DeviceConfigurations": null,
        "ActivityResponse": {
            "AlcatelDatabaseDeviceId": 1710,
            "DeviceId": "7C:0B:C6:A8:47:8B",
            "Password": "1234",
            "UserName": "USER",
            "SipPhoneNumber": "2510",
            "DeviceLabel": "",
            "SipProfile": {
                "Domain": "10.9.223.238",
                "UserId": "2510",
                "DisplayName": "USER",
                "OutboundProxy": "10.9.223.25",
                "Password": "1234",
                "LoginName": "2510",
                "ContactUri": "",
                "OutboundProxyPort": "5060",
                "Protocol": "UDP",
                "DomainPort": "5060"
            }
        },
        "ActionType": "QR",
        "AdditionalDevices": null
        },
        "ResponseStatus": {
        "ResponseCode": "M1001",
        "ResponseFlag": "SUCCESS",
        "ResponseMessage": "Guest Device is already Registered ",
        "ResponseId": "0"
        }
        }*/