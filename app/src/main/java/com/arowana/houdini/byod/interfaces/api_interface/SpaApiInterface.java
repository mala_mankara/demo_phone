package com.arowana.houdini.byod.interfaces.api_interface;

import com.arowana.houdini.byod.model.spa.SpaBookingConfigObject;
import com.arowana.houdini.byod.model.spa.SpaConfigObject;
import com.google.gson.JsonObject;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.POST;

public interface SpaApiInterface {

    @FormUrlEncoded
    @POST("SPA/GetAll")
    Call<SpaConfigObject> getAllSpa(@Field("ImageSize") String imageSize);


    @POST("SPA/Booking")
    Call<SpaBookingConfigObject> submitSpa(@Body JsonObject submitJson);



}
