package com.arowana.houdini.byod.view.fragments.inroomdining;


import android.graphics.Paint;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.arowana.houdini.byod.R;
import com.arowana.houdini.byod.model.inroomdining.IRDCartObj;
import com.arowana.houdini.byod.model.inroomdining.IRDiningImagesObj;
import com.arowana.houdini.byod.model.inroomdining.IRDiningMenuItemsObj;
import com.arowana.houdini.byod.utils.RealmUtil;
import com.arowana.houdini.byod.view.fragments.BaseFragment;
import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;

import io.realm.Realm;
import io.realm.RealmResults;

import static com.arowana.houdini.byod.view.activity.InRoomDiningActivity.fragmentHolder;


public class IRDiningItemDetailsFragment extends BaseFragment  {

    private static final String TAG="IRD_MIdetails";
    private int itemCountInCart;

    public IRDiningItemDetailsFragment() {
    }

    private View mRootView;
    private IRDiningMenuItemsObj mObj;
    private TextView mDescription, mIngredients, mCount, mBtnIncrease, mBtnDecrease, mPrice, mBtnAdd, strikePrice;
    private ImageView mBtncustomize;
    private ImageView isVegNonveg;
    private Realm realm;
    private LinearLayout mCountLayout;


    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        mRootView = view;
        realm = RealmUtil.getInstance();
        setTitle(mObj.getName());
        initComponents();
        initData();
    }

    public void initComponents() {
        mDescription = mRootView.findViewById(R.id.item_description);
        mIngredients = mRootView.findViewById(R.id.ingredients_list);
        ImageView mImg = mRootView.findViewById(R.id.item_image);
        isVegNonveg = mRootView.findViewById(R.id.ir_dining_is_veg);

        mCount = mRootView.findViewById(R.id.item_count);
        mBtnIncrease = mRootView.findViewById(R.id.btn_increase);
        mBtnDecrease = mRootView.findViewById(R.id.btn_decrease);

        mPrice = mRootView.findViewById(R.id.price);
        strikePrice = mRootView.findViewById(R.id.ir_dining_menu_price);

        mBtncustomize = mRootView.findViewById(R.id.btn_customize);
        mBtnAdd = mRootView.findViewById(R.id.btn_ir_dining_menu_add);
        mCountLayout = mRootView.findViewById(R.id.count_layout);


        if (mObj.getImages().size() > 0) {
            IRDiningImagesObj img = mObj.getImages().get(0);
            Glide.with(getContext()).load(img.getImageUrl()).apply(new RequestOptions()
                    .optionalFitCenter()).into(mImg);
        } else mImg.setVisibility(View.GONE);
    }

    public void initData() {
        mDescription.setText(mObj.getDescription());
        mIngredients.setText(mObj.getIngredients());
        mBtncustomize.setVisibility(mObj.getCustomization().size() > 0 || mObj.getAddons().size()>0 ? View.VISIBLE : View.GONE);
        isVegNonveg.setBackgroundResource(mObj.getDietType().toLowerCase().equals("veg") ? R.drawable.veg_symbol : R.drawable.non_veg_symbol);

        realm.executeTransaction(new Realm.Transaction() {
            @Override
            public void execute(Realm realm) {
                mObj.setSelected(mObj.getItemCount() > 0 ? true : false);
            }
        });

        itemCountInCart = RealmUtil.getMenuItemCountInCart(mObj.getId());
        mCount.setText(String.valueOf(itemCountInCart));
        mBtnAdd.setVisibility(itemCountInCart <= 0 ? View.VISIBLE : View.GONE);
        mCountLayout.setVisibility(itemCountInCart < 1 ? View.GONE : View.VISIBLE);


        if (mObj.getDiscount().size() > 0) {
            strikePrice.setText(RealmUtil.getDiningCurrency().concat(" ").concat(mObj.getPrice()));
            strikePrice.setPaintFlags(strikePrice.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);
            mPrice.setText(RealmUtil.getDiningCurrency().concat(" ").concat(mObj.getDiscount().get(0).getDiscountPrice()));
        } else {
            strikePrice.setVisibility(View.GONE);
            mPrice.setText(RealmUtil.getDiningCurrency().concat(" ").concat(mObj.getPrice()));
        }

        mBtnIncrease.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                realm.executeTransaction(new Realm.Transaction() {
                    @Override
                    public void execute(Realm realm) {
                        if(mObj.getCustomization().size() > 0 || mObj.getAddons().size()>0) {
                            IRDining_RepeatCustom_PromptFragment fragment = new IRDining_RepeatCustom_PromptFragment();
                            IRDCartObj cartObj=realm.where(IRDCartObj.class).equalTo("MenuItemObj.Id", mObj.getId()).findAll().last();
                            fragment.setMenuItemObj(cartObj,true);
                            mHandler.addFragment(fragmentHolder.getId(), fragment,"PromptFragment");



                        }else {
                            IRDCartObj cartObj=realm.where(IRDCartObj.class).equalTo("MenuItemObj.Id", mObj.getId()).findFirst();
                            cartObj.setItemCount(cartObj.getItemCount()+1);
                            realm.copyToRealmOrUpdate(cartObj);
                            mCountLayout.setVisibility(View.VISIBLE);
                            mBtnAdd.setVisibility(View.GONE);
                            mCount.setText(String.valueOf(cartObj.getItemCount()));
                        }
                    }
                });
            }
        });


        mBtnDecrease.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Log.d(TAG,"decrease count.."+itemCountInCart);
                realm.executeTransaction(new Realm.Transaction() {
                    @Override
                    public void execute(Realm realm) {
                        IRDCartObj cartObj;
                        int cartCount=0;
                        if(mObj.getCustomization().size() > 0 || mObj.getAddons().size()>0) {
                            RealmResults<IRDCartObj> cartObjList= realm.where(IRDCartObj.class).equalTo("MenuItemObj.Id", mObj.getId()).findAll();
                            cartObj=realm.where(IRDCartObj.class).equalTo("MenuItemObj.Id", mObj.getId()).findAll().last();
                            Log.d("IRDMenuITEMADpt","size of cartObj.."+cartObj.getItemCount()+"/cartList... "+cartObjList.size());
                            cartCount=cartObjList.size();
                            if(cartCount>1){
                                populateSnackbar();
                            }else if(cartCount == 1 && cartObj.getItemCount()>1) {
                                cartObj.setItemCount(cartObj.getItemCount() - 1);
                                realm.copyToRealmOrUpdate(cartObj);
                                mCount.setText(String.valueOf(cartObj.getItemCount()));
                            }
                            else{
                                mBtnAdd.setVisibility(View.VISIBLE);
                                mCountLayout.setVisibility(View.GONE);
                                cartObj.deleteFromRealm();
                            }
                        }else {
                            cartObj=realm.where(IRDCartObj.class).equalTo("MenuItemObj.Id", mObj.getId()).findFirst();
                            cartCount=cartObj.getItemCount();
                            if(cartCount > 1) {
                                cartObj.setItemCount(cartCount - 1);
                                realm.copyToRealmOrUpdate(cartObj);
                                mCount.setText(String.valueOf(cartObj.getItemCount()));
                            }else if(cartCount == 1){
                                cartObj.deleteFromRealm();
                                mBtnAdd.setVisibility(View.VISIBLE);
                                mCountLayout.setVisibility(View.GONE);
                            }
                        }

                    }
                });

            }
        });

        mBtnAdd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mBtnAdd.setVisibility(View.GONE);
                mCountLayout.setVisibility(View.VISIBLE);

                if(mObj.getCustomization().size() > 0 || mObj.getAddons().size()>0) {
                    IRDiningCustomizationFragment fragment = new IRDiningCustomizationFragment();
                    fragment.setMenuItemObj(RealmUtil.createTmpCartObj(mObj), true);
                    mHandler.showFragment(fragmentHolder.getId(), fragment);
                }else {
                    RealmUtil.createCartObj(mObj);
                    mCountLayout.setVisibility(View.VISIBLE);
                    mBtnAdd.setVisibility(View.GONE);
                    mCount.setText("1");
                }

            }
        });
    }

    @Override
    protected int getFragmentLayout() {
        return R.layout.fragment_irdining_item_details;
    }

    public void setMenuObj(IRDiningMenuItemsObj obj) {
        this.mObj = obj;

    }

    private void populateSnackbar() {
        final FrameLayout layout = getActivity().findViewById(R.id.snacbar_layout);
        layout.setVisibility(View.VISIBLE);
        View view = getActivity().getLayoutInflater().inflate(R.layout.custom_confirm_snackbar_layout, null);
        TextView message= view.findViewById(R.id.dineMessage);
        message.setText("This item has multiple customizations added.Proceed to cart to edit");
        TextView mOkMessage = view.findViewById(R.id.snack_ok_message);
        mOkMessage.setTextSize(18);
        mOkMessage.setText("GO TO CART");
        TextView mCancelMessage = view.findViewById(R.id.snack_cancel_message);
        mCancelMessage.setTextSize(18);

        mOkMessage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                IRDiningCartFragment fragment = new IRDiningCartFragment();
                mHandler.showFragment(fragmentHolder.getId(), fragment);
                layout.setVisibility(View.GONE);
                layout.removeAllViews();

            }
        });

        mCancelMessage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                layout.setVisibility(View.GONE);
                layout.removeAllViews();

            }
        });

        layout.addView(view);
    }
}
