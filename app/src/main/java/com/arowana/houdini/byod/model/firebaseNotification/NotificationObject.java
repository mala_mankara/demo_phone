package com.arowana.houdini.byod.model.firebaseNotification;

import io.realm.RealmObject;

public class NotificationObject extends RealmObject{

    private String ResponseStatus;
    private String ResponseCode;
    private String ResponseFlag;
    private String ResponseMessage;
    private String ResponseId;

    public String getResponseStatus() {
        return ResponseStatus;
    }

    public void setResponseStatus(String responseStatus) {
        ResponseStatus = responseStatus;
    }

    public String getResponseCode() {
        return ResponseCode;
    }

    public void setResponseCode(String responseCode) {
        ResponseCode = responseCode;
    }

    public String getResponseFlag() {
        return ResponseFlag;
    }

    public void setResponseFlag(String responseFlag) {
        ResponseFlag = responseFlag;
    }

    public String getResponseMessage() {
        return ResponseMessage;
    }

    public void setResponseMessage(String responseMessage) {
        ResponseMessage = responseMessage;
    }

    public String getResponseId() {
        return ResponseId;
    }

    public void setResponseId(String responseId) {
        ResponseId = responseId;
    }

    @Override
    public String toString() {
        return "NotificationObject{" +
                "ResponseStatus='" + ResponseStatus + '\'' +
                ", ResponseCode='" + ResponseCode + '\'' +
                ", ResponseFlag='" + ResponseFlag + '\'' +
                ", ResponseMessage='" + ResponseMessage + '\'' +
                ", ResponseId='" + ResponseId + '\'' +
                '}';
    }
}
