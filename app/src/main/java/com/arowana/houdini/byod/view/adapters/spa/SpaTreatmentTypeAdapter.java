package com.arowana.houdini.byod.view.adapters.spa;

import android.content.Context;
import androidx.recyclerview.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.TextView;

import com.arowana.houdini.byod.R;
import com.arowana.houdini.byod.interfaces.listeners.FragmentNavigationHandler;
import com.arowana.houdini.byod.model.spa.SpaAvailabilityTimeObj;
import com.arowana.houdini.byod.model.spa.SpaObject;
import com.arowana.houdini.byod.model.spa.SpaTreatmentTypeObj;
import com.arowana.houdini.byod.view.fragments.spa.SpaDetailsFragment;

import io.realm.RealmList;

public class SpaTreatmentTypeAdapter extends RecyclerView.Adapter<SpaTreatmentTypeAdapter.MyViewHolder> {

    private Context context;
    private RealmList<SpaTreatmentTypeObj> spaTreatmentTypeObj;
    FragmentNavigationHandler mHandler;
    FrameLayout mHolder;
    private SpaObject spaObj;
    String hotelTimeZone;


    public class MyViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        TextView treatmentType;

        public MyViewHolder(View view) {
            super(view);
            treatmentType = view.findViewById(R.id.spa_treatment_type);
            view.setOnClickListener(this);
        }

        @Override
        public void onClick(View view) {
            Log.d("spa name is&&&&&&&",""+spaObj.getSpaName());

            SpaAvailabilityTimeObj timeAvailabilityObj = spaObj.getAvailabilityTime().first();
            Long count=Long.valueOf(spaObj.getTreatmentType().where().equalTo("Treatment.isSelected", true).count());
          /*  if (count==0) {
                RealmUtil.resetSpa();
            }*/
            SpaDetailsFragment fragment = new SpaDetailsFragment();
            /* if(spaTreatmentTypeObj.get(getAdapterPosition()))*/
            fragment.setTreatmenttypeObj(hotelTimeZone,spaTreatmentTypeObj.get(getAdapterPosition()),timeAvailabilityObj );
            mHandler.showFragment(mHolder.getId(), fragment);
        }
    }


    public SpaTreatmentTypeAdapter(Context mContext, String hotelTimezone, SpaObject obj, FragmentNavigationHandler handler, FrameLayout holder) {
        this.spaObj = obj;
        this.context = mContext;
        this.spaTreatmentTypeObj = obj.getTreatmentType();
        hotelTimeZone = hotelTimezone;
        mHandler = handler;
        mHolder = holder;

    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_spa_treatment_type, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, int position) {
        SpaTreatmentTypeObj obj = spaTreatmentTypeObj.get(position);
        holder.treatmentType.setText(obj.getTypeName());


    }


    @Override
    public int getItemCount() {
        return spaTreatmentTypeObj.size();
    }


}
