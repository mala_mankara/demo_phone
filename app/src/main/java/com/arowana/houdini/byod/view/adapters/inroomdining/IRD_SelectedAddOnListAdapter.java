package com.arowana.houdini.byod.view.adapters.inroomdining;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.arowana.houdini.byod.R;
import com.arowana.houdini.byod.model.inroomdining.IRDiningAddonsObj;

import io.realm.RealmList;

public class IRD_SelectedAddOnListAdapter extends RecyclerView.Adapter<IRD_SelectedAddOnListAdapter.CustomViewHolder> {


    private RealmList<IRDiningAddonsObj> addOnsList;
    private boolean isFromSummaryScreen;


    class CustomViewHolder extends RecyclerView.ViewHolder {
        TextView name;

        CustomViewHolder(View itemView) {
            super(itemView);
            name = itemView.findViewById(R.id.ird_addonsprompt_name);
        }
    }

    public IRD_SelectedAddOnListAdapter(RealmList<IRDiningAddonsObj> addons, boolean isFromSummary) {
        addOnsList = addons;
        isFromSummaryScreen = isFromSummary;
    }

    @NonNull
    @Override
    public CustomViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.ird_addonsprompt_value, parent, false);
        return new CustomViewHolder(itemView);

    }

    @Override
    public void onBindViewHolder(@NonNull CustomViewHolder holder, int position) {
        if(addOnsList != null) {
            IRDiningAddonsObj addOnObj = addOnsList.get(position);
            if (isFromSummaryScreen)
                holder.name.setText(addOnObj.getAddOnName().concat(","));
            else
                holder.name.setText(addOnObj.isSelected() ? addOnObj.getAddOnName().concat(",") : "");
        }


    }

    @Override
    public int getItemCount() {
        return addOnsList.size();
    }


}
