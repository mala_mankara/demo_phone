package com.arowana.houdini.byod.model.speeddial;

import io.realm.RealmObject;

public class SpeedDialResponseData  extends RealmObject{

    private String ExternalNumber;

    private String ActivityId;

    private String Number;

    private String ModifiedBy;

    private String Order;

    private String Name;

    private String ModifiedDate;

    private String TypeNumber;

    private String CreatedBy;

    private String CreatedDate;

    private String Id;

    private String MessageCode;

    private String Version;

    private String Icon;

    private String Tenant;

    public String getExternalNumber ()
    {
        return ExternalNumber;
    }

    public void setExternalNumber (String ExternalNumber)
    {
        this.ExternalNumber = ExternalNumber;
    }

    public String getActivityId ()
    {
        return ActivityId;
    }

    public void setActivityId (String ActivityId)
    {
        this.ActivityId = ActivityId;
    }

    public String getNumber ()
    {
        return Number;
    }

    public void setNumber (String Number)
    {
        this.Number = Number;
    }

    public String getModifiedBy ()
    {
        return ModifiedBy;
    }

    public void setModifiedBy (String ModifiedBy)
    {
        this.ModifiedBy = ModifiedBy;
    }

    public String getOrder ()
    {
        return Order;
    }

    public void setOrder (String Order)
    {
        this.Order = Order;
    }

    public String getName ()
    {
        return Name;
    }

    public void setName (String Name)
    {
        this.Name = Name;
    }

    public String getModifiedDate ()
    {
        return ModifiedDate;
    }

    public void setModifiedDate (String ModifiedDate)
    {
        this.ModifiedDate = ModifiedDate;
    }

    public String getTypeNumber ()
    {
        return TypeNumber;
    }

    public void setTypeNumber (String TypeNumber)
    {
        this.TypeNumber = TypeNumber;
    }

    public String getCreatedBy ()
    {
        return CreatedBy;
    }

    public void setCreatedBy (String CreatedBy)
    {
        this.CreatedBy = CreatedBy;
    }

    public String getCreatedDate ()
    {
        return CreatedDate;
    }

    public void setCreatedDate (String CreatedDate)
    {
        this.CreatedDate = CreatedDate;
    }

    public String getId ()
    {
        return Id;
    }

    public void setId (String Id)
    {
        this.Id = Id;
    }

    public String getMessageCode ()
    {
        return MessageCode;
    }

    public void setMessageCode (String MessageCode)
    {
        this.MessageCode = MessageCode;
    }

    public String getVersion ()
    {
        return Version;
    }

    public void setVersion (String Version)
    {
        this.Version = Version;
    }

    public String getIcon ()
    {
        return Icon;
    }

    public void setIcon (String Icon)
    {
        this.Icon = Icon;
    }

    public String getTenant ()
    {
        return Tenant;
    }

    public void setTenant (String Tenant)
    {
        this.Tenant = Tenant;
    }

    @Override
    public String toString()
    {
        return "ClassPojo [ExternalNumber = "+ExternalNumber+", ActivityId = "+ActivityId+", Number = "+Number+", ModifiedBy = "+ModifiedBy+", Order = "+Order+", Name = "+Name+", ModifiedDate = "+ModifiedDate+", TypeNumber = "+TypeNumber+", CreatedBy = "+CreatedBy+", CreatedDate = "+CreatedDate+", Id = "+Id+", MessageCode = "+MessageCode+", Version = "+Version+", Icon = "+Icon+", Tenant = "+Tenant+"]";
    }




}
