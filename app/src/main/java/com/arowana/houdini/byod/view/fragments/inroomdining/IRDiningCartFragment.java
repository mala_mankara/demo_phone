package com.arowana.houdini.byod.view.fragments.inroomdining;


import android.app.ProgressDialog;
import android.os.Bundle;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.text.InputType;
import android.util.Log;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.arowana.houdini.byod.Consts;
import com.arowana.houdini.byod.MainApplication;
import com.arowana.houdini.byod.R;
import com.arowana.houdini.byod.interfaces.ApiClient;
import com.arowana.houdini.byod.interfaces.api_interface.InRoomDiningApiInterface;
import com.arowana.houdini.byod.model.inroomdining.IRDCartObj;
import com.arowana.houdini.byod.model.inroomdining.IRDiningAddonsObj;
import com.arowana.houdini.byod.model.inroomdining.IRDiningBookingConfigObj;
import com.arowana.houdini.byod.model.inroomdining.IRDiningBookingObj;
import com.arowana.houdini.byod.model.inroomdining.IRDiningCustomizationObj;
import com.arowana.houdini.byod.model.inroomdining.IRDiningMenuItemsObj;
import com.arowana.houdini.byod.model.inroomdining.IRDiningMenusObj;
import com.arowana.houdini.byod.model.inroomdining.RoomDiningInfoObj;
import com.arowana.houdini.byod.utils.NetworkUtils;
import com.arowana.houdini.byod.utils.RealmUtil;
import com.arowana.houdini.byod.utils.ViewUtils;
import com.arowana.houdini.byod.view.adapters.ICart;
import com.arowana.houdini.byod.view.adapters.inroomdining.InRoomDiningCartAdapter;
import com.arowana.houdini.byod.view.fragments.BaseFragment;
import com.arowana.houdini.byod.view.fragments.telephone.PhoneCallFragment;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;

import alcatel.model.ByodManager;
import io.realm.Realm;
import io.realm.RealmResults;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.arowana.houdini.byod.view.activity.InRoomDiningActivity.fragmentHolder;


public class IRDiningCartFragment extends BaseFragment implements ICart {


    public IRDiningCartFragment() {

    }

    private View mRootView;
    private RecyclerView mRecyclerView, upsellingRecyclerview;
    private TextView totalCost;
    private EditText mSpecialInstructions;
    private LinearLayout upsellingLayout;
    private Realm realm;
    private ProgressDialog mDialog;
    private InRoomDiningCartAdapter.InRoomDiningUpsellingAdapter upsellingAdapter;
    private String speedDialNo;


    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        mRootView = view;
        realm = RealmUtil.getInstance();
        setTitle(getString(R.string.cart));
        initDialog();
        initComponents();
        populateData();

    }

    private void initDialog() {
        mDialog = new ProgressDialog(getContext());
        mDialog.setMessage(getString(R.string.dialog_message));
        mDialog.setCancelable(false);
    }

    public void initComponents() {
        mRecyclerView = mRootView.findViewById(R.id.ir_dining_cart_recyclerview);
        upsellingRecyclerview = mRootView.findViewById(R.id.ir_dining_upselling_recyclerview);
        totalCost = mRootView.findViewById(R.id.ir_dining_cart_total_price);
        RelativeLayout btnSubmit = mRootView.findViewById(R.id.ir_dining_cart_btn_submit);
        upsellingLayout = mRootView.findViewById(R.id.upselling_layout);
        mSpecialInstructions = mRootView.findViewById(R.id.special_instructions);

        View call_conceirge = mRootView.findViewById(R.id.btn_ir_dining_cart_conceirge);
       // call_conceirge.setVisibility(Consts.APP_CURRENT_STATE > 1 ? View.VISIBLE : View.GONE);
        if (MainApplication.isContainedSpeedDial("Palace Ceremony") && Consts.APP_CURRENT_STATE > 1) {
            call_conceirge.setVisibility(View.VISIBLE);
            speedDialNo = MainApplication.getValueforKey();
        }else
            call_conceirge.setVisibility(View.GONE);

        mSpecialInstructions.setHorizontallyScrolling(false);
        mSpecialInstructions.setScrollContainer(true);
        mSpecialInstructions.setMaxLines(3);
        mSpecialInstructions.setImeOptions(EditorInfo.IME_ACTION_DONE);
        mSpecialInstructions.setRawInputType(InputType.TYPE_CLASS_TEXT);

        call_conceirge.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!ByodManager.getInstance().isSipConnected()) {
                    Toast.makeText(getActivity(), "Call service not working. Please try later..", Toast.LENGTH_SHORT).show();
                } else {

                    if (!speedDialNo.isEmpty()) {
                        PhoneCallFragment fragment = new PhoneCallFragment();
                        Bundle bundle = new Bundle();
                        bundle.putString("phoneNo", speedDialNo);
                        bundle.putString("name", "Palace Ceremony");
                        bundle.putBoolean("isMakingCall", true);
                        fragment.setArguments(bundle);
                        mHandler.showFragment(fragmentHolder.getId(), fragment);
                    }
                }
            }
        });


        btnSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (RealmUtil.getDiningCartCount() > 0) {
                 //   populateSnackbar();
                    createSubmitJson();
                } else {
                    Toast.makeText(getContext(), "Please Add items to cart!", Toast.LENGTH_SHORT).show();
                }

            }
        });
    }

    public void populateData() {
        totalCost.setText(RealmUtil.getDiningCurrency().concat(" ").concat(String.valueOf(RealmUtil.getDiningTotalPrice())));

        LinearLayoutManager linearVertical = new LinearLayoutManager(getContext(), RecyclerView.VERTICAL, false);
        mRecyclerView.setLayoutManager(linearVertical);
        InRoomDiningCartAdapter adapter = new InRoomDiningCartAdapter(getContext(), this, mHandler,  upsellingAdapter, upsellingRecyclerview, upsellingLayout);
        mRecyclerView.setAdapter(adapter);
        adapter.notifyDataSetChanged();


    }

    @Override
    protected int getFragmentLayout() {
        return R.layout.fragment_irdining_cart;
    }

    @Override
    public void updateCartInfo() {
        totalCost.setText(RealmUtil.getDiningCurrency().concat(" ").concat(String.valueOf(RealmUtil.getDiningTotalPrice())));
    }


    public void createSubmitJson() {
        try {
            RealmResults<IRDCartObj> cartObjs = realm.where(IRDCartObj.class).findAll();

            if(cartObjs != null) {

                JsonObject submitJson = new JsonObject();

                submitJson.addProperty("RoomNumber", RealmUtil.getRoomNo());
                submitJson.addProperty("BookingId", RealmUtil.getBookingId());

                submitJson.addProperty("Comments", "" + mSpecialInstructions.getText().toString());
                JsonArray diningMenuArray = new JsonArray();

                for (IRDCartObj cartObj : cartObjs) {
                    IRDiningMenuItemsObj diningMenuItemsObj = cartObj.getMenuItemObj();
                    RoomDiningInfoObj infoObj = realm.where(RoomDiningInfoObj.class).equalTo("Menus.MenuItems.Id", diningMenuItemsObj.getId()).findFirst();
                    IRDiningMenusObj diningMenuObjList = infoObj.getMenus().where().equalTo("MenuItems.Id", diningMenuItemsObj.getId()).findFirst();


                    JsonObject diningMenuItemArrayObj = new JsonObject();

                    diningMenuItemArrayObj.addProperty("ItemCode", diningMenuItemsObj.getId());
                    diningMenuItemArrayObj.addProperty("ItemName", diningMenuItemsObj.getName());
                    diningMenuItemArrayObj.addProperty("MenuCode", diningMenuObjList.getMenuCode());
                    diningMenuItemArrayObj.addProperty("MenuName", diningMenuObjList.getMenuName());
                    diningMenuItemArrayObj.addProperty("CategoryCode", infoObj.getCategoryCode());

                    if (diningMenuItemsObj.getDiscount().size() > 0) {
                        diningMenuItemArrayObj.addProperty("Price", diningMenuItemsObj.getDiscount().get(0).getDiscountPrice());

                    } else {
                        diningMenuItemArrayObj.addProperty("Price", diningMenuItemsObj.getPrice());
                    }
                    diningMenuItemArrayObj.addProperty("CourseNumber", "");
                    diningMenuItemArrayObj.addProperty("CoverNumber", "");
                    diningMenuItemArrayObj.addProperty("OrderedQuantity", String.valueOf(cartObj.getItemCount()));
                    diningMenuItemArrayObj.addProperty("KotComment", "");
                    diningMenuItemArrayObj.addProperty("OrderStatus", "");
                    diningMenuItemArrayObj.addProperty("DietType", diningMenuItemsObj.getDietType());
                    diningMenuItemArrayObj.addProperty("Description", "");
                    diningMenuItemArrayObj.addProperty("IsCrossellItem", "");
                    diningMenuItemArrayObj.addProperty("IsUpsellItem", diningMenuItemsObj.isUpsellingItem());

                    // RealmResults<IRDiningAddonsObj> addonList = diningMenuItemsObj.getAddons().where().equalTo("isSelected", true).findAll();
                    RealmResults<IRDiningAddonsObj> addonList = cartObj.getAddons().where().equalTo("isSelected", true).findAll();

                    JsonArray addonArray = new JsonArray();
                    for (IRDiningAddonsObj addonObj : addonList) {
                        Log.d("cartFrag", "the addons...." + addonObj.getAddOnName());
                        JsonObject addonArrayObj = new JsonObject();
                        addonArrayObj.addProperty("AddOnId", addonObj.getId());
                        addonArrayObj.addProperty("AddOnName", addonObj.getAddOnName());
                        addonArrayObj.addProperty("Price", addonObj.getPrice());
                        addonArray.add(addonArrayObj);
                    }

                    diningMenuItemArrayObj.add("AddOns", addonArray);
                    // RealmResults<IRDiningCustomizationObj> customizationList = diningMenuItemsObj.getCustomization().where().equalTo("CustomTypes.isSelected", true).findAll();
                    RealmResults<IRDiningCustomizationObj> customizationList = cartObj.getCustomization().where().equalTo("isSelected", true).findAll();
//             "Customization": [
//              {"Id": "5b76539ecda870254c3bd576","CustomizeItem": "Spicy",
//                 "CustomTypes": [{"CustomTypeId": null,"CustomTypeName": "More spicy"},
//                                 {"CustomTypeId": null,"CustomTypeName": "Normal spicy"}, { "CustomTypeId": null,"CustomTypeName": "Less spicy"}]
//              },
//              {"Id": "5b2b3214cda8700d08c49483","CustomizeItem": "Toast",
//                 "CustomTypes": [{"CustomTypeId": null,"CustomTypeName": "White"},{"CustomTypeId": null,"CustomTypeName": "Brown"}]
//              }],
                    JsonArray customizationArray = new JsonArray();
                    for (IRDiningCustomizationObj customizationObj : customizationList) {
                        Log.d("cartFrag", "selected InDEX@@@@@@...." + customizationObj.getSelectedIndex());
                        int index = customizationObj.getSelectedIndex();
                        JsonObject customizationArrayObj = new JsonObject();
                        customizationArrayObj.addProperty("CustomizeItemId", customizationObj.getId());
                        customizationArrayObj.addProperty("CustomizeItemName", customizationObj.getCustomizeItem());
                        JsonArray customTypesArray = new JsonArray();
                        for (int pos = 0; pos < customizationObj.getCustomTypes().size(); pos++) {
                            if (pos == index) {
                                JsonObject customTypesArrayObj = new JsonObject();
                                customTypesArrayObj.addProperty("CustomTypeId", customizationObj.getCustomTypes().get(pos).getCustomTypeId());
                                customTypesArrayObj.addProperty("CustomTypeName", customizationObj.getCustomTypes().get(pos).getCustomTypeName());
                                customTypesArray.add(customTypesArrayObj);
                            }
                        }
                        customizationArrayObj.add("CustomType", customTypesArray);
                        customizationArray.add(customizationArrayObj);
                    }
                    diningMenuItemArrayObj.add("ItemCustomzation", customizationArray);
                    diningMenuArray.add(diningMenuItemArrayObj);

                }

                submitJson.add("OrderLineItems", diningMenuArray);

                JsonObject guestProfileObject = new JsonObject();
                guestProfileObject.addProperty("Email", RealmUtil.getEmailId());
                guestProfileObject.addProperty("MembershipId", RealmUtil.getMembershipId());
                guestProfileObject.addProperty("FullName", RealmUtil.getFullName());
                guestProfileObject.addProperty("PhoneNumber", RealmUtil.getPhoneNumber());
                submitJson.add("GuestProfile", guestProfileObject);

                JsonObject hotelPropertyObject = new JsonObject();
                hotelPropertyObject.addProperty("GroupCode", "");
                hotelPropertyObject.addProperty("BrandCode", "");
                hotelPropertyObject.addProperty("HotelCode", "");
                submitJson.add("HotelProperty", hotelPropertyObject);

                JsonObject deviceObject = new JsonObject();
                deviceObject.addProperty("DeviceType", "");
                deviceObject.addProperty("MacId", "");
                submitJson.add("Device", deviceObject);


                Log.d("diningJson", "diningJson is************" + submitJson);

                if (NetworkUtils.checkConnectivityStatus(getContext())) {
                    mDialog.show();
                    callSubmitAPI(submitJson);
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void callSubmitAPI(JsonObject json) {
        Call<IRDiningBookingConfigObj> responseObj;
        responseObj = ApiClient.getApiClient(InRoomDiningApiInterface.class).submitOrder(json);
        responseObj.enqueue(new Callback<IRDiningBookingConfigObj>() {
            @Override
            public void onResponse(Call<IRDiningBookingConfigObj> call, Response<IRDiningBookingConfigObj> response) {

                if (mDialog.isShowing())
                    mDialog.dismiss();
                String flag = response.body().getResponseStatus().getResponseFlag();
                final String message = response.body().getResponseStatus().getResponseMessage();
                if (flag.equals("SUCCESS")) {
                    IRDiningBookingObj obj = response.body().getResponseData();
                    IRDiningBookingDetailsFragment fragment = new IRDiningBookingDetailsFragment();
                    fragment.setBookingObj(obj, message);
                    mHandler.showFragment(fragmentHolder.getId(), fragment);

                    realm.executeTransaction(new Realm.Transaction() {
                        @Override
                        public void execute(Realm realm) {
                            RealmResults<IRDiningMenuItemsObj> result = realm.where(IRDiningMenuItemsObj.class).findAll();
                            result.deleteAllFromRealm();
                        }
                    });
                } else ViewUtils.showSnackBarMessage(message);
            }

            @Override
            public void onFailure(Call<IRDiningBookingConfigObj> call, Throwable t) {
                if (mDialog.isShowing())
                    mDialog.dismiss();
                t.printStackTrace();
            }
        });
    }



   /* @Override
    public void onChange(IRDiningMenuItemsObj irDiningMenuItemsObj) {
       // adapter.notifyDataSetChanged();
       // upsellingAdapter.notifyDataSetChanged();
    }*/
}
