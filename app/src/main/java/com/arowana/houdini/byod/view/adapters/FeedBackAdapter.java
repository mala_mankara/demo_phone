package com.arowana.houdini.byod.view.adapters;

import android.content.Context;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RatingBar;
import android.widget.TextView;

import com.arowana.houdini.byod.R;
import com.arowana.houdini.byod.model.feedback.FeedBackResponseData;
import com.arowana.houdini.byod.utils.RealmUtil;

import io.realm.Realm;
import io.realm.RealmList;

public class FeedBackAdapter extends RecyclerView.Adapter<FeedBackAdapter.MyViewHolder> {
    private Realm realm;
    private RealmList<FeedBackResponseData> dataObj;
    private Context cntxt;

    public FeedBackAdapter(Context context, RealmList<FeedBackResponseData> results) {
        dataObj = results;
        cntxt = context;
        realm = RealmUtil.getInstance();
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.feedback_item_rating, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, final int position) {
        final FeedBackResponseData positionData=dataObj.get(position);
        if(positionData != null ){
            int numOfStars = positionData.getMaxRating();
            int givenRating = positionData.getPreviousRating();
            holder.title.setText(positionData.getName());
            holder.ratingBar.setNumStars(numOfStars);
            holder.ratingBar.setRating((float) givenRating);
            holder.ratingBar.setStepSize(1.0f);
            holder.ratingBar.setTag(position);

            holder.ratingBar.setOnRatingBarChangeListener(new RatingBar.OnRatingBarChangeListener() {
                @Override
                public void onRatingChanged(RatingBar ratingBar, float rating, boolean fromUser) {
                    if (!realm.isInTransaction())
                        realm.beginTransaction();
                    positionData.setPreviousRating((int)rating);
                    realm.copyToRealm(positionData);
                    realm.commitTransaction();
                }
            });
        }
    }

    @Override
    public int getItemCount() {
        return dataObj.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        TextView title;
        RatingBar ratingBar;

        public MyViewHolder(View itemView) {
            super(itemView);
            title = itemView.findViewById(R.id.feedbackTittle);
            ratingBar = itemView.findViewById(R.id.feedback_rate_bar);
        }
    }
}
