package com.arowana.houdini.byod.view.adapters.cityguide;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentStatePagerAdapter;

import com.arowana.houdini.byod.model.cityguide.CityguideResponseDataObj;
import com.arowana.houdini.byod.view.fragments.cityguide.CityGuideFragment;

import io.realm.RealmList;


public class CityGuidePagerAdapter extends FragmentStatePagerAdapter {

    private RealmList<CityguideResponseDataObj> mObj;

    public CityGuidePagerAdapter(FragmentManager fm, RealmList<CityguideResponseDataObj> mObj) {
        super(fm);
        this.mObj = mObj;
    }


    // Returns total number of pages
    @Override
    public int getCount() {

        return mObj.size();
    }

    // Returns the fragment to display for that page
    @Override
    public Fragment getItem(int position) {

        CityGuideFragment fragment = new CityGuideFragment();
        fragment.setObj(mObj.get(position));
        return fragment;

    }

    // Returns the page title for the top indicator
    @Override
    public String getPageTitle(int position) {

        return mObj.get(position).getCategoryName();

    }


}