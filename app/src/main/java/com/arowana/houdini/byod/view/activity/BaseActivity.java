package com.arowana.houdini.byod.view.activity;

import android.content.Intent;
import android.content.res.Configuration;
import android.media.Ringtone;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.ActionBarDrawerToggle;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.view.GravityCompat;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.arowana.houdini.byod.Consts;
import com.arowana.houdini.byod.MainApplication;
import com.arowana.houdini.byod.R;
import com.arowana.houdini.byod.firebase.NotificationConfig;
import com.arowana.houdini.byod.interfaces.ApiClient;
import com.arowana.houdini.byod.interfaces.api_interface.CheckDeviceStatusApiInterface;
import com.arowana.houdini.byod.interfaces.api_interface.FirebaseNotificationApiInterface;
import com.arowana.houdini.byod.interfaces.listeners.AppstateChangeListener;
import com.arowana.houdini.byod.interfaces.listeners.FragmentNavigationHandler;
import com.arowana.houdini.byod.model.deviceactivation_QR.QR_DeviceRegisterConfigObj;
import com.arowana.houdini.byod.model.deviceactivation_QR.QR_RegisterResponseData;
import com.arowana.houdini.byod.model.deviceactivation_QR.QR_RegisterResponseStatus;
import com.arowana.houdini.byod.model.firebaseNotification.NotificationObject;
import com.arowana.houdini.byod.model.home.HomeMenuObject;
import com.arowana.houdini.byod.model.sharedpreference.SharedPreferenceUtil;
import com.arowana.houdini.byod.model.sharedpreference.StoreKeys;
import com.arowana.houdini.byod.utils.FragmentTransactionUtils;
import com.arowana.houdini.byod.utils.JSONUtil;
import com.arowana.houdini.byod.utils.NavigationManager;
import com.arowana.houdini.byod.utils.NetworkUtils;
import com.arowana.houdini.byod.utils.RealmUtil;
import com.arowana.houdini.byod.utils.ViewUtils;
import com.arowana.houdini.byod.view.adapters.HamburgerMenuAdapter;
import com.google.android.material.appbar.AppBarLayout;

import org.json.JSONArray;
import org.json.JSONObject;

import io.realm.Case;
import io.realm.Realm;
import io.realm.RealmList;
import io.realm.RealmResults;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class BaseActivity extends AppCompatActivity implements View.OnClickListener, FragmentNavigationHandler, AppstateChangeListener {

    private DrawerLayout mDrawerLayout;
    private ImageView mNavigationToggle;
    private RealmList<HomeMenuObject> mList;
    private RecyclerView mMenuLayout;
    private FrameLayout view_stub;
    private ActionBarDrawerToggle mDrawerToggle;
    Fragment showingFragment;
    private ImageView mBtnBack;

    private FragmentManager mFragmentManager;
    private FragmentTransaction fragmentTransaction;

    private int mLayoutRes;

    public static FragmentNavigationHandler mHandler;
    public static AppstateChangeListener mListener;

    private TextView mUserName, mMembershipId;
    private LinearLayout footerSection;
    Realm realm;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        super.setContentView(R.layout.activity_base);

        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        realm = RealmUtil.getInstance();
        initMainComponents();
        init();
        initMainListeners();
        populateHamburgerMenu();

        if (getIntent().getExtras() != null) {
            Log.d("test", "------onCreate MainActivity-------" + getIntent().getExtras().getString("Module") + "");
            Log.d("test", "------onCreate MainActivity-------" + getIntent().getExtras().getString("Message") + "");
            String message = getIntent().getExtras().getString("Message");


            String notificationType = getIntent().getExtras().getString("Module");
//            if (notificationType.equalsIgnoreCase(NotificationConfig.NOTIFICATION_CHECKOUT) /*&& !(showingFragment instanceof FeedBackFragment)*/) {
//                Uri notification = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
//                Ringtone r = RingtoneManager.getRingtone(this, notification);
//                r.play();
//                ViewUtils.showSnackBarMessage("Thank you for visiting Grand Millennium", this);
//                new Handler().postDelayed(new Runnable() {
//                    @Override
//                    public void run() {
//                        //// showFragment(new FeedbackFragment());
//                        //NavigationManager.getInstance().showFragment(NavigationConsts.ACC_FEEDBACK);
//                        NavigationManager.loadView("feedback",HomeActivity.this, mListener);
//                    }
//                }, 5000);
//
//                //IsUserCheckedIn();
//                changeAppState(Consts.APP_LOGIN_STATE);
//
//
//            }
        }

    }


    private void initMainComponents() {
        view_stub = findViewById(R.id.view_stub);
        mDrawerLayout = findViewById(R.id.drawer_layout);
        mNavigationToggle = findViewById(R.id.navigation_toggle);
        mBtnBack = findViewById(R.id.btn_back);
        mMenuLayout = findViewById(R.id.hamburger_menu_list);
        mUserName = findViewById(R.id.userName);
        mMembershipId = findViewById(R.id.membershipId);

        footerSection=findViewById(R.id.footer_section);


    }


    private void initMainListeners() {
        mNavigationToggle.setOnClickListener(this);
        mBtnBack.setOnClickListener(this);

        populateFooter();

    }

    public void populateFooter(){
        View view=getLayoutInflater().inflate(R.layout.footer_layout, null);
        LinearLayout mBtnSignout=view.findViewById(R.id.hamburger_signoutBtn);
        mBtnSignout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Consts.APP_CURRENT_STATE = Consts.APP_DEFAULT__STATE;
                SharedPreferenceUtil.putIntegerIntoStore(StoreKeys.APPLICATION_STATE,Consts.APP_DEFAULT__STATE );
                init();
                changeAppState(Consts.APP_DEFAULT__STATE );
                if(!(MainApplication.getCurrentActivity() instanceof HomeActivity)){
                    MainApplication.getCurrentActivity().finish();
                }

                RealmUtil.deleteRealm();
            }
        });
        footerSection.addView(view);
    }

    public void init() {
        Consts.APP_CURRENT_STATE = SharedPreferenceUtil.getIntegerFromStore(StoreKeys.APPLICATION_STATE);
        getUSerType();
        if (Consts.APP_CURRENT_STATE > 0) {
            mDrawerLayout.setDrawerLockMode(DrawerLayout.LOCK_MODE_UNLOCKED);
            mNavigationToggle.setVisibility(View.VISIBLE);
            mUserName.setText(RealmUtil.getFullName());
            mMembershipId.setText(RealmUtil.getMembershipId());
        } else {
            mDrawerLayout.setDrawerLockMode(DrawerLayout.LOCK_MODE_LOCKED_CLOSED);
            mNavigationToggle.setVisibility(View.GONE);
        }
    }


    public void populateHamburgerMenu() {
        String currState = String.valueOf(Consts.APP_CURRENT_STATE);

        String response;
     /*   if (currState == "2")
            response = JSONUtil.ReadFromfile(this, "json/device_activated_menu.json");
        else*/
        response = JSONUtil.ReadFromfile(this, "json/hamburger_menu.json");
        try {
            JSONObject obj = new JSONObject(response);
            final JSONArray array = obj.getJSONArray("menu");
            Realm realm = RealmUtil.getInstance();
            realm.executeTransaction(new Realm.Transaction() {
                @Override
                public void execute(Realm realm) {
                    HomeMenuObject obj = realm.where(HomeMenuObject.class).findFirst();
                    if (obj == null) {
                        realm.createAllFromJson(HomeMenuObject.class, array);
                    }
                }
            });

            RealmResults<HomeMenuObject> results = realm.where(HomeMenuObject.class).contains("appState", currState, Case.INSENSITIVE)
                    .findAll();

            if (!RealmUtil.isPrimaryCheckin()) {

                results = results.where().notEqualTo("key", "secondary_device_activation").and().notEqualTo("key", "check_out").findAll();

            } else if (RealmUtil.IsUserCheckedIn()) {
                results = results.where().notEqualTo("key", "deactivate").findAll();
            }

            mList = new RealmList<>();
            LinearLayoutManager linearVertical = new LinearLayoutManager(this, RecyclerView.VERTICAL, false);
            mMenuLayout.setLayoutManager(linearVertical);
            mList.clear();
            mList.addAll(results);
            System.out.println("size is" + mList.size());
            //  HamburgerMenuAdapter adapter = new HamburgerMenuAdapter(getApplicationContext(), mList, currState);
            HamburgerMenuAdapter adapter = new HamburgerMenuAdapter(this, mList, mDrawerLayout,this);
            mMenuLayout.setAdapter(adapter);
            adapter.notifyDataSetChanged();

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onClick(View view) {


        int id = view.getId();

        switch (id) {
            case R.id.navigation_toggle:
                if (!mDrawerLayout.isDrawerOpen(Gravity.RIGHT))
                    mDrawerLayout.openDrawer(Gravity.RIGHT);
                else
                    mDrawerLayout.closeDrawer(Gravity.RIGHT);
                break;

            case R.id.btn_back:
                onBackPressed();
                break;


        }
    }


    @Override
    public void setContentView(int layoutResID) {
        if (view_stub != null) {
            LayoutInflater inflater = (LayoutInflater) getSystemService(LAYOUT_INFLATER_SERVICE);
            ViewGroup.LayoutParams lp = new ViewGroup.LayoutParams(
                    ViewGroup.LayoutParams.MATCH_PARENT,
                    ViewGroup.LayoutParams.MATCH_PARENT);
            View stubView = inflater.inflate(layoutResID, view_stub, false);
            view_stub.addView(stubView, lp);
        }
    }

    @Override
    public void setContentView(View view) {
        if (view_stub != null) {
            ViewGroup.LayoutParams lp = new ViewGroup.LayoutParams(
                    ViewGroup.LayoutParams.MATCH_PARENT,
                    ViewGroup.LayoutParams.MATCH_PARENT);
            view_stub.addView(view, lp);
        }
    }

    @Override
    public void setContentView(View view, ViewGroup.LayoutParams params) {
        if (view_stub != null) {
            view_stub.addView(view, params);
        }
    }

    @Override
    protected void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);

    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);

    }


    @Override
    public void showFragment(int id, Fragment fragment, String tag, boolean backStack, String transactionType) {

        if (fragment == null)
            return;


        showingFragment = fragment;
        mFragmentManager = getSupportFragmentManager();
/*

        if (mFragmentManager.getBackStackEntryCount() > Consts.MAX_STACK_COUNT)
            mFragmentManager.popBackStack();
*/

        fragmentTransaction = mFragmentManager.beginTransaction();

        FragmentTransactionUtils.getInstance().setTransaction(fragmentTransaction, transactionType);

        try {
            fragmentTransaction.replace(id, fragment, tag);
            if (backStack)
                fragmentTransaction.addToBackStack(tag);
            clearBackStack();
            fragmentTransaction.commitAllowingStateLoss();
        } catch (IllegalStateException ex) {
            ex.printStackTrace();
        }


    }


    @Override
    public void onBackPressed() {
        InputMethodManager imm = (InputMethodManager) getSystemService(INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(mBtnBack.getWindowToken(), 0);

        if (mDrawerLayout.isDrawerOpen(GravityCompat.END))
            mDrawerLayout.closeDrawer(GravityCompat.END);
        else {
            int fragments = mFragmentManager.getBackStackEntryCount();
            if (fragments == 0 || fragments == 1) finish();
            else if (getFragmentManager().getBackStackEntryCount() > 1) {
                getFragmentManager().popBackStack();
            } else super.onBackPressed();
        }
    }


    @Override
    public void showFragment(int id, Fragment fragment) {
        showFragment(id, fragment, fragment.getClass().getName(), true, FragmentTransactionUtils.TYPE4);
    }


    @Override
    public void showFragment(int id, Fragment fragment, String tag) {
        showFragment(id, fragment, tag, true, FragmentTransactionUtils.TYPE4);
    }

    @Override
    public void showFragment(int id, Fragment fragment, String tag, boolean backStack) {
        showFragment(id, fragment, tag, backStack, FragmentTransactionUtils.TYPE4);
    }

    @Override
    public void showFragment(int id, Fragment fragment, String tag, boolean backStack, boolean hasSubFragment, String transactionType) {

    }


    @Override
    public void clearBackStackFragment(String tag) {

        Log.d("frtrrr", showingFragment.getClass().getName());
        try {

            for (int i = 0; i < mFragmentManager.getBackStackEntryCount(); ++i) {
                Fragment fragment = getSupportFragmentManager().findFragmentByTag(tag);
                if (fragment != null)
                    getSupportFragmentManager().beginTransaction().remove(fragment).commit();

            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void clearBackStack() {
        try {
            if ((this instanceof HomeActivity) && mFragmentManager.getBackStackEntryCount() > 0)
                for (int i = 0; i < mFragmentManager.getBackStackEntryCount(); ++i) {
                    FragmentManager.BackStackEntry first = mFragmentManager.getBackStackEntryAt(0);
                    mFragmentManager.popBackStack(first.getId(), FragmentManager.POP_BACK_STACK_INCLUSIVE);
                }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    public void hideActionBar(boolean bool) {
        if (bool) {
            findViewById(R.id.toolbar).setVisibility(View.GONE);
            ((AppBarLayout) findViewById(R.id.toolbar)).setExpanded(false, false);
        } else
            findViewById(R.id.toolbar).setVisibility(View.VISIBLE);
    }


    @Override
    public void changeAppState(int state) {
        Consts.APP_CURRENT_STATE = state;
        Log.d("baseactivity", "APP_CURRENT_STATE>>>>" + Consts.APP_CURRENT_STATE);
        Log.d("baseactivity", "details are>>>>" + RealmUtil.getFullName());
        SharedPreferenceUtil.putIntegerIntoStore(StoreKeys.APPLICATION_STATE, state);
        if (Consts.APP_CURRENT_STATE > 0) {
            mUserName.setText(RealmUtil.getFullName());
            mMembershipId.setText(RealmUtil.getMembershipId());
        }
        else{
            hideDrawerLayout();
        }

        populateHamburgerMenu();


    }

    public void hideDrawerLayout() {
        if (mDrawerLayout.isDrawerOpen(GravityCompat.END))
            mDrawerLayout.closeDrawer(GravityCompat.END);
    }


    public void showSnackBar(String msg){

    }

    public void getUSerType(){
        if(RealmUtil.isPrimaryCheckin()){
            Consts.IS_PRIMARYUSER_CHECKIN = true;
        }else
            Consts.IS_PRIMARYUSER_CHECKIN =false;

    }
    String token;


    @Override
    protected void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
        try {
            Bundle extras = intent.getExtras();

            if (extras != null) {
                String notificationType = extras.getString("Module");
                if (notificationType.equalsIgnoreCase(NotificationConfig.NOTIFICATION_CHECKOUT) /*&& !(showingFragment instanceof FeedbackFragment)*/) {
                    Uri notification = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
                    Ringtone r = RingtoneManager.getRingtone(this, notification);
                    r.play();
                    ViewUtils.showSnackBarMessage("Thank you for visiting EmiratePalace", this);
                    new Handler().postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            // showFragment(new FeedbackFragment());
                            //NavigationManager.getInstance().showFragment(Consts.ACC_FEEDBACK);
                            NavigationManager.loadView("feedback",BaseActivity.this, mListener);
                        }
                    }, 5000);
                    changeAppState(Consts.APP_LOGIN_STATE);
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (RealmUtil.IsUserLoggedin()){
            IsUserCheckedIn();
        }

    }

    public void sendNotificationDeviceId(String token) {
        String deviceId= token;

        Call<NotificationObject> configObj = ApiClient.getApiClient(FirebaseNotificationApiInterface.class).getAll(
                Consts.DEVICE_MAC_ID,Consts.DEVICE_TYPE,deviceId);
        configObj.enqueue(new Callback<NotificationObject>() {
            @Override
            public void onResponse(Call<NotificationObject> call, Response<NotificationObject> response) {
                if(response.body()!= null){
                    NotificationObject obj = response.body();
                    String id = obj.getResponseId();
                    Log.d("HOME ACTIVITY","FCM_TOKEN DATA...."+obj.toString());
                }
            }

            @Override
            public void onFailure(Call<NotificationObject> call, Throwable t) {
                t.printStackTrace();
            }
        });

    }

    String responseStatus, flag, message;
    String isCheckInStatus;
    boolean prevCheckedIn;
    public void IsUserCheckedIn() {
        if (Consts.APP_CURRENT_STATE > 0) {
            if (NetworkUtils.checkConnectivityStatus(this)) {
                Call<QR_DeviceRegisterConfigObj> statusObject = ApiClient.getApiClient(CheckDeviceStatusApiInterface.class).getAll(RealmUtil.getMembershipId(), Consts.DEVICE_MAC_ID);
                statusObject.enqueue(new Callback<QR_DeviceRegisterConfigObj>() {
                    @Override
                    public void onResponse(Call<QR_DeviceRegisterConfigObj> call, Response<QR_DeviceRegisterConfigObj> response) {
                        QR_DeviceRegisterConfigObj responseBody = response.body();
                        if(Consts.APP_CURRENT_STATE == Consts.APP_CHECKIN_STATE)
                            prevCheckedIn = true;
                        else
                            prevCheckedIn = false;

                        if (responseBody != null && RealmUtil.responseIsSuccess(responseBody)) {
                            QR_RegisterResponseStatus statusObj = responseBody.getResponseStatus();
                            isCheckInStatus = responseBody.getResponseData().getIsCheckedIn();
                            message = statusObj.getResponseMessage();

                            if (isCheckInStatus.equalsIgnoreCase("true")) {
                                flag = statusObj.getResponseFlag();

                                Realm realm = RealmUtil.getInstance();
                                realm.beginTransaction();
                                realm.delete(QR_RegisterResponseData.class);
                                realm.copyToRealmOrUpdate(responseBody);
                                realm.commitTransaction();
                                if (!prevCheckedIn)
                                    changeAppState(Consts.APP_CHECKIN_STATE);
                            } else{
                                if (Consts.APP_CURRENT_STATE == Consts.APP_CHECKIN_STATE) {
                                    if (RealmUtil.IsUserLoggedin()) {
                                        changeAppState(Consts.APP_LOGIN_STATE);
                                        if (RealmUtil.isPrimaryCheckin()) {
                                            Intent intent = new Intent(getApplicationContext(), HomeActivity.class);
                                            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                                            intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                                            startActivity(intent);
                                            NavigationManager.loadView("feedback", getBaseContext(), mListener);
                                        }else{
                                            Intent intent = new Intent(getApplicationContext(), HomeActivity.class);
                                           // intent.putExtra("login_message", message);
                                            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                                            intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                                            startActivity(intent);
                                        }

                                    } else
                                        changeAppState(Consts.APP_DEFAULT__STATE);

                                    RealmUtil.clearDataOnCheckout();
                                    Handler handler = new Handler(Looper.getMainLooper());
                                    handler.postDelayed(new Runnable() {
                                        @Override
                                        public void run() {
                                            //layout.setVisibility(View.GONE);
                                            ViewUtils.showSnackBarMessage(message);
                                        }
                                    }, 200);
                                }
                            }
                        } else {
                            ViewUtils.showSnackBarMessage(message != null ? message : "Unable to update check-in status");
                        }
                    }

                    @Override
                    public void onFailure(Call<QR_DeviceRegisterConfigObj> call, Throwable t) {
                        t.printStackTrace();
                    }
                });
            }else  Toast.makeText(this,"Check your network connectivity !",Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public void addFragment(int id, Fragment fragment, String tag, boolean backStack, String transactionType) {

        if (fragment == null)
            return;
        showingFragment = fragment;
        mFragmentManager = getSupportFragmentManager();
        fragmentTransaction = mFragmentManager.beginTransaction();
        FragmentTransactionUtils.getInstance().setTransaction(fragmentTransaction, transactionType);
        try {
            fragmentTransaction.add(id, fragment, tag);
            if (backStack)
                fragmentTransaction.addToBackStack(tag);
            clearBackStack();
            fragmentTransaction.commitAllowingStateLoss();
        } catch (IllegalStateException ex) {
            ex.printStackTrace();
        }
    }

    @Override
    public void addFragment(int id, Fragment fragment, String tag) {
        addFragment(id, fragment, tag, true, FragmentTransactionUtils.TYPE4);
    }

}
