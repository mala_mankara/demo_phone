package com.arowana.houdini.byod.view.fragments.myprofile;

import android.os.Bundle;


public interface UpdateProfileDataListener {
    void onUpdateProfileData(Bundle data);
}
