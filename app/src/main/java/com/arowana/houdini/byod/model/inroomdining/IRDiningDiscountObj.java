package com.arowana.houdini.byod.model.inroomdining;

import io.realm.RealmObject;

public class IRDiningDiscountObj extends RealmObject {

    private String DiscountCode;

    private String Description;

    private String EndDate;

    private String StartDate;

    private String DiscountPrice;

    private String DiscountMessage;

    private String EndTime;

    private String Id;

    private String StartTime;

    private String DiscountValue;

    public String getDiscountCode() {
        return DiscountCode;
    }

    public void setDiscountCode(String discountCode) {
        DiscountCode = discountCode;
    }

    public String getDescription() {
        return Description;
    }

    public void setDescription(String description) {
        Description = description;
    }

    public String getEndDate() {
        return EndDate;
    }

    public void setEndDate(String endDate) {
        EndDate = endDate;
    }

    public String getStartDate() {
        return StartDate;
    }

    public void setStartDate(String startDate) {
        StartDate = startDate;
    }

    public String getDiscountPrice() {
        return DiscountPrice;
    }

    public void setDiscountPrice(String discountPrice) {
        DiscountPrice = discountPrice;
    }

    public String getDiscountMessage() {
        return DiscountMessage;
    }

    public void setDiscountMessage(String discountMessage) {
        DiscountMessage = discountMessage;
    }

    public String getEndTime() {
        return EndTime;
    }

    public void setEndTime(String endTime) {
        EndTime = endTime;
    }

    public String getId() {
        return Id;
    }

    public void setId(String id) {
        Id = id;
    }

    public String getStartTime() {
        return StartTime;
    }

    public void setStartTime(String startTime) {
        StartTime = startTime;
    }

    public String getDiscountValue() {
        return DiscountValue;
    }

    public void setDiscountValue(String discountValue) {
        DiscountValue = discountValue;
    }
}
