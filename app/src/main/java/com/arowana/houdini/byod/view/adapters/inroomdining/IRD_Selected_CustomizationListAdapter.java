package com.arowana.houdini.byod.view.adapters.inroomdining;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.arowana.houdini.byod.R;
import com.arowana.houdini.byod.model.inroomdining.IRDiningCustomizationObj;

import io.realm.RealmList;

public class IRD_Selected_CustomizationListAdapter extends RecyclerView.Adapter<IRD_Selected_CustomizationListAdapter.MyViewHolder> {

    private RealmList<IRDiningCustomizationObj> selectedList;

    public IRD_Selected_CustomizationListAdapter( RealmList<IRDiningCustomizationObj> obj) {
        selectedList = new RealmList<>();
        for (IRDiningCustomizationObj customobj : obj) {
            if (customobj.getSelectedIndex() > -1)
                selectedList.add(customobj);
        }
    }

    public class MyViewHolder extends RecyclerView.ViewHolder{
        TextView name, value;

        public MyViewHolder(View itemView) {
            super(itemView);
            name = itemView.findViewById(R.id.ird_prompt_name);
            value = itemView.findViewById(R.id.ird_promt_value);
        }
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.promp_itemvalue_layout, parent, false);
        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, int position) {
        IRDiningCustomizationObj customobj = selectedList.get(position);
        if(customobj!= null){
            if(customobj.getCustomizeItem()!= null)
                holder.name.setText(customobj.getCustomizeItem());
            if(customobj.getCustomTypes().size()>0)
                holder.value.setText(customobj.getCustomTypes().get(customobj.getSelectedIndex()).getCustomTypeName());
        }
    }

    @Override
    public int getItemCount() {
        return selectedList.size() ;
    }
}
