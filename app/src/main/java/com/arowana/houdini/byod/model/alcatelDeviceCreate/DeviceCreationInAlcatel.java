package com.arowana.houdini.byod.model.alcatelDeviceCreate;

import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.POST;

public interface DeviceCreationInAlcatel {
    @FormUrlEncoded
    @POST("device")
    Call<Object> createDevice(@Field("deviceId") String deviceId, @Field("password") String password, @Field("userName") String userName, @Field("roomNumber") String roomNumber);
}
