package com.arowana.houdini.byod.model.speeddial;

import io.realm.RealmList;
import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

public class SpeedDialConfigObj extends RealmObject {
    @PrimaryKey
    private String RealmId = "1";

    private SpeedDialResponseStatus ResponseStatus;
    private RealmList<SpeedDialResponseData> ResponseData;

    public SpeedDialResponseStatus getResponseStatus() {
        return ResponseStatus;
    }

    public void setResponseStatus(SpeedDialResponseStatus responseStatus) {
        ResponseStatus = responseStatus;
    }

    public RealmList<SpeedDialResponseData> getResponseData() {
        return ResponseData;
    }

    public void setResponseData(RealmList<SpeedDialResponseData> responseData) {
        ResponseData = responseData;
    }
    @Override
    public String toString()
    {
        return "ClassPojo [ResponseStatus = "+ResponseStatus+", ResponseData = "+ResponseData+"]";
    }
}
