package com.arowana.houdini.byod.model.spa;

import io.realm.RealmList;
import io.realm.RealmObject;

public class SpaObject extends RealmObject {

    private String Description;

    private RealmList<SpaReviewsObj> Reviews;

    private RealmList<SpaTreatmentTypeObj> TreatmentType;

    private String SpaName;

    private String Id;

    private RealmList<SpaImagesObj> Images;
    private boolean isSelected;

    private RealmList<SpaAvailabilityTimeObj> AvailabilityTime;

    public RealmList<SpaAvailabilityTimeObj> getAvailabilityTime() {
        return AvailabilityTime;
    }

    public void setAvailabilityTime(RealmList<SpaAvailabilityTimeObj> availabilityTime) {
        AvailabilityTime = availabilityTime;
    }

    public String getDescription() {
        return Description;
    }

    public void setDescription(String description) {
        Description = description;
    }

    public RealmList<SpaReviewsObj> getReviews() {
        return Reviews;
    }

    public void setReviews(RealmList<SpaReviewsObj> reviews) {
        Reviews = reviews;
    }

    public RealmList<SpaTreatmentTypeObj> getTreatmentType() {
        return TreatmentType;
    }

    public void setTreatmentType(RealmList<SpaTreatmentTypeObj> treatmentType) {
        TreatmentType = treatmentType;
    }

    public String getSpaName() {
        return SpaName;
    }

    public void setSpaName(String spaName) {
        SpaName = spaName;
    }

    public String getId() {
        return Id;
    }

    public void setId(String id) {
        Id = id;
    }

    public RealmList<SpaImagesObj> getImages() {
        return Images;
    }

    public void setImages(RealmList<SpaImagesObj> images) {
        Images = images;
    }
}
