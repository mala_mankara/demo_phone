package com.arowana.houdini.byod.view.fragments.login;


import android.os.Bundle;
import android.text.Html;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.CheckBox;
import android.widget.TextView;

import androidx.annotation.Nullable;

import com.arowana.houdini.byod.Consts;
import com.arowana.houdini.byod.R;
import com.arowana.houdini.byod.interfaces.ApiClient;
import com.arowana.houdini.byod.interfaces.api_interface.LoginApiInterface;
import com.arowana.houdini.byod.model.login.LoginConfigObject;
import com.arowana.houdini.byod.utils.TextUtil;
import com.arowana.houdini.byod.utils.ViewUtils;
import com.arowana.houdini.byod.view.fragments.BaseFragment;
import com.google.android.material.textfield.TextInputLayout;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

import org.json.JSONArray;
import org.json.JSONObject;

import io.realm.Realm;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static android.content.Context.INPUT_METHOD_SERVICE;
import static com.arowana.houdini.byod.view.activity.LoginActivity.fragmentHolder;


public class SignupFragment extends BaseFragment implements View.OnClickListener {


    public SignupFragment() {

    }


    private View mRootView;
    private TextInputLayout mFirstName, mLastname, mEmail, mPhoneCode, mPhoneNumber;
    private TextView mBtnSignup, mNavigateLogin;
    private CheckBox subscriptionBox;
    private Realm realm;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setTitle(getResources().getString(R.string.signup));
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        mRootView = view;

        initComponents();
        initListeners();


    }

    public void initComponents() {

        mFirstName = mRootView.findViewById(R.id.signup_firstname);
        mLastname = mRootView.findViewById(R.id.signup_lastname);
        mEmail = mRootView.findViewById(R.id.signup_email);
        mPhoneCode = mRootView.findViewById(R.id.signup_code);
        mPhoneNumber = mRootView.findViewById(R.id.signup_ph_no);

        subscriptionBox = mRootView.findViewById(R.id.signUp_subscriptionchkBox);
        mBtnSignup = mRootView.findViewById(R.id.signup_btn_signup);
        mNavigateLogin = mRootView.findViewById(R.id.navigate_login_txt);

        mFirstName.getEditText().setHint(TextUtils.concat(getString(R.string.first_name), Html.fromHtml(getString(R.string.required_asterisk))));
        mLastname.getEditText().setHint(TextUtils.concat(getString(R.string.last_name), Html.fromHtml(getString(R.string.required_asterisk))));
        mEmail.getEditText().setHint(TextUtils.concat(getString(R.string.email), Html.fromHtml(getString(R.string.required_asterisk))));
        mPhoneCode.getEditText().setHint(TextUtils.concat(getString(R.string.code), Html.fromHtml(getString(R.string.required_asterisk))));
        mPhoneNumber.getEditText().setHint(TextUtils.concat(getString(R.string.phone_number), Html.fromHtml(getString(R.string.required_asterisk))));

    }


    public void initListeners() {
        mBtnSignup.setOnClickListener(this);
        mNavigateLogin.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
        int id = view.getId();

        InputMethodManager imm = (InputMethodManager)getActivity(). getSystemService(INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(view.getWindowToken(), 0);

        switch (id) {
            case R.id.signup_btn_signup:
                validate();
                break;

            case R.id.navigate_login_txt:
                mHandler.showFragment(fragmentHolder.getId(), new LoginFragment());
                break;
        }

    }


    public void validate() {
        if (!TextUtil.isEmpty(mFirstName, "FirstName required")) {
            if (!TextUtil.isEmpty(mLastname, "LastName required")) {
                if (!TextUtil.isEmpty(mEmail, "Email required")) {
                    if (!TextUtil.isEmpty(mPhoneCode, "Code required")) {
                        if (!TextUtil.isEmpty(mPhoneNumber, "PhoneNumber required")) {
                            if (TextUtil.isEmailValid(mEmail, "Enter valid Email")) {
                                if(TextUtil.validatePhoneNo(mPhoneNumber, "Enter Valid phone number")){
                                    submit();
                                }
                            }
                        }
                    }
                }
            }
        }
    }


    private void submit() {

        JsonObject gsonObject = new JsonObject();
        try {


            JSONObject jsonObject = new JSONObject();
            jsonObject.put("FirstName", mFirstName.getEditText().getText().toString());
            jsonObject.put("LastName", mLastname.getEditText().getText().toString());

            jsonObject.put("LoginType", "Houdini");
            jsonObject.put("MacId", Consts.DEVICE_MAC_ID);
            jsonObject.put("Subscriptions", subscriptionBox.isChecked()? true: false);

            // Phone Number Array
            String phone = mPhoneNumber.getEditText().getText().toString();
            String mCcode = mPhoneCode.getEditText().getText().toString();
            JSONArray jsonPhone = new JSONArray();
            JSONObject jsonPh = new JSONObject();
            jsonPh.put("MobileNumber", phone);
            jsonPh.put("CountryCode", mCcode);
            jsonPhone.put(0, jsonPh);
            jsonObject.put("MobileNumber", jsonPhone);

            JSONObject jsonEmail = new JSONObject();
            jsonEmail.put("PrimaryEmail", "" + mEmail.getEditText().getText().toString().trim());

            jsonObject.put("EmailId", jsonEmail);
            jsonObject.put("UserId", mEmail.getEditText().getText().toString().trim());

            JsonParser jsonParser = new JsonParser();
            gsonObject = (JsonObject) jsonParser.parse(jsonObject.toString());
            Log.d("test", "Request Input : " + jsonObject.toString());

            callAPI(gsonObject);

        } catch (Exception e) {
            e.printStackTrace();
        }


    }

    public void callAPI(JsonObject jsonObject) {
        final Call<LoginConfigObject> responseObj = ApiClient.getApiClient(LoginApiInterface.class).signup(jsonObject);
        responseObj.enqueue(new Callback<LoginConfigObject>() {
            @Override
            public void onResponse(Call<LoginConfigObject> call, Response<LoginConfigObject> response) {

                String flag = response.body().getResponseStatus().getResponseFlag();
                final String message = response.body().getResponseStatus().getResponseMessage();
                if (flag.equals("SUCCESS")) {
                    String membershipid = response.body().getResponseData().getMembershipId();
                  /*  LoginConfigObject obj = response.body();
                    Realm realm = Realm.getDefaultInstance();

                    realm.beginTransaction();
                    realm.copyToRealm(obj);

                    if (realm.where(LoginConfigObject.class).equalTo("RealmId", "1").count() == 0) {
                        realm.copyToRealm(obj);
                    }
                    realm.commitTransaction();

                    mListener.changeAppState(Consts.APP_LOGIN_STATE);*/


                    SignupConfirmationFragment fragment = new SignupConfirmationFragment();
                    fragment.setMsg(message, membershipid != null ? membershipid : "");
                    mHandler.showFragment(fragmentHolder.getId(), fragment);
                    mHandler.clearBackStackFragment(getTag());


                } else
                    ViewUtils.showSnackBarMessage(message, getActivity());
            }

            @Override
            public void onFailure(Call<LoginConfigObject> call, Throwable t) {
                t.printStackTrace();
            }
        });
    }


    @Override
    protected int getFragmentLayout() {
        return R.layout.fragment_signup;
    }
}
