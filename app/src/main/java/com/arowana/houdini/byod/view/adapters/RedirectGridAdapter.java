package com.arowana.houdini.byod.view.adapters;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.arowana.houdini.byod.Consts;
import com.arowana.houdini.byod.MainApplication;
import com.arowana.houdini.byod.R;
import com.arowana.houdini.byod.interfaces.listeners.FragmentNavigationHandler;
import com.arowana.houdini.byod.model.redirct.RedirectSubModule;
import com.arowana.houdini.byod.utils.FactoryUtil;
import com.arowana.houdini.byod.utils.ViewUtils;
import com.arowana.houdini.byod.view.activity.BaseActivity;
import com.arowana.houdini.byod.view.fragments.WebViewFragment;

import io.realm.RealmList;

import static com.arowana.houdini.byod.view.activity.RedirectActivity.fragmentHolder;

public class RedirectGridAdapter extends RecyclerView.Adapter<RedirectGridAdapter.GridViewHolder> {

    public static final String TAG = "RedirectGridAdapter";
    private Context context;
    private RealmList<RedirectSubModule> responsesList;
    private String key, headerData;
    private FragmentNavigationHandler handler;
    private ProgressDialog progressDialog;

    public RedirectGridAdapter(Context context, FragmentNavigationHandler mHandler, RealmList<RedirectSubModule> data, String key, String title, ProgressDialog mDialog) {
        this.context = context;
        responsesList = data;
        this.key = key;
        headerData = title;
        handler = mHandler;
        progressDialog = mDialog;
        progressDialog.show();
        Log.d(TAG, "SIZE in Adapter.." + responsesList.size() + "/KEy.." + key);
    }

    @NonNull
    @Override
    public RedirectGridAdapter.GridViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = null;
        progressDialog.show();
        if (key.equalsIgnoreCase(Consts.PARSE_FLIGHT_DATA)) {

            itemView = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.listitem, parent, false);
        } else if (key.equalsIgnoreCase(Consts.PARSE_SOCIAL_MEDIA_DATA)) {
            itemView = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.mediaitem, parent, false);
        }

        // set the view's size, margins, paddings and layout parameters
        return new RedirectGridAdapter.GridViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull RedirectGridAdapter.GridViewHolder holder, int position) {
        if(progressDialog.isShowing())
            progressDialog.cancel();
        RedirectSubModule submodule = responsesList.get(position);
        if (submodule == null)
            return;
        holder.title.setText(submodule.getTitle());
        if (submodule.getImage() != null) {
            if (submodule.getImage().getImageUrl() != null && FactoryUtil.isValidURL(submodule.getImage().getImageUrl()))
                FactoryUtil.loadImage(context, submodule.getImage().getImageUrl(), holder.thumbnail);
        }
    }


    @Override
    public int getItemCount() {
        return responsesList.size();
    }


    public class GridViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        private TextView title;
        private ImageView thumbnail;
        RelativeLayout row;


        GridViewHolder(View itemView) {
            super(itemView);
            title = itemView.findViewById(R.id.grid_title);
            thumbnail = itemView.findViewById(R.id.grid_image);
            row = itemView.findViewById(R.id.row_view);
            row.setOnClickListener(this);

        }

        public ImageView getImage() {
            return this.thumbnail;
        }

        @Override
        public void onClick(View v) {
            onItemClick(getAdapterPosition());
        }

        void onItemClick(int position) {
            if (responsesList.get(position) != null) {
                if (key.equalsIgnoreCase(Consts.PARSE_SOCIAL_MEDIA_DATA)) {
                    if (!responsesList.get(position).getTitle().equalsIgnoreCase("WhatsApp")) {
                        String url = responsesList.get(position).getSubModuleUrl();
//                        Intent intent = new Intent(Intent.ACTION_VIEW).setData(Uri.parse(url));
//                        context.startActivity(intent);
                        WebViewFragment webview = new WebViewFragment();
                        Bundle bundle = new Bundle();
                        bundle.putString("title", headerData);
                        bundle.putString("url", url);
                        webview.setArguments(bundle);
                        handler.showFragment(fragmentHolder.getId(), webview);

                    } else {
                        if (!MainApplication.appInstalledOrNot("com.whatsapp")) {
                            ViewUtils.showSnackBarMessage("Feature unavailable. WhatsApp needs to be installed",(BaseActivity)context);
                        } else {
                            String whatsAppNo = responsesList.get(position).getSubModuleUrl();
                            if (!(whatsAppNo == null && whatsAppNo.isEmpty())) {
                                FactoryUtil.openWhatsApp(context, whatsAppNo);
                            } else
                                ViewUtils.showSnackBarMessage("WhatsApp no. is not valid.");
                        }
                    }
                } else {
                    WebViewFragment webview = new WebViewFragment();
                    Bundle bundle = new Bundle();
                    bundle.putString("title", headerData);
                    bundle.putString("url", (responsesList.get(position)).getSubModuleUrl());
                    webview.setArguments(bundle);
                    handler.showFragment(fragmentHolder.getId(), webview);
                }
            }
        }
    }
}