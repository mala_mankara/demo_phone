package com.arowana.houdini.byod.view.adapters.spa;

import android.content.Context;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TableRow;
import android.widget.TextView;

import com.arowana.houdini.byod.R;
import com.arowana.houdini.byod.model.spa.SpaBookingTreatmentObj;
import com.arowana.houdini.byod.utils.DateUtil;

import java.text.NumberFormat;
import java.text.ParseException;

import io.realm.RealmList;

public class SpaBookingTreatmentsAdapter extends RecyclerView.Adapter<SpaBookingTreatmentsAdapter.MyViewHolder> {

    private Context context;
    private RealmList<SpaBookingTreatmentObj> mObj;


    public class MyViewHolder extends RecyclerView.ViewHolder {
        TextView date, time, treatmentName, femaleCount, maleCount, coupleCount,durationpack, price;
        TableRow femaleRow, maleRow, couplesRow;


        public MyViewHolder(View view) {
            super(view);

            date = view.findViewById(R.id.spa_booking_date);
            time = view.findViewById(R.id.spa_booking_time);
            treatmentName = view.findViewById(R.id.spa_booking_treatment_name);
            femaleCount = view.findViewById(R.id.spa_booking_female_count);
            maleCount = view.findViewById(R.id.spa_booking_male_count);
            coupleCount = view.findViewById(R.id.spa_booking_couple_count);
            price = view.findViewById(R.id.spa_booking_price);

            femaleRow = view.findViewById(R.id.female_row);
            maleRow = view.findViewById(R.id.male_row);
            couplesRow = view.findViewById(R.id.couple_row);

            durationpack = view.findViewById(R.id.spa_booking_time_pricepack);
        }


    }

    public SpaBookingTreatmentsAdapter(Context mContext, RealmList<SpaBookingTreatmentObj> obj) {
        this.context = mContext;
        this.mObj = obj;
    }


    @Override
    public SpaBookingTreatmentsAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_spa_booking_treatment, parent, false);

        return new SpaBookingTreatmentsAdapter.MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final SpaBookingTreatmentsAdapter.MyViewHolder holder, int position) {
        final SpaBookingTreatmentObj bean = mObj.get(position);


            holder.date.setText(DateUtil.getLocalDateFromUTC(bean.getRequestDate(), true));
            holder.time.setText(DateUtil.getLocalDateFromUTC(bean.getRequestDate(), false));

        holder.durationpack.setText(bean.getTime());
        // holder.time.setText(bean.getRequestTime());
        holder.treatmentName.setText(bean.getTreatmentName());
        if (bean.getCouplesTreatment().equals("true")) {
            holder.femaleRow.setVisibility(View.GONE);
            holder.maleRow.setVisibility(View.GONE);
            holder.couplesRow.setVisibility(View.VISIBLE);
            holder.coupleCount.setText(String.valueOf(bean.getCouplesCount()));
        } else {
            holder.femaleRow.setVisibility(View.VISIBLE);
            holder.maleRow.setVisibility(View.VISIBLE);
            holder.couplesRow.setVisibility(View.GONE);

            holder.femaleCount.setText(String.valueOf(bean.getFemalePax()));
            holder.maleCount.setText(String.valueOf(bean.getMalePax()));
        }

        try {
            holder.price.setText(NumberFormat.getInstance().parse(bean.getPrice()).toString());
        } catch (ParseException e) {
            e.printStackTrace();
        }
    }


    @Override
    public int getItemCount() {
        return mObj.size();
    }
}
