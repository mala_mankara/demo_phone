package com.arowana.houdini.byod.view.adapters.inroomdining;

import android.content.Context;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.TextView;

import com.arowana.houdini.byod.R;
import com.arowana.houdini.byod.interfaces.listeners.FragmentNavigationHandler;
import com.arowana.houdini.byod.model.inroomdining.IRDiningAvailabilityTimeObj;
import com.arowana.houdini.byod.model.inroomdining.RoomDiningInfoObj;
import com.arowana.houdini.byod.view.fragments.inroomdining.InRoomDiningMenuFragment;

import io.realm.RealmList;

public class InRoomDiningMainAdapter extends RecyclerView.Adapter<InRoomDiningMainAdapter.MyViewHolder> {


    private Context context;
    private RealmList<RoomDiningInfoObj> mDiningInfoObj;

    FragmentNavigationHandler mHandler;
    FrameLayout mHolder;


    public class MyViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        private TextView title, timings;
        private View mDivider;


        public MyViewHolder(View view) {
            super(view);
            title = view.findViewById(R.id.ir_dining_main_menu_title);
            timings = view.findViewById(R.id.ir_dining_menu_timings);
            mDivider = view.findViewById(R.id.ir_dining_main_menu_divider);

            view.setOnClickListener(this);

        }

        @Override
        public void onClick(View view) {
            InRoomDiningMenuFragment fragment = new InRoomDiningMenuFragment();

            fragment.setMenuObj(mDiningInfoObj.get(getAdapterPosition()).getMenus(), mDiningInfoObj.get(getAdapterPosition()).getCategoryName());
            mHandler.showFragment(mHolder.getId(), fragment);
        }
    }


    public InRoomDiningMainAdapter(Context mContext, RealmList<RoomDiningInfoObj> obj, FragmentNavigationHandler handler, FrameLayout holder) {
        this.context = mContext;
        this.mDiningInfoObj = obj;
        this.mHandler = handler;
        this.mHolder = holder;


    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_ir_dining_main_menu_component, parent, false);


        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, int position) {
        RoomDiningInfoObj obj = mDiningInfoObj.get(position);

        holder.title.setText(obj.getCategoryName());
       /* if (position == mDiningInfoObj.size()) {
            holder.mDivider.setVisibility(View.GONE);
        }*/

        String mTimings = "";
        for (IRDiningAvailabilityTimeObj timingsObj : obj.getAvailabilityTime()) {
            mTimings = timingsObj.getOpeningTime().concat(" - ").concat(timingsObj.getClosingTime());
        }
        holder.timings.setText(mTimings);


    }


    @Override
    public int getItemCount() {
        return mDiningInfoObj.size();
    }


}
