package com.arowana.houdini.byod.view.adapters.inroomdining;

import android.content.Context;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.arowana.houdini.byod.R;
import com.arowana.houdini.byod.interfaces.listeners.FragmentNavigationHandler;
import com.arowana.houdini.byod.model.inroomdining.IRDiningImagesObj;
import com.arowana.houdini.byod.model.inroomdining.IRDiningMenuItemsObj;
import com.arowana.houdini.byod.model.inroomdining.IRDiningUpsellingItemObj;
import com.arowana.houdini.byod.utils.RealmUtil;
import com.arowana.houdini.byod.view.adapters.ICart;
import com.squareup.picasso.Picasso;

import io.realm.Realm;
import io.realm.RealmList;
import io.realm.RealmResults;

public class InRoomDiningUpsellingAdapter extends RecyclerView.Adapter<InRoomDiningUpsellingAdapter.MyViewHolder> {


    private Context context;
    private RealmList<IRDiningUpsellingItemObj> upsellingObj;

    FragmentNavigationHandler mHandler;
    FrameLayout mHolder;
    Realm realm;
    LinearLayout layout;
    ICart cartListener;
    RealmList<IRDiningUpsellingItemObj> results1, results2;


    public class MyViewHolder extends RecyclerView.ViewHolder {
        private TextView title, price, btnAdd;
        private ImageView mImg;

        public MyViewHolder(View view) {
            super(view);
            title = view.findViewById(R.id.upselling_item_name);
            price = view.findViewById(R.id.upselling_price);
            btnAdd = view.findViewById(R.id.upselling_btn_add);
            mImg = view.findViewById(R.id.upselling_img);
        }
    }

    public InRoomDiningUpsellingAdapter(Context mContext, ICart listener, FragmentNavigationHandler handler, FrameLayout holder, LinearLayout upsellingLayout) {
        this.context = mContext;

        this.mHandler = handler;
        this.mHolder = holder;
        realm = RealmUtil.getInstance();
        cartListener = listener;

        results1 = new RealmList<>();
        final RealmResults<IRDiningMenuItemsObj> results = RealmUtil.findMenuItems().where().greaterThan("ItemCount", 0).findAll();
        for (IRDiningMenuItemsObj menuItem : results) {
            for (final IRDiningUpsellingItemObj upselling : menuItem.getUpsellingItems()) {
                if (!upselling.getId().equals(menuItem.getId())) {
                    realm.executeTransaction(new Realm.Transaction() {
                        @Override
                        public void execute(Realm realm) {

                            results1.add(upselling);
                        }
                    });
                }

            }
        }

        upsellingObj = results1;
        this.layout = upsellingLayout;
        layout.setVisibility(upsellingObj.size() > 0 ? View.VISIBLE : View.GONE);


    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_ir_dining_upselling_component, parent, false);


        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, int position) {
        final IRDiningUpsellingItemObj bean = upsellingObj.get(position);

        holder.title.setText(bean.getItemName());
        holder.price.setText(RealmUtil.getDiningCurrency().concat(" ").concat(bean.getPrice()));

        if (bean.getImages().size() > 0) {
            IRDiningImagesObj img = bean.getImages().get(0);
           /* Glide.with(context).load(img.getImageUrl()).apply(new RequestOptions()

                    .optionalFitCenter()).into(holder.mImg);*/

            Picasso.get().load(img.getImageUrl()).fit().into(holder.mImg);
        } else holder.mImg.setVisibility(View.GONE);

        holder.btnAdd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                realm.executeTransaction(new Realm.Transaction() {
                    @Override
                    public void execute(Realm realm) {

                        IRDiningMenuItemsObj obj = realm.where(IRDiningMenuItemsObj.class).equalTo("Id", bean.getId()).findAll().first();
                        obj.setSelected(true);
                        obj.setItemCount(1);
                        obj.setUpsellingItem(true);
                        cartListener.updateCartInfo();
                    }
                });
                notifyDataSetChanged();

            }
        });


    }


    @Override
    public int getItemCount() {
        return upsellingObj.size() > 0 ? upsellingObj.size() : 0;
    }


}
