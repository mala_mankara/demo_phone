package com.arowana.houdini.byod.view.fragments;


import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import androidx.annotation.Nullable;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.arowana.houdini.byod.Consts;
import com.arowana.houdini.byod.R;
import com.arowana.houdini.byod.view.activity.LoginActivity;
import com.arowana.houdini.byod.view.activity.OnboardingActivity;

public class LandingFragment extends BaseFragment implements View.OnClickListener {

    private View mRootView;
    private TextView mBtnLogin, mBtnSignup, mOnboardingTxt;
    private LinearLayout mBtnLater;
    private Context context;


    public LandingFragment() {
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        context = getActivity();
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        mRootView = view;


        initComponents();
        initListeners();


    }

    public void initComponents() {
        mBtnLogin = mRootView.findViewById(R.id.landing_btn_login);
        mBtnSignup = mRootView.findViewById(R.id.landing_btn_signup);
        mBtnLater = mRootView.findViewById(R.id.landing_txt_later);
        mOnboardingTxt = mRootView.findViewById(R.id.onboarding_text);
    }

    public void initListeners() {
        mBtnLogin.setOnClickListener(this);
        mBtnSignup.setOnClickListener(this);
        mBtnLater.setOnClickListener(this);
        mOnboardingTxt.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
        mListener.changeAppState(Consts.APP_DEFAULT__STATE);
        int id = view.getId();
        switch (id) {


            case R.id.landing_btn_login:
                Intent intent = new Intent(getActivity(), LoginActivity.class);
                intent.putExtra("fragment","login");
                startActivity(intent);

                break;

            case R.id.landing_btn_signup:
                Intent intentsignup = new Intent(getActivity(), LoginActivity.class);
                intentsignup.putExtra("fragment","signup");
                startActivity(intentsignup);

                break;

            case R.id.landing_txt_later:
                getActivity().finish();
                break;

            case R.id.onboarding_text:
                Intent intentonboarding = new Intent(getActivity(), OnboardingActivity.class);
                startActivity(intentonboarding);
                break;

        }

    }

    @Override
    protected int getFragmentLayout() {
        return R.layout.fragment_landing;
    }
}
