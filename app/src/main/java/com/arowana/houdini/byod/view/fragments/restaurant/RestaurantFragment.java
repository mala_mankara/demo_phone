package com.arowana.houdini.byod.view.fragments.restaurant;


import android.app.ProgressDialog;
import android.os.Bundle;
import android.view.View;

import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.arowana.houdini.byod.Consts;
import com.arowana.houdini.byod.R;
import com.arowana.houdini.byod.interfaces.ApiClient;
import com.arowana.houdini.byod.interfaces.api_interface.RestaurantApiInterface;
import com.arowana.houdini.byod.model.restaurant.RestaurantConfigObject;
import com.arowana.houdini.byod.utils.DialogUtil;
import com.arowana.houdini.byod.utils.ViewUtils;
import com.arowana.houdini.byod.view.adapters.restaurant.RestaurantAdapter;
import com.arowana.houdini.byod.view.fragments.BaseFragment;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.arowana.houdini.byod.view.activity.RestaurantActivity.OfferSize;
import static com.arowana.houdini.byod.view.activity.RestaurantActivity.fragmentHolder;


public class RestaurantFragment extends BaseFragment {


    private View mRootView;
    private RecyclerView mRecyclerView;
    private ProgressDialog mDialog;


    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        mRootView = view;
        mDialog = DialogUtil.showProgressDialog(getActivity(), "");
        setTitle("Restaurant & Bars");
        initComponents();
        fetchData();

    }

    @Override
    protected int getFragmentLayout() {
        return R.layout.fragment_restaurant;
    }

    public void initComponents() {
        mRecyclerView = mRootView.findViewById(R.id.restaurant_recyclerview);
    }

    public void fetchData() {
        mDialog.show();
        Call<RestaurantConfigObject> responseObj;


        responseObj = ApiClient.getApiClient(RestaurantApiInterface.class).getAllRestaurant(Consts.DEVICE_DENSITY, Consts.DEVICE_DENSITY);

        responseObj.enqueue(new Callback<RestaurantConfigObject>() {
            @Override
            public void onResponse(Call<RestaurantConfigObject> call, Response<RestaurantConfigObject> response) {
                if (mDialog.isShowing())
                    mDialog.dismiss();

                String flag = response.body().getResponseStatus().getResponseFlag();
                final String message = response.body().getResponseStatus().getResponseMessage();
                if (flag.equals("SUCCESS")) {
                    RestaurantConfigObject obj = response.body();
                    populateData(obj);
                } else ViewUtils.showSnackBarMessage(message);
            }

            @Override
            public void onFailure(Call<RestaurantConfigObject> call, Throwable t) {
                if (mDialog.isShowing())
                    mDialog.dismiss();
                t.printStackTrace();
            }
        });

    }

    public void populateData(RestaurantConfigObject obj) {
        LinearLayoutManager linearVertical = new LinearLayoutManager(getContext(), RecyclerView.VERTICAL, false);
        mRecyclerView.setLayoutManager(linearVertical);
        RestaurantAdapter adapter = new RestaurantAdapter(getContext(), obj, mHandler, fragmentHolder,OfferSize);
        mRecyclerView.setAdapter(adapter);
        adapter.notifyDataSetChanged();

    }
}
