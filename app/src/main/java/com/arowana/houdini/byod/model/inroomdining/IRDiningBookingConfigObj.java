package com.arowana.houdini.byod.model.inroomdining;

import io.realm.RealmObject;

public class IRDiningBookingConfigObj extends RealmObject{

    private IRDiningBookingResponseStatusObj ResponseStatus;

    private String GuestData;

    private String KotResponse;

    private IRDiningBookingObj ResponseData;

    public IRDiningBookingResponseStatusObj getResponseStatus ()
    {
        return ResponseStatus;
    }

    public void setResponseStatus (IRDiningBookingResponseStatusObj ResponseStatus)
    {
        this.ResponseStatus = ResponseStatus;
    }

    public String getGuestData ()
    {
        return GuestData;
    }

    public void setGuestData (String GuestData)
    {
        this.GuestData = GuestData;
    }

    public String getKotResponse ()
    {
        return KotResponse;
    }

    public void setKotResponse (String KotResponse)
    {
        this.KotResponse = KotResponse;
    }

    public IRDiningBookingObj getResponseData ()
    {
        return ResponseData;
    }

    public void setResponseData (IRDiningBookingObj ResponseData)
    {
        this.ResponseData = ResponseData;
    }

    @Override
    public String toString()
    {
        return "ClassPojo [ResponseStatus = "+ResponseStatus+", GuestData = "+GuestData+", KotResponse = "+KotResponse+", ResponseData = "+ResponseData+"]";
    }
}
