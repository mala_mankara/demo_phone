package com.arowana.houdini.byod.model.checkDeviceActivation;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

public class CheckDeviceResponseData extends RealmObject{
    @PrimaryKey
    private String RealmId = "1";

    private String UniqueCode;

    private String AdditionalDevices;

    private String MacId;

    private String PrimaryConnectionId;

    private String RoomNo;

    private String IsPrimary;

    private String LastName;

    private String DeviceConfigurations;

    private String ActivityResponse;

    private String IsCheckedIn;

    private String MobileNo;

    private String GuestName;

    private String ReservationNo;

    private String ActionType;

    private String MembershipId;

    private String FirstName;

    private String Id;

    public String getRealmId() {
        return RealmId;
    }

    public void setRealmId(String realmId) {
        RealmId = realmId;
    }

    public String getUniqueCode ()
    {
        return UniqueCode;
    }

    public void setUniqueCode (String UniqueCode)
    {
        this.UniqueCode = UniqueCode;
    }

    public String getAdditionalDevices ()
    {
        return AdditionalDevices;
    }

    public void setAdditionalDevices (String AdditionalDevices)
    {
        this.AdditionalDevices = AdditionalDevices;
    }

    public String getMacId ()
    {
        return MacId;
    }

    public void setMacId (String MacId)
    {
        this.MacId = MacId;
    }

    public String getPrimaryConnectionId ()
    {
        return PrimaryConnectionId;
    }

    public void setPrimaryConnectionId (String PrimaryConnectionId)
    {
        this.PrimaryConnectionId = PrimaryConnectionId;
    }

    public String getRoomNo ()
    {
        return RoomNo;
    }

    public void setRoomNo (String RoomNo)
    {
        this.RoomNo = RoomNo;
    }

    public String getIsPrimary ()
    {
        return IsPrimary;
    }

    public void setIsPrimary (String IsPrimary)
    {
        this.IsPrimary = IsPrimary;
    }

    public String getLastName ()
    {
        return LastName;
    }

    public void setLastName (String LastName)
    {
        this.LastName = LastName;
    }

    public String getDeviceConfigurations ()
    {
        return DeviceConfigurations;
    }

    public void setDeviceConfigurations (String DeviceConfigurations)
    {
        this.DeviceConfigurations = DeviceConfigurations;
    }

    public String getActivityResponse ()
    {
        return ActivityResponse;
    }

    public void setActivityResponse (String ActivityResponse)
    {
        this.ActivityResponse = ActivityResponse;
    }

    public String getIsCheckedIn ()
    {
        return IsCheckedIn;
    }

    public void setIsCheckedIn (String IsCheckedIn)
    {
        this.IsCheckedIn = IsCheckedIn;
    }

    public String getMobileNo ()
    {
        return MobileNo;
    }

    public void setMobileNo (String MobileNo)
    {
        this.MobileNo = MobileNo;
    }

    public String getGuestName ()
    {
        return GuestName;
    }

    public void setGuestName (String GuestName)
    {
        this.GuestName = GuestName;
    }

    public String getReservationNo ()
    {
        return ReservationNo;
    }

    public void setReservationNo (String ReservationNo)
    {
        this.ReservationNo = ReservationNo;
    }

    public String getActionType ()
    {
        return ActionType;
    }

    public void setActionType (String ActionType)
    {
        this.ActionType = ActionType;
    }

    public String getMembershipId ()
    {
        return MembershipId;
    }

    public void setMembershipId (String MembershipId)
    {
        this.MembershipId = MembershipId;
    }

    public String getFirstName ()
    {
        return FirstName;
    }

    public void setFirstName (String FirstName)
    {
        this.FirstName = FirstName;
    }

    public String getId ()
    {
        return Id;
    }

    public void setId (String Id)
    {
        this.Id = Id;
    }

    @Override
    public String toString()
    {
        return "ClassPojo [UniqueCode = "+UniqueCode+", AdditionalDevices = "+AdditionalDevices+", MacId = "+MacId+", PrimaryConnectionId = "+PrimaryConnectionId+", RoomNo = "+RoomNo+", IsPrimary = "+IsPrimary+", LastName = "+LastName+", DeviceConfigurations = "+DeviceConfigurations+", ActivityResponse = "+ActivityResponse+", IsCheckedIn = "+IsCheckedIn+", MobileNo = "+MobileNo+", GuestName = "+GuestName+", ReservationNo = "+ReservationNo+", ActionType = "+ActionType+", MembershipId = "+MembershipId+", FirstName = "+FirstName+", Id = "+Id+"]";
    }
}
