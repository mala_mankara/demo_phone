package com.arowana.houdini.byod.interfaces.listeners;


import android.app.FragmentManager;
import androidx.fragment.app.Fragment;

public interface FragmentNavigationHandler {
    void showFragment(int id, Fragment fragment);

    void showFragment(int id,Fragment fragment, String tag);

    void addFragment(int id,Fragment fragment, String tag);

    void showFragment(int id,Fragment fragment, String tag, boolean backStack);

    void showFragment(int id,Fragment fragment, String tag, boolean backStack, String transactionType);

    void showFragment(int id,Fragment fragment, String tag, boolean backStack, boolean hasSubFragment, String transactionType);

    void addFragment(int id,Fragment fragment, String tag, boolean backStack, String transactionType);

    void clearBackStackFragment(String tag);

    FragmentManager getFragmentManager();


}
