package com.arowana.houdini.byod.model.speeddial;

import io.realm.RealmObject;

public class SpeedDialResponseStatus extends RealmObject{

    private String ResponseFlag;

    private String ResponseId;

    private String ResponseCode;

    private String ResponseMessage;

    public String getResponseFlag ()
    {
        return ResponseFlag;
    }

    public void setResponseFlag (String ResponseFlag)
    {
        this.ResponseFlag = ResponseFlag;
    }

    public String getResponseId ()
    {
        return ResponseId;
    }

    public void setResponseId (String ResponseId)
    {
        this.ResponseId = ResponseId;
    }

    public String getResponseCode ()
    {
        return ResponseCode;
    }

    public void setResponseCode (String ResponseCode)
    {
        this.ResponseCode = ResponseCode;
    }

    public String getResponseMessage ()
    {
        return ResponseMessage;
    }

    public void setResponseMessage (String ResponseMessage)
    {
        this.ResponseMessage = ResponseMessage;
    }

}
