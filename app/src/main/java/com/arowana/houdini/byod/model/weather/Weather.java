package com.arowana.houdini.byod.model.weather;


import io.realm.RealmList;

public class Weather {
    private RealmList<Forecast> Forecast;

    private Current Current;

    public RealmList<Forecast> getForecast() {
        return Forecast;
    }

    public void setForecast(RealmList<Forecast> forecast) {
        Forecast = forecast;
    }

    public Current getCurrent ()
    {
        return Current;
    }

    public void setCurrent (Current Current)
    {
        this.Current = Current;
    }



    public class Current {
        private String Description;

        private String Maximum;

        private String WindDirection;

        private String Minimum;

        private String Temperature;

        private String IconCode;

        private String Humidity;

        private String WindSpeed;

        private String Pressure;

        private String Title;

        private String IconImage;

        public String getDescription ()
        {
            return Description;
        }

        public void setDescription (String Description)
        {
            this.Description = Description;
        }

        public String getMaximum ()
        {
            return Maximum;
        }

        public void setMaximum (String Maximum)
        {
            this.Maximum = Maximum;
        }

        public String getWindDirection ()
        {
            return WindDirection;
        }

        public void setWindDirection (String WindDirection)
        {
            this.WindDirection = WindDirection;
        }

        public String getMinimum ()
        {
            return Minimum;
        }

        public void setMinimum (String Minimum)
        {
            this.Minimum = Minimum;
        }

        public String getTemperature ()
        {
            return Temperature;
        }

        public void setTemperature (String Temperature)
        {
            this.Temperature = Temperature;
        }

        public String getIconCode ()
        {
            return IconCode;
        }

        public void setIconCode (String IconCode)
        {
            this.IconCode = IconCode;
        }

        public String getHumidity ()
        {
            return Humidity;
        }

        public void setHumidity (String Humidity)
        {
            this.Humidity = Humidity;
        }

        public String getWindSpeed ()
        {
            return WindSpeed;
        }

        public void setWindSpeed (String WindSpeed)
        {
            this.WindSpeed = WindSpeed;
        }

        public String getPressure ()
        {
            return Pressure;
        }

        public void setPressure (String Pressure)
        {
            this.Pressure = Pressure;
        }

        public String getTitle ()
        {
            return Title;
        }

        public void setTitle (String Title)
        {
            this.Title = Title;
        }

        public String getIconImage ()
        {
            return IconImage;
        }

        public void setIconImage (String IconImage)
        {
            this.IconImage = IconImage;
        }

        @Override
        public String toString()
        {
            return "ClassPojo [Description = "+Description+", Maximum = "+Maximum+", WindDirection = "+WindDirection+", Minimum = "+Minimum+", Temperature = "+Temperature+", IconCode = "+IconCode+", Humidity = "+Humidity+", WindSpeed = "+WindSpeed+", Pressure = "+Pressure+", Title = "+Title+", IconImage = "+IconImage+"]";
        }
    }
}
