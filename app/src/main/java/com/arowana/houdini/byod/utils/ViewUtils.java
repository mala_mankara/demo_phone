package com.arowana.houdini.byod.utils;

import android.app.Activity;
import android.os.Handler;
import android.os.Looper;
import android.util.Log;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.view.inputmethod.InputMethodManager;
import android.widget.FrameLayout;
import android.widget.TextView;

import com.arowana.houdini.byod.MainApplication;
import com.arowana.houdini.byod.R;

public class ViewUtils {


    public static void showSnackBarMessage(final String msg) {

        Log.d("msggggg", "---" + msg);
        Handler handler = new Handler(Looper.getMainLooper());
        handler.post(new Runnable() {
            @Override
            public void run() {
                Activity activity = MainApplication.getCurrentActivity();

                final FrameLayout layout = activity.findViewById(R.id.snacbar_layout);
                layout.setVisibility(View.VISIBLE);
                // layout.bringToFront();
                Animation up = AnimationUtils.loadAnimation(activity.getApplicationContext(), R.anim.slide);

                View view = activity.getLayoutInflater().inflate(R.layout.custom_snackbar_layout, null);
                TextView mOkMessage = view.findViewById(R.id.snackbar_msg);
                mOkMessage.setText(msg);
                layout.addView(view);
                layout.startAnimation(up);

                Handler handler = new Handler(Looper.getMainLooper());
                handler.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        //layout.setVisibility(View.GONE);
                        layout.removeAllViews();
                        layout.setVisibility(View.GONE);
                    }
                }, 4000);
            }
        });
    }

    public static void showSnackBarMessage(String msg, Activity activity) {

        Log.d("msggggg", "---" + msg);
        if (activity == null) {
            activity = MainApplication.getCurrentActivity();
        }
        final FrameLayout layout = activity.findViewById(R.id.snacbar_layout);
        layout.setVisibility(View.VISIBLE);

        Animation up = AnimationUtils.loadAnimation(activity.getApplicationContext(), R.anim.slide);

        View view = activity.getLayoutInflater().inflate(R.layout.custom_snackbar_layout, null);
        TextView mOkMessage = view.findViewById(R.id.snackbar_msg);
        mOkMessage.setText(msg);
        layout.addView(view);
        layout.startAnimation(up);

        Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                //layout.setVisibility(View.GONE);
                layout.removeAllViews();
                layout.setVisibility(View.GONE);
            }
        }, 4000);

    }

    public static void hideSoftKeyboard(View view) {
        Activity   activity = MainApplication.getCurrentActivity();
        InputMethodManager inputMethodManager =
                (InputMethodManager) activity.getSystemService(
                        Activity.INPUT_METHOD_SERVICE);
        inputMethodManager.hideSoftInputFromWindow(
                view.getWindowToken(), 0);
    }



}
