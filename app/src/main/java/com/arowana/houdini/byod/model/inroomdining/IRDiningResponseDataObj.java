package com.arowana.houdini.byod.model.inroomdining;

import io.realm.RealmList;
import io.realm.RealmObject;

public class IRDiningResponseDataObj extends RealmObject{

    private RealmList<RoomDiningInfoObj> RoomDiningInfo;

    private String CurrencyShortName;

    private String CurrencySymbol;

    public RealmList<RoomDiningInfoObj> getRoomDiningInfo() {
        return RoomDiningInfo;
    }

    public void setRoomDiningInfo(RealmList<RoomDiningInfoObj> roomDiningInfo) {
        RoomDiningInfo = roomDiningInfo;
    }

    public String getCurrencyShortName() {
        return CurrencyShortName;
    }

    public void setCurrencyShortName(String currencyShortName) {
        CurrencyShortName = currencyShortName;
    }

    public String getCurrencySymbol() {
        return CurrencySymbol;
    }

    public void setCurrencySymbol(String currencySymbol) {
        CurrencySymbol = currencySymbol;
    }
}
