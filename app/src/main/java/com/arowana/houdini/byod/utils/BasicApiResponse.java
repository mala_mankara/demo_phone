package com.arowana.houdini.byod.utils;

import io.realm.RealmObject;

public class BasicApiResponse extends RealmObject {
    private ApiResponseStatus ResponseStatus;

    public ApiResponseStatus getResponseStatus() {
        return ResponseStatus;
    }

    public void setResponseStatus(ApiResponseStatus responseStatus) {
        ResponseStatus = responseStatus;
    }
}
