package com.arowana.houdini.byod.model.cityguide;

import io.realm.RealmList;
import io.realm.RealmObject;

public class CityGuideDetailsObj extends RealmObject{

    private String Website;

    private String Contact;

    private String FromDate;

    private RealmList<CityGuideImagesObj> Images;

    private String CloseHours;

    private String EntryFees;

    private String Title;

    private String CityGuideCategoryId;

    private String Tags;

    private String ToDate;

    private String ThingsToBuy;

    private String OpenHours;

    private String Address;

    private String Latitude;

    private String Id;

    private String DistanceFromHotel;

    private String Longitude;

    private String Overview;

    private String Specialities;


    public String getWebsite() {
        return Website;
    }

    public void setWebsite(String website) {
        Website = website;
    }

    public String getContact() {
        return Contact;
    }

    public void setContact(String contact) {
        Contact = contact;
    }

    public String getFromDate() {
        return FromDate;
    }

    public void setFromDate(String fromDate) {
        FromDate = fromDate;
    }

    public RealmList<CityGuideImagesObj> getImages() {
        return Images;
    }

    public void setImages(RealmList<CityGuideImagesObj> images) {
        Images = images;
    }

    public String getCloseHours() {
        return CloseHours;
    }

    public void setCloseHours(String closeHours) {
        CloseHours = closeHours;
    }

    public String getEntryFees() {
        return EntryFees;
    }

    public void setEntryFees(String entryFees) {
        EntryFees = entryFees;
    }

    public String getTitle() {
        return Title;
    }

    public void setTitle(String title) {
        Title = title;
    }

    public String getCityGuideCategoryId() {
        return CityGuideCategoryId;
    }

    public void setCityGuideCategoryId(String cityGuideCategoryId) {
        CityGuideCategoryId = cityGuideCategoryId;
    }

    public String getTags() {
        return Tags;
    }

    public void setTags(String tags) {
        Tags = tags;
    }

    public String getToDate() {
        return ToDate;
    }

    public void setToDate(String toDate) {
        ToDate = toDate;
    }

    public String getThingsToBuy() {
        return ThingsToBuy;
    }

    public void setThingsToBuy(String thingsToBuy) {
        ThingsToBuy = thingsToBuy;
    }

    public String getOpenHours() {
        return OpenHours;
    }

    public void setOpenHours(String openHours) {
        OpenHours = openHours;
    }

    public String getAddress() {
        return Address;
    }

    public void setAddress(String address) {
        Address = address;
    }

    public String getLatitude() {
        return Latitude;
    }

    public void setLatitude(String latitude) {
        Latitude = latitude;
    }

    public String getId() {
        return Id;
    }

    public void setId(String id) {
        Id = id;
    }

    public String getDistanceFromHotel() {
        return DistanceFromHotel;
    }

    public void setDistanceFromHotel(String distanceFromHotel) {
        DistanceFromHotel = distanceFromHotel;
    }

    public String getLongitude() {
        return Longitude;
    }

    public void setLongitude(String longitude) {
        Longitude = longitude;
    }

    public String getOverview() {
        return Overview;
    }

    public void setOverview(String overview) {
        Overview = overview;
    }

    public String getSpecialities() {
        return Specialities;
    }

    public void setSpecialities(String specialities) {
        Specialities = specialities;
    }
}
