package com.arowana.houdini.byod.utils;

import android.widget.EditText;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class UserInputValidations {

    public static boolean isEmailValid(EditText emailTxt) {
        if (emailTxt.getText().toString().equals("")) {
            return false;
        } else {
            String editEmail = emailTxt.getText().toString();
            Pattern pattern1 = Pattern.compile("^([a-zA-Z0-9_.-])+@([a-zA-Z0-9_.-])+\\.([a-zA-Z])+([a-zA-Z])+");
            Matcher matcher1 = pattern1.matcher(editEmail);
            return matcher1.matches();
        }
    }

    public static boolean isValidMobile(String phone) {
        if (phone.equalsIgnoreCase("0000000000") || (phone.length() < 10))
            return false;
        else
            return android.util.Patterns.PHONE.matcher(phone).matches();
    }
}
