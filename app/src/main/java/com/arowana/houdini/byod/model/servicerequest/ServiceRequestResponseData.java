package com.arowana.houdini.byod.model.servicerequest;

import io.realm.RealmList;
import io.realm.RealmObject;

public class ServiceRequestResponseData extends RealmObject{

    private String ServiceTypeId;

    private String HotelId;

    private String ActivityId;

    private String ServiceType;

    private RealmList<Images> Images;

    private String GroupId;

    private String LanguageCode;

    private String ServiceId;

    private String MessageCode;

    private String Version;

    private String MaxRequestQuantity;

    private String Tenant;

    private String ServiceItem;

    private String Description;

    private String ModifiedBy;

    private String IsReadOnly;

    private String BrandCode;

    private String ResidentService;

    private String BrandId;

    private String ModifiedDate;

    private String Status;

    private String HotelCode;

    private String GroupCode;

    private String CreatedBy;

    private String CreatedDate;

    private RealmList<AvailTime> AvailTime;

    private String Id;

    //for Rrealm Object......start
    private int pos = -1;
    private int itemCount;
    private Boolean DefaultStayFeature;


    public Boolean isDefaultStayFeature() {
        return DefaultStayFeature;
    }

    public void setDefaultStayFeature(Boolean defaultStayFeature) {
        DefaultStayFeature = defaultStayFeature;
    }


    public int getPos() {
        return pos;
    }

    public void setPos(int pos) {
        this.pos = pos;
    }


    public int getItemCount() {
        return itemCount;
    }

    public void setItemCount(int itemCount) {
        this.itemCount = itemCount;
    }

    //end

    public String getServiceTypeId ()
    {
        return ServiceTypeId;
    }

    public void setServiceTypeId (String ServiceTypeId)
    {
        this.ServiceTypeId = ServiceTypeId;
    }

    public String getHotelId ()
    {
        return HotelId;
    }

    public void setHotelId (String HotelId)
    {
        this.HotelId = HotelId;
    }

    public String getActivityId ()
    {
        return ActivityId;
    }

    public void setActivityId (String ActivityId)
    {
        this.ActivityId = ActivityId;
    }

    public String getServiceType ()
    {
        return ServiceType;
    }

    public void setServiceType (String ServiceType)
    {
        this.ServiceType = ServiceType;
    }

    public RealmList<Images> getImages ()
    {
        return Images;
    }

    public void setImages (RealmList<Images> Images)
    {
        this.Images = Images;
    }

    public String getGroupId ()
    {
        return GroupId;
    }

    public void setGroupId (String GroupId)
    {
        this.GroupId = GroupId;
    }

    public String getLanguageCode ()
    {
        return LanguageCode;
    }

    public void setLanguageCode (String LanguageCode)
    {
        this.LanguageCode = LanguageCode;
    }

    public String getServiceId ()
    {
        return ServiceId;
    }

    public void setServiceId (String ServiceId)
    {
        this.ServiceId = ServiceId;
    }

    public String getMessageCode ()
    {
        return MessageCode;
    }

    public void setMessageCode (String MessageCode)
    {
        this.MessageCode = MessageCode;
    }

    public String getVersion ()
    {
        return Version;
    }

    public void setVersion (String Version)
    {
        this.Version = Version;
    }

    public String getMaxRequestQuantity ()
    {
        return MaxRequestQuantity;
    }

    public void setMaxRequestQuantity (String MaxRequestQuantity)
    {
        this.MaxRequestQuantity = MaxRequestQuantity;
    }

    public String getTenant ()
    {
        return Tenant;
    }

    public void setTenant (String Tenant)
    {
        this.Tenant = Tenant;
    }

    public String getServiceItem ()
    {
        return ServiceItem;
    }

    public void setServiceItem (String ServiceItem)
    {
        this.ServiceItem = ServiceItem;
    }

    public String getDescription ()
    {
        return Description;
    }

    public void setDescription (String Description)
    {
        this.Description = Description;
    }

    public String getModifiedBy ()
    {
        return ModifiedBy;
    }

    public void setModifiedBy (String ModifiedBy)
    {
        this.ModifiedBy = ModifiedBy;
    }

    public String getIsReadOnly ()
    {
        return IsReadOnly;
    }

    public void setIsReadOnly (String IsReadOnly)
    {
        this.IsReadOnly = IsReadOnly;
    }

    public String getBrandCode ()
    {
        return BrandCode;
    }

    public void setBrandCode (String BrandCode)
    {
        this.BrandCode = BrandCode;
    }

    public String getResidentService ()
    {
        return ResidentService;
    }

    public void setResidentService (String ResidentService)
    {
        this.ResidentService = ResidentService;
    }

    public String getBrandId ()
    {
        return BrandId;
    }

    public void setBrandId (String BrandId)
    {
        this.BrandId = BrandId;
    }

    public String getModifiedDate ()
    {
        return ModifiedDate;
    }

    public void setModifiedDate (String ModifiedDate)
    {
        this.ModifiedDate = ModifiedDate;
    }

    public String getStatus ()
    {
        return Status;
    }

    public void setStatus (String Status)
    {
        this.Status = Status;
    }

    public String getHotelCode ()
    {
        return HotelCode;
    }

    public void setHotelCode (String HotelCode)
    {
        this.HotelCode = HotelCode;
    }

    public String getGroupCode ()
    {
        return GroupCode;
    }

    public void setGroupCode (String GroupCode)
    {
        this.GroupCode = GroupCode;
    }

    public String getCreatedBy ()
    {
        return CreatedBy;
    }

    public void setCreatedBy (String CreatedBy)
    {
        this.CreatedBy = CreatedBy;
    }

    public String getCreatedDate ()
    {
        return CreatedDate;
    }

    public void setCreatedDate (String CreatedDate)
    {
        this.CreatedDate = CreatedDate;
    }

    public RealmList<AvailTime> getAvailTime ()
    {
        return AvailTime;
    }

    public void setAvailTime (RealmList<AvailTime> AvailTime)
    {
        this.AvailTime = AvailTime;
    }

    public String getId ()
    {
        return Id;
    }

    public void setId (String Id)
    {
        this.Id = Id;
    }
}
