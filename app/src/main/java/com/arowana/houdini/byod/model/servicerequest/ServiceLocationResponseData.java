package com.arowana.houdini.byod.model.servicerequest;

import io.realm.RealmObject;

public class ServiceLocationResponseData extends RealmObject{
    private String Place;

    private String Id;

    public String getPlace ()
    {
        return Place;
    }

    public void setPlace (String Place)
    {
        this.Place = Place;
    }

    public String getId ()
    {
        return Id;
    }

    public void setId (String Id)
    {
        this.Id = Id;
    }


}
