package com.arowana.houdini.byod.view.adapters;

import android.content.Context;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.arowana.houdini.byod.R;
import com.arowana.houdini.byod.model.home.HomeGridViewObject;
import com.arowana.houdini.byod.utils.JSONUtil;
import com.arowana.houdini.byod.utils.NavigationManager;
import com.arowana.houdini.byod.view.activity.HomeActivity;

import io.realm.RealmList;

import static com.arowana.houdini.byod.view.activity.BaseActivity.mListener;

public class HomeGridAdapter extends RecyclerView.Adapter<HomeGridAdapter.MyViewHolder> {

    private Context context;
    private RealmList<HomeGridViewObject> mGridObj;
    String currstate;

    public class MyViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener{
        public TextView title;
        public ImageView thumbnail;

        public MyViewHolder(View view) {
            super(view);
            title = view.findViewById(R.id.home_grid_title);
            thumbnail = view.findViewById(R.id.home_grid_icon);
            view.setOnClickListener(this);
        }

        @Override
        public void onClick(View view) {
//            Log.d(TAG,"on click Position Data ..."+mGridObj.get(getAdapterPosition()));
           NavigationManager.loadView(mGridObj.get(getAdapterPosition()).getKey(),context,mListener);

        }
    }


    public HomeGridAdapter(HomeActivity mContext, RealmList<HomeGridViewObject> obj, String currState) {
        this.context = mContext;
        this.mGridObj = obj;
        this.currstate = currstate;
    }
    /*public HomeGridAdapter(Context mContext, RealmList<HomeGridViewObject> obj, String currstate) {
        this.context = mContext;
        this.mGridObj = obj;
        this.currstate = currstate;
    }*/

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_home_grid, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, int position) {
        HomeGridViewObject obj = mGridObj.get(position);

        holder.title.setText(obj.getTitle());

        if(obj.getImage()!=null)
            holder.thumbnail.setImageDrawable(JSONUtil.GetDrawableByName(obj.getImage()));

    }


    @Override
    public int getItemCount() {
        return mGridObj.size();
    }


}
