package com.arowana.houdini.byod.interfaces.api_interface;

import com.arowana.houdini.byod.model.redirct.RedirctConfigObj;

import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.POST;

public interface RedirectApiInterface {

    @FormUrlEncoded
    @POST("RedirectUrl/GetAll")
    Call<RedirctConfigObj>fetchAll(@Field("ImageSize") String imageSize);
}
