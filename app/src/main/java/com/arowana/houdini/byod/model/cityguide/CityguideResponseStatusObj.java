package com.arowana.houdini.byod.model.cityguide;

import io.realm.RealmObject;

public class CityguideResponseStatusObj extends RealmObject{


    private String ResponseFlag;

    private String ResponseId;

    private String ResponseCode;

    private String ResponseMessage;

    public String getResponseFlag() {
        return ResponseFlag;
    }

    public void setResponseFlag(String responseFlag) {
        ResponseFlag = responseFlag;
    }

    public String getResponseId() {
        return ResponseId;
    }

    public void setResponseId(String responseId) {
        ResponseId = responseId;
    }

    public String getResponseCode() {
        return ResponseCode;
    }

    public void setResponseCode(String responseCode) {
        ResponseCode = responseCode;
    }

    public String getResponseMessage() {
        return ResponseMessage;
    }

    public void setResponseMessage(String responseMessage) {
        ResponseMessage = responseMessage;
    }
}
