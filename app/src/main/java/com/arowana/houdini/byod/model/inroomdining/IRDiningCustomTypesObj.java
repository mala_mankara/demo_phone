package com.arowana.houdini.byod.model.inroomdining;

import io.realm.RealmObject;

public class IRDiningCustomTypesObj extends RealmObject {

    private String CustomTypeId;

    private String CustomTypeName;

    private boolean isSelected;

    public String getCustomTypeId() {
        return CustomTypeId;
    }

    public void setCustomTypeId(String customTypeId) {
        CustomTypeId = customTypeId;
    }

    public String getCustomTypeName() {
        return CustomTypeName;
    }

    public void setCustomTypeName(String customTypeName) {
        CustomTypeName = customTypeName;
    }

    public boolean isSelected() {
        return isSelected;
    }

    public void setSelected(boolean selected) {
        isSelected = selected;
    }
}
