package com.arowana.houdini.byod.view.activity;

import android.os.Bundle;
import android.widget.FrameLayout;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;

import com.arowana.houdini.byod.MainApplication;
import com.arowana.houdini.byod.R;
import com.arowana.houdini.byod.interfaces.listeners.UpdateBundleListener;
import com.arowana.houdini.byod.interfaces.listeners.UpdatePreviousScreenListener;
import com.arowana.houdini.byod.utils.RealmUtil;
import com.arowana.houdini.byod.view.fragments.inroomdining.IRDiningBookingDetailsFragment;
import com.arowana.houdini.byod.view.fragments.inroomdining.InRoomDiningMainFragment;

public class InRoomDiningActivity extends BaseActivity implements UpdatePreviousScreenListener{

    public static FrameLayout fragmentHolder;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_module_layout);
        MainApplication.setCurrentActivity(this);
        RealmUtil.deleteCartRealm();
        RealmUtil.deleteDiningRealm();
        initComponents();
    }

    public void initComponents() {

        fragmentHolder = findViewById(R.id.fragment_holder);
        loadFragments();

    }

    public void loadFragments() {
        showFragment(fragmentHolder.getId(), new InRoomDiningMainFragment());

    }

    @Override
    public void onBackPressed() {
        Fragment fragment = getSupportFragmentManager().findFragmentById(R.id.fragment_holder);
        if (fragment instanceof IRDiningBookingDetailsFragment) {
            RealmUtil.deleteDiningRealm();
            RealmUtil.deleteCartRealm();
            finish();
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public void updatePreviousScreen(Bundle bundle) {
        super.onBackPressed();
        FragmentManager mgr = getSupportFragmentManager();
        Fragment fragment = mgr.findFragmentById(R.id.fragment_holder);
        if (fragment instanceof UpdateBundleListener) {
            ((UpdateBundleListener) fragment).onBundleUpdate(bundle);
        }
    }
}
