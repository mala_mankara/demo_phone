package com.arowana.houdini.byod.utils;

import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.util.Log;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.arowana.houdini.byod.Consts;
import com.arowana.houdini.byod.MainApplication;
import com.arowana.houdini.byod.R;
import com.arowana.houdini.byod.interfaces.listeners.AppstateChangeListener;
import com.arowana.houdini.byod.model.redirct.RedirctConfigObj;
import com.arowana.houdini.byod.model.redirct.RedirectResponseData;
import com.arowana.houdini.byod.model.redirct.RedirectSubModule;
import com.arowana.houdini.byod.view.activity.BaseActivity;
import com.arowana.houdini.byod.view.activity.CityGuideActivity;
import com.arowana.houdini.byod.view.activity.DeviceActivationActivity;
import com.arowana.houdini.byod.view.activity.FacilityServiceActivity;
import com.arowana.houdini.byod.view.activity.FeedBackActivity;
import com.arowana.houdini.byod.view.activity.InRoomDiningActivity;
import com.arowana.houdini.byod.view.activity.LoginActivity;
import com.arowana.houdini.byod.view.activity.MyProfileActivity;
import com.arowana.houdini.byod.view.activity.OffersActivity;
import com.arowana.houdini.byod.view.activity.PalaceInfoActivity;
import com.arowana.houdini.byod.view.activity.RedirectActivity;
import com.arowana.houdini.byod.view.activity.RestaurantActivity;
import com.arowana.houdini.byod.view.activity.SecondaryActivationActivity;
import com.arowana.houdini.byod.view.activity.ServiceRequestActivity;
import com.arowana.houdini.byod.view.activity.SpaActivity;
import com.arowana.houdini.byod.view.activity.TelephoneActivity;
import com.arowana.houdini.byod.view.activity.WeatherActivity;

import io.realm.RealmList;
import io.realm.RealmResults;

public class NavigationManager {


    public static void loadView(String type, Context context, AppstateChangeListener listener) {

        switch (type) {
            case "bookARoom":
                Intent bookARoomIntent;
                if (Consts.APP_CURRENT_STATE > 0) {

                    RedirctConfigObj responsObj = RealmUtil.getInstance().where(RedirctConfigObj.class).findFirst();
                    RealmResults<RedirectResponseData> keyResponseData = responsObj.getResponseData().where().equalTo("Key", type).findAll();
                    String url = "";
                    for (RedirectResponseData data : keyResponseData) {
                        url = data.getModuleUrl();
                    }
                    if (FactoryUtil.isValidURL(url)) {
                        bookARoomIntent = new Intent(context, RedirectActivity.class);
                        bookARoomIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                        bookARoomIntent.putExtra("key", type);
                        bookARoomIntent.putExtra("title", "Book A Room");
                        context.startActivity(bookARoomIntent);
                    } else {
                        ViewUtils.showSnackBarMessage("Feature currently unavailable.Please contact front desk");
                    }
                } else {
                    bookARoomIntent = new Intent(context, LoginActivity.class);
                    bookARoomIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    context.startActivity(bookARoomIntent);
                }
                break;

            case "device_activation":
                Intent deviceIntent = new Intent(context, DeviceActivationActivity.class);
                deviceIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                context.startActivity(deviceIntent);

                break;

            case "secondary_device_activation":
                Intent addDeviceIntent = new Intent(context, SecondaryActivationActivity.class);
                addDeviceIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                context.startActivity(addDeviceIntent);

                break;

            case "my_profile":
                Intent myProfileintent = new Intent(context, MyProfileActivity.class);
                myProfileintent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                context.startActivity(myProfileintent);
                break;

            case "palace_information":
                Intent palaceIntent = new Intent(context, PalaceInfoActivity.class);
                palaceIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                context.startActivity(palaceIntent);
                break;

            case "facilities_and_services":
                Intent facilitiesIntent = new Intent(context, FacilityServiceActivity.class);
                facilitiesIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                context.startActivity(facilitiesIntent);
                break;

            case "city_guide":
                Intent cityGuideIntent = new Intent(context, CityGuideActivity.class);
                cityGuideIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                context.startActivity(cityGuideIntent);
                break;

            case "airlines":
                // intent = new Intent(context, FlightActivity.class);
                Intent airlinesIntent = new Intent(context, RedirectActivity.class);
                airlinesIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                airlinesIntent.putExtra("key", type);
                airlinesIntent.putExtra("title", "Flights");
                context.startActivity(airlinesIntent);
                break;

            case "offers":
                Intent offersIntent = new Intent(context, OffersActivity.class);
                offersIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                context.startActivity(offersIntent);
                break;

            case "weather":
                Intent weatherIntent = new Intent(context, WeatherActivity.class);
                weatherIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                context.startActivity(weatherIntent);

                break;

            case "feedback":
                Intent feedbackIntent = new Intent(context, FeedBackActivity.class);
                feedbackIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                if(Consts.APP_CURRENT_STATE != Consts.APP_CHECKIN_STATE)
                    feedbackIntent.putExtra("msg","checkout");
                Log.d(" MSG..NavigationManager","the app current state data..."+Consts.APP_CURRENT_STATE);
                context.startActivity(feedbackIntent);
                break;

            case "socialMedia":
                Intent mediaIntent = new Intent(context, RedirectActivity.class);
                mediaIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                mediaIntent.putExtra("title", "Social Media");
                mediaIntent.putExtra("key", type);
                context.startActivity(mediaIntent);
                break;

            case "guest_relation":
                Intent guestRelationIntent = new Intent(context, TelephoneActivity.class);
                guestRelationIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                context.startActivity(guestRelationIntent);
                break;

            case "in_room_dining":
                Intent inroomIntent = new Intent(context, InRoomDiningActivity.class);
                inroomIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                context.startActivity(inroomIntent);
                break;

            case "restaurantBar":
                Intent restaurantIntent = new Intent(context, RestaurantActivity.class);
                restaurantIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                context.startActivity(restaurantIntent);
                break;
            case "spa":
                Intent spaIntent = new Intent(context, SpaActivity.class);
                spaIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                context.startActivity(spaIntent);
                break;

            case "service_request":
                Intent serviceIntent = new Intent(context, ServiceRequestActivity.class);
                serviceIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                context.startActivity(serviceIntent);
                break;


            case "whatsapp":
                boolean installed = MainApplication.appInstalledOrNot("com.whatsapp");
                if (!installed) {
                    System.out.println("App is not installed on your phone");
                    ViewUtils.showSnackBarMessage("Feature unavailable. WhatsApp needs to be installed", (BaseActivity) context);
                } else {
                    RedirctConfigObj responsObj1 = RealmUtil.getInstance().where(RedirctConfigObj.class).findFirst();
                    RedirectResponseData keyResponseData1 = responsObj1.getResponseData().where().equalTo("Key", type).findFirst();
                    if(keyResponseData1!= null) {
                        String whatsAppNo = keyResponseData1.getModuleUrl();
                        if (!(whatsAppNo == null && whatsAppNo.isEmpty()))
                            FactoryUtil.openWhatsApp(context, whatsAppNo);
                    }
                    ViewUtils.showSnackBarMessage("Currently No Data are available.", (BaseActivity) context);
                    //openWhatsApp(context);
                }
                break;

            case "subscribe":

                RedirctConfigObj responsObj = RealmUtil.getInstance().where(RedirctConfigObj.class).findFirst();
                RealmResults<RedirectResponseData> keyResponseData = responsObj.getResponseData().where().equalTo("Key", type).findAll();
                if(keyResponseData.size()>0) {
                    String url = "";
                    for (RedirectResponseData data : keyResponseData) {
                        url = data.getModuleUrl();
                    }
                    if (FactoryUtil.isValidURL(url)) {
                        Intent subscribeIntent = new Intent(context, RedirectActivity.class);
                        subscribeIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                        subscribeIntent.putExtra("key", type);
                        subscribeIntent.putExtra("title", "Subscribe");
                        context.startActivity(subscribeIntent);
                    } else {
                        ViewUtils.showSnackBarMessage("Feature currently unavailable.Please contact front desk");
                    }
                }else
                    ViewUtils.showSnackBarMessage("Currently No Data are available.", (BaseActivity) context);
                break;

            case "pressreader":


                boolean isInstalled = MainApplication.appInstalledOrNot("com.newspaperdirect.pressreader.android");
                if (!isInstalled) {
                    System.out.println("App is not installed on your phone");
                    populateSnackBar(context, type);
                } else {
                    openPressRader(context);
                }
                break;

            default:
                Toast.makeText(context, "Not found", Toast.LENGTH_SHORT).show();
        }
      /*  if (intent != null) {
            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        }*/
        if (context instanceof BaseActivity) {
            (((BaseActivity) context)).hideDrawerLayout();
        }
    }

    private static void openPressRader(Context context) {


        PackageManager pm = context.getPackageManager();
        try {
            PackageInfo info = pm.getPackageInfo("com.newspaperdirect.pressreader.android", PackageManager.GET_META_DATA);
            Intent launchIntent = pm.getLaunchIntentForPackage("com.newspaperdirect.pressreader.android");
            context.startActivity(launchIntent);
        } catch (PackageManager.NameNotFoundException e) {
            ViewUtils.showSnackBarMessage("Feature unavailable. App needs to be installed");
        }


    }

    public static void populateSnackBar(final Context context, final String type) {

        BaseActivity activity = MainApplication.getCurrentActivity();
        final FrameLayout layout = activity.findViewById(R.id.snacbar_layout);
        layout.setVisibility(View.VISIBLE);
        layout.bringToFront();
        final View inflateView = activity.getLayoutInflater().inflate(R.layout.custom_confirm_snackbar_layout, null);
        TextView message = inflateView.findViewById(R.id.dineMessage);
        message.setText("Open PressReader In :");
        TextView openApp = inflateView.findViewById(R.id.snack_ok_message);
        openApp.setText("app");
        openApp.setAllCaps(false);
        TextView openBrowser = inflateView.findViewById(R.id.snack_cancel_message);
        openBrowser.setText("browser");
        openBrowser.setAllCaps(false);
        openApp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                layout.setVisibility(View.GONE);
                layout.removeAllViews();
                RedirctConfigObj readerObj = RealmUtil.getInstance().where(RedirctConfigObj.class).findFirst();
                RedirectResponseData readerResponse = readerObj.getResponseData().where().equalTo("Key", type).findFirst();
                RealmList<RedirectSubModule> subModuleObj = readerResponse.getSubModule();
                Log.d("RedirectFrag", "fetchRealmData.." + subModuleObj.size());
                for (RedirectSubModule moduleData : subModuleObj) {
                    if (moduleData.getKey().equalsIgnoreCase("pressreaderplaystoreurl")) {
                        Log.d("fetchRealData", "PRessReader URl.." + moduleData.getSubModuleUrl());
                        String moduleUrl = moduleData.getSubModuleUrl();
                        Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(moduleUrl));
                        context.startActivity(intent);
                    }
                }
            }
        });

        openBrowser.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                layout.setVisibility(View.GONE);
                layout.removeAllViews();
                Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse("http://www.pressreader.com/"));
                context.startActivity(intent);
            }
        });
        inflateView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                layout.setVisibility(View.GONE);
                layout.removeAllViews();
            }
        });
        layout.addView(inflateView);
    }


    public static void openWhatsApp(Context context, String whatsAppNo) {
        PackageManager pm = context.getPackageManager();
        Uri uri = Uri.parse("whatsapp to:" + whatsAppNo);
        try {
            Intent waIntent = new Intent(Intent.ACTION_SEND, uri);
            waIntent.setType("text/plain");


            PackageInfo info = pm.getPackageInfo("com.whatsapp", PackageManager.GET_META_DATA);
            //Check if package exists or not. If not then code
            //in catch block will be called
            waIntent.setPackage("com.whatsapp");

            // waIntent.putExtra(Intent.EXTRA_TEXT, text);
            context.startActivity(waIntent);

        } catch (PackageManager.NameNotFoundException e) {

            e.printStackTrace();

        } catch (Exception e) {
            e.printStackTrace();
        }

    }


}
