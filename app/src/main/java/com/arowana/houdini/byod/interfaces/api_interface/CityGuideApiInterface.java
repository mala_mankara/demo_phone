package com.arowana.houdini.byod.interfaces.api_interface;

import com.arowana.houdini.byod.model.cityguide.CityGuideConfigObj;

import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.POST;

public interface CityGuideApiInterface {

    @FormUrlEncoded
    @POST("CityGuide/Get")
    Call<CityGuideConfigObj> getAllCityGuide(@Field("ImageSize") String imageSize);
}
