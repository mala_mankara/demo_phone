package com.arowana.houdini.byod.model.restaurant;

import io.realm.RealmList;
import io.realm.RealmObject;

public class RestaurantResponseDataObj extends RealmObject{

    private String BookingInfo;

    private RealmList<RestaurantBarObject> RestauarantBar;

    public String getBookingInfo() {
        return BookingInfo;
    }

    public void setBookingInfo(String bookingInfo) {
        BookingInfo = bookingInfo;
    }

    public RealmList<RestaurantBarObject> getRestauarantBar() {
        return RestauarantBar;
    }

    public void setRestauarantBar(RealmList<RestaurantBarObject> restauarantBar) {
        RestauarantBar = restauarantBar;
    }
}
