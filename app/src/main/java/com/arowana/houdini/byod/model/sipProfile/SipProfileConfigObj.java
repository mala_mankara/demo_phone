package com.arowana.houdini.byod.model.sipProfile;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;
/*"SipProfile": {
// new response
   "Domain": "10.9.223.238",
   "UserId": "2501",
   "DisplayName": "CL2008",
   "OutboundProxy": "",
   "Password": "1234",
   "LoginName": "2501",
   "ContactUri": "",
   "OutboundProxyPort": "5060",
   "Protocol": "UDP",
   "DomainPort": "5060",
   "SbcPublicIpAddress": "",
   "SbcPort": "",
   "SbcUseTls": ""

// old ............
        "Domain": "10.9.223.238",
        "UserId": "2510",
        "DisplayName": "USER",
        "OutboundProxy": "10.9.223.25",
        "Password": "1234",
        "LoginName": "2510",
        "ContactUri": "",
        "OutboundProxyPort": "5060",
        "Protocol": "UDP",
        "DomainPort": "5060"
        }
         configuration.domain 		      = alcatelSipProfileObj.getDomain();
          configuration.loginName		 	  = alcatelSipProfileObj.getLoginName();
           configuration.outboundProxy 	  = alcatelSipProfileObj.getOutboundProxy();
            configuration.password		 	  = alcatelSipProfileObj.getPassword();
             configuration.outboundProxyPort   = (Integer.parseInt(alcatelSipProfileObj.getOutboundProxyPort()));
             configuration.domainPort 	 	  =Integer.parseInt(alcatelSipProfileObj.getDomainPort());
              configuration.displayName	  	  = alcatelSipProfileObj.getDisplayName();
       configuration.directProtocol 	  = alcatelSipProfileObj.getProtocol();


        configuration.userName 		 	  = alcatelResponseObj.getUserName();
       configuration.deviceId 			  = alcatelResponseObj.getDeviceId();*/
public class SipProfileConfigObj extends RealmObject{

    private String SipPhoneNumber;

    private String DeviceId;

    private String Domain;

    private String LoginName;

    private String OutboundProxy;

    private String Password;

    private String OutboundProxyPort;

    private String UserId;

    private String DomainPort;

    private String DisplayName;

    private String Protocol;


    private String SbcPublicIpAddress;
    private String SbcPort;




    public String getUserName() {
        return SipPhoneNumber;
    }

    public void setUserName(String userName) {
        SipPhoneNumber = userName;
    }

    public String getDeviceId() {
        return DeviceId;
    }

    public void setDeviceId(String deviceId) {
        DeviceId = deviceId;
    }

    public String getDomain() {
        return Domain;
    }

    public void setDomain(String domain) {
        Domain = domain;
    }

    public String getLoginName() {
        return LoginName;
    }

    public void setLoginName(String loginName) {
        LoginName = loginName;
    }

    public String getOutboundProxy() {
        return OutboundProxy;
    }

    public void setOutboundProxy(String outboundProxy) {
        OutboundProxy = outboundProxy;
    }

    public String getPassword() {
        return Password;
    }

    public void setPassword(String password) {
        Password = password;
    }

    public String getOutboundProxyPort() {
        return OutboundProxyPort;
    }

    public void setOutboundProxyPort(String outboundProxyPort) {
        OutboundProxyPort = outboundProxyPort;
    }

    public String getUserId() {
        return UserId;
    }

    public void setUserId(String userId) {
        UserId = userId;
    }

    public String getDomainPort() {
        return DomainPort;
    }

    public void setDomainPort(String domainPort) {
        DomainPort = domainPort;
    }

    public String getDisplayName() {
        return DisplayName;
    }

    public void setDisplayName(String displayName) {
        DisplayName = displayName;
    }

    public String getProtocol() {
        return Protocol;
    }

    public void setProtocol(String protocol) {
        Protocol = protocol;
    }

    public String getSbcPublicIpAddress() {
        return SbcPublicIpAddress;
    }

    public void setSbcPublicIpAddress(String sbcPublicIpAddress) {
        SbcPublicIpAddress = sbcPublicIpAddress;
    }

    public String getSbcPort() {
        return SbcPort;
    }

    public void setSbcPort(String sbcPort) {
        SbcPort = sbcPort;
    }


}
