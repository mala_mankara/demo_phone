package com.arowana.houdini.byod.interfaces.api_interface;

import com.arowana.houdini.byod.model.login.ForgotPasswordResponseObject;
import com.arowana.houdini.byod.model.login.LoginConfigObject;
import com.arowana.houdini.byod.model.login.ResetPasswordResponseObj;
import com.google.gson.JsonObject;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.POST;
import retrofit2.http.PUT;


public interface LoginApiInterface {




    @POST("GuestUser/SignUp")
    Call<LoginConfigObject> signup(@Body JsonObject signupData);


    @POST("GuestLogIn/Login")
    Call<LoginConfigObject> login(@Body JsonObject loginData);


    @FormUrlEncoded
    @POST("GuestLogIn/ForgotPassword")
    Call<ForgotPasswordResponseObject> forgotPassword(@Field("EmailId") String email,@Field("MacId") String macId);



    @PUT("GuestLogIn/ChangePassword")
    Call<ResetPasswordResponseObj> changePassword(@Body JsonObject passwordData);

}
