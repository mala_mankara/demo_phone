package com.arowana.houdini.byod.view.fragments.myprofile;

import android.content.Context;
import android.os.Bundle;
import com.google.android.material.textfield.TextInputLayout;
import android.text.Html;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.TextView;

import com.arowana.houdini.byod.R;
import com.arowana.houdini.byod.model.login.LoginResponseDataObject;
import com.arowana.houdini.byod.utils.TextUtil;
import com.arowana.houdini.byod.utils.ViewUtils;


public class EditEmailAddress implements View.OnClickListener {

    private View mRootView;
    private TextInputLayout mEmailTxt;
    private TextView mConfirmBtn;
    private Context mContext;
    private LoginResponseDataObject myProfileObject;
    private UpdateProfileDataListener mListener;


    public EditEmailAddress(View mRootView, Context mContext, LoginResponseDataObject profileObject, UpdateProfileDataListener listener) {
        Log.d("test", "---EditEmailAddress----");
        this.mRootView = mRootView;
        this.mContext = mContext;
        this.myProfileObject = profileObject;
        this.mListener = listener;

        init();
    }

    private void init() {

        View view = LayoutInflater.from(mContext).inflate(R.layout.layout_edit_emailid, null);
        FrameLayout layout = mRootView.findViewById(R.id.inflaterView);
        layout.addView(view);


        mEmailTxt = view.findViewById(R.id.email_txt);
        mEmailTxt.getEditText().setHint(TextUtils.concat(mContext.getString(R.string.email_address), Html.fromHtml(mContext.getString(R.string.required_asterisk))));
        if (myProfileObject.getEmailId() != null && !myProfileObject.getEmailId().isEmpty()) {
            mEmailTxt.getEditText().setText(myProfileObject.getEmailId());
        }


        mConfirmBtn = view.findViewById(R.id.confirmButton);
        mConfirmBtn.setOnClickListener(this);

    }


    @Override
    public void onClick(View v) {
        int id = v.getId();

        switch (id) {
            case R.id.confirmButton:
                ViewUtils.hideSoftKeyboard(v);
                validateField();
                break;
        }


    }

    private void validateField() {
        if (TextUtil.isEmpty(mEmailTxt, "Please enter valid email address")) {
            return;
        }
        if (TextUtil.isEmailValid(mEmailTxt, "Please enter valid email address")) {
            return;
        }
        if (mListener != null) {
            Bundle data = new Bundle();
            data.putString("email", mEmailTxt.getEditText().getText().toString());
            mListener.onUpdateProfileData(data);
        }
    }

}
