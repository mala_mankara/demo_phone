package com.arowana.houdini.byod.interfaces.api_interface;

import com.arowana.houdini.byod.model.myprofile.MyProfileUpdateConfigObj;
import com.google.gson.JsonObject;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.PUT;


public interface MyProfileApiInterface {


    @PUT("GuestUser/UpdateGuestInfo")
    Call<MyProfileUpdateConfigObj> updateGuestInfo(@Body JsonObject profileInfo);



    @PUT("GuestUser/ChangeProfilePassword")
    Call<MyProfileUpdateConfigObj> updatePassword(@Body JsonObject passwordInfo);

}
