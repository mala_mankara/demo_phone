package com.arowana.houdini.byod.view.fragments.myprofile;

/**
 * Created by sivam.narayanan on 9/12/2017.
 */

public interface GotoPreviousScreenListener {
    void gotoPreviousScreen();
}
