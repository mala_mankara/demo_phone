package com.arowana.houdini.byod.view.adapters;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.arowana.houdini.byod.R;
import com.arowana.houdini.byod.interfaces.listeners.AdapterCallbacks;
import com.arowana.houdini.byod.model.speeddial.SpeedDialResponseData;

import io.realm.RealmList;

public class SpeedDialAdapter extends RecyclerView.Adapter<SpeedDialAdapter.ContentViewHolder> {
    private Context mContext;
    private RealmList<SpeedDialResponseData> dataList;
    public SpeedDialResponseData dataObj;
    AdapterCallbacks callBacks;


    public SpeedDialAdapter(Context context, RealmList<SpeedDialResponseData> responseDataList, AdapterCallbacks callback) {
        mContext    = context;
        dataList    = responseDataList;
        this.callBacks = callback;
    }

    @NonNull
    @Override
    public ContentViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.singletext_item, parent, false);
        return new ContentViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull ContentViewHolder holder, int position) {

        dataObj = dataList.get(position);
        Log.d("Telephone adapter...","the data.."+dataObj.getName());

        holder.title.setTag(position);
        holder.title.setText(dataObj.getName());

    }



    @Override
    public int getItemCount() {
        return dataList.size();
    }

    public class ContentViewHolder extends RecyclerView.ViewHolder  {
        public TextView title;

        public ContentViewHolder(View itemView) {
            super(itemView);
            title  = itemView.findViewById(R.id.item_name);
            title.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Log.d("TA ContentViewHolder...","the position/data.."+v.getTag()+" / "+dataList.get((Integer) v.getTag()).getName());
                    //  Toast.makeText(mContext,"Clicked Data.."+v.getTag()+",data.."+dataObj.getName(),Toast.LENGTH_SHORT).show();
                    callBacks.methodCallBacks(dataList.get((Integer)v.getTag()));
                }
            });

        }


    }



}
