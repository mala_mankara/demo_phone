package com.arowana.houdini.byod.view.adapters.inroomdining;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.arowana.houdini.byod.R;
import com.arowana.houdini.byod.model.inroomdining.IRDiningAddonsObj;
import com.arowana.houdini.byod.model.inroomdining.IRDiningOrderLineItemsObj;
import com.arowana.houdini.byod.utils.RealmUtil;

import java.text.DecimalFormat;

import io.realm.RealmList;

public class InRoomDiningBookingDetailsAdapter extends RecyclerView.Adapter<InRoomDiningBookingDetailsAdapter.MyViewHolder> {


    private Context context;
    private RealmList<IRDiningOrderLineItemsObj> mBookingObj;


    public class MyViewHolder extends RecyclerView.ViewHolder {
        private TextView name, price;
        private TextView customizedPrice;
        private RelativeLayout customizeLayout;
        private RecyclerView addonRecyclerView,customizedListRecyclerView;



        public MyViewHolder(View view) {
            super(view);

            name = view.findViewById(R.id.name);
            price = view.findViewById(R.id.price);
            //isCustomised = view.findViewById(R.id.iscustomized);

            customizedPrice = view.findViewById(R.id.customized_price);
            customizeLayout = view.findViewById(R.id.customize_layout);
            addonRecyclerView = itemView.findViewById(R.id.ird_summary_addonlist_recycler);
            customizedListRecyclerView = itemView.findViewById(R.id.ird_summary_customizedlist_recycler);

        }
    }


    public InRoomDiningBookingDetailsAdapter(Context mContext, RealmList<IRDiningOrderLineItemsObj> obj) {
        this.context = mContext;
        this.mBookingObj = obj;
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent,int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_dining_booking_details_component, parent, false);
        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull final MyViewHolder holder, int position) {

        DecimalFormat format = new DecimalFormat();
        format.setDecimalSeparatorAlwaysShown(false);
        IRDiningOrderLineItemsObj obj = mBookingObj.get(position);
        if(obj != null) {
            holder.customizeLayout.setVisibility((obj.getAddOns().size() > 0 || obj.getItemCustomzation().size() > 0) ? View.VISIBLE : View.GONE);
            holder.name.setText(obj.getOrderedQuantity().concat(" x ").concat(obj.getItemName()));
            holder.price.setText(RealmUtil.getDiningCurrency().concat(" ").concat(String.valueOf(format.format(Double.parseDouble(obj.getPrice()) * Double.parseDouble(obj.getOrderedQuantity())))));
            if (obj.getItemCustomzation().size() > 0) {
                LinearLayoutManager linearHorizontal1 = new LinearLayoutManager(context, LinearLayoutManager.HORIZONTAL, false);
                holder.customizedListRecyclerView.setLayoutManager(linearHorizontal1);
                IRD_summary_customizationListAdapter adapter = new IRD_summary_customizationListAdapter(obj.getItemCustomzation());
                holder.customizedListRecyclerView.setAdapter(adapter);
            }
            if (obj.getAddOns().size() > 0) {
                LinearLayoutManager linearHorizontal = new LinearLayoutManager(context, LinearLayoutManager.HORIZONTAL, false);
                holder.addonRecyclerView.setLayoutManager(linearHorizontal);
                IRD_SelectedAddOnListAdapter addon_adapter = new IRD_SelectedAddOnListAdapter(obj.getAddOns(), true);
                holder.addonRecyclerView.setAdapter(addon_adapter);
                //    String price=String.valueOf(obj.getAddOns().where().sum("Price"));
                Double addonPrice = 0d;
                for (IRDiningAddonsObj addon : obj.getAddOns()) {
                    addonPrice += Double.parseDouble(addon.getPrice())*Double.parseDouble(obj.getOrderedQuantity());
                }
                holder.customizedPrice.setText(RealmUtil.getDiningCurrency().concat(" ").concat(String.valueOf(format.format((addonPrice)))));
            }
        /* else {
            holder.customizeLayout.setVisibility(View.GONE);
        }*/
        }
    }


    @Override
    public int getItemCount() {
        return mBookingObj.size();
    }


}
