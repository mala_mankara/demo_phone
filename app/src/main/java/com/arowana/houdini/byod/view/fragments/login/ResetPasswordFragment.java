package com.arowana.houdini.byod.view.fragments.login;


import android.app.ProgressDialog;
import android.os.Bundle;
import android.os.Handler;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import com.google.android.material.textfield.TextInputLayout;
import androidx.fragment.app.Fragment;
import android.text.Html;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.TextView;

import com.arowana.houdini.byod.R;
import com.arowana.houdini.byod.interfaces.ApiClient;
import com.arowana.houdini.byod.interfaces.api_interface.LoginApiInterface;
import com.arowana.houdini.byod.model.login.ResetPasswordResponseObj;
import com.arowana.houdini.byod.utils.DialogUtil;
import com.arowana.houdini.byod.utils.NetworkUtils;
import com.arowana.houdini.byod.utils.TextUtil;
import com.arowana.houdini.byod.utils.ViewUtils;
import com.arowana.houdini.byod.view.fragments.BaseFragment;
import com.google.gson.JsonObject;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * A simple {@link Fragment} subclass.
 */
public class ResetPasswordFragment extends BaseFragment {


    public ResetPasswordFragment() {
        // Required empty public constructor
    }

    private View mRootView;
    private TextInputLayout mNewPassword, mRetypePassword, mConfirmCode;
    private TextView mBtnsave;

    JsonObject jsonObject;
    ProgressDialog mDialog;


    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        mRootView = view;
        setTitle(getResources().getString(R.string.forgot_password));
        initComponents();
        initListeners();
        mDialog = DialogUtil.showProgressDialog(getActivity(), "");

    }

    @Override
    protected int getFragmentLayout() {
        return R.layout.fragment_password_confirm;
    }

    public void initComponents() {

        mNewPassword = mRootView.findViewById(R.id.new_password);
        mRetypePassword = mRootView.findViewById(R.id.retype_password);
        mConfirmCode = mRootView.findViewById(R.id.confirm_code);

        mNewPassword.getEditText().setHint(TextUtils.concat(getString(R.string.new_password), Html.fromHtml(getString(R.string.required_asterisk))));
        mRetypePassword.getEditText().setHint(TextUtils.concat(getString(R.string.retype_password), Html.fromHtml(getString(R.string.required_asterisk))));
        mConfirmCode.getEditText().setHint(TextUtils.concat(getString(R.string.confirmation_code), Html.fromHtml(getString(R.string.required_asterisk))));

        mBtnsave = mRootView.findViewById(R.id.btn_password_save);

    }

    public void initListeners() {
        mBtnsave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                validate();
            }
        });
    }


    public void resetPassword(JsonObject jsonObject) {
        this.jsonObject = jsonObject;
        Log.d("confirm_json",jsonObject.toString());
    }

    public void validate() {

        if (!TextUtil.isEmpty(mNewPassword, "Field required")) {
            if (!TextUtil.isEmpty(mRetypePassword, "Field required")) {
                if (!TextUtil.isEmpty(mConfirmCode, "Confirmation code required")) {
                    if (!(mNewPassword.getEditText().getText().toString().equals(mRetypePassword.getEditText().getText().toString()))) {
                        mRetypePassword.setError("Passwords does not match");
                        Handler handler = new Handler();
                        handler.postDelayed(new Runnable() {
                            @Override
                            public void run() {
                                mRetypePassword.setError(null);
                            }
                        }, 2000);

                    } else if (!(mConfirmCode.getEditText().getText().toString().equals(jsonObject.get("ConfirmationCode").getAsString()))) {
                        mConfirmCode.setError("Confirmation code does not match");
                        Log.d("code",mConfirmCode.getEditText().getText().toString());
                        Log.d("code",jsonObject.get("ConfirmationCode").getAsString());
                        Handler handler = new Handler();
                        handler.postDelayed(new Runnable() {
                            @Override
                            public void run() {
                                mConfirmCode.setError(null);
                            }
                        }, 2000);
                    } else {

                        if (NetworkUtils.checkConnectivityStatus(getActivity())) {
                            jsonObject.addProperty("NewPassword",mNewPassword.getEditText().getText().toString());
                            callAPI(jsonObject);
                        }

                    }
                }
            }
        }
    }

    public void callAPI(JsonObject json) {

        Call<ResetPasswordResponseObj> responseObj;
        if (mDialog.isShowing())
            mDialog.dismiss();

        responseObj = ApiClient.getApiClient(LoginApiInterface.class).changePassword(json);

        responseObj.enqueue(new Callback<ResetPasswordResponseObj>() {
            @Override
            public void onResponse(Call<ResetPasswordResponseObj> call, Response<ResetPasswordResponseObj> response) {
                String flag = response.body().getResponseStatus().getResponseFlag();
                String message = response.body().getResponseStatus().getResponseMessage();
                if (flag.equals("SUCCESS")) {
                   // mHandler.showFragment(fragmentHolder.getId(), new LoginFragment(),getTag(),false);
                    getFragmentManager().popBackStack();
                    ViewUtils.showSnackBarMessage(message);
                    getActivity().onBackPressed();

                }
                else
                    ViewUtils.showSnackBarMessage(message);
            }

            @Override
            public void onFailure(Call<ResetPasswordResponseObj> call, Throwable t) {

            }
        });

    }
}
