package com.arowana.houdini.byod.view.fragments.redirect;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.fragment.app.DialogFragment;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.arowana.houdini.byod.Consts;
import com.arowana.houdini.byod.R;
import com.arowana.houdini.byod.model.redirct.RedirctConfigObj;
import com.arowana.houdini.byod.model.redirct.RedirectResponseData;
import com.arowana.houdini.byod.model.redirct.RedirectSubModule;
import com.arowana.houdini.byod.utils.DialogUtil;
import com.arowana.houdini.byod.utils.FactoryUtil;
import com.arowana.houdini.byod.utils.RealmUtil;
import com.arowana.houdini.byod.utils.ViewUtils;
import com.arowana.houdini.byod.view.activity.HomeActivity;
import com.arowana.houdini.byod.view.activity.RedirectActivity;
import com.arowana.houdini.byod.view.adapters.RedirectGridAdapter;
import com.arowana.houdini.byod.view.fragments.BaseFragment;
import com.arowana.houdini.byod.view.fragments.WebViewFragment;

import io.realm.Realm;
import io.realm.RealmList;
import io.realm.RealmResults;

import static com.arowana.houdini.byod.view.activity.RedirectActivity.fragmentHolder;

/**
 * Mala Mahanta@ to work with Redirect API
 */

public class RedirectViewFragment extends BaseFragment {

    ProgressDialog mDialog;
    public RedirectViewFragment() {
        // Required empty public constructor
    }

    public static final String TAG = "RedirectViewFragment";
    private View mRootView;
    private RecyclerView mRecyclerView;
    private Context mContext;
    private String key, title;
    private  boolean isViewLoaded ;
    private RealmResults<RedirectResponseData> keyResponseData;
    private RealmList<RedirectSubModule> subModuleData ;
    private String moduleUrl;

    @Override
    protected int getFragmentLayout() {  return R.layout.fragment_flight;  }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mContext = getActivity();
        Bundle extra = getArguments();
        if(extra != null) {
            title = extra.getString("title");
            key = extra.getString("key");
        }


    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        mRootView = inflater.inflate(R.layout.fragment_flight, container, false);
        mDialog = DialogUtil.showProgressDialog(getActivity(), "");
        mDialog.show();
        /*MainApplication.fetchRedirectData();
        fetchRedirectData();*/

        return mRootView;
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        mRootView = view;
        setTitle(title);
        initComponents();
        fetchRealmData(key);
    }

    public void initComponents() {
        Log.d("RedirectFrag","initComp.."+key);
        mRecyclerView = mRootView.findViewById(R.id.flighthome_recycleview);
    }

    private void populateView() {
        GridLayoutManager mLayoutManager = null;

        if(key.equalsIgnoreCase(Consts.PARSE_FLIGHT_DATA)) {
            mLayoutManager = new GridLayoutManager(mContext, 3);

        }else if(key.equalsIgnoreCase(Consts.PARSE_SOCIAL_MEDIA_DATA) ) {
            mRecyclerView.setBackgroundColor(getResources().getColor(R.color.color_fservice_grid_border));
            mLayoutManager = new GridLayoutManager(mContext, 2);

            // Create a custom SpanSizeLookup where the first item spans both columns
            if(subModuleData.size()%2 != 0)
                mLayoutManager.setSpanSizeLookup(new GridLayoutManager.SpanSizeLookup() {
                    @Override
                    public int getSpanSize(int position) {

                        return position == 0 ? 2 : 1;
                    }
                });
        }
        mRecyclerView.setLayoutManager(mLayoutManager);
        populateData(key);
    }


    private void fetchRealmData(String key) {
        if(mDialog.isShowing())
            mDialog.dismiss();
        Realm mRealm = RealmUtil.getInstance();
        RedirctConfigObj responsObj = mRealm.where(RedirctConfigObj.class).findFirst();
        if(responsObj.getResponseData().size()>0) {
            keyResponseData = responsObj.getResponseData().where().equalTo("Key", key).findAll();
            if (key.equalsIgnoreCase(Consts.PARSE_BOOK_AROOM_DATA) || key.equalsIgnoreCase(Consts.PARSE_WHATSAPP_DATA) || key.equalsIgnoreCase("subscribe")) {
                moduleUrl = keyResponseData.first().getModuleUrl();
            } else if (key.equalsIgnoreCase(Consts.PARSE_FLIGHT_DATA) || key.equalsIgnoreCase(Consts.PARSE_SOCIAL_MEDIA_DATA)) {
                if (keyResponseData.first().getModule() != null) {
                    subModuleData = keyResponseData.first().getSubModule();
                }
            }else if(key.equalsIgnoreCase("pressreader")){
                   RealmList<RedirectSubModule> subModuleObj = keyResponseData.first().getSubModule();
                    Log.d("RedirectFrag", "fetchRealmData.." + subModuleObj.size());
                    for (RedirectSubModule moduleData : subModuleObj) {
                        if (moduleData.getKey().equalsIgnoreCase("pressreaderplaystoreurl")) {
                            Log.d("fetchRealData", "PRessReader URl.." + moduleData.getSubModuleUrl());
                            moduleUrl = moduleData.getSubModuleUrl();
                        }
                    }
                //}
            }
            populateView();
        }
        ViewUtils.showSnackBarMessage("Currently No data are available. " );
    }

    private void populateData(String key) {
        if(this.key.equalsIgnoreCase(Consts.PARSE_FLIGHT_DATA) || this.key.equalsIgnoreCase(Consts.PARSE_SOCIAL_MEDIA_DATA)){
            if(keyResponseData.first().getModule()!= null )
                mRecyclerView.setAdapter(new RedirectGridAdapter(mContext,mHandler, subModuleData, this.key,title, mDialog));
        }else {
            if(isViewLoaded){
                getActivity().onBackPressed();
                isViewLoaded=false;
            }else {
                if(RedirectActivity.isBrowser){
                    Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(moduleUrl));
                    startActivity(intent);
                }else {
                    if (FactoryUtil.isValidURL(moduleUrl))
                        loadFragments(moduleUrl);
                    else {
                        Intent intent = new Intent(getContext(), HomeActivity.class);
                        intent.putExtra("login_message", "Feature Currently unavailable. please contact front desk");
                        getActivity().finishAffinity();
                        startActivity(intent);
                    }
                }
            }
        }
    }

    @Override
    public void onStop() {
        super.onStop();
    }

    public void loadFragments(String url){
        isViewLoaded = true;
        Fragment webViewFragment = new WebViewFragment();
        Bundle bundle = new Bundle();
        bundle.putString("title", title);
        bundle.putString("url", url);
        webViewFragment.setArguments(bundle);
        mHandler.showFragment(fragmentHolder.getId(), webViewFragment);
    }
}
