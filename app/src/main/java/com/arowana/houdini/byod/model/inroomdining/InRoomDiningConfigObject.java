package com.arowana.houdini.byod.model.inroomdining;

import io.realm.RealmList;
import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

public class InRoomDiningConfigObject extends RealmObject{

    @PrimaryKey
    private String RealmId = "1";

    private String IsReadOnly;

    private IRDiningResponseStatusObj ResponseStatus;

    private RealmList<IRDiningResponseDataObj> ResponseData;

    public String getIsReadOnly() {
        return IsReadOnly;
    }

    public void setIsReadOnly(String isReadOnly) {
        IsReadOnly = isReadOnly;
    }

    public IRDiningResponseStatusObj getResponseStatus() {
        return ResponseStatus;
    }

    public void setResponseStatus(IRDiningResponseStatusObj responseStatus) {
        ResponseStatus = responseStatus;
    }

    public RealmList<IRDiningResponseDataObj> getResponseData() {
        return ResponseData;
    }

    public void setResponseData(RealmList<IRDiningResponseDataObj> responseData) {
        ResponseData = responseData;
    }

    public String getRealmId() {
        return RealmId;
    }

    public void setRealmId(String realmId) {
        RealmId = realmId;
    }
}
