package com.arowana.houdini.byod.interfaces.api_interface;

import com.arowana.houdini.byod.model.home.HomeFetchMediaResponseObject;

import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.POST;

public interface HomeApiInterface {

    @FormUrlEncoded
    @POST("ApplicationMedia/Get")
    Call<HomeFetchMediaResponseObject> fetchLandingMedia(@Field("ImageSize") String image, @Field("BrandCode") String brandCode, @Field("HotelCode") String hotelCode);
}
