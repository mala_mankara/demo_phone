package com.arowana.houdini.byod.utils;

import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.content.Context;
import android.content.pm.PackageManager;
import android.content.res.Configuration;
import android.hardware.Camera;
import android.location.LocationManager;
import android.net.wifi.WifiInfo;
import android.net.wifi.WifiManager;
import android.os.AsyncTask;
import android.os.Build;
import android.speech.SpeechRecognizer;
import android.util.Log;

import java.net.NetworkInterface;
import java.util.Collections;
import java.util.List;
@SuppressLint("ObsoleteSdkInt")
public class DeviceOSFeatureUtils {

    @TargetApi(Build.VERSION_CODES.GINGERBREAD)
    public static boolean hasCamera(Context c) {
        PackageManager pm = c.getPackageManager();
        boolean hasFeature = pm.hasSystemFeature(PackageManager.FEATURE_CAMERA);

        if (hasFeature && hasGingerbread()) {
            return Camera.getNumberOfCameras() > 0;
        }

        return hasFeature;
    }


    public static boolean hasFroyo() {
        // Can use static final constants like FROYO, declared in later versions
        // of the OS since they are inlined at compile time. This is guaranteed
        // behavior.
        return Build.VERSION.SDK_INT >= Build.VERSION_CODES.FROYO;
    }

    public static boolean hasGingerbread() {
        return Build.VERSION.SDK_INT >= Build.VERSION_CODES.GINGERBREAD;
    }

    public static boolean hasHoneycomb() {
        return Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB;
    }

    public static boolean hasHoneycombMR1() {
        return Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB_MR1;
    }

    public static boolean hasHoneycombMR2() {
        return Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB_MR2;
    }

    public static boolean hasICS() {
        return Build.VERSION.SDK_INT >= Build.VERSION_CODES.ICE_CREAM_SANDWICH;
    }

    public static boolean hasICSMR1() {
        return Build.VERSION.SDK_INT >= Build.VERSION_CODES.ICE_CREAM_SANDWICH_MR1;
    }


    public static boolean hasKitKat() {
        return Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT;
    }

    public static boolean hasJellyBean() {
        return Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN;
    }

    public static boolean hasJellyBeanMR1() {
        return Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR1;
    }

    public static boolean hasJellyBeanMR2() {
        return Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR2;
    }

    public static boolean isTablet(Context context) {
        return (context.getResources().getConfiguration().screenLayout & Configuration.SCREENLAYOUT_SIZE_MASK) >= Configuration.SCREENLAYOUT_SIZE_LARGE;
    }

    public static boolean isHoneycombOrLaterTablet(Context context) {
        return hasHoneycomb() && isTablet(context);
    }

    public static boolean isLocationServicesSupported(Context c) {
        LocationManager lm = (LocationManager) c.getSystemService(Context.LOCATION_SERVICE);
        return lm != null;
    }

    @TargetApi(Build.VERSION_CODES.HONEYCOMB)
    public static <Param, Progress, Result> AsyncTask<Param, Progress, Result>
    executeOnThreadPoolExecutor(AsyncTask<Param, Progress, Result> task, Param... params) {

        if (hasHoneycomb()) {
            return task.executeOnExecutor(AsyncTask.SERIAL_EXECUTOR, params);
        }

        return task.execute(params);
    }

    public static boolean isVoiceRecognitionSupported(Context c) {
        return SpeechRecognizer.isRecognitionAvailable(c);
    }

    public static String getMacAddress(Context context) {
        String macAddress = "02:00:00:00:00:00";
        Log.d("test","macAddress>>>"+macAddress);
        try {
            WifiManager wifiManager = (WifiManager) context.getSystemService(Context.WIFI_SERVICE);
            WifiInfo wifiInfo = wifiManager.getConnectionInfo();
            //macAddress = wifiInfo.getBSSID();
            macAddress = wifiInfo.getMacAddress();
            Log.d("test","macAddress 23>>>"+macAddress);
            return macAddress;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return macAddress;
    }

    public static String getMacAddress() {
        try {
            List<NetworkInterface> all = Collections.list(NetworkInterface.getNetworkInterfaces());
            for (NetworkInterface nif : all) {
                if (!nif.getName().equalsIgnoreCase("wlan0")) continue;

                byte[] macBytes = nif.getHardwareAddress();
                if (macBytes == null) {
                    return "";
                }

                StringBuilder res1 = new StringBuilder();
                for (byte b : macBytes) {
                    res1.append(String.format("%02X:",b));
                }

                if (res1.length() > 0) {
                    res1.deleteCharAt(res1.length() - 1);
                }
                return res1.toString();
            }
        } catch (Exception ex) {
        }
        return "02:00:00:00:00:00";
    }

    public static String getNetworkMacAddress() {
        try {
            List<NetworkInterface> all = Collections.list(NetworkInterface.getNetworkInterfaces());
            for (NetworkInterface nif : all) {
                if (!nif.getName().equalsIgnoreCase("wlan0")) continue;
                byte[] macBytes = nif.getHardwareAddress();
                if (macBytes == null) {
                    return "";
                }
                StringBuilder res1 = new StringBuilder();
                for (byte b : macBytes) {
                    res1.append(Integer.toHexString(b & 0xFF) + ":");
                }

                if (res1.length() > 0) {
                    res1.deleteCharAt(res1.length() - 1);
                }
                return res1.toString();
            }
        } catch (Exception ex) {
        }
        return "02:00:00:00:00:00";
    }

    public static String getDeviceDensity(Context context) {
        float density = context.getResources().getDisplayMetrics().density;
        if (density >= 4.0) {
            return "4X";
        }
        if (density >= 3.0) {
            return "3X";
        }
        if (density >= 2.0) {
            return "2X";
        }
        if (density >= 1.5) {
            return "1.5X";
        }
        if (density >= 1.0) {
            return "1X";
        }
        return "0.5X";
    }

}
