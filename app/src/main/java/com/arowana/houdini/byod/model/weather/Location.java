package com.arowana.houdini.byod.model.weather;

public class Location {
    private String Region;

    private String Population;

    private String Latitude;

    private String Longitude;

    private String Id;

    private String Country;

    private String City;

    public String getRegion ()
    {
        return Region;
    }

    public void setRegion (String Region)
    {
        this.Region = Region;
    }

    public String getPopulation ()
    {
        return Population;
    }

    public void setPopulation (String Population)
    {
        this.Population = Population;
    }

    public String getLatitude ()
    {
        return Latitude;
    }

    public void setLatitude (String Latitude)
    {
        this.Latitude = Latitude;
    }

    public String getLongitude ()
    {
        return Longitude;
    }

    public void setLongitude (String Longitude)
    {
        this.Longitude = Longitude;
    }

    public String getId ()
    {
        return Id;
    }

    public void setId (String Id)
    {
        this.Id = Id;
    }

    public String getCountry ()
    {
        return Country;
    }

    public void setCountry (String Country)
    {
        this.Country = Country;
    }

    public String getCity ()
    {
        return City;
    }

    public void setCity (String City)
    {
        this.City = City;
    }

    @Override
    public String toString()
    {
        return "ClassPojo [Region = "+Region+", Population = "+Population+", Latitude = "+Latitude+", Longitude = "+Longitude+", Id = "+Id+", Country = "+Country+", City = "+City+"]";
    }
}
