package com.arowana.houdini.byod.model.facilitiesAndServices;

import io.realm.RealmList;
import io.realm.RealmObject;

public class FServiceResponseData  extends RealmObject{


   /* private String[] Amenities;

    private Images[] Images;*/


    private String BrandId;

    private String HotelCode;

    private String Url;

    private String HotelId;

    private String GroupCode;

    private String Type;

    private RealmList<FServiceAmenities> Amenities;

    private String Id;

   /* private String[] Images;*/
   private RealmList<FServicesImages> Images;

    private String BrandCode;

    private String Sequence;

    private String Module;

    private String GroupId;

    public String getBrandId ()
    {
        return BrandId;
    }

    public void setBrandId (String BrandId)
    {
        this.BrandId = BrandId;
    }

    public String getHotelCode ()
    {
        return HotelCode;
    }

    public void setHotelCode (String HotelCode)
    {
        this.HotelCode = HotelCode;
    }

    public String getUrl ()
    {
        return Url;
    }

    public void setUrl (String Url)
    {
        this.Url = Url;
    }

    public String getHotelId ()
    {
        return HotelId;
    }

    public void setHotelId (String HotelId)
    {
        this.HotelId = HotelId;
    }

    public String getGroupCode ()
    {
        return GroupCode;
    }

    public void setGroupCode (String GroupCode)
    {
        this.GroupCode = GroupCode;
    }

    public String getType ()
    {
        return Type;
    }

    public void setType (String Type)
    {
        this.Type = Type;
    }

    public RealmList<FServiceAmenities> getAmenities ()
    {
        return Amenities;
    }

    public void setAmenities (RealmList<FServiceAmenities> Amenities)
    {
        this.Amenities = Amenities;
    }

    public String getId ()
    {
        return Id;
    }

    public void setId (String Id)
    {
        this.Id = Id;
    }

    public RealmList<FServicesImages> getImages ()
    {
        return Images;
    }

    public void setImages (RealmList<FServicesImages> Images)
    {
        this.Images = Images;
    }

    public String getBrandCode ()
    {
        return BrandCode;
    }

    public void setBrandCode (String BrandCode)
    {
        this.BrandCode = BrandCode;
    }

    public String getSequence ()
    {
        return Sequence;
    }

    public void setSequence (String Sequence)
    {
        this.Sequence = Sequence;
    }

    public String getModule ()
    {
        return Module;
    }

    public void setModule (String Module)
    {
        this.Module = Module;
    }

    public String getGroupId ()
    {
        return GroupId;
    }

    public void setGroupId (String GroupId)
    {
        this.GroupId = GroupId;
    }

    @Override
    public String toString()
    {
        return "ClassPojo [BrandId = "+BrandId+", HotelCode = "+HotelCode+", Url = "+Url+", HotelId = "+HotelId+", GroupCode = "+GroupCode+", Type = "+Type+", Amenities = "+Amenities+", Id = "+Id+", Images = "+Images+", BrandCode = "+BrandCode+", Sequence = "+Sequence+", Module = "+Module+", GroupId = "+GroupId+"]";
    }
}
