package com.arowana.houdini.byod.utils;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;

/**
 * Created by sivam.narayanan on 7/7/2016.
 */
public class NetworkUtils {

    private static int TYPE_WIFI = 1;
    private static int TYPE_MOBILE = 2;
    private static int TYPE_OTHERS = 0;

    private static int NETWORK_TYPE = TYPE_WIFI;
    private static boolean IS_CONNECTED = false;


    public static boolean checkConnectivityStatus(Context context) {
        ConnectivityManager cm = (ConnectivityManager) context
                .getSystemService(Context.CONNECTIVITY_SERVICE);

        NetworkInfo activeNetwork = cm.getActiveNetworkInfo();

        IS_CONNECTED = false;
        if (activeNetwork != null) {
            if (activeNetwork.getType() == ConnectivityManager.TYPE_WIFI) {
                NETWORK_TYPE = TYPE_WIFI;
            } else if (activeNetwork.getType() == ConnectivityManager.TYPE_MOBILE) {
                NETWORK_TYPE = TYPE_MOBILE;
            } else {
                NETWORK_TYPE = TYPE_OTHERS;
            }

            IS_CONNECTED = activeNetwork.isConnectedOrConnecting();
        }

        return IS_CONNECTED;
    }

    public static boolean isConnected() {
        return IS_CONNECTED;
    }

}