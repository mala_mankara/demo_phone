package com.arowana.houdini.byod.interfaces.api_interface;

import com.arowana.houdini.byod.model.deviceActivationSecondary.QRGenerationResponse;
import com.arowana.houdini.byod.model.deviceActivationSecondary.SecondaryUserListResponse;
import com.arowana.houdini.byod.model.deviceactivation_QR.QR_DeviceRegisterConfigObj;
import com.arowana.houdini.byod.utils.BasicApiResponse;
import com.google.gson.JsonObject;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.HTTP;
import retrofit2.http.POST;

public interface DeviceActivationApiInterface {

    /*@POST("Userregistration/GuestStatus")
    Call<DeviceActivationConfigObject> getActivationStatus(@Body JsonObject registrationData);*/

    @POST("QRDevice/Register")
    Call<QR_DeviceRegisterConfigObj> setQRDevRegistration(@Body JsonObject data);


    @POST("QRCode/Generate")
    Call<QRGenerationResponse>generateQR(@Body JsonObject data);

    @POST("QRDevice/GetDevicesByRoomNumber")
    Call<SecondaryUserListResponse>getActiveUsers(@Body JsonObject data);

    @HTTP(method = "DELETE", path = "QRDevice/DeactivateDevices", hasBody = true)
    Call<BasicApiResponse>deactivateDevice(@Body JsonObject data);

    @POST("Userregistration/CheckOut")
    Call<BasicApiResponse>checkoutGuest(@Body JsonObject data);
}
