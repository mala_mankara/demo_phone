package com.arowana.houdini.byod.view.activity;

import android.os.Bundle;
import androidx.fragment.app.Fragment;
import android.widget.FrameLayout;

import com.arowana.houdini.byod.MainApplication;
import com.arowana.houdini.byod.R;
import com.arowana.houdini.byod.model.spa.SpaBookingConfigObject;
import com.arowana.houdini.byod.utils.RealmUtil;
import com.arowana.houdini.byod.view.fragments.spa.SpaBookingDetailsFragment;
import com.arowana.houdini.byod.view.fragments.spa.SpaFragment;

import io.realm.Realm;
import io.realm.RealmResults;

public class SpaActivity extends BaseActivity {


    public static FrameLayout fragmentHolder;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_module_layout);
        MainApplication.setCurrentActivity(this);
        RealmUtil.deleteAllSpa();
        initComponents();
    }

    public void initComponents() {

        fragmentHolder = findViewById(R.id.fragment_holder);
        loadFragments();

    }

    public void loadFragments() {

        showFragment(fragmentHolder.getId(), new SpaFragment());

    }

    @Override
    public void onBackPressed() {
        Fragment fragment = getSupportFragmentManager().findFragmentById(R.id.fragment_holder);
        if(fragment instanceof SpaBookingDetailsFragment){
            finish();
            RealmUtil.getInstance().executeTransaction(new Realm.Transaction() {
                @Override
                public void execute(Realm realm) {
                    RealmResults<SpaBookingConfigObject> result = realm.where(SpaBookingConfigObject.class).findAll();
                    result.deleteAllFromRealm();
                }
            });
        }
        else {
            super.onBackPressed();
        }



    }
}
