package com.arowana.houdini.byod.model.feedback;

import io.realm.RealmObject;

public class FeedBackResponseData extends RealmObject {

    private String Name;

    private int MaxRating;

    private int PreviousRating;

    private String Type;

    private String ID;

    public String getName() {
        return Name;
    }

    public void setName(String Name) {
        this.Name = Name;
    }

    public int getMaxRating() {
        return MaxRating;
    }

    public void setMaxRating(int MaxRating) {
        this.MaxRating = MaxRating;
    }

    public int getPreviousRating() {
        return PreviousRating;
    }

    public void setPreviousRating(int PreviousRating) {
        this.PreviousRating = PreviousRating;
    }

    public String getType() {
        return Type;
    }

    public void setType(String Type) {
        this.Type = Type;
    }

    public String getID() {
        return ID;
    }

    public void setID(String ID) {
        this.ID = ID;
    }

    @Override
    public String toString() {
        return "ClassPojo [Name = " + Name + ", MaxRating = " + MaxRating + ", PreviousRating = " + PreviousRating + ", Type = " + Type + ", ID = " + ID + "]";
    }
}
