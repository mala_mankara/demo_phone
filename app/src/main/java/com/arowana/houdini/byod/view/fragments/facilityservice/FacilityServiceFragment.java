package com.arowana.houdini.byod.view.fragments.facilityservice;


import android.app.Fragment;
import android.app.ProgressDialog;
import android.content.Context;
import android.os.Bundle;
import android.view.View;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.arowana.houdini.byod.R;
import com.arowana.houdini.byod.interfaces.ApiClient;
import com.arowana.houdini.byod.interfaces.api_interface.FServicesApiInterface;
import com.arowana.houdini.byod.model.facilitiesAndServices.FServiceResponseData;
import com.arowana.houdini.byod.model.facilitiesAndServices.FacilityServiceConfigObj;
import com.arowana.houdini.byod.utils.DialogUtil;
import com.arowana.houdini.byod.view.adapters.facilityServices.FacilityAdapter;
import com.arowana.houdini.byod.view.fragments.BaseFragment;

import io.realm.RealmList;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.arowana.houdini.byod.view.activity.FacilityServiceActivity.fragmentHolder;

/**
 * A simple {@link Fragment} subclass.
 */
public class FacilityServiceFragment extends BaseFragment {

    public static final String TAG = "FacilityServiceFragment";
    private  View mRootView;
    private Context mContext;
    ProgressDialog mDialog;
    RecyclerView mRecyclerView;
    FacilityServiceConfigObj obj;

    public FacilityServiceFragment() {
        // Required empty public constructor
    }

    @Override
    protected int getFragmentLayout() {
        /*return R.layout.fragment_facility ;*/
        return R.layout.fragment_flight;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mContext=getActivity();

    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        mRootView=view;
        mDialog = DialogUtil.showProgressDialog(getActivity(), "");
        mDialog.show();
        setTitle(getString(R.string.Title_FService));
        initView();
        fetchData();
    }

    RealmList<FServiceResponseData> responseData;
    private void fetchData() {
        try {

            final Call<FacilityServiceConfigObj> srviceObj = ApiClient.getApiClient(FServicesApiInterface.class).getAllFacilities("2X");
            srviceObj.enqueue(new Callback<FacilityServiceConfigObj>() {

                @Override
                public void onResponse(@NonNull Call<FacilityServiceConfigObj> call, @NonNull Response<FacilityServiceConfigObj> response) {
                    if (mDialog.isShowing())
                        mDialog.dismiss();
                    if(response.isSuccessful() && response.body() != null){
                        obj = response.body();
                        if(obj != null) {
                            responseData = obj.getResponseData();
                            populateData(responseData);
                        }
                    } else {
                        Toast.makeText(getContext(), "No Data To display..", Toast.LENGTH_SHORT).show();
                    }
                }
                @Override
                public void onFailure(@NonNull Call<FacilityServiceConfigObj> call, @NonNull Throwable t) {
                }
            });

        }catch (Exception e){
            if (mDialog.isShowing())
                mDialog.dismiss();
            e.printStackTrace();
        }
    }

    public void populateData(RealmList<FServiceResponseData> obj) {
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(mContext);
        mRecyclerView.setLayoutManager(mLayoutManager);
        FacilityAdapter adapter = new FacilityAdapter(getContext(),obj,mHandler,fragmentHolder,mListener);
        mRecyclerView.setAdapter(adapter);
        adapter.notifyDataSetChanged();
    }

    private void initView() {
        mRecyclerView = mRootView.findViewById(R.id.flighthome_recycleview);
    }


}
