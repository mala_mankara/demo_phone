package com.arowana.houdini.byod.model.inroomdining;

import io.realm.RealmList;
import io.realm.RealmObject;

public class IRDiningCustomizationObj extends RealmObject {

    private String CustomizeItem;

    private RealmList<IRDiningCustomTypesObj> CustomTypes;

    private String Id;

    private int selectedIndex = -1;

    private boolean isSelected;

    public String getCustomizeItem() {
        return CustomizeItem;
    }

    public void setCustomizeItem(String customizeItem) {
        CustomizeItem = customizeItem;
    }

    public RealmList<IRDiningCustomTypesObj> getCustomTypes() {
        return CustomTypes;
    }

    public void setCustomTypes(RealmList<IRDiningCustomTypesObj> customTypes) {
        CustomTypes = customTypes;
    }

    public String getId() {
        return Id;
    }

    public void setId(String id) {
        Id = id;
    }

    public int getSelectedIndex() {
        return selectedIndex;
    }

    public void setSelectedIndex(int selectedIndex) {
        this.selectedIndex = selectedIndex;
    }

    public boolean isSelected() {
        return isSelected;
    }

    public void setSelected(boolean selected) {
        isSelected = selected;
    }

    public IRDiningCustomizationObj duplicateObj(){
        IRDiningCustomizationObj  newObj= new IRDiningCustomizationObj();
        newObj.CustomizeItem=this.CustomizeItem;
        newObj.CustomTypes = this.CustomTypes;
        newObj.Id = this.Id;
        newObj.selectedIndex = this.selectedIndex;
        newObj.isSelected = this.isSelected;
        return newObj;
    }
}
