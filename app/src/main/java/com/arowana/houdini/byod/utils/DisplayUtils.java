package com.arowana.houdini.byod.utils;

import android.app.Activity;
import android.content.Context;
import android.util.TypedValue;
import android.view.inputmethod.InputMethodManager;

/**
 * Created by vivek.m on 18-05-2017.
 */

public class DisplayUtils {

    private static DisplayUtils mInstance;
    private Context mContext;

    private DisplayUtils(Context mContext) {
        this.mContext = mContext;
    }

    public static DisplayUtils getInstance(Context mContext) {
        return mInstance == null ? new DisplayUtils(mContext) : mInstance;
    }




    // Hide Keyboard
    public void hideKeyBoard() {
        try {
            final InputMethodManager imm = (InputMethodManager) mContext.getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(((Activity) mContext).getWindow().getDecorView().getWindowToken(), 0);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public int dp2px(int dp) {
        return (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, dp, mContext.getResources().getDisplayMetrics());
    }

}