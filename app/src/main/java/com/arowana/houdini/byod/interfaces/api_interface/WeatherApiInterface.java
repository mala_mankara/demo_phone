package com.arowana.houdini.byod.interfaces.api_interface;

import com.arowana.houdini.byod.model.weather.WeatherConfigObject;

import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.POST;

public interface WeatherApiInterface {

    @POST("Weather/GetWeatherInfo")
    Call<WeatherConfigObject> weatherData();

    @FormUrlEncoded
    @POST("Weather/GetWeatherInfo")
    Call<WeatherConfigObject> weatherData(@Field("Unit") String unitData );
}
