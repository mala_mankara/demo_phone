package com.arowana.houdini.byod.model.inroomdining;

import io.realm.RealmList;
import io.realm.RealmObject;

public class IRDiningMenusObj extends RealmObject {

    private String Ingredients;

    private String CategoryCode;

    private String Description;

    private String AddToCart;

    private RealmList<IRDiningUpsellingItemObj> UpsellingItems;

    private RealmList<IRDiningImagesObj> Images;

    private String MenuCode;

    private String MenuName;

    private String Price;

    private String DietType;

    private RealmList<IRDiningCustomizationObj> Customization;

    private RealmList<IRDiningDiscountObj> Discount;

    private String Id;

    private RealmList<IRDiningAddonsObj> Addons;

    private RealmList<IRDiningMenuItemsObj> MenuItems;

    public String getIngredients() {
        return Ingredients;
    }

    public void setIngredients(String ingredients) {
        Ingredients = ingredients;
    }

    public String getCategoryCode() {
        return CategoryCode;
    }

    public void setCategoryCode(String categoryCode) {
        CategoryCode = categoryCode;
    }

    public String getDescription() {
        return Description;
    }

    public void setDescription(String description) {
        Description = description;
    }

    public String getAddToCart() {
        return AddToCart;
    }

    public void setAddToCart(String addToCart) {
        AddToCart = addToCart;
    }

    public RealmList<IRDiningUpsellingItemObj> getUpsellingItems() {
        return UpsellingItems;
    }

    public void setUpsellingItems(RealmList<IRDiningUpsellingItemObj> upsellingItems) {
        UpsellingItems = upsellingItems;
    }

    public RealmList<IRDiningImagesObj> getImages() {
        return Images;
    }

    public void setImages(RealmList<IRDiningImagesObj> images) {
        Images = images;
    }

    public String getMenuCode() {
        return MenuCode;
    }

    public void setMenuCode(String menuCode) {
        MenuCode = menuCode;
    }

    public String getMenuName() {
        return MenuName;
    }

    public void setMenuName(String menuName) {
        MenuName = menuName;
    }

    public String getPrice() {
        return Price;
    }

    public void setPrice(String price) {
        Price = price;
    }

    public String getDietType() {
        return DietType;
    }

    public void setDietType(String dietType) {
        DietType = dietType;
    }

    public RealmList<IRDiningCustomizationObj> getCustomization() {
        return Customization;
    }

    public void setCustomization(RealmList<IRDiningCustomizationObj> customization) {
        Customization = customization;
    }

    public RealmList<IRDiningDiscountObj> getDiscount() {
        return Discount;
    }

    public void setDiscount(RealmList<IRDiningDiscountObj> discount) {
        Discount = discount;
    }

    public String getId() {
        return Id;
    }

    public void setId(String id) {
        Id = id;
    }

    public RealmList<IRDiningAddonsObj> getAddons() {
        return Addons;
    }

    public void setAddons(RealmList<IRDiningAddonsObj> addons) {
        Addons = addons;
    }

    public RealmList<IRDiningMenuItemsObj> getMenuItems() {
        return MenuItems;
    }

    public void setMenuItems(RealmList<IRDiningMenuItemsObj> menuItems) {
        MenuItems = menuItems;
    }
}
