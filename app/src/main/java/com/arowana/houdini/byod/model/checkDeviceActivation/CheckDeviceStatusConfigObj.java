package com.arowana.houdini.byod.model.checkDeviceActivation;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

public class CheckDeviceStatusConfigObj extends RealmObject{
    @PrimaryKey
    private String RealmId = "1";

    private CheckDeviceResponseStatus ResponseStatus;

    private CheckDeviceResponseData ResponseData;

    public String getRealmId() {
        return RealmId;
    }

    public void setRealmId(String realmId) {
        RealmId = realmId;
    }

    public CheckDeviceResponseStatus getResponseStatus ()
    {
        return ResponseStatus;
    }

    public void setResponseStatus (CheckDeviceResponseStatus ResponseStatus)
    {
        this.ResponseStatus = ResponseStatus;
    }

    public CheckDeviceResponseData getResponseData ()
    {
        return ResponseData;
    }

    public void setResponseData (CheckDeviceResponseData ResponseData)
    {
        this.ResponseData = ResponseData;
    }

    @Override
    public String toString()
    {
        return "ClassPojo [ResponseStatus = "+ResponseStatus+", ResponseData = "+ResponseData+"]";
    }
}
