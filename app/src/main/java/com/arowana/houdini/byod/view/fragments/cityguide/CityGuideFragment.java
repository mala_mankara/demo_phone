package com.arowana.houdini.byod.view.fragments.cityguide;


import android.os.Bundle;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.view.View;

import com.arowana.houdini.byod.R;
import com.arowana.houdini.byod.model.cityguide.CityguideResponseDataObj;
import com.arowana.houdini.byod.view.adapters.cityguide.CityGuideAdapter;
import com.arowana.houdini.byod.view.fragments.BaseFragment;

import static com.arowana.houdini.byod.view.activity.CityGuideActivity.fragmentHolder;


public class CityGuideFragment extends BaseFragment {


    public CityGuideFragment() {

    }

    private View mRootView;
    private RecyclerView mRecyclerView;
    private CityguideResponseDataObj mObj;


    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        mRootView=view;
        initComponents();
        populateData();

    }

    public void initComponents(){
        mRecyclerView=mRootView.findViewById(R.id.cityguide_recyclerview);
    }


    @Override
    protected int getFragmentLayout() {
        return R.layout.fragment_city_guide;
    }

    public void setObj(CityguideResponseDataObj obj){
        mObj=obj;
    }

    public void populateData() {
        LinearLayoutManager linearVertical = new LinearLayoutManager(getContext(), RecyclerView.VERTICAL, false);
        mRecyclerView.setLayoutManager(linearVertical);
        CityGuideAdapter adapter = new CityGuideAdapter(getContext(),mObj,mHandler,fragmentHolder);
        mRecyclerView.setAdapter(adapter);
        adapter.notifyDataSetChanged();

    }
}
