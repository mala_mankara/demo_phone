package com.arowana.houdini.byod.view.adapters.spa;

import android.app.Activity;
import android.app.DatePickerDialog;
import android.content.Context;
import android.graphics.Color;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.DatePicker;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;

import com.arowana.houdini.byod.Consts;
import com.arowana.houdini.byod.R;
import com.arowana.houdini.byod.model.spa.SpaAvailabilityTimeObj;
import com.arowana.houdini.byod.model.spa.SpaTimeAndPriceObj;
import com.arowana.houdini.byod.model.spa.SpaTreatmentObj;
import com.arowana.houdini.byod.model.spa.SpaTreatmentTypeObj;
import com.arowana.houdini.byod.utils.DateUtil;
import com.arowana.houdini.byod.utils.ExpandableTextView;
import com.arowana.houdini.byod.utils.RealmUtil;
import com.arowana.houdini.byod.view.adapters.ICart;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.TimeZone;

import io.realm.Realm;

public class SpaDetailsAdapter extends RecyclerView.Adapter<SpaDetailsAdapter.MyViewHolder> {

    private final String TAG="SpaDetailsAdapter";
    private Context context;
    private SpaTreatmentTypeObj spaTreatmentTypeObj;
    private SpaAvailabilityTimeObj spaTimeAvailabilityObj;

    private Realm realm;
    private int mYear;
    private int mMonth;
    private int mDay;
    private String SpaOpenTime;
    private String SpaCloseTime;
    private String SpaHotelTimeZone;

    private ICart cartListener;
    private boolean isUnselected =false;


    public class MyViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        TextView treatmentName, treatmentCost, currency, txtReadMore;
        ExpandableTextView treatmentDesc;
        ImageView btnAddTreatment, btnExpandTreatment;
        LinearLayout bookingLayout;
        TextView disclaimerTxt;
        LinearLayout disclaimerlayout;
        int spaPosition = -1;
        SpaTreatmentObj bean;


        public MyViewHolder(View view) {
            super(view);
            treatmentName = view.findViewById(R.id.spaTreatmentName);
            treatmentCost = view.findViewById(R.id.spaTreatmentCost);

            currency = view.findViewById(R.id.spaTreatmentCurrency);
            treatmentDesc = view.findViewById(R.id.spaTreatmentDescription);
            btnAddTreatment = view.findViewById(R.id.spaTreatmentAdd);
            btnExpandTreatment = view.findViewById(R.id.spa_expand_treatment);

            txtReadMore = view.findViewById(R.id.spa_read_more);

            disclaimerTxt = view.findViewById(R.id.disclaimer_text);
            disclaimerlayout = view.findViewById(R.id.spadetails_disclaimer_layout);

            bookingLayout = view.findViewById(R.id.spa_booking_layout);
            if (Consts.APP_CURRENT_STATE > 0) {
                view.setOnClickListener(this);
            }

            btnAddTreatment.setVisibility(Consts.APP_CURRENT_STATE > 0 ? View.VISIBLE : View.GONE);
        }

        @Override
        public void onClick(View view) {
            if (bookingLayout.getVisibility() == View.VISIBLE) {
                bookingLayout.setVisibility(View.GONE);
            } else {
                bookingLayout.setVisibility(View.VISIBLE);


            }
        }
    }


    public SpaDetailsAdapter(Context mContext, ICart mlistener, String spaHotelTimeZone, SpaTreatmentTypeObj obj, SpaAvailabilityTimeObj mTimeAvailabilityObj) {
        cartListener = mlistener;
        this.context = mContext;
        this.spaTreatmentTypeObj = obj;
        SpaHotelTimeZone = spaHotelTimeZone;
        spaTimeAvailabilityObj = mTimeAvailabilityObj;
        Log.d(TAG, " SpaDetailsAdapter Available time Open Time .."+spaTimeAvailabilityObj.getOpeningTime()+" /Close Time, "+spaTimeAvailabilityObj.getClosingTime());
        realm = RealmUtil.getInstance();


    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_spa_treatment, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull final MyViewHolder holder, int position) {

        if (holder.bean == null || holder.spaPosition != position) {
            final SpaTreatmentObj treatmentObj = spaTreatmentTypeObj.getTreatment().get(position);
            if (treatmentObj != null) {
                holder.bean = realm.copyFromRealm(treatmentObj);
            }
            holder.spaPosition = position;
        }
        final SpaTreatmentObj bean = holder.bean;

        if( bean != null) {
            holder.treatmentName.setText(bean.getTreatmentName());
      /*  try {
            holder.treatmentCost.setText(String.valueOf( NumberFormat.getInstance().parse(bean.getPrice()).intValue()));
        } catch (ParseException e) {
            e.printStackTrace();
        }*/
            //holder.treatmentDuration.setText(bean.getTime());
            //holder.treatmentCost.setText(bean.getTimeAndPrice().first().getPrice());
            //holder.treatmentCost.setText(bean.getBookingTimeAndPrice()!=null? bean.getBookingTimeAndPrice().getPrice():bean.getTimeAndPrice().first().getPrice());
            holder.treatmentCost.setText(bean.getSpaSelectedDuartion() > -1 ? bean.getTimeAndPrice().get(bean.getSpaSelectedDuartion()).getPrice() : bean.getTimeAndPrice().first().getPrice());

            holder.currency.setText(RealmUtil.getSpaCurrency());
            holder.treatmentDesc.setText(bean.getDescription());
            holder.disclaimerTxt.setText(bean.getDisclaimer());

            if (bean.getDisclaimer() == null || bean.getDisclaimer().isEmpty())
                holder.disclaimerlayout.setVisibility(View.GONE);
            else
                holder.disclaimerlayout.setVisibility(View.VISIBLE);

            populateBookingLayout(position, holder, bean);


            holder.btnAddTreatment.setBackgroundResource(bean.isSelected() ? R.drawable.selected_item_tick : R.drawable.add_item_spa);
            holder.btnExpandTreatment.setVisibility(bean.isSelected() ? View.VISIBLE : View.GONE);

            holder.treatmentDesc.post(new Runnable() {
                @Override
                public void run() {
                    // Perform any actions you want based on the line count here.
                    int count = holder.treatmentDesc.getLineCount();
                    holder.txtReadMore.setVisibility(count < 4 ? View.GONE : View.VISIBLE);
                }
            });


            holder.txtReadMore.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    holder.treatmentDesc.toggle();
                    holder.txtReadMore.setText(holder.treatmentDesc.isExpanded() ? R.string.read_more : R.string.read_less);

                }
            });

            holder.btnAddTreatment.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    holder.bookingLayout.setVisibility(holder.bookingLayout.getVisibility() == View.VISIBLE ? View.GONE : View.VISIBLE);
                    if (bean.isSelected()) {
                        holder.btnAddTreatment.setBackgroundResource(R.drawable.add_item_spa);
                        holder.bookingLayout.setVisibility(View.GONE);
                        holder.btnExpandTreatment.setVisibility(View.GONE);
                        isUnselected = true;
                        realm.executeTransaction(new Realm.Transaction() {
                            @Override
                            public void execute(@NonNull Realm realm) {
                                bean.setSubmit(false);
                                bean.setSelected(false);
                                bean.setCount(0);
                                bean.setMalePaxCount(0);
                                bean.setFemalePaxCount(0);
                                bean.setCoupleCount(0);
                                realm.copyToRealmOrUpdate(bean);
                            }
                        });

                        cartListener.updateCartInfo();
                    } else {
                        isUnselected = false;
                        realm.executeTransaction(new Realm.Transaction() {
                            @Override
                            public void execute(@NonNull Realm realm) {
                                bean.setSubmit(false);
                            }
                        });

                        notifyDataSetChanged();
                    }

                }
            });

            holder.btnExpandTreatment.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    holder.bookingLayout.setVisibility(holder.bookingLayout.getVisibility() == View.VISIBLE ? View.GONE : View.VISIBLE);
                }
            });
        }
    }

    @Override
    public int getItemCount() {
        return spaTreatmentTypeObj.getTreatment().size();
    }

    private String priceData;
    private int OpenTime_24_hr, OpenTime_24_min, CloseTime_24_hr, CloseTime_24_min;

    void spinnerClick(){

    }
    private int currentHour = 0;
    private int currentMinute = 0;
    int compareDateresult =0;
    private void populateBookingLayout(final int position, final MyViewHolder holder, final SpaTreatmentObj bean) {
        holder.bookingLayout.removeAllViews();
        if(spaTimeAvailabilityObj != null) {
            SpaOpenTime = spaTimeAvailabilityObj.getOpeningTime();
            SpaCloseTime = spaTimeAvailabilityObj.getClosingTime();
        }

        if(SpaHotelTimeZone.equals("GST")){
            SpaHotelTimeZone="Asia/Dubai";
        }


        final View view = ((Activity) context).getLayoutInflater().inflate(R.layout.item_spa_book_layout, null);
        view.animate();
        LinearLayout layoutDate = view.findViewById(R.id.layout_spa_date);
        final TextView dateView = view.findViewById(R.id.spa_book_date);
        final TimePicker timePicker = view.findViewById(R.id.spa_time_picker);
        final TextView btnOk = view.findViewById(R.id.spa_details_btn_ok);
        LinearLayout layoutTreatmentType = view.findViewById(R.id.spa_treatment_type_layout);
        if(isUnselected) {
            btnOk.setEnabled(true);
//           bean.setCoupleCount(0);
        }
        LinearLayout spinnerLayout = view.findViewById(R.id.spa_spinnerlayout);
        final Spinner serviceTimepicker = view.findViewById(R.id.spa_servicetimepicker);
        final ArrayList<String>timelist = new ArrayList<>(bean.getTimeAndPrice().size());
        for(SpaTimeAndPriceObj timeObj : bean.getTimeAndPrice())
            timelist.add(timeObj.getTime());


        ArrayAdapter<String> spinnerAdapter = new ArrayAdapter<String>(context, android.R.layout.simple_spinner_item, timelist){
            //By using this method we will define how
            // the text appears before clicking a spinner
            public View getView(int position, View convertView,
                                ViewGroup parent) {
                View v = super.getView(position, convertView, parent);
                ((TextView) v).setTextColor(Color.parseColor("#8C8179"));
                return v;
            }
            //By using this method we will define
            //how the listview appears after clicking a spinner
            public View getDropDownView(int position, View convertView,
                                        ViewGroup parent) {
                View v = super.getDropDownView(position, convertView,parent);
                ((TextView) v).setTextColor(Color.parseColor("#8C8179"));
                return v;
            }
        };


        spinnerAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        serviceTimepicker.setAdapter(spinnerAdapter);

        if(bean.isDurationSelected()){
            serviceTimepicker.setSelection(bean.getSpaSelectedDuartion(), false);
        }else
            serviceTimepicker.setSelection(0, false);

        serviceTimepicker.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {


                priceData = bean.getTimeAndPrice().get(position).getPrice();
                holder.treatmentCost.setText(priceData);
                if (!realm.isInTransaction())
                    realm.beginTransaction();
                bean.setDurationSelected(true);
                bean.setSubmit(false);
                bean.setSpaSelectedDuartion(position);
                bean.setBookingTimeAndPrice(bean.getTimeAndPrice().get(position));
                realm.commitTransaction();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        layoutTreatmentType.removeAllViews();
        if (bean.getCouplesTreatment().equals("true")) {
            layoutTreatmentType.addView(getCountLayout(context.getResources().getString(R.string.no_of_couples), bean, timePicker));
        } else if (bean.getGender().equalsIgnoreCase("Male")) {
            layoutTreatmentType.addView(getCountLayout(context.getResources().getString(R.string.male_pax), bean, timePicker));
        }else if (bean.getGender().equalsIgnoreCase("Female")) {
            layoutTreatmentType.addView(getCountLayout(context.getResources().getString(R.string.female_pax), bean, timePicker));
        }else{
            layoutTreatmentType.setWeightSum(2);
            layoutTreatmentType.addView(getCountLayout(context.getResources().getString(R.string.female_pax), bean, timePicker), 0);
            layoutTreatmentType.addView(getCountLayout(context.getResources().getString(R.string.male_pax), bean, timePicker), 1);
        }


        // get Hotel Specific tIme Zone..

        final TimeZone timeZone=TimeZone.getTimeZone(SpaHotelTimeZone);

        final Calendar c = Calendar.getInstance(timeZone);
        mYear = c.get(Calendar.YEAR);
        mMonth = c.get(Calendar.MONTH);
        mDay = c.get(Calendar.DAY_OF_MONTH);
        btnOk.setEnabled(!bean.isSubmit() || !bean.isSelected());

        Date c_date,selected_date;

        if (bean.getBookingDate() != null) {
            try {
                SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
                sdf.setTimeZone(TimeZone.getTimeZone(SpaHotelTimeZone));
                c_date = new Date(c.get(Calendar.DATE));
                selected_date= DateUtil.getDate(bean.getBookingDate());
                compareDateresult = sdf.format(selected_date).compareTo(sdf.format(c_date));
                Log.d(TAG, "ppltBLayout compare Date booking date .." + compareDateresult);
            } catch (ParseException e) {
                e.printStackTrace();
            }
        }

        if(bean.getBookingDate()!= null && compareDateresult > 0 ) {

            if (bean.getBookingTime() != null) {
                 try {
                    timePicker.setCurrentHour(DateUtil.getTimepickerDate(bean.getBookingTime()).getHours());
                    timePicker.setCurrentMinute(DateUtil.getTimepickerDate(bean.getBookingTime()).getMinutes());
                } catch (ParseException e) {
                    e.printStackTrace();
                }
            } else {
                timePicker.setCurrentMinute(c.get(Calendar.MINUTE));

            }
        }else {
            if(bean.getBookingTime()!= null){
                try {
                     timePicker.setCurrentHour(DateUtil.getTimepickerDate(bean.getBookingTime()).getHours());
                    timePicker.setCurrentMinute(DateUtil.getTimepickerDate(bean.getBookingTime()).getMinutes());
                } catch (ParseException e) {
                    e.printStackTrace();
                }
            }else {
                timePicker.setCurrentHour(c.get(Calendar.HOUR_OF_DAY));
                timePicker.setCurrentMinute(c.get(Calendar.MINUTE));
                if(!checkTimePickerValidity(timePicker,SpaOpenTime,SpaCloseTime)){

                    timePicker.setEnabled(false);
                }
            }

        }


        if (bean.getBookingDate() != null) {
            try {
                c.setTime(DateUtil.getDate(bean.getBookingDate()));
            } catch (ParseException e) {
                e.printStackTrace();
            }
        }

        realm.executeTransaction(new Realm.Transaction() {
            @Override
            public void execute(Realm realm) {
                if (bean.getBookingDate() == null)
                    bean.setBookingDate(DateUtil.getCurrentDateInSpecificFormat(c));
                if (bean.getBookingTime() == null)
                    bean.setBookingTime(DateUtil.getTimepickerTime(timePicker));
                if(bean.getSpaSelectedDuartion() <0){
                    bean.setSpaSelectedDuartion(0);
                    bean.setBookingTimeAndPrice(bean.getTimeAndPrice().first());
                }
            }
        });


        dateView.setText(bean.getBookingDate() != null ? bean.getBookingDate() : DateUtil.getCurrentDateInSpecificFormat(c));

        final DatePickerDialog.OnDateSetListener dates = new DatePickerDialog.OnDateSetListener() {

            @Override
            public void onDateSet(DatePicker view, int year, int monthOfYear,
                                  int dayOfMonth) {
                c.set(year, monthOfYear, dayOfMonth);
                if (!realm.isInTransaction())
                    realm.beginTransaction();
                bean.setSubmit(false);
                bean.setBookingDate(DateUtil.getCurrentDateInSpecificFormat(c));
                notifyItemRangeChanged(position, 1, bean);
                realm.commitTransaction();
            }

        };

        //  ------------------New  Implementation Mala...........
        layoutDate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Log.d(TAG,"ppltBLayout click listener min date.."+ c.getTimeInMillis());
                Calendar c_calender = Calendar.getInstance();
                mYear = c_calender.get(Calendar.YEAR);
                mMonth = c_calender.get(Calendar.MONTH);
                mDay = c_calender.get(Calendar.DAY_OF_MONTH);
                DatePickerDialog datePickerDialog = new DatePickerDialog(context, dates, mYear, mMonth, mDay);
                datePickerDialog.getDatePicker().setMinDate(c_calender.getTimeInMillis());
                if (bean.getBookingDate() != null) {
                    try {
                        c_calender.setTime(DateUtil.getDate(bean.getBookingDate()));
                        mYear = c_calender.get(Calendar.YEAR);
                        mMonth = c_calender.get(Calendar.MONTH);
                        mDay = c_calender.get(Calendar.DAY_OF_MONTH);
                        datePickerDialog.updateDate(mYear,mMonth,mDay);
                    } catch (ParseException e) {
                        e.printStackTrace();
                    }
                }
                datePickerDialog.show();
            }
        });

        OpenTime_24_hr = DateUtil.getHr_in24Hr_Timeformat(SpaOpenTime);
        OpenTime_24_min = DateUtil.getMin_24Hr_Timeformat(SpaOpenTime);
        CloseTime_24_hr = DateUtil.getHr_in24Hr_Timeformat(SpaCloseTime);
        CloseTime_24_min = DateUtil.getMin_24Hr_Timeformat(SpaCloseTime);

        timePicker.setOnTimeChangedListener(new TimePicker.OnTimeChangedListener() {
            @Override
            public void onTimeChanged(TimePicker timePicker, int hourOfDay, int minute) {
                boolean validTime = true;
                if (hourOfDay < OpenTime_24_hr || (hourOfDay == OpenTime_24_hr && minute < OpenTime_24_min)){
                    validTime = false;
                    timePicker.setCurrentHour(OpenTime_24_hr);
                    timePicker.setCurrentMinute(OpenTime_24_min);
                }
                if (hourOfDay > CloseTime_24_hr || (hourOfDay == CloseTime_24_hr && minute > CloseTime_24_min)){
                    validTime = false;
                    timePicker.setCurrentHour(CloseTime_24_hr);
                    timePicker.setCurrentMinute(CloseTime_24_min);
                }
                if (validTime) {
                    currentHour = hourOfDay;
                    currentMinute = minute;
                    if (!realm.isInTransaction())
                        realm.beginTransaction();
                    bean.setSubmit(false);
                    btnOk.setEnabled(bean.isSubmit() == true ? false : true);
                    bean.setBookingTime(DateUtil.getTimepickerTime(timePicker));
                    realm.commitTransaction();
                    Log.d(TAG,"ppltBLayout 33onTimeChanged.."+ hourOfDay+" : "+minute);
                    Log.d(TAG,"ppltBLayout !!!!Bean booking time.."+bean.getBookingTime());
                }
            }
        });

        btnOk.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View view) {

                timePicker.setEnabled(false);
                realm.executeTransaction(new Realm.Transaction() {
                    @Override
                    public void execute(Realm realm) {

                        if (bean.getMalePaxCount() > 0 || bean.getFemalePaxCount() > 0 || bean.getCoupleCount() > 0) {
                            bean.setSubmit(true);
                            bean.setSelected(true);
                            bean.setCount(1);
                            cartListener.updateCartInfo();
                        } else {
                            bean.setSelected(false);
                            bean.setCount(0);
                            cartListener.updateCartInfo();
                            Toast.makeText(context, R.string.select_pax_msg, Toast.LENGTH_SHORT).show();
                        }
                        notifyDataSetChanged();
                        realm.copyToRealmOrUpdate(bean);
                    }
                });
                cartListener.updateCartInfo();
            }

        });
        holder.bookingLayout.addView(view);
    }

    private boolean checkTimePickerValidity(TimePicker timePicker, String spaOpenTime, String spaCloseTime) {
        int hourOfDay= timePicker.getCurrentHour();
        int  minuteOfDay = timePicker.getCurrentMinute();

        OpenTime_24_hr = DateUtil.getHr_in24Hr_Timeformat(SpaOpenTime);
        OpenTime_24_min = DateUtil.getMin_24Hr_Timeformat(SpaOpenTime);
        CloseTime_24_hr = DateUtil.getHr_in24Hr_Timeformat(SpaCloseTime);
        CloseTime_24_min = DateUtil.getMin_24Hr_Timeformat(SpaCloseTime);
        if (hourOfDay > CloseTime_24_hr || (hourOfDay == CloseTime_24_hr && minuteOfDay > OpenTime_24_min)){
            return false;

        }else
            return true;
    }


    private View getCountLayout(final String type, final SpaTreatmentObj bean, final TimePicker timePicker) {

        LinearLayout view = (LinearLayout) ((Activity) context).getLayoutInflater().inflate(R.layout.item_spa_treatment_type_count_layout, null);
        view.setGravity(Gravity.CENTER);
        LinearLayout.LayoutParams param = new LinearLayout.LayoutParams(0, LinearLayout.LayoutParams.WRAP_CONTENT);
        param.weight = 1;

        view.setLayoutParams(param);
        TextView coupleText = view.findViewById(R.id.type);
        TextView btnIncrease = view.findViewById(R.id.btn_increase);
        final TextView countTxt = view.findViewById(R.id.item_count);

        TextView btnDecrease = view.findViewById(R.id.btn_decrease);
        final TextView btnAdd = view.findViewById(R.id.btn_spa_add);
        final LinearLayout countLayout = view.findViewById(R.id.count_layout);
        switch (type) {
            case "Male pax":
                coupleText.setText(type);
                countTxt.setText(String.valueOf(bean.getMalePaxCount()));
                btnAdd.setVisibility(bean.getMalePaxCount() <= 0 ? View.VISIBLE : View.GONE);
                countLayout.setVisibility(bean.getMalePaxCount() < 1 ? View.GONE : View.VISIBLE);


                break;

            case "Female pax":
                coupleText.setText(type);
                countTxt.setText(String.valueOf(bean.getFemalePaxCount()));
                btnAdd.setVisibility(bean.getFemalePaxCount() <= 0 ? View.VISIBLE : View.GONE);
                countLayout.setVisibility(bean.getFemalePaxCount() < 1 ? View.GONE : View.VISIBLE);

                break;

            case "No. of couples":
                coupleText.setText(type);
                countTxt.setText(String.valueOf(bean.getCoupleCount()));
                btnAdd.setVisibility(bean.getCoupleCount() <= 0 ? View.VISIBLE : View.GONE);
                countLayout.setVisibility(bean.getCoupleCount() < 1 ? View.GONE : View.VISIBLE);

                break;

        }


        btnAdd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                btnAdd.setVisibility(View.GONE);
                countLayout.setVisibility(View.VISIBLE);

                realm.executeTransaction(new Realm.Transaction() {
                    @Override
                    public void execute(Realm realm) {
                        bean.setSubmit(false);
                        if (type.equals(context.getResources().getString(R.string.male_pax))) {
                            bean.setMalePaxCount(1);
                        } else if (type.equals(context.getResources().getString(R.string.female_pax))) {
                            bean.setFemalePaxCount(1);
                        } else if (type.equals(context.getResources().getString(R.string.no_of_couples))) {
                            bean.setCoupleCount(1);
                        }
                        timePicker.setEnabled(false);
                        notifyDataSetChanged();
                    }
                });

            }
        });


        btnIncrease.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {


                realm.executeTransaction(new Realm.Transaction() {
                    @Override
                    public void execute(Realm realm) {
                        bean.setSubmit(false);
                        if (type.equals(context.getResources().getString(R.string.male_pax))) {
                            int count = bean.getMalePaxCount() + 1;
                            bean.setMalePaxCount(count > 0 ? count : 0);
                        } else if (type.equals(context.getResources().getString(R.string.female_pax))) {
                            int count = bean.getFemalePaxCount() + 1;
                            bean.setFemalePaxCount(count > 0 ? count : 0);
                        } else if (type.equals(context.getResources().getString(R.string.no_of_couples))) {
                            int count = bean.getCoupleCount() + 1;
                            bean.setCoupleCount(count > 0 ? count : 0);
                        }
                        timePicker.setEnabled(false);
                        notifyDataSetChanged();
                    }
                });


            }
        });

        btnDecrease.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {


                realm.executeTransaction(new Realm.Transaction() {
                    @Override
                    public void execute(Realm realm) {
                        bean.setSubmit(false);
                        if (type.equals(context.getResources().getString(R.string.male_pax))) {
                            int count = bean.getMalePaxCount() - 1;

                            bean.setMalePaxCount(count > 0 ? count : 0);
                        } else if (type.equals(context.getResources().getString(R.string.female_pax))) {
                            int count = bean.getFemalePaxCount() - 1;
                            bean.setFemalePaxCount(count > 0 ? count : 0);
                        } else if (type.equals(context.getResources().getString(R.string.no_of_couples))) {
                            int count = bean.getCoupleCount() - 1;
                            bean.setCoupleCount(count > 0 ? count : 0);
                        }
                        timePicker.setEnabled(false);
                        notifyDataSetChanged();
                    }

                });


            }
        });
        return view;
    }


}
