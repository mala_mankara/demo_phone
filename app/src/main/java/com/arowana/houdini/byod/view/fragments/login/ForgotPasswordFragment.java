package com.arowana.houdini.byod.view.fragments.login;


import android.app.ProgressDialog;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import com.google.android.material.textfield.TextInputLayout;
import androidx.fragment.app.Fragment;
import android.text.Html;
import android.text.TextUtils;
import android.view.View;
import android.widget.TextView;

import com.arowana.houdini.byod.Consts;
import com.arowana.houdini.byod.R;
import com.arowana.houdini.byod.interfaces.ApiClient;
import com.arowana.houdini.byod.interfaces.api_interface.LoginApiInterface;
import com.arowana.houdini.byod.model.login.ForgotPasswordResponseObject;
import com.arowana.houdini.byod.utils.DialogUtil;
import com.arowana.houdini.byod.utils.DisplayUtils;
import com.arowana.houdini.byod.utils.NetworkUtils;
import com.arowana.houdini.byod.utils.TextUtil;
import com.arowana.houdini.byod.utils.ViewUtils;
import com.arowana.houdini.byod.view.activity.LoginActivity;
import com.arowana.houdini.byod.view.fragments.BaseFragment;
import com.google.gson.JsonObject;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.arowana.houdini.byod.view.activity.LoginActivity.fragmentHolder;

/**
 * A simple {@link Fragment} subclass.
 */
public class ForgotPasswordFragment extends BaseFragment implements View.OnClickListener {


    public ForgotPasswordFragment() {
        // Required empty public constructor
    }


    TextView mBtnCancel, mBtnReset, mSignupTxt;

    TextInputLayout mEmail;
    View mRootView;

    ProgressDialog mDialog;
    View view;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);


    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        mRootView = view;
        setTitle(getResources().getString(R.string.forgot_password));
        initComponents();
        initListeners();
        mDialog = DialogUtil.showProgressDialog(getActivity(), "");
    }


    public void initComponents() {
        mEmail = mRootView.findViewById(R.id.forgot_pwd_email);
        mBtnCancel = mRootView.findViewById(R.id.password_cancel);
        mBtnReset = mRootView.findViewById(R.id.password_reset);
        mSignupTxt = mRootView.findViewById(R.id.forgot_pwd_navigate_txt);

        mEmail.getEditText().setHint(TextUtils.concat(getString(R.string.email), Html.fromHtml(getString(R.string.required_asterisk))));

    }

    public void initListeners() {
        mBtnCancel.setOnClickListener(this);
        mBtnReset.setOnClickListener(this);
        mSignupTxt.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
        DisplayUtils.getInstance(getContext()).hideKeyBoard();
        int id = view.getId();
        switch (id) {
            case R.id.password_cancel:
                getActivity().onBackPressed();
                break;
            case R.id.password_reset:
                if (!TextUtil.isEmpty(mEmail, "Email required")) {
                    if (TextUtil.isEmailValid(mEmail, "Enter valid Email")) {
                        if (NetworkUtils.checkConnectivityStatus(getActivity())) {
                            mDialog.show();
                            callAPI();
                        }
                    }
                }


                break;
            case R.id.forgot_pwd_navigate_txt:
                mHandler.showFragment(fragmentHolder.getId(), new SignupFragment());
                break;
        }
    }
    ResetPasswordFragment fragment;
    public void callAPI() {

        Call<ForgotPasswordResponseObject> responseObj;
        if (mDialog.isShowing())
            mDialog.dismiss();

        responseObj = ApiClient.getApiClient(LoginApiInterface.class).forgotPassword(mEmail.getEditText().getText().toString(), Consts.DEVICE_MAC_ID);


        responseObj.enqueue(new Callback<ForgotPasswordResponseObject>() {
            @Override
            public void onResponse(Call<ForgotPasswordResponseObject> call, Response<ForgotPasswordResponseObject> response) {
                String flag = response.body().getResponseStatus().getResponseFlag();
                String message = response.body().getResponseStatus().getResponseMessage();
                if (flag.equals("SUCCESS")) {


                    JsonObject jsonObject = new JsonObject();
                    jsonObject.addProperty("UserId", mEmail.getEditText().getText().toString().trim());
                    jsonObject.addProperty("NewPassword", "");
                    jsonObject.addProperty("ConfirmationCode",  response.body().getResponseData().getConfirmationCode());

                    fragment = new ResetPasswordFragment();
                    fragment.resetPassword(jsonObject);
                    ViewUtils.showSnackBarMessage(message);
                    Handler handler = new Handler(Looper.getMainLooper());
                    handler.postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            mHandler.showFragment(LoginActivity.fragmentHolder.getId(), fragment);
                            mHandler.clearBackStackFragment(getTag());
                        }
                    }, 4010);
                } else {
                    ViewUtils.showSnackBarMessage(message);
                }
            }

            @Override
            public void onFailure(Call<ForgotPasswordResponseObject> call, Throwable t) {
                t.printStackTrace();
            }
        });
    }

    @Override
    protected int getFragmentLayout() {
        return R.layout.fragment_forgot_password;
    }
}
