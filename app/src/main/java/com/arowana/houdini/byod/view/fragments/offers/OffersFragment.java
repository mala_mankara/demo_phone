package com.arowana.houdini.byod.view.fragments.offers;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import com.arowana.houdini.byod.R;
import com.arowana.houdini.byod.interfaces.ApiClient;
import com.arowana.houdini.byod.interfaces.api_interface.OffersApiInterface;
import com.arowana.houdini.byod.model.offers.OffersConfigObject;
import com.arowana.houdini.byod.model.offers.OffersResponseStatus;
import com.arowana.houdini.byod.utils.DialogUtil;
import com.arowana.houdini.byod.utils.ViewUtils;
import com.arowana.houdini.byod.view.adapters.offers.OffersAdapter;
import com.arowana.houdini.byod.view.fragments.BaseFragment;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.arowana.houdini.byod.view.activity.OffersActivity.fragmentHolder;

/**
 * A simple {@link Fragment} subclass.
 */
public class OffersFragment extends BaseFragment {

    private View mRootView;
    private Context mContext;
    public static final String TAG="Offers";
    private RecyclerView mRecyclerView;
    private ProgressDialog mDialog;


    public OffersFragment() {
        // Required empty public constructor
    }

    @Override
    protected int getFragmentLayout() {
        return R.layout.fragment_offers;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mContext=getActivity();
        setTitle(getResources().getString((R.string.offers)));
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        mRootView=view;
        mDialog = DialogUtil.showProgressDialog(getActivity(), "");
        mDialog.show();
        initView();
        getData();
    }

    private void getData() {

        Call<OffersConfigObject> offersConfigObject = ApiClient.getApiClient(OffersApiInterface.class).getOfferDetails("2X");
        offersConfigObject.enqueue(new Callback<OffersConfigObject>() {
            @Override
            public void onResponse(@NonNull Call<OffersConfigObject> call, @NonNull Response<OffersConfigObject> response) {
                if (mDialog.isShowing())
                    mDialog.dismiss();
                if (response.isSuccessful() && response.body() != null) {
                    Log.d(TAG, "response body " + response.body());

                    OffersConfigObject configObject = response.body();
                    if (configObject != null) {
                        if (((configObject).getResponseStatus() != null) && configObject.getResponseStatus().isValid()) {
                            OffersResponseStatus responsestatusObj = configObject.getResponseStatus();

                            String flag = responsestatusObj.getResponseFlag();
                            String message = responsestatusObj.getResponseMessage();
                            if (flag.equals("SUCCESS")) {
                                OffersConfigObject obj = response.body();
                                if(obj.getResponseData() != null)
                                    populateData(obj);
                            } else ViewUtils.showSnackBarMessage(message);
                        }
                    } else {
                        Toast.makeText(getActivity(), "No Data To display..", Toast.LENGTH_SHORT).show();
                    }
                }
            }

            @Override
            public void onFailure(@NonNull Call<OffersConfigObject> call, @NonNull Throwable t) {
                if (mDialog.isShowing())
                    mDialog.dismiss();
                t.printStackTrace();
            }
        });

    }

    private void populateData(OffersConfigObject obj) {
        LinearLayoutManager linearVertical = new LinearLayoutManager(mContext, RecyclerView.VERTICAL, false);
        mRecyclerView.setLayoutManager(linearVertical);
        OffersAdapter adapter = new OffersAdapter(getContext(), obj, mHandler,fragmentHolder);
        mRecyclerView.setAdapter(adapter);
        adapter.notifyDataSetChanged();
    }

    private void initView() {
        mRecyclerView = mRootView.findViewById(R.id.offer_recyclerview);


    }



}
