package com.arowana.houdini.byod.view.fragments.facilityservice;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.Bundle;
import android.view.View;

import androidx.annotation.Nullable;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.arowana.houdini.byod.R;
import com.arowana.houdini.byod.model.facilitiesAndServices.FServiceAmenities;
import com.arowana.houdini.byod.utils.DialogUtil;
import com.arowana.houdini.byod.view.adapters.facilityServices.FacilityDetailsAdapter;
import com.arowana.houdini.byod.view.fragments.BaseFragment;

import io.realm.RealmList;


public class FacilityDetailsFragment extends BaseFragment {

    private View mRootView;
    private Context mContext;
    public static final String TAG="FacilityDetailsFragment";
    RealmList<FServiceAmenities> detailsObj;
    ProgressDialog mDialog;
    String title;

    public FacilityDetailsFragment() {
    }

    @Override
    protected int getFragmentLayout() {
        return R.layout.fragment_offers;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mContext = getActivity();


    }
    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        mRootView=view;
        mDialog = DialogUtil.showProgressDialog(getActivity(), "");
       // mDialog.show();
        setTitle(title);
        initView();

    }

    private void initView() {
        RecyclerView mRecyclerView = mRootView.findViewById(R.id.offer_recyclerview);
        mRecyclerView.setLayoutManager(new LinearLayoutManager(mContext));
        mRecyclerView.setBackgroundColor(getResources().getColor(R.color.color_white));
        FacilityDetailsAdapter adapter = new FacilityDetailsAdapter(mContext, detailsObj,mHandler);
        mRecyclerView.setAdapter(adapter);
        adapter.notifyDataSetChanged();
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    public void setDetailsObj(RealmList<FServiceAmenities> amenities, String detailTitle) {
        detailsObj = amenities;
        title      = detailTitle;
    }
}
