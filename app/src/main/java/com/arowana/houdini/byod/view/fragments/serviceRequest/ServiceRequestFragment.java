package com.arowana.houdini.byod.view.fragments.serviceRequest;

import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.content.Context;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.WindowManager;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.AutoCompleteTextView;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.arowana.houdini.byod.Consts;
import com.arowana.houdini.byod.MainApplication;
import com.arowana.houdini.byod.R;
import com.arowana.houdini.byod.interfaces.ApiClient;
import com.arowana.houdini.byod.interfaces.api_interface.ServiceRequestApiInterface;
import com.arowana.houdini.byod.interfaces.listeners.UpdateBundleListener;
import com.arowana.houdini.byod.model.servicerequest.AvailTime;
import com.arowana.houdini.byod.model.servicerequest.ServiceLocationConfigObject;
import com.arowana.houdini.byod.model.servicerequest.ServiceLocationResponseData;
import com.arowana.houdini.byod.model.servicerequest.ServiceRequestConfigObject;
import com.arowana.houdini.byod.model.servicerequest.ServiceRequestResponseData;
import com.arowana.houdini.byod.model.servicerequest.ServiceResponseStatusObject;
import com.arowana.houdini.byod.utils.NavigationConsts;
import com.arowana.houdini.byod.utils.RealmUtil;
import com.arowana.houdini.byod.utils.ViewUtils;
import com.arowana.houdini.byod.view.adapters.CustomListAdapter;
import com.arowana.houdini.byod.view.fragments.BaseFragment;
import com.arowana.houdini.byod.view.fragments.telephone.PhoneCallFragment;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.Objects;

import alcatel.model.ByodManager;
import io.realm.Realm;
import io.realm.RealmList;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static android.view.View.GONE;
import static android.view.View.VISIBLE;
import static com.arowana.houdini.byod.view.activity.ServiceRequestActivity.fragmentHolder;

public class ServiceRequestFragment extends BaseFragment implements View.OnClickListener, UpdateBundleListener {

    private final static String SERVICE_TYPE_REQUEST = "REQUEST";
    private final static String SERVICE_TYPE_PICK_UP = "PICK UP";
    public static final String TAG = "ServiceRequest_Fragment";

    Context mContext;
    View mRootView;

    RealmList<ServiceLocationResponseData> locationObj;
    private AutoCompleteTextView locationListView;

    private static RealmList<ServiceRequestResponseData> mServiceList;
    private LinearLayout mServiceListHolder;
    private ProgressBar pBar;
    private TextView panelRequest;
    private FrameLayout layout;
    private TextView mSelectedRoomTxt;
    private TextView otherRequest;
    private View serviceView;
    private int mCount = 0;
    JSONArray services;

    Realm mRealm;
    String locData;
    String timeRequest;
    private String speedDialNo;
    private ProgressDialog mDialog;

    @Override
    protected int getFragmentLayout() {
        return R.layout.fragment_service_request;
    }

    public ServiceRequestFragment() {   }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mContext = getActivity();

    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        setTitle(getResources().getString(R.string.service_request));
        mRootView = view;
        mRealm = RealmUtil.getInstance();
        initComponents();
       // mDialog = DialogUtil.showProgressDialog(getActivity(), "");
        mDialog = new ProgressDialog(getActivity());
        mDialog.setMessage("Please wait..");
        mDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        mDialog.show();
        getActivity().getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN);
        getLocationObj();
        fetchJSONBody();
    }

    private void initComponents() {
        locationListView    = mRootView.findViewById(R.id.add_locations);
        otherRequest        = mRootView.findViewById(R.id.other_request);

        pBar                = mRootView.findViewById(R.id.service_progress);
        layout              = Objects.requireNonNull(getActivity()).findViewById(R.id.snacbar_layout);
        mServiceListHolder  = mRootView.findViewById(R.id.serviceListHolder);
        View call_conceirge = mRootView.findViewById(R.id.btn_conceirge_serviceRequest);
        panelRequest        = mRootView.findViewById(R.id.btnNext);

       // call_conceirge.setVisibility(Consts.APP_CURRENT_STATE > 1 ? View.VISIBLE : View.GONE);
        if(MainApplication.isContainedSpeedDial("housekeeping")&& Consts.APP_CURRENT_STATE > 1) {
            call_conceirge.setVisibility(View.VISIBLE);
            speedDialNo = MainApplication.getValueforKey();
            Log.d("SERVICE REQUEST..", "is exist no" + speedDialNo);
        }else
            call_conceirge.setVisibility(View.GONE);
        call_conceirge.setOnClickListener(this);
        otherRequest.setHorizontallyScrolling(false);
        otherRequest.setScrollContainer(true);
        otherRequest.setMaxLines(3);
        otherRequest.setImeOptions(EditorInfo.IME_ACTION_DONE);
        initListeners();
    }

    private void initListeners() {
        panelRequest.setOnClickListener(this);
    }

    @SuppressLint({"InflateParams", "SetTextI18n"})
    private void updateServiceList() {
        if(mDialog.isShowing())
            mDialog.dismiss();
        mServiceListHolder.setVisibility(VISIBLE);
        mServiceListHolder.removeAllViews();
        otherRequest.setText("");
        int itemCount = 0;

        int totalItem = mServiceList.size();
        for (final ServiceRequestResponseData service : mServiceList) {

            LayoutInflater inflater                 = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            if (inflater != null) {
                serviceView                         = inflater.inflate(R.layout.item_service_request, null, false);
            }
            mServiceListHolder.addView(serviceView);

            final TextView serviceName              = serviceView.findViewById(R.id.servicename);
            final TextView limitCaption             = serviceView.findViewById(R.id.limitData);
            final TextView txtRequestBtnCaption     = serviceView.findViewById(R.id.txtAdd);

            final LinearLayout panelAddItem         = serviceView.findViewById(R.id.panelAddItem);
            final TextView txtIncrease              = serviceView.findViewById(R.id.txtIncrease);
            final TextView txtCount                 = serviceView.findViewById(R.id.txtCount);
            final TextView txtDecrease              = serviceView.findViewById(R.id.txtDecrease);

            final LinearLayout panelAddButton       = serviceView.findViewById(R.id.panelAddButton);
            final LinearLayout panelCancelItem      = serviceView.findViewById(R.id.panelCancelItem);
            final TextView txtServiceRequestCaption = serviceView.findViewById(R.id.txtChange);
            final TextView txtServiceRequestTiming  = serviceView.findViewById(R.id.txtContent);

            final View bottomLine                   = serviceView.findViewById(R.id.bottomLine);

            itemCount++;
            if (itemCount == totalItem) {
                bottomLine.setVisibility(GONE);
            }


            serviceName.setText(service.getServiceItem());
            txtCount.setText("" + service.getItemCount());

            txtIncrease.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (!mRealm.isInTransaction())
                        mRealm.beginTransaction();
                    service.setItemCount((service.getItemCount() + 1) > Integer.parseInt(service.getMaxRequestQuantity())
                            ? Integer.parseInt(service.getMaxRequestQuantity()) : service.getItemCount() + 1);
                    txtCount.setText("" + service.getItemCount());
                    mRealm.commitTransaction();
                }
            });

            txtDecrease.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (!mRealm.isInTransaction())
                        mRealm.beginTransaction();
                    service.setItemCount((service.getItemCount() - 1) < 0 ? 0 : service.getItemCount() - 1);
                    txtCount.setText("" + service.getItemCount());
                    panelAddButton.setVisibility(service.getItemCount() == 0 ? VISIBLE : GONE);
                    panelAddItem.setVisibility(service.getItemCount() > 0 ? VISIBLE : GONE);
                    mRealm.commitTransaction();
                }
            });

            txtServiceRequestCaption.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    switch (service.getServiceType().toUpperCase()) {
                        case SERVICE_TYPE_REQUEST:
                            MakeMyRoomFragment makeMyRoomFagment = new MakeMyRoomFragment();
                            makeMyRoomFagment.setServiceObject(service);
                            mHandler.showFragment(fragmentHolder.getId(), makeMyRoomFagment);
                            mSelectedRoomTxt = txtServiceRequestTiming;
                            break;

                        case SERVICE_TYPE_PICK_UP:
                            populateSnackbar(false, serviceView, service);
                            break;
                    }
                }
            });


            switch (service.getServiceType().toUpperCase()) {

                case SERVICE_TYPE_REQUEST:
                    txtRequestBtnCaption.setText(R.string.Service_Request);
                    panelAddItem.setVisibility(GONE);
                    panelAddButton.setVisibility(service.getItemCount() == 0 ? VISIBLE : GONE);
                    panelCancelItem.setVisibility(service.getItemCount() > 0 ? VISIBLE : GONE);
                    txtServiceRequestCaption.setText(R.string.Service_Change);
                    limitCaption.setVisibility(GONE);
                    Log.d("", "");
                    int pos = (service.getPos() == -1) ? 0 : service.getPos();
                    if(service.getAvailTime() != null && ! service.getAvailTime().isEmpty()) {
                        AvailTime availTime = service.getAvailTime().get(pos);
                        if (availTime != null)
                            txtServiceRequestTiming.setText(availTime.getTime());
                    }

                    break;

                case SERVICE_TYPE_PICK_UP:
                    txtRequestBtnCaption.setText(R.string.Service_pickUp);
                    panelAddItem.setVisibility(GONE);
                    panelAddButton.setVisibility(service.getItemCount() == 0 ? VISIBLE : GONE);
                    panelCancelItem.setVisibility(service.getItemCount() > 0 ? VISIBLE : GONE);
                    txtServiceRequestCaption.setText(R.string.Service_cancel);
                    limitCaption.setVisibility(GONE);
                    if(service.getAvailTime() != null) {
                        AvailTime availTime = service.getAvailTime().first();
                        if(availTime != null)
                            txtServiceRequestTiming.setText(availTime.getTime());
                    }
                    break;

                default:
                    txtRequestBtnCaption.setText(R.string.Service_add);
                    panelAddButton.setVisibility(service.getItemCount() == 0 ? VISIBLE : GONE);
                    panelAddItem.setVisibility(service.getItemCount() > 0 ? VISIBLE : GONE);
                    limitCaption.setVisibility(VISIBLE);
                    limitCaption.setText("(max. limit- " + service.getMaxRequestQuantity() + ")");
                    panelCancelItem.setVisibility(GONE);
            }

            panelAddButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    switch (service.getServiceType().toUpperCase()) {
                        case SERVICE_TYPE_REQUEST:
                            panelAddItem.setVisibility(GONE);
                            panelAddButton.setVisibility(VISIBLE);
                            panelCancelItem.setVisibility(GONE);

                            MakeMyRoomFragment makeMyRoomFagment = new MakeMyRoomFragment();
                            makeMyRoomFagment.setServiceObject(service);
                            mHandler.showFragment(fragmentHolder.getId(), makeMyRoomFagment);
                            mSelectedRoomTxt = txtServiceRequestTiming;

                            break;
                        case SERVICE_TYPE_PICK_UP:
                            if (!mRealm.isInTransaction())
                                mRealm.beginTransaction();
                            service.setItemCount(1);
                            txtCount.setText("" + service.getItemCount());
                            mRealm.commitTransaction();
                            panelAddItem.setVisibility(GONE);
                            panelAddButton.setVisibility(GONE);
                            panelCancelItem.setVisibility(VISIBLE);
                            txtServiceRequestCaption.setText("Cancel");
                            if(service.getAvailTime() != null) {
                                AvailTime availTime = service.getAvailTime().first();
                                if (availTime != null)
                                    txtServiceRequestTiming.setText(availTime.getTime());
                            }
                            break;
                        default:

                            if (!mRealm.isInTransaction())
                                mRealm.beginTransaction();
                            service.setItemCount(1);
                            txtCount.setText("" + service.getItemCount());
                            mRealm.commitTransaction();
                            panelAddItem.setVisibility(VISIBLE);
                            panelAddButton.setVisibility(GONE);
                            panelCancelItem.setVisibility(GONE);
                            break;
                    }
                }
            });

        }
    }

    @SuppressLint("InflateParams")
    private void populateSnackbar(final boolean bool, final View serviceView, final ServiceRequestResponseData service) {
        layout.setVisibility(View.VISIBLE);
        LayoutInflater inflate= Objects.requireNonNull(getActivity()).getLayoutInflater();
        View view = inflate.inflate(R.layout.custom_confirm_snackbar_layout, null);
        //  View view = getActivity().getLayoutInflater().inflate(R.layout.big_custom_snackbar_layout, null);
        TextView message = view.findViewById(R.id.dineMessage);
        TextView mYes = view.findViewById(R.id.snack_ok_message);
        TextView mNo = view.findViewById(R.id.snack_cancel_message);

        if (!bool) {
            message.setText(R.string.Service_cancelRequest_msg);

        }
        mYes.setText(R.string.Service_yes);
        mNo.setText(R.string.Service_no);

        mYes.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                layout.setVisibility(View.GONE);
                layout.removeAllViews();
                if (bool) {

                    try {
                        JSONObject roomObj = new JSONObject();
                        roomObj.put("RoomNo", RealmUtil.getRoomNo());
                        roomObj.put("BookingId", RealmUtil.getBookingId());
                        roomObj.put("Location", locData);
                        roomObj.put("AdditionalRequest", otherRequest.getText().toString());

                        JSONObject jsonObject = new JSONObject();
                        jsonObject.put("MembershipId", "" + RealmUtil.getMembershipId());
                        jsonObject.put("FirstName", "" + RealmUtil.getUserName());
                        jsonObject.put("LastName", "" + RealmUtil.getLastName());
                        jsonObject.put("PhoneNumber", "" + RealmUtil.getMobileNo());
                        jsonObject.put("Email", "" + RealmUtil.getEmailId());

                        roomObj.put("Services", services);
                        roomObj.put("GuestProfile", jsonObject);
                        Log.d(TAG, "Requst data" + roomObj.toString());

                        JsonParser jsonParser = new JsonParser();
                        JsonObject gsonObject = (JsonObject) jsonParser.parse(roomObj.toString());

                        callServiceRequestApi(gsonObject);


                    } catch (Exception e) {
                        e.printStackTrace();
                        e.printStackTrace();
                    }


                } else {
                    if (!mRealm.isInTransaction())
                        mRealm.beginTransaction();
                    service.setItemCount(0);
                    mRealm.commitTransaction();

                    LinearLayout panelAddButton = serviceView.findViewById(R.id.panelAddButton);
                    LinearLayout panelCancelItem = serviceView.findViewById(R.id.panelCancelItem);
                    panelAddButton.setVisibility(VISIBLE);
                    panelCancelItem.setVisibility(GONE);
                    updateServiceList();
                }

            }
        });

        mNo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                layout.setVisibility(View.GONE);
                layout.removeAllViews();
            }
        });

        layout.addView(view);

    }
    String flag;
    String message;
    private void callServiceRequestApi(JsonObject jsonObject) {

        Call<ServiceRequestConfigObject> serviceRequestObject = ApiClient.getApiClient(ServiceRequestApiInterface.class).addedServiceRequest(jsonObject);

        serviceRequestObject.enqueue(new Callback<ServiceRequestConfigObject>() {

            @Override
            public void onResponse(@NonNull Call<ServiceRequestConfigObject> call, @NonNull Response<ServiceRequestConfigObject> response) {
                if(response.body() != null ){
                    ServiceRequestConfigObject responseBody=response.body();
                    if(responseBody != null){
                        ServiceResponseStatusObject responseState=responseBody.getResponseStatus();

                        if(responseState.isValid()) {
                            flag = responseState.getResponseFlag();
                            message=responseState.getResponseMessage();
                            //Toast.makeText(mContext,""+message,Toast.LENGTH_SHORT).show();
                            ViewUtils.showSnackBarMessage(message,getActivity());

                            try {
                                new Handler().postDelayed(new Runnable() {
                                    @Override
                                    public void run() {
                                        getActivity().finish();
                                    }
                                },4100);


                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        }
                    }
                }
            }
            @Override
            public void onFailure(@NonNull Call<ServiceRequestConfigObject> call, @NonNull Throwable t) {
                t.printStackTrace();
            }
        });
    }


    /* @SuppressLint("ClickableViewAccessibility")*/
    @SuppressLint("ClickableViewAccessibility")
    private void displayLocationData(final RealmList<ServiceLocationResponseData> locationObj) {
        CustomListAdapter adapter;
        adapter = new CustomListAdapter(Objects.requireNonNull(getActivity()).getBaseContext(), android.R.layout.simple_spinner_dropdown_item, locationObj);
        locationListView.setAdapter(adapter);
        locationListView.setThreshold(1);
        locationListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Log.d("SERVICE Request Frag...","setOnItemClickListener");
                InputMethodManager in = (InputMethodManager)mContext.getSystemService(Context.INPUT_METHOD_SERVICE);
                if(in != null) {
                    in.hideSoftInputFromWindow(parent.getApplicationWindowToken(), 0);
                }
            }
        });
        locationListView.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                Log.d("SERVICE Request Frag...","onTouch");
                locationListView.showDropDown();
                InputMethodManager in = (InputMethodManager)mContext.getSystemService(Context.INPUT_METHOD_SERVICE);
                if(in != null) {
                    in.hideSoftInputFromWindow(v.getApplicationWindowToken(), 0);
                }
                return false;
            }
        });

        locData = locationListView.getText().toString();

    }

    @Override
    public void onDetach() {
        super.onDetach();
        //  mListener = null;
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.btnNext:

                services = createJsonObjects();
                String otherRequestData=otherRequest.getText().toString();

                if(mCount == 0 && (isEmptyString(otherRequestData))){
                    ViewUtils.showSnackBarMessage(getString(R.string.Service_requestSelect_msg), getActivity());
                }else
                if (mCount > 0 || !(isEmptyString(otherRequestData)))
                    populateSnackbar(true, null, null);


                break;
            case R.id.btn_conceirge_serviceRequest:
                if(!ByodManager.getInstance().isSipConnected()) {
                    Toast.makeText(getActivity(),"Call service not working. Please try later..",Toast.LENGTH_SHORT).show();
                }else {
                    if (!speedDialNo.isEmpty()) {
                        PhoneCallFragment fragment = new PhoneCallFragment();
                        Bundle bundle = new Bundle();
                        bundle.putString("phoneNo", speedDialNo);
                        bundle.putString("name", "housekeeping");
                        bundle.putBoolean("isMakingCall",true);
                        fragment.setArguments(bundle);

                        mHandler.showFragment(fragmentHolder.getId(), fragment);
                    }
                }
                break;
        }
    }

    private JSONArray createJsonObjects() {
        JSONArray mRequestJson = new JSONArray();
        try {
            for (ServiceRequestResponseData responseData : mServiceList) {
                if (responseData.getItemCount() == 0) {
                    continue;
                }
                mCount++;
                JSONObject servicesObject;
                servicesObject = new JSONObject();
                if( responseData.isValid() ){

                    servicesObject.put("ServiceId", responseData.getServiceId());
                    servicesObject.put("ServiceName", responseData.getServiceItem());
                    if (responseData.getServiceType().toUpperCase().equalsIgnoreCase("COUNT")) {
                        servicesObject.put("Quantity", responseData.getItemCount());
                    }
                    String time;
                    if (responseData.getServiceType().toUpperCase().equalsIgnoreCase("REQUEST")) {
                        int  pos=responseData.getPos();
                        AvailTime timeObj=responseData.getAvailTime().get(pos);
                        if (timeObj != null){
                            time = timeObj.getTime();


                            servicesObject.put("AvailTime", time);
                        }
                        //  servicesObject.put("AvailTime", responseData.getAvailTime().get(responseData.getPos()).getTime());
                    } else if (responseData.getServiceType().toUpperCase().equalsIgnoreCase("PICK UP")) {
                        //  servicesObject.put("AvailTime", responseData.getAvailTime().get(0).getTime());
                        AvailTime timeObj= responseData.getAvailTime().first();
                        if(timeObj != null && !timeObj.isValid()) {
                            servicesObject.put("AvailTime", timeObj.getTime());
                        }
                    }

                    mRequestJson.put(mRequestJson.length(), servicesObject);
                }
            }
            Log.d("createJsonObjects", "JSONArray createJsonObjects mRequestJson.toString()>>." + mRequestJson.toString());

        } catch (Exception e) {
            e.printStackTrace();
        }

        return mRequestJson;
    }

    private void getLocationObj() {
        final Call<ServiceLocationConfigObject> locationConfigObject = ApiClient.getApiClient(ServiceRequestApiInterface.class).get_serviceLocations();
        locationConfigObject.enqueue(new Callback<ServiceLocationConfigObject>() {
            @Override
            public void onResponse(@NonNull Call<ServiceLocationConfigObject> call,@NonNull Response<ServiceLocationConfigObject> response) {
                if (response.isSuccessful() && response.body() != null) {

                    ServiceLocationConfigObject responseBody = response.body();
                    //  Log.d("", "Location response Code........ " + response.body().getResponseData());
                    RealmList<ServiceLocationResponseData> responseData = null;
                    if (responseBody != null) {
                        responseData = responseBody.getResponseData();
                    }
                    if(responseData != null && !(responseBody.getResponseData()).isEmpty()){
                        for (int i = 0; i <responseBody.getResponseData().size(); i++) {

                            locationObj = responseBody.getResponseData();
                        }
                        displayLocationData(locationObj);

                    }
                }
            }

            @Override
            public void onFailure(@NonNull Call<ServiceLocationConfigObject> call, @NonNull Throwable t) {
                t.printStackTrace();
            }
        });
    }

    public void fetchJSONBody() {
        JsonObject gsonObject = new JsonObject();
        JSONObject guestObj ;
        JSONObject memberIdObj = new JSONObject();
        JSONObject roomInfoObj;

        //if(RealmUtil.isDeviceActivated()){
        if(Consts.APP_CURRENT_STATE >1){
            try {
                Log.d("fetchJSONBody...","devices activated............");
                roomInfoObj   = new JSONObject();
                roomInfoObj.put("RoomNo", RealmUtil.getRoomNo());
                roomInfoObj.put("BookingId",RealmUtil.getBookingId());

                memberIdObj.put("MembershipId", RealmUtil.getMembershipId());
                roomInfoObj.put("GuestProfile", memberIdObj);
                JsonParser jsonParser = new JsonParser();
                gsonObject = (JsonObject) jsonParser.parse(roomInfoObj.toString());
            }catch (JSONException ex){
                ex.printStackTrace();
            }

        }else {
            try {
                Log.d("fetchJSONBody...","devices NOT activated............");
                memberIdObj.put("MembershipId", RealmUtil.getMembershipId());
                guestObj = new JSONObject();
                guestObj.put("GuestProfile", memberIdObj);
                JsonParser jsonParser = new JsonParser();
                gsonObject = (JsonObject) jsonParser.parse(guestObj.toString());
            }catch (JSONException ex){
                ex.printStackTrace();
            }
        }
        getServiceData(gsonObject);
        Log.d("fetchJSONBody...","service REquestData............"+gsonObject);
    }

    public void getServiceData(JsonObject gsonObject) {

       /* if(mDialog.isShowing())
            mDialog.dismiss();*/

        final Call<ServiceRequestConfigObject> servReqestObject = ApiClient.getApiClient(ServiceRequestApiInterface.class).get_serviceRequest(gsonObject);
        servReqestObject.enqueue(new Callback<ServiceRequestConfigObject>() {
            @Override
            public void onResponse(@NonNull Call<ServiceRequestConfigObject> call, @NonNull Response<ServiceRequestConfigObject> response) {
                Log.d(TAG, "response Code " + response.code());
                if (response.isSuccessful() && response.body() != null) {
                    Log.d(TAG, "response body " + response.body());
                    //create Realm Object for service Request.......
                    ServiceRequestConfigObject servicerealmObj = response.body();
                    mRealm = RealmUtil.getInstance();
                    mRealm.beginTransaction();
                    if (mRealm.where(ServiceRequestConfigObject.class).equalTo("RealmId", "1").count() == 0) {

                        assert servicerealmObj != null;
                        mRealm.copyToRealm(servicerealmObj);
                    }
                    mServiceList= Objects.requireNonNull(mRealm.where(ServiceRequestConfigObject.class).findFirst()).getResponseData();
                    mRealm.commitTransaction();
                    updateServiceList();
                    pBar.setVisibility(GONE);
                } else {
                    Toast.makeText(getContext(), "No Data To display..", Toast.LENGTH_SHORT).show();
                }
            }
            @Override
            public void onFailure(@NonNull Call<ServiceRequestConfigObject> call, @NonNull Throwable t) {
                t.printStackTrace();
            }
        });
    }

    @Override
    public void onBundleUpdate(Bundle bundle) {
        if (bundle == null) {
            return;
        }
        String dataType = bundle.getString("data_type");
        if (dataType != null && dataType.equalsIgnoreCase(NavigationConsts.MAKE_MY_ROOM)) {
            timeRequest = bundle.getString("time_request");
            if (mSelectedRoomTxt != null) {
                mSelectedRoomTxt.setText(timeRequest);
            }
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        mRealm.executeTransaction(new Realm.Transaction() {
            @Override
            public void execute(@NonNull Realm realm) {
                realm.delete(ServiceRequestConfigObject.class);
            }
        });
    }

    private boolean isEmptyString(String text) {
        return (text == null || text.trim().equals("null") || text.trim()
                .length() <= 0);
    }
}
