package com.arowana.houdini.byod.utils;

import android.content.Context;
import android.graphics.drawable.Drawable;
//import android.util.Log;

import com.arowana.houdini.byod.MainApplication;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;

/**
 * Created by vivek.m on 27-07-2016.
 */

public class JSONUtil {

    public static String ReadFromfile(Context context, String fileName) {
        StringBuilder returnString = new StringBuilder();
        InputStream fIn = null;
        InputStreamReader isr = null;
        BufferedReader input = null;
        try {
            fIn = context.getResources().getAssets().open(fileName, Context.MODE_WORLD_READABLE);
            isr = new InputStreamReader(fIn);
            input = new BufferedReader(isr);
            String line = "";
            while ((line = input.readLine()) != null) {
                returnString.append(line);
            }
        } catch (Exception e) {
            e.getMessage();
        } finally {
            try {
                if (isr != null)
                    isr.close();
                if (fIn != null)
                    fIn.close();
                if (input != null)
                    input.close();
            } catch (Exception e2) {
                e2.getMessage();
            }
        }
        return returnString.toString();
    }

    public static JSONObject getJSONObject(String response) {
        JSONObject lRoomObj = null;
        try {
            lRoomObj = new JSONObject(response);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return lRoomObj;
    }

    public static JSONArray getJSONArray(String response) {
        JSONArray lRoomObj = null;
        try {
            lRoomObj = new JSONArray(response);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return lRoomObj;
    }

    public static String getMonth(String number) {
        String strMonth = "january";
        Integer month = Integer.parseInt(number);
        switch (month) {
            case 1:
                strMonth = "january";
                break;
            case 2:
                strMonth = "february";
                break;
            case 3:
                strMonth = "march";
                break;
            case 4:
                strMonth = "april";
                break;
            case 5:
                strMonth = "may";
                break;
            case 6:
                strMonth = "june";
                break;
            case 7:
                strMonth = "july";
                break;
            case 8:
                strMonth = "august";
                break;
            case 9:
                strMonth = "september";
                break;
            case 10:
                strMonth = "october";
                break;
            case 11:
                strMonth = "november";
                break;
            case 12:
                strMonth = "december";
                break;
        }
        return strMonth;
    }

    public static Drawable GetDrawableByName(String name) {
       /* Context context = MainApplication.getInstance().getApplicationContext();
        Resources res = context.getResources();
        try {
            System.out.println(name);
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP)
                return res.getDrawable(res.getIdentifier(name, "drawable", context.getPackageName()), null);
            else
                return res.getDrawable(res.getIdentifier(name, "drawable", context.getPackageName()));
        } catch (Exception e) {
            e.printStackTrace();
            return res.getDrawable(res.getIdentifier( "image_place_holder","drawable", context.getPackageName()));
        }*/

        Context context = MainApplication.getInstance().getApplicationContext();
        int resourceId = context.getResources().getIdentifier(name, "drawable",  MainApplication.getInstance().getApplicationContext().getPackageName());
       // Log.d("JSONUTIL",""+MainApplication.getInstance().getApplicationContext().getPackageName());
        return context.getResources().getDrawable(resourceId);
    }

    public static JsonObject getGsonfromJSON(JSONObject jsonObject){
        JsonParser jsonParser = new JsonParser();
        JsonObject gsonObject = (JsonObject) jsonParser.parse(jsonObject.toString());

        return gsonObject;
    }


}
