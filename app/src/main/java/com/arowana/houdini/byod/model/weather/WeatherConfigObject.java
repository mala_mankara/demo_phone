package com.arowana.houdini.byod.model.weather;

public class WeatherConfigObject {
    private ResponseStatus ResponseStatus;

    private  WeatherResponseDataObject ResponseData;


    public WeatherConfigObject.ResponseStatus getResponseStatus()
    {
        return ResponseStatus;
    }

    public void setResponseStatus(WeatherConfigObject.ResponseStatus responseStatus) {
        ResponseStatus = responseStatus;
    }

    public WeatherResponseDataObject getResponseData() {
        return ResponseData;
    }

    public void setResponseData(WeatherResponseDataObject responseData) {
        ResponseData = responseData;
    }




    public static class ResponseStatus {

        private String ResponseCode;

        private String ResponseFlag;

        private String ResponseMessage;

        private String ResponseId;


        public String getResponseCode() {
            return ResponseCode;
        }

        public void setResponseCode(String responseCode) {
            ResponseCode = responseCode;
        }

        public String getResponseFlag() {
            return ResponseFlag;
        }

        public void setResponseFlag(String responseFlag) {
            ResponseFlag = responseFlag;
        }



        public String getResponseMessage() {
            return ResponseMessage;
        }

        public void setResponseMessage(String responseMessage) { ResponseMessage = responseMessage; }

        public String getResponseId() {
            return ResponseId;
        }

        public void setResponseId(String responseId) {
            ResponseId = responseId;
        }
    }
}
