package com.arowana.houdini.byod.interfaces.api_interface;

import com.arowana.houdini.byod.model.deviceactivation_QR.QR_DeviceRegisterConfigObj;

import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.POST;

public interface CheckDeviceStatusApiInterface {

    @FormUrlEncoded
    @POST("Userregistration/GuestStatus")
    Call<QR_DeviceRegisterConfigObj> getAll(@Field("MembershipId") String membershipId, @Field("MacId") String deviceMacId);

}
