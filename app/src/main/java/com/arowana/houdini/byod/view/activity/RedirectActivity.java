package com.arowana.houdini.byod.view.activity;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import androidx.fragment.app.Fragment;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;
import android.util.Log;
import android.widget.FrameLayout;

import com.arowana.houdini.byod.Consts;
import com.arowana.houdini.byod.MainApplication;
import com.arowana.houdini.byod.R;
import com.arowana.houdini.byod.utils.ViewUtils;
import com.arowana.houdini.byod.view.fragments.WebViewFragment;
import com.arowana.houdini.byod.view.fragments.redirect.RedirectViewFragment;


public class RedirectActivity extends BaseActivity {

    public static final String TAG = "RedirectActivity";
    public static FrameLayout fragmentHolder;
    private String intentData;
    String key;
    static boolean installed = MainApplication.appInstalledOrNot("com.whatsapp");
    public static boolean isBrowser;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_module_layout);
        MainApplication.setCurrentActivity(this);
        LocalBroadcastManager.getInstance(this).registerReceiver(mReceiver,
                new IntentFilter("start.fragment.action"));
        Bundle extras = getIntent().getExtras();
        if (extras != null) {
            intentData = extras.getString("title");
            key = extras.getString("key");
           /* if (key.equalsIgnoreCase("pressreader") ) {
                isBrowser = extras.getBoolean("openBrowser");
                Log.d("Redirect Activity","is Browser"+isBrowser);
            }*/

        }


        if (key.equalsIgnoreCase(Consts.PARSE_WHATSAPP_DATA) && !installed) {
            System.out.println("REDirect App is not installed on your phone");
            ViewUtils.showSnackBarMessage("App is not installed on your phone");
          //  this.finish();
        } else {
            initComponents();

        }
    }

    public void initComponents() {
        fragmentHolder = findViewById(R.id.fragment_holder);
        loadFragments();
    }

    public void loadFragments() {
        Fragment redirectFragment = new RedirectViewFragment();
        Bundle bundle = new Bundle();
        bundle.putString("title", intentData);
        bundle.putString("key", key);
        redirectFragment.setArguments(bundle);
        showFragment(fragmentHolder.getId(), redirectFragment);
    }

    private BroadcastReceiver mReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            //do something based on the intent's action
            //This piece of code will be executed when you click on your item
            // Call your fragment...
            String title = intent.getStringExtra("title");
            String link = intent.getStringExtra("url");
            Fragment webViewFragment = new WebViewFragment();
            Bundle bundle = new Bundle();
            Log.d(TAG, "RECEIVER...." + link);
            bundle.putString("title", title);
            bundle.putString("url", link);
            webViewFragment.setArguments(bundle);
            showFragment(fragmentHolder.getId(), webViewFragment);

        }
    };



}
