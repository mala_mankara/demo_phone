package com.arowana.houdini.byod.view.adapters;

import android.content.Context;
import androidx.recyclerview.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.arowana.houdini.byod.R;
import com.arowana.houdini.byod.model.weather.Forecast;
import com.arowana.houdini.byod.utils.FactoryUtil;

import io.realm.RealmList;

import static com.arowana.houdini.byod.utils.FactoryUtil.DayConverter;

public class WeatherAdapter  extends RecyclerView.Adapter<WeatherAdapter.MyViewHolder> {

    public static final String TAG="WeatherAdapter";
    Context context;
    private RealmList<Forecast> forecastData;

    public WeatherAdapter(Context context, RealmList<Forecast> forecastDataObj) {
        this.context=context;
        forecastData=forecastDataObj;
        Log.d(TAG,"SIZE in Adapter.."+forecastData.size());
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.layout_items, parent, false);
        return new MyViewHolder(itemView);
    }


    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {
        
        holder.title.setText(DayConverter(forecastData.get(position).getDate()));
        FactoryUtil.loadImage(context,forecastData.get(position).getIconImage(),holder.thumbnail);
        holder.downText.setText(forecastData.get(position).getMinimum()+" / "+forecastData.get(position).getMaximum());

    }

    @Override
    public int getItemCount() {
        return forecastData.size();
    }



    class MyViewHolder extends RecyclerView.ViewHolder {

        private TextView title;
        private ImageView thumbnail;
        private TextView downText;

        MyViewHolder(View itemView) {
            super(itemView);
            title =itemView.findViewById(R.id.topdata);
            thumbnail =itemView.findViewById(R.id.dayimage);
            downText=itemView.findViewById(R.id.bottomdata);

        }

        ImageView getImage(){
            return this.thumbnail;}
    }
}