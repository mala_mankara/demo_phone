package com.arowana.houdini.byod.model.servicerequest;

import io.realm.RealmList;
import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

public class ServiceRequestConfigObject extends RealmObject{

    @PrimaryKey
    private String RealmId = "1";

    private ServiceResponseStatusObject ResponseStatus;

    private RealmList<ServiceRequestResponseData> ResponseData;

    private String RequestData;

    public ServiceResponseStatusObject getResponseStatus ()
    {
        return ResponseStatus;
    }

    public void setResponseStatus (ServiceResponseStatusObject ResponseStatus) { this.ResponseStatus = ResponseStatus;  }

    public RealmList<ServiceRequestResponseData> getResponseData ()
    {
        return ResponseData;
    }

    public void setResponseData (RealmList<ServiceRequestResponseData> ResponseData) {
        this.ResponseData = ResponseData;
    }

    public String getRequestData() {
        return RequestData;
    }

    public void setRequestData(String requestData) {
        RequestData = requestData;
    }


}
