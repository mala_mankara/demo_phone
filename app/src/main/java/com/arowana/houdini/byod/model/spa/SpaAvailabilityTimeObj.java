package com.arowana.houdini.byod.model.spa;

import io.realm.RealmObject;

public class SpaAvailabilityTimeObj extends RealmObject {

    private String Timing;

    private String OpeningTime;

    private String ClosingTime;

    public String getTiming ()
    {
        return Timing;
    }

    public void setTiming (String Timing)
    {
        this.Timing = Timing;
    }

    public String getOpeningTime ()
    {
        return OpeningTime;
    }

    public void setOpeningTime (String OpeningTime)
    {
        this.OpeningTime = OpeningTime;
    }

    public String getClosingTime ()
    {
        return ClosingTime;
    }

    public void setClosingTime (String ClosingTime)
    {
        this.ClosingTime = ClosingTime;
    }

}
