package com.arowana.houdini.byod.view.adapters.facilityServices;

import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.arowana.houdini.byod.MainApplication;
import com.arowana.houdini.byod.R;
import com.arowana.houdini.byod.interfaces.listeners.FragmentNavigationHandler;
import com.arowana.houdini.byod.model.facilitiesAndServices.FServiceAmenities;
import com.arowana.houdini.byod.model.facilitiesAndServices.FServicesInformation;
import com.arowana.houdini.byod.utils.ExpandableTextView;
import com.arowana.houdini.byod.utils.FactoryUtil;
import com.arowana.houdini.byod.utils.ViewUtils;
import com.arowana.houdini.byod.view.activity.BaseActivity;
import com.arowana.houdini.byod.view.fragments.WebViewFragment;

import java.util.Objects;

import io.realm.RealmList;

import static com.arowana.houdini.byod.view.activity.FacilityServiceActivity.fragmentHolder;

public class FacilityDetailsAdapter extends RecyclerView.Adapter<FacilityDetailsAdapter.ViewHolder> {

    private  RealmList<FServiceAmenities> dataList;
    private Context mContext;
    private FServiceAmenities data;
    FragmentNavigationHandler handler;


    public FacilityDetailsAdapter(Context items, RealmList<FServiceAmenities> list, FragmentNavigationHandler mHandler) {
        mContext = items;
        dataList = list;
        handler = mHandler;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_details, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull final ViewHolder holder, final int position) {

        data     = dataList.get(position);
        holder.title.setText(Objects.requireNonNull(data).getName());
        holder.rowlayout.setTag(position);
        holder.next.setTag(position);

        if (data.getImages() != null && data.getImages().first().getImageUrl() != null) {
            FactoryUtil.loadImage(mContext, data.getImages().first().getImageUrl(), holder.imageHolder);
        }
        if (data.getDescription() != null) {
            holder.description.setText(data.getDescription());
            holder.readOption.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    holder.description.toggle();
                    holder.readOption.setText(holder.description.isExpanded() ? "Read more" : "Read less");
                }
            });

            if (data.getInformation() != null) {
                holder.listView.setAdapter(new SequeceListAdapter(mContext, data.getInformation(),handler));
            }
            holder.description.addOnExpandListener(holder);
        }
    }

    @Override
    public int getItemCount() {
        return dataList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder implements  View.OnClickListener ,ExpandableTextView.OnExpandListener {

        public TextView title;
        public RelativeLayout rowlayout;
        public ImageView next;
        public LinearLayout expandlayout;
        public ImageView imageHolder;
        public ExpandableTextView description;
        public TextView readOption;
        RecyclerView listView;
        private int totalLines = -1;


        ViewHolder(View itemView) {
            super(itemView);
            rowlayout         = itemView.findViewById(R.id.amenities_row);
            title             = itemView.findViewById(R.id.facility_title);
            next              = itemView.findViewById(R.id.fs_arrow);

            expandlayout      = itemView.findViewById(R.id.facility_exlayout);
            imageHolder       = itemView.findViewById(R.id.facility_imageHolder);
            description       = itemView.findViewById(R.id.facility_expand_txt);
            readOption        = itemView.findViewById(R.id.facility_read_more);
            listView          = itemView.findViewById(R.id.facility_list);

            listView.setLayoutManager(new LinearLayoutManager(mContext));

            rowlayout.setOnClickListener(this);

        }



        @Override
        public String toString() {
            return super.toString() + " '" + title.getText() + "'";
        }

        @Override
        public void onClick(View v) {
            onItemClick(getAdapterPosition());
        }

        private void onItemClick(int adapterPosition) {

            if (expandlayout.getVisibility() == View.VISIBLE) {
                next.setVisibility(View.VISIBLE);
                expandlayout.setVisibility(View.GONE);
            } else {
                next.setVisibility(View.INVISIBLE);
                expandlayout.setVisibility(View.VISIBLE);
            }
        }

        @Override
        public void onExpand(@NonNull ExpandableTextView view) {
        }

        @Override
        public void onCollapse(@NonNull ExpandableTextView view) {
        }

        @Override
        public void onLayedout(boolean changed, int left, int top, int right, int bottom, @NonNull ExpandableTextView view) {
            description.post(new Runnable() {
                @Override
                public void run() {
                    // Perform any actions you want based on the line count here.
                    if (totalLines == -1) {
                        totalLines = description.getLineCount();
                        Log.d("FD Adapter","the line count..."+totalLines);
                        readOption.setVisibility(totalLines < 4 ? View.GONE : View.VISIBLE);
                        description.setMaxLines(3);
                        description.requestLayout();
                       /* description.post(new Runnable() {
                            @Override
                            public void run() {
                                description.requestLayout(); }
                        });*/
                    }
                }
            });
        }
    }

    class SequeceListAdapter extends RecyclerView.Adapter<SequeceListAdapter.CustomViewholder> {
        RealmList<FServicesInformation> infoList;
        FServicesInformation infoData;
        Context cntxt;
        FragmentNavigationHandler mHandler;

        SequeceListAdapter(Context mContext, RealmList<FServicesInformation> information, FragmentNavigationHandler handler) {
            super();
            cntxt = mContext;
            infoList = information;
            mHandler = handler;
        }


        @NonNull
        @Override
        public CustomViewholder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
            View view = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.facility_infoitems, parent, false);
            return new CustomViewholder(view);

        }

        @Override
        public void onBindViewHolder(@NonNull CustomViewholder holder, int position) {
            infoData = infoList.get(position);
            holder.info_field.setText(Objects.requireNonNull(infoData).getField());
            holder.info_value.setText(infoData.getValue());
            holder.info_value.setTag(position);
            if(infoData.getField().equalsIgnoreCase("Phone") ||infoData.getField().equalsIgnoreCase("Email")
                    ||infoData.getField().equalsIgnoreCase("Reservations")
                    ||infoData.getField().equalsIgnoreCase("Reservation")
                    ||infoData.getField().equalsIgnoreCase("Whatsapp")|| infoData.getField().equalsIgnoreCase("Telephone")
                    || infoData.getField().equalsIgnoreCase("Phone number")
                    ||(!infoData.getValue().isEmpty() && FactoryUtil.isValidURL(infoData.getValue()))) {
                holder.info_value.setLinkTextColor(cntxt.getResources().getColor(R.color.autolink_text_color));
                holder.info_value.setTextColor(cntxt.getResources().getColor(R.color.autolink_text_color));
                //holder.info_value.setAutoLinkMask(position);
            }
        }

        @Override
        public int getItemCount() {
            return infoList.size();
        }

        class CustomViewholder extends RecyclerView.ViewHolder implements  View.OnClickListener{
            TextView info_field;
            TextView info_value;

            CustomViewholder(View itemView) {
                super(itemView);
                info_field    = itemView.findViewById(R.id.field);
                info_value    = itemView.findViewById(R.id.value);
                info_value.setOnClickListener(this);

            }

            @Override
            public void onClick(View v) {

                infoData = infoList.get((Integer) v.getTag());
                Log.d("FacilityDetailsAdapter","customViewHolder..."+infoData.getField()+"/,value.."+infoData.getValue());

                String value     = infoData.getValue();
                String fieldName = infoData.getField();

                Intent actionIntent ;
                if(fieldName.equalsIgnoreCase("Phone")|| fieldName.equalsIgnoreCase("Telephone") || fieldName.equalsIgnoreCase("Phone number")){
                    info_value.setTextColor(cntxt.getResources().getColor(R.color.autolink_text_color));
                    actionIntent = new Intent(Intent.ACTION_DIAL);
                    String p = "tel:" + (value);
                    actionIntent.setData(Uri.parse(p));
                    cntxt.startActivity(actionIntent);

                }if(fieldName.equalsIgnoreCase("Email")){
                    info_value.setTextColor(cntxt.getResources().getColor(R.color.autolink_text_color));
                    actionIntent = new Intent(Intent.ACTION_SENDTO);
                    actionIntent.setType("text/plain");
                    actionIntent.setData(Uri.fromParts("mailto",value, null));
                    cntxt.startActivity(Intent.createChooser(actionIntent, "Choose an Email client :"));

                }if(!value.isEmpty() && FactoryUtil.isValidURL(value)){
                   // FactoryUtil.openUrlinBrowser(value,cntxt);
                    WebViewFragment webview = new WebViewFragment();
                    Bundle bundle = new Bundle();
                    bundle.putString("title", fieldName);
                    bundle.putString("url", value);
                    webview.setArguments(bundle);
                    mHandler.showFragment(fragmentHolder.getId(), webview);


                }if(fieldName.equalsIgnoreCase("Whatsapp")){
                    info_value.setTextColor(cntxt.getResources().getColor(R.color.autolink_text_color));
                    PackageManager pm = cntxt.getPackageManager();

                    if (!MainApplication.appInstalledOrNot("com.whatsapp")) {
                        ViewUtils.showSnackBarMessage("Feature unavailable. WhatsApp needs to be installed",(BaseActivity)cntxt);
                    } else {
                        String whatsAppNo = value;
                        if (!(whatsAppNo == null && whatsAppNo.isEmpty())) {
                            FactoryUtil.openWhatsApp(cntxt, whatsAppNo);
                        } else
                            ViewUtils.showSnackBarMessage("WhatsApp no. is not valid.");
                    }
                }
            }
        }
    }
}
