package com.arowana.houdini.byod.view.fragments;

import android.app.ProgressDialog;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.util.Base64;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.FragmentActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.arowana.houdini.byod.R;
import com.arowana.houdini.byod.interfaces.ApiClient;
import com.arowana.houdini.byod.interfaces.api_interface.DeviceActivationApiInterface;
import com.arowana.houdini.byod.model.deviceActivationSecondary.QRDetails;
import com.arowana.houdini.byod.model.deviceActivationSecondary.QRGenerationResponse;
import com.arowana.houdini.byod.model.deviceActivationSecondary.SecondaryUser;
import com.arowana.houdini.byod.model.deviceActivationSecondary.SecondaryUserListData;
import com.arowana.houdini.byod.model.deviceActivationSecondary.SecondaryUserListResponse;
import com.arowana.houdini.byod.utils.BasicApiResponse;
import com.arowana.houdini.byod.utils.DialogUtil;
import com.arowana.houdini.byod.utils.RealmUtil;
import com.arowana.houdini.byod.utils.ViewUtils;
import com.arowana.houdini.byod.view.adapters.SecondaryUserListAdapter;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;

import io.realm.Realm;
import io.realm.RealmResults;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.arowana.houdini.byod.view.activity.InRoomDiningActivity.fragmentHolder;

public class SecondaryActivationFragment extends BaseFragment implements SecondaryUserListAdapter.Listner {

    Context mContext;
    private View mRootView;
    private TextView mRoomNo;
    private ImageView mQRImageView;
    private RecyclerView mRecyclerView;
    private ProgressDialog mDialog;

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        mRootView = view;
        mDialog = DialogUtil.showProgressDialog(getActivity(), "");
        mRecyclerView = mRootView.findViewById(R.id.secondary_users_view);
        mRoomNo = mRootView.findViewById(R.id.room_no_text);
        mQRImageView = mRootView.findViewById(R.id.secondary_qr_image);
        TextView mName = mRootView.findViewById(R.id.name_text);
        TextView mMembershipId = mRootView.findViewById(R.id.membership_id_text);
        mMembershipId.setText(RealmUtil.getMembershipId());
        mName.setText(RealmUtil.getFullName());

        mRoomNo.setText(RealmUtil.getRoomNo());

        QRGenerationResponse response = RealmUtil.getInstance().where(QRGenerationResponse.class).findFirst();

        fetchQRCode();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mContext = getActivity();
        setTitle(getResources().getString(R.string.title_secondary_activation));
    }

    @Override
    protected int getFragmentLayout() {
        return R.layout.fragment_secondary_activation;
    }

    public void fetchQRCode() {
      //  mDialog.show();

        JsonObject jsonObject = new JsonObject();
        jsonObject.addProperty("RoomNumber", mRoomNo.getText().toString());
        jsonObject.addProperty("MembershipId", RealmUtil.getMembershipId());

        Call<QRGenerationResponse> responseObj = ApiClient.getApiClient(DeviceActivationApiInterface.class).generateQR(jsonObject);

        responseObj.enqueue(new Callback<QRGenerationResponse>() {
            @Override
            public void onResponse(@NonNull Call<QRGenerationResponse> call, @NonNull Response<QRGenerationResponse> response) {

                if (mDialog.isShowing())
                    mDialog.dismiss();

                QRGenerationResponse qrResponse = response.body();

                if (qrResponse != null && RealmUtil.responseIsSuccess(qrResponse)) {

                    Realm realm = RealmUtil.getInstance();
                    realm.beginTransaction();
                    realm.copyToRealmOrUpdate(qrResponse);
                    realm.commitTransaction();

                    updateQRImage(qrResponse.getResponseData().getQRDetails());
                } else {
                    final String message = RealmUtil.getResponseMessage(qrResponse);
                    ViewUtils.showSnackBarMessage(message != null ? message : "A server error occurred");
                }
                fetchUserList();
            }

            @Override
            public void onFailure(@Nullable Call<QRGenerationResponse> call, @Nullable Throwable t) {
                ViewUtils.showSnackBarMessage(t != null ? t.getMessage() : "Error in fetching data");
            }
        });

    }

    private void updateQRImage(QRDetails qrDetails) {
        String dataString = qrDetails.getEncryptedData().replace("data:image/png;base64,", ""); // remove "data:image/png;base64,"
        byte[] decodedString = Base64.decode(dataString, Base64.DEFAULT);
        Bitmap decodedByte = BitmapFactory.decodeByteArray(decodedString, 0, decodedString.length);
        mQRImageView.setImageBitmap(decodedByte);
    }

    private JsonObject createRequest() {
        JsonObject jsonObject = new JsonObject();
        jsonObject.addProperty("RoomNo", mRoomNo.getText().toString());
        jsonObject.addProperty("MembershipId", RealmUtil.getMembershipId());

        return jsonObject;
    }

    public void fetchUserList() {

        Call<SecondaryUserListResponse> responseObj = ApiClient.getApiClient(DeviceActivationApiInterface.class).getActiveUsers(createRequest());

        responseObj.enqueue(new Callback<SecondaryUserListResponse>() {
            @Override
            public void onResponse(@NonNull Call<SecondaryUserListResponse> call, @NonNull Response<SecondaryUserListResponse> response) {

                SecondaryUserListResponse responseBody = response.body();

                if (responseBody != null && RealmUtil.responseIsSuccess(responseBody)) {

                    Realm realm = RealmUtil.getInstance();
                    realm.beginTransaction();
                    realm.copyToRealmOrUpdate(responseBody);
                    realm.commitTransaction();
                    SecondaryUserListData userListData = realm.where(SecondaryUserListData.class).findFirst();

                    populateData(userListData);

                } else {
                    final String message = RealmUtil.getResponseMessage(responseBody);
                    ViewUtils.showSnackBarMessage(message != null ? message : "A server error occurred");
                }
            }

            @Override
            public void onFailure(@Nullable Call<SecondaryUserListResponse> call, @Nullable Throwable t) {
                ViewUtils.showSnackBarMessage(t != null ? t.getMessage() : "Error in fetching data");
            }
        });

    }

    public void populateData(SecondaryUserListData obj) {

        LinearLayoutManager linearVertical = new LinearLayoutManager(getContext(), RecyclerView.VERTICAL, false);
        mRecyclerView.setLayoutManager(linearVertical);
        SecondaryUserListAdapter adapter = new SecondaryUserListAdapter(this, obj, mHandler, fragmentHolder);
        mRecyclerView.setAdapter(adapter);
    }

    private void deactivateUser(final SecondaryUser user) {
        JsonObject request = createRequest();

        JsonObject additionalDevices = new JsonObject();
        additionalDevices.addProperty("Id", user.getId());
        additionalDevices.addProperty("MacId", user.getMacId());
        JsonArray jsonArray = new JsonArray();
        jsonArray.add(additionalDevices);

        request.add("UserAdditionalDevices", jsonArray);

        Call<BasicApiResponse> responseObj = ApiClient.getApiClient(DeviceActivationApiInterface.class).deactivateDevice(request);

        responseObj.enqueue(new Callback<BasicApiResponse>() {
            @Override
            public void onResponse(@NonNull Call<BasicApiResponse> call, @NonNull Response<BasicApiResponse> response) {

                BasicApiResponse responseBody = response.body();

                if (responseBody != null && RealmUtil.responseIsSuccess(responseBody)) {

                    Realm realm = RealmUtil.getInstance();
                    realm.beginTransaction();
                    RealmResults<SecondaryUser> secondaryUsers = realm.where(SecondaryUser.class).equalTo("Id", user.getId()).findAll();
                    secondaryUsers.deleteAllFromRealm();
                    realm.commitTransaction();

                    mRecyclerView.getAdapter().notifyDataSetChanged();

                } else {
                    final String message = RealmUtil.getResponseMessage(responseBody);
                    ViewUtils.showSnackBarMessage(message != null ? message : "A server error occurred");
                }
            }

            @Override
            public void onFailure(@Nullable Call<BasicApiResponse> call, @Nullable Throwable t) {
                ViewUtils.showSnackBarMessage(t != null ? t.getMessage() : "Error in fetching data");
            }
        });
    }


    @Override
    public void deleteUser(final SecondaryUser user) {

        FragmentActivity activity = getActivity();
        if (activity == null)
            return;

        final FrameLayout layout = activity.findViewById(R.id.snacbar_layout);
        layout.setVisibility(View.VISIBLE);
        View view = getActivity().getLayoutInflater().inflate(R.layout.custom_confirm_snackbar_layout, null);
        TextView message = view.findViewById(R.id.dineMessage);
        message.setText(R.string.confirm_user_delete_message);
        TextView mOkMessage = view.findViewById(R.id.snack_ok_message);
        TextView mCancelMessage = view.findViewById(R.id.snack_cancel_message);

        mOkMessage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                deactivateUser(user);

                layout.setVisibility(View.GONE);
                layout.removeAllViews();

            }
        });

        mCancelMessage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                layout.setVisibility(View.GONE);
                layout.removeAllViews();

            }
        });

        layout.addView(view);
    }
}
