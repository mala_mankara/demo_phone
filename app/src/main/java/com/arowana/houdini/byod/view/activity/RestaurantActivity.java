package com.arowana.houdini.byod.view.activity;

import android.os.Bundle;
import androidx.annotation.NonNull;
import android.widget.FrameLayout;

import com.arowana.houdini.byod.MainApplication;
import com.arowana.houdini.byod.R;
import com.arowana.houdini.byod.interfaces.ApiClient;
import com.arowana.houdini.byod.interfaces.api_interface.OffersApiInterface;
import com.arowana.houdini.byod.model.offers.OffersConfigObject;
import com.arowana.houdini.byod.model.offers.OffersResponseStatus;
import com.arowana.houdini.byod.view.fragments.restaurant.RestaurantFragment;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class RestaurantActivity extends BaseActivity {

    public static FrameLayout fragmentHolder;
    public static int OfferSize;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_module_layout);
        MainApplication.setCurrentActivity(this);
        OfferSize = getOfferData();
        initComponents();

    }

    public void initComponents() {

        fragmentHolder = findViewById(R.id.fragment_holder);
        loadFragments();

    }

    public void loadFragments() {
        showFragment(fragmentHolder.getId(), new RestaurantFragment());

    }


    private int  getOfferData() {

        Call<OffersConfigObject> offersConfigObject = ApiClient.getApiClient(OffersApiInterface.class).getOfferDetails("2X");
        offersConfigObject.enqueue(new Callback<OffersConfigObject>() {
            @Override
            public void onResponse(@NonNull Call<OffersConfigObject> call, @NonNull Response<OffersConfigObject> response) {

                if (response.isSuccessful() && response.body() != null) {
                    OffersConfigObject configObject = response.body();
                    if (configObject != null) {
                        if (((configObject).getResponseStatus() != null) && configObject.getResponseStatus().isValid()) {
                            OffersResponseStatus responsestatusObj = configObject.getResponseStatus();
                            String flag = responsestatusObj.getResponseFlag();
                            if (flag.equals("SUCCESS")) {

                                OfferSize = response.body().getResponseData().size();
                            } else
                                OfferSize =0;
                        }
                    } else {
                        OfferSize=0;
                    }
                }
            }

            @Override
            public void onFailure(@NonNull Call<OffersConfigObject> call, @NonNull Throwable t) {
                t.printStackTrace();
            }
        });

        return OfferSize;
    }

}
