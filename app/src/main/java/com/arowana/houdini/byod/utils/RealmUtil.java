package com.arowana.houdini.byod.utils;

import android.util.Log;

import com.arowana.houdini.byod.model.deviceActivationSecondary.QRGenerationResponse;
import com.arowana.houdini.byod.model.deviceactivation_QR.QR_DeviceRegisterConfigObj;
import com.arowana.houdini.byod.model.inroomdining.IRDCartObj;
import com.arowana.houdini.byod.model.inroomdining.IRDiningAddonsObj;
import com.arowana.houdini.byod.model.inroomdining.IRDiningCustomizationObj;
import com.arowana.houdini.byod.model.inroomdining.IRDiningMenuItemsObj;
import com.arowana.houdini.byod.model.inroomdining.IRDiningResponseDataObj;
import com.arowana.houdini.byod.model.inroomdining.IRDiningUpsellingItemObj;
import com.arowana.houdini.byod.model.login.LoginConfigObject;
import com.arowana.houdini.byod.model.spa.SpaResponseDataObj;
import com.arowana.houdini.byod.model.spa.SpaTreatmentObj;
import com.arowana.houdini.byod.model.speeddial.SpeedDialConfigObj;
import com.arowana.houdini.byod.model.speeddial.SpeedDialResponseData;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.lang.reflect.Field;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.text.ParseException;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;

import io.realm.Realm;
import io.realm.RealmList;
import io.realm.RealmObject;
import io.realm.RealmResults;
import io.realm.annotations.PrimaryKey;

public class RealmUtil {

    private static RealmList<String> nameList;
    @PrimaryKey
    public String realmId;


    private static Realm realm_instance = null;

    public static Realm getInstance() {
        if (realm_instance == null)
            realm_instance = Realm.getDefaultInstance();

        return realm_instance;
    }

    private static Field getField(@Nullable Object obj, @Nonnull String fieldName) throws NoSuchFieldException {
        if (obj == null)
            return null;

        Class<?> clazz = obj.getClass();

        Field field = clazz.getDeclaredField(fieldName);
        field.setAccessible(true);
        return field;
    }

    public static <T extends RealmObject> boolean responseIsSuccess(@Nullable T response) {

        try {
            Field field = getField(response, "ResponseStatus");
            Object responseStatus = field.get(response);
            field = getField(responseStatus, "ResponseFlag");
            return field != null && "SUCCESS".equalsIgnoreCase((String) field.get(responseStatus));
        } catch (Exception e) {
            return false;
        }
    }

    public static <T extends RealmObject> String getResponseMessage(@Nullable T response) {

        try {
            Field field = getField(response, "ResponseStatus");
            Object responseStatus = field.get(response);
            field = getField(responseStatus, "ResponseMessage");
            if (field == null)
                return null;

            return (String) field.get(responseStatus);
        } catch (Exception e) {
            return null;
        }
    }

    public static <T extends RealmObject> T getFirstInstance(Class<T> type) {
        Realm realm = getInstance();
        RealmResults<T> objects = realm.where(type).findAll();
        if (!objects.isEmpty())
            return objects.get(0);
        return null;
    }

    public static void clearDataOnCheckout() {
        QRGenerationResponse response = RealmUtil.getInstance().where(QRGenerationResponse.class).findFirst();
        QR_DeviceRegisterConfigObj configObj = RealmUtil.getInstance().where(QR_DeviceRegisterConfigObj.class).findFirst();

        Realm realm = RealmUtil.getInstance();
        realm.beginTransaction();

        if (configObj != null) {
            configObj.getResponseData().setPrimaryConnectionId(null);
            configObj.getResponseData().setIsCheckedIn("false");
        }

        if (response != null)
            response.getResponseData().setQRDetails(null);

        realm.commitTransaction();
    }

    public static boolean IsUserLoggedin() {
        LoginConfigObject results = getInstance().where(LoginConfigObject.class).equalTo("RealmId", "1").findFirst();
        if (results != null)
            return results.getResponseData() != null;
        else return false;


    }

    public static boolean IsUserCheckedIn() {
        QR_DeviceRegisterConfigObj results = getInstance().where(QR_DeviceRegisterConfigObj.class).equalTo("RealmId", "1").findFirst();

        if (results != null /*&& IsUserLoggedin()*/)
            return results.getResponseData().getIsCheckedIn().toLowerCase().equals("true");
        else
            return false;
    }


    public static String getUserName() {
        LoginConfigObject results = getInstance().where(LoginConfigObject.class).equalTo("RealmId", "1").findFirst();
        if (results != null)
            return results.getResponseData().getFirstName();
        else return "";


    }

    public static String getLastName() {
        LoginConfigObject results = getInstance().where(LoginConfigObject.class).equalTo("RealmId", "1").findFirst();
        if (results != null)
            return results.getResponseData().getLastName();
        else return "";


    }

    public static String getPhoneNumber() {
        LoginConfigObject results = getInstance().where(LoginConfigObject.class).equalTo("RealmId", "1").findFirst();
        if (results != null)
            return results.getResponseData().getMobileNumber();
        else return "";


    }

    public static String getFullName() {
        LoginConfigObject results = getInstance().where(LoginConfigObject.class).equalTo("RealmId", "1").findFirst();
        if (results != null)
            return results.getResponseData().getFirstName().concat(" ").concat(results.getResponseData().getLastName());
        else return "";


    }


    public static String getMembershipId() {
        LoginConfigObject results = getInstance().where(LoginConfigObject.class).equalTo("RealmId", "1").findFirst();
        if (results != null)
            return results.getResponseData().getMembershipId();
        else return "";

    }

    public static int getSpaCartCount() {
        RealmResults<SpaTreatmentObj> results = getInstance().where(SpaTreatmentObj.class).equalTo("isSelected", true).findAll();
        if (results != null)
            return results.size();
        else return 0;
    }

    public static boolean isSubmittedSpa() {
        SpaTreatmentObj obj = getInstance().where(SpaTreatmentObj.class).findFirst();
        if (obj != null)
            return obj.isSubmit();
        return false;

    }



    public static void resetSpa() {
        final RealmResults<SpaTreatmentObj> results = getInstance().where(SpaTreatmentObj.class).equalTo("isSelected", true).findAll();
        getInstance().executeTransaction(new Realm.Transaction() {
            @Override
            public void execute(Realm realm) {
                for (SpaTreatmentObj obj : results) {

                    obj.setSelected(false);
                }
            }
        });

    }

    public static void deleteAllSpa() {
        RealmUtil.getInstance().executeTransaction(new Realm.Transaction() {
            @Override
            public void execute(Realm realm) {
                RealmResults<SpaTreatmentObj> result = realm.where(SpaTreatmentObj.class).findAll();
                result.deleteAllFromRealm();
            }
        });
    }

    public static String getSpaCurrency() {
        SpaResponseDataObj obj = getInstance().where(SpaResponseDataObj.class).findFirst();
        if (obj != null)
            return obj.getCurrency();
        else return "";

    }

    public static String getSpaTotalPrice() {
        Float price = 0.0f;
        RealmResults<SpaTreatmentObj> results = getInstance().where(SpaTreatmentObj.class).equalTo("isSelected", true).findAll();
        DecimalFormat format = new DecimalFormat();
        format.setDecimalSeparatorAlwaysShown(false);

        if (results != null) {
            RealmList<SpaTreatmentObj> list = new RealmList<>();
            list.addAll(results);
            for (SpaTreatmentObj obj : results) {
                Log.d("price", "getCoupleCount is-------------" + obj.getCoupleCount());
                Log.d("price", "getFemalePaxCount is-------------" + obj.getFemalePaxCount());
                Log.d("price", "getMalePaxCount is-------------" + obj.getMalePaxCount());
                Log.d("price", "price before is-------------" + String.valueOf(price));


                try {
                    int pkgPrice=NumberFormat.getInstance().parse(obj.getBookingTimeAndPrice().getPrice()).intValue();
                    price += (obj.getCoupleCount() + obj.getFemalePaxCount() + obj.getMalePaxCount()) * pkgPrice;
                } catch (ParseException e) {
                    e.printStackTrace();
                }

                Log.d("price", "price later is-------------" + String.valueOf(price));
            }
            return String.valueOf(format.format(price));

        }
        return "0";
    }


    public static String getMobileNo() {
        LoginConfigObject results = getInstance().where(LoginConfigObject.class).findFirst();
        if (results != null)
            return results.getResponseData().getMobileNumber();
        else return "";
    }

    public static String getEmailId() {

        LoginConfigObject results = getInstance().where(LoginConfigObject.class).equalTo("RealmId", "1").findFirst();
        if (results != null)
            return results.getResponseData().getEmailId();
        else return "";
    }

    public static boolean isDeviceActivated() {
        QR_DeviceRegisterConfigObj results = getInstance().where(QR_DeviceRegisterConfigObj.class).findFirst();
        if (results != null && results.getResponseData().getIsCheckedIn().equalsIgnoreCase("true"))
            return true;
        else
            return false;
    }

    public static boolean isPrimaryCheckin() {
        QR_DeviceRegisterConfigObj results = getInstance().where(QR_DeviceRegisterConfigObj.class).findFirst();
        return results != null
                && results.getResponseData().getIsCheckedIn().equalsIgnoreCase("true")
                && (results.getResponseData().getPrimaryConnectionId() == null || results.getResponseData().getPrimaryConnectionId().isEmpty());
    }

    public static String getPrimaryConnectionId() {
        QR_DeviceRegisterConfigObj results = getInstance().where(QR_DeviceRegisterConfigObj.class).findFirst();
        if (results != null)
            return results.getResponseData().getPrimaryConnectionId();
        else
            return null;
    }

    public static String getRoomNo() {
        QR_DeviceRegisterConfigObj results = getInstance().where(QR_DeviceRegisterConfigObj.class).findFirst();
        if (results != null)
            return results.getResponseData().getRoomNo();
        else
            return null;
    }

    public static String getBookingId() {
        QR_DeviceRegisterConfigObj results = getInstance().where(QR_DeviceRegisterConfigObj.class).equalTo("RealmId", "1").findFirst();
        if (results != null)
            return results.getResponseData().getReservationNo();
        else
            return null;
    }

    public static void deleteRealm() {


        Realm realm = Realm.getDefaultInstance();
        realm.beginTransaction();

// delete all realm objects
        realm.delete(LoginConfigObject.class);

//commit realm changes
        realm.commitTransaction();

    }

    public static String getDiningCurrency() {
        IRDiningResponseDataObj obj = getInstance().where(IRDiningResponseDataObj.class).findFirst();
        if (obj != null)
            return obj.getCurrencyShortName();
        else return "";

    }

    public static int getMenuItemCountInCart(String id){
        int count=0;
        RealmResults<IRDCartObj> results = getInstance().where(IRDCartObj.class).equalTo("MenuItemObj.Id", id).findAll();
        for( IRDCartObj obj:results){
            count+= obj.getItemCount() ;
        }
        return count ;
    }

    public static IRDCartObj isThisCartAlreadyExist(IRDCartObj latestCartObj, String id) {
        RealmResults<IRDCartObj> cartObjectList = getInstance().where(IRDCartObj.class).equalTo("MenuItemObj.Id", id).findAll();
        for(IRDCartObj cartObj :cartObjectList) {

            if (cartObj.getMenuItemObj().getAddons().size()>0 && cartObj.getMenuItemObj().getCustomization().size()>0 ){
                if(checkAddOns(latestCartObj.getAddons(), cartObj.getAddons())&& checkCustomozations(latestCartObj.getCustomization(), cartObj.getCustomization())){
                    return cartObj;
                }
                else
                    continue;
            }else if((cartObj.getMenuItemObj().getAddons().size()>0) && (cartObj.getMenuItemObj().getCustomization().size()==0)){
                if(checkAddOns(latestCartObj.getAddons(), cartObj.getAddons())) {
                    return cartObj;
                }
                else
                    continue;
            }else if ((cartObj.getMenuItemObj().getCustomization().size() > 0)&& (cartObj.getMenuItemObj().getAddons().size() == 0)) {
                if (checkCustomozations(latestCartObj.getCustomization(), cartObj.getCustomization())) {
                    return cartObj;
                }
                else
                    continue;
            }else{
                return cartObj;
            }
        }
        return null;
    }

    private static boolean checkCustomozations(RealmList<IRDiningCustomizationObj> lastObjCustomizationList, RealmList<IRDiningCustomizationObj> customization) {
        boolean isAllEquals = false;
        for(int i=0;i<lastObjCustomizationList.size();i++){
            if(lastObjCustomizationList.get(i).getSelectedIndex()== customization.get(i).getSelectedIndex()){
                isAllEquals= true;

            }else {
                isAllEquals = false;
                break;
            }
        }
        return isAllEquals;

    }

    private static boolean checkAddOns(RealmList<IRDiningAddonsObj> lastObjAddonList, RealmList<IRDiningAddonsObj> addons) {
        boolean isAllEquals = false;
        for(int i=0;i<lastObjAddonList.size();i++)
            if(lastObjAddonList.get(i).isSelected()!=addons.get(i).isSelected()){
                isAllEquals= false;
                break;
            }
            else
                isAllEquals=true;

        return isAllEquals;
    }


    public static int getDiningCartCount() {
        int count=0;
        RealmResults<IRDCartObj> results = getInstance().where(IRDCartObj.class).findAll();
        for( IRDCartObj obj:results){
            count+= obj.getItemCount() ;
        }
        return count;
    }

    public static String getDiningTotalPrice() {
        Double price = Double.valueOf(0);
        RealmResults<IRDCartObj> results = getInstance().where(IRDCartObj.class).findAll();
        DecimalFormat format = new DecimalFormat();
        format.setDecimalSeparatorAlwaysShown(false);



        if (results != null) {
            RealmList<IRDCartObj> list = new RealmList<>();
            list.addAll(results);
            for (IRDCartObj objCart : results) {
                IRDiningMenuItemsObj obj=objCart.getMenuItemObj();
                if (obj.getDiscount().size() > 0) {

                    price = price + objCart.getItemCount() * Double.parseDouble(obj.getDiscount().get(0).getDiscountPrice());
                } else {
                    price += objCart.getItemCount() * Double.parseDouble(obj.getPrice());

                }

                if (objCart.getAddons().size() > 0) {
                    for (IRDiningAddonsObj addon : objCart.getAddons()) {
                        if (addon.isSelected())
                            price = price + Double.parseDouble(addon.getPrice())*objCart.getItemCount();
                    }
                }


            }
            return String.valueOf(format.format(price));

        }
        return "0";
    }

    public static void deleteDiningRealm() {
        RealmUtil.getInstance().executeTransaction(new Realm.Transaction() {
            @Override
            public void execute(Realm realm) {
                RealmResults<IRDiningMenuItemsObj> result = realm.where(IRDiningMenuItemsObj.class).findAll();
                result.deleteAllFromRealm();
            }
        });
    }

//    public static RealmResults<IRDiningMenuItemsObj> getCartItems() {
//
//        RealmResults<IRDiningMenuItemsObj> results = getInstance().where(IRDiningMenuItemsObj.class).equalTo("isSelected", true).findAll();
//        return results;
//    }

    public static void deleteCartRealm() {
        RealmUtil.getInstance().executeTransaction(new Realm.Transaction() {
            @Override
            public void execute(Realm realm) {
                RealmResults<IRDCartObj> result = realm.where(IRDCartObj.class).findAll();
                result.deleteAllFromRealm();
            }
        });
    }



    public static RealmResults<IRDCartObj> getCartItems() {

        RealmResults<IRDCartObj> results = getInstance().where(IRDCartObj.class).findAll();
        return results;
    }

    public static RealmResults<IRDiningMenuItemsObj> findMenuItems(){
        final RealmList<IRDiningMenuItemsObj> finalResults = new RealmList<>();
        for (IRDCartObj cartItem : getCartItems()) {
            finalResults.add(cartItem.getMenuItemObj());

        }
        return finalResults.where().findAll();
    }



    public static RealmList<IRDiningUpsellingItemObj> getupsellingItems() {
        RealmResults<IRDiningMenuItemsObj> results1 = findMenuItems().where().findAll();
        final RealmList<IRDiningUpsellingItemObj> finalResults = new RealmList<>();

        for (IRDiningMenuItemsObj menuItem : results1) {
            finalResults.addAll(menuItem.getUpsellingItems().where().findAll());

        }


        final RealmList<IRDiningUpsellingItemObj> results = new RealmList<>();

        for (final IRDiningMenuItemsObj menuItem : results1) {
            for (final IRDiningUpsellingItemObj obj : finalResults) {
                if (!getInstance().isInTransaction())
                    getInstance().beginTransaction();
                results.addAll(finalResults.where().notEqualTo(obj.getId(), menuItem.getId()).findAll());
                getInstance().commitTransaction();




             /*  if(!menuItem.getId().equals(obj.getId())){
                   upsellingResults.add(obj);
               }*/
            }
        }


        return results;
    }

    public static boolean isContailsKey(String key) {
        Log.d("RealmUtil", "kkkkkkkkkkkk" + key);

        return true;
    }


    private static RealmList<SpeedDialResponseData> getSpeedDialKey() throws JSONException {
        RealmList<SpeedDialResponseData> data = getInstance().where(SpeedDialConfigObj.class).findFirst().getResponseData();
        JSONArray keyList = new JSONArray();
        JSONObject obj = new JSONObject();
        for (int i = 0; i < data.size(); i++) {
            obj.getString(data.get(i).getName());
            obj.getString(data.get(i).getNumber());
            keyList.put(i, obj);
            Log.i("%%%%%%", "*****" + keyList.length());
        }


/*
        RealmResults<SpeedDialResponseData> newResults;
        RealmQuery<SpeedDialResponseData> where = r.where(SpeedDialConfigObj.class).beginGroup();
        for (String Name : keywords) {
            where = where.contains("name", keyword, Case.INSENSITIVE)
                    .or()
                    .contains("description", keyword, Case.INSENSITIVE)
                    .or()
                    .equalTo("tags.tag", keyword, Case.INSENSITIVE);
        }

        newResults = where.endGroup().findAll();*/

        String name = data.get(0).getName();
        String no = data.get(0).getNumber();

        return data;
    }

    public static IRDCartObj createCartObj(IRDiningMenuItemsObj bean) {
        int key=0;

        realm_instance.beginTransaction();
        try {
            key = realm_instance.where(IRDCartObj.class).max("RealmId").intValue() + 1;
        }catch (NullPointerException ex){
            ex.printStackTrace();
        }

        IRDCartObj cartObj = realm_instance.createObject(IRDCartObj.class,key);
        cartObj.setMenuItemObj(bean);
        cartObj.setItemCount(1);
        if(bean.getAddons().size()>0) {
            RealmList<IRDiningAddonsObj> addOnsList = bean.getAddons();
            RealmList<IRDiningAddonsObj> addOnsListtocart = new RealmList<>();
            for (IRDiningAddonsObj addOn : addOnsList) {
                IRDiningAddonsObj copyOFAddOns = addOn.duplicateObj();

                addOnsListtocart.add(RealmUtil.getInstance().copyToRealm(copyOFAddOns));
            }
            cartObj.setAddons(addOnsListtocart);
        }
        if(bean.getCustomization().size()>0) {
            RealmList<IRDiningCustomizationObj> customizationList = bean.getCustomization();
            RealmList<IRDiningCustomizationObj> customizationListtocart = new RealmList<>();
            for (IRDiningCustomizationObj customization : customizationList) {
                IRDiningCustomizationObj copyOfCustomization = customization.duplicateObj();
                customizationListtocart.add(RealmUtil.getInstance().copyToRealm(copyOfCustomization));
            }
            cartObj.setCustomization(customizationListtocart);
        }
        realm_instance.commitTransaction();
        return cartObj;

    }

    public static IRDCartObj createTmpCartObj(IRDiningMenuItemsObj bean) {
        IRDCartObj cartObj = new IRDCartObj();
        cartObj.setMenuItemObj(bean);
        cartObj.setItemCount(1);
        cartObj.setRealmId(-1);
        if(bean.getAddons().size()>0) {
            RealmList<IRDiningAddonsObj> addOnsList = bean.getAddons();
            RealmList<IRDiningAddonsObj> addOnsListtocart = new RealmList<>();
            for (IRDiningAddonsObj addOn : addOnsList) {
                IRDiningAddonsObj copyOFAddOns = addOn.duplicateObj();
                addOnsListtocart.add(copyOFAddOns);
            }
            cartObj.setAddons(addOnsListtocart);
        }
        if(bean.getCustomization().size()>0) {
            RealmList<IRDiningCustomizationObj> customizationList = bean.getCustomization();
            RealmList<IRDiningCustomizationObj> customizationListtocart = new RealmList<>();
            for (IRDiningCustomizationObj customization : customizationList) {
                IRDiningCustomizationObj copyOfCustomization = customization.duplicateObj();
                customizationListtocart.add(copyOfCustomization);
            }
            cartObj.setCustomization(customizationListtocart);
        }
        return cartObj;
    }


//    /*the data..Butler
//the data..Palace Ceremony
//the data..Restaurants and Bars
//the data..SPA
//the data..Concierge
//the data..Reception
//the data..Operator
//the data..Housekeeping*/

}
