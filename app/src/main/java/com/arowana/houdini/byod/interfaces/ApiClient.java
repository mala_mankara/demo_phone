package com.arowana.houdini.byod.interfaces;


import androidx.annotation.NonNull;

import java.io.IOException;
import java.util.concurrent.TimeUnit;

import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

import static com.arowana.houdini.byod.ServerConfig.AUTHORIZATION_TOKEN;
import static com.arowana.houdini.byod.ServerConfig.URL;

public class ApiClient {

    private static final String BASE_URL = URL;


    private static Retrofit retrofit = null;


    private static Retrofit getClient() {
        if (retrofit == null) {
            retrofit = new Retrofit.Builder()
                    .baseUrl(BASE_URL)
                    .client(getOkHttpClient().build())
                    .addConverterFactory(GsonConverterFactory.create())
                    .build();
        }
        return retrofit;
    }

    private static OkHttpClient.Builder getOkHttpClient(){
        OkHttpClient.Builder oKHttpBuilder=new OkHttpClient.Builder()
                .readTimeout(60, TimeUnit.SECONDS)
                .connectTimeout(60, TimeUnit.SECONDS);

        oKHttpBuilder.addInterceptor(new Interceptor() {
            @Override
            public Response intercept(@NonNull Chain chain) throws IOException {
               Request request= chain.request();
               Request.Builder newRequest=request
                       .newBuilder()
                       /*.cacheControl(CacheControl.FORCE_NETWORK)*/
                       .addHeader("Authorization",AUTHORIZATION_TOKEN);
                return chain.proceed(newRequest.build());
            }
        });

        return oKHttpBuilder;
    }

    public static <T> T getApiClient(Class<T> type){
        return ApiClient.getClient().create(type);
    }
}
