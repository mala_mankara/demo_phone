package com.arowana.houdini.byod.view.adapters.offers;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import androidx.viewpager.widget.ViewPager;

import com.arowana.houdini.byod.R;
import com.arowana.houdini.byod.interfaces.listeners.FragmentNavigationHandler;
import com.arowana.houdini.byod.model.offers.OffersConfigObject;
import com.arowana.houdini.byod.model.offers.OffersResponseData;
import com.arowana.houdini.byod.view.adapters.MultiImageAdapter;
import com.arowana.houdini.byod.view.fragments.offers.OfferDetailFragment;

import java.util.ArrayList;
import java.util.Objects;

import io.realm.RealmList;

import static com.arowana.houdini.byod.view.activity.OffersActivity.fragmentHolder;

public class OffersAdapter extends RecyclerView.Adapter<OffersAdapter.OfferViewHolder> {

    private Context mContext;
    private RealmList<OffersResponseData> dataObj;
    private FragmentNavigationHandler mhandler;
    private FrameLayout mholder;
    private OffersResponseData rowData;

    public OffersAdapter(Context context, OffersConfigObject obj, FragmentNavigationHandler mHandler, FrameLayout fragmentHolder) {

        mContext = context;
        dataObj = obj.getResponseData();
        mhandler=mHandler;
        mholder=fragmentHolder;
    }

    @NonNull
    public OfferViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.offer_items, parent, false);
        return new OfferViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull OfferViewHolder holder, final int position) {

        rowData= dataObj.get(position);

        if(rowData != null) {
            if (rowData.getImages() == null || rowData.getImages().isEmpty()) {

                holder.descriptionLayout.setVisibility(View.VISIBLE);
                holder.imageViewLayout.setVisibility(View.GONE);
                holder.offerType.setText(rowData.getName());
                holder.offer.setText(rowData.getDescription());
                holder.offervalidity.setText("");
                holder.descriptionLayout.setTag(position);
            } else {

                holder.imageViewLayout.setVisibility(View.VISIBLE);
                holder.descriptionLayout.setVisibility(View.GONE);
                holder.imageViewLayout.setTag(position);
                ArrayList<String> imageList = new ArrayList<>(rowData.getImages().size());
                for (int i = 0; i < rowData.getImages().size(); i++) {
                    imageList.add(Objects.requireNonNull(rowData.getImages().get(i)).getImageUrl());
                }
                int holderPosition = (int) holder.imageViewLayout.getTag();
                holder.imgHolder.setAdapter(new MultiImageAdapter(mContext, mhandler, fragmentHolder,
                        imageList, Objects.requireNonNull(dataObj.get(holderPosition)).getDetails(), true));
            }
            holder.descriptionLayout.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (rowData.getDetails().size() > 0) {
                        OfferDetailFragment fragment = new OfferDetailFragment();
                        fragment.setDetailsObj(Objects.requireNonNull(dataObj.get((Integer) v.getTag())).getDetails());
                        mhandler.showFragment(mholder.getId(), fragment);
                    }
                }
            });
        }
    }

    @Override
    public void registerAdapterDataObserver(@NonNull RecyclerView.AdapterDataObserver observer) {
        super.registerAdapterDataObserver(observer);
    }

    @Override
    public int getItemCount() {
        return dataObj.size();
    }

    @Override
    public long getItemId(int position) {
        super.getItemId(position);
        return position;
    }

    class OfferViewHolder extends RecyclerView.ViewHolder {
        LinearLayout descriptionLayout;
        TextView offerType;
        TextView offer;
        TextView offervalidity;

        FrameLayout imageViewLayout;
        ViewPager imgHolder;
        LinearLayout ll_dots;

        OfferViewHolder(View itemView) {
            super(itemView);
            descriptionLayout = itemView.findViewById(R.id.offer_textlayout);
            offerType         = itemView.findViewById(R.id.offerType);
            offer             = itemView.findViewById(R.id.offer_description);
            offervalidity      = itemView.findViewById(R.id.validity);

            imageViewLayout   = itemView.findViewById(R.id.pagerLayout);
            imgHolder         = itemView.findViewById(R.id.offer_ImageHolder);
            ll_dots           = itemView.findViewById(R.id.ll_dots);

            descriptionLayout.setClipToOutline(true);
            imageViewLayout.setClipToOutline(true);

        }
    }

}
