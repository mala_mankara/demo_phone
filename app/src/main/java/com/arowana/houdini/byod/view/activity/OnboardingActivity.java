package com.arowana.houdini.byod.view.activity;

import android.os.Bundle;
import android.widget.FrameLayout;

import com.arowana.houdini.byod.MainApplication;
import com.arowana.houdini.byod.R;
import com.arowana.houdini.byod.view.fragments.OnBoardingFragment;

public class OnboardingActivity extends BaseActivity {

    public static FrameLayout fragmentHolder;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_module_layout);
        MainApplication.setCurrentActivity(this);

        initComponents();

    }


    public void initComponents() {

        fragmentHolder = findViewById(R.id.fragment_holder);
        loadFragments();

    }

    public void loadFragments() {
        showFragment(fragmentHolder.getId(), new OnBoardingFragment());

    }

}
