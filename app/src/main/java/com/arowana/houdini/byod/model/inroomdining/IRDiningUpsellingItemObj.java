package com.arowana.houdini.byod.model.inroomdining;

import io.realm.RealmList;
import io.realm.RealmObject;

public class IRDiningUpsellingItemObj extends RealmObject {

    private String Ingredients;

    private String ItemCode;

    private String Description;

    private String CategoryCode;

    private String Price;

    private String ItemName;

    private String DietType;

    private RealmList<IRDiningDiscountObj> Discount;

    private String Id;

    private RealmList<IRDiningImagesObj> Images;

    private String MenuCode;

    public String getIngredients() {
        return Ingredients;
    }

    public void setIngredients(String ingredients) {
        Ingredients = ingredients;
    }

    public String getItemCode() {
        return ItemCode;
    }

    public void setItemCode(String itemCode) {
        ItemCode = itemCode;
    }

    public String getDescription() {
        return Description;
    }

    public void setDescription(String description) {
        Description = description;
    }

    public String getCategoryCode() {
        return CategoryCode;
    }

    public void setCategoryCode(String categoryCode) {
        CategoryCode = categoryCode;
    }

    public String getPrice() {
        return Price;
    }

    public void setPrice(String price) {
        Price = price;
    }

    public String getItemName() {
        return ItemName;
    }

    public void setItemName(String itemName) {
        ItemName = itemName;
    }

    public String getDietType() {
        return DietType;
    }

    public void setDietType(String dietType) {
        DietType = dietType;
    }

    public RealmList<IRDiningDiscountObj> getDiscount() {
        return Discount;
    }

    public void setDiscount(RealmList<IRDiningDiscountObj> discount) {
        Discount = discount;
    }

    public String getId() {
        return Id;
    }

    public void setId(String id) {
        Id = id;
    }

    public RealmList<IRDiningImagesObj> getImages() {
        return Images;
    }

    public void setImages(RealmList<IRDiningImagesObj> images) {
        Images = images;
    }

    public String getMenuCode() {
        return MenuCode;
    }

    public void setMenuCode(String menuCode) {
        MenuCode = menuCode;
    }
}
