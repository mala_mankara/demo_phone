package com.arowana.houdini.byod.view.fragments.offers;

import android.content.Context;
import android.os.Bundle;
import androidx.annotation.Nullable;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.util.Log;
import android.view.View;

import com.arowana.houdini.byod.R;
import com.arowana.houdini.byod.model.offers.OfferDetails;
import com.arowana.houdini.byod.view.adapters.offers.OfferDetailsAdapter;
import com.arowana.houdini.byod.view.fragments.BaseFragment;

import io.realm.RealmList;

public class OfferDetailFragment  extends BaseFragment {

    private View mRootView;
    private Context mContext;
    public static final String TAG="OfferDetails";
    private RecyclerView mRecyclerView;
    RealmList<OfferDetails>detailsObj;

    public OfferDetailFragment() {
        // Required empty public constructor
    }

    @Override
    protected int getFragmentLayout() {
        return R.layout.fragment_offers;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mContext = getActivity();
        setTitle(getResources().getString((R.string.offers)));
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        mRootView = view;
        initView();
        populateData(detailsObj);
    }

    private void initView() {
        mRecyclerView = mRootView.findViewById(R.id.offer_recyclerview);
        mRecyclerView.setBackgroundColor(getResources().getColor(R.color.color_white));
    }

    public void setDetailsObj( RealmList<OfferDetails> details) {
        detailsObj = details;
    }

    private void populateData(RealmList<OfferDetails> detailsObj) {
        Log.d("DetailsFrag","detailsData..."+ detailsObj.size());
        LinearLayoutManager linearVertical = new LinearLayoutManager(mContext, RecyclerView.VERTICAL, false);
        mRecyclerView.setLayoutManager(linearVertical);
        OfferDetailsAdapter adapter = new OfferDetailsAdapter(getContext(), detailsObj);
        mRecyclerView.setAdapter(adapter);
        adapter.notifyDataSetChanged();

    }
}
