package com.arowana.houdini.byod.interfaces.api_interface;

import com.arowana.houdini.byod.model.firebaseNotification.NotificationObject;

import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.POST;

public interface FirebaseNotificationApiInterface {

    @FormUrlEncoded
    @POST("GuestUser/RegisterDevice")
    Call<NotificationObject> getAll( @Field("MacId") String deviceMacId,@Field("DeviceType") String deviceType, @Field("DeviceId") String deviceId);
}


