package com.arowana.houdini.byod.model.redirct;

import io.realm.RealmObject;

public class RedirectSubModule extends RealmObject {

    private String SubModuleUrl;

    private String Key;

    private String CategoryId;

    private RedirectImage Image;

    private String Id;

    private String SubModule;

    private String Title;

    public String getSubModuleUrl ()
    {
        return SubModuleUrl;
    }

    public void setSubModuleUrl (String SubModuleUrl)
    {
        this.SubModuleUrl = SubModuleUrl;
    }

    public String getKey ()
    {
        return Key;
    }

    public void setKey (String Key)
    {
        this.Key = Key;
    }

    public String getCategoryId ()
    {
        return CategoryId;
    }

    public void setCategoryId (String CategoryId)
    {
        this.CategoryId = CategoryId;
    }

    public RedirectImage getImage ()
    {
        return Image;
    }

    public void setImage (RedirectImage Image)
    {
        this.Image = Image;
    }

    public String getId ()
    {
        return Id;
    }

    public void setId (String Id)
    {
        this.Id = Id;
    }

    public String getSubModule ()
    {
        return SubModule;
    }

    public void setSubModule (String SubModule)
    {
        this.SubModule = SubModule;
    }

    public String getTitle ()
    {
        return Title;
    }

    public void setTitle (String Title)
    {
        this.Title = Title;
    }

}
