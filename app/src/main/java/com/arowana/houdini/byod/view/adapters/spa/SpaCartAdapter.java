package com.arowana.houdini.byod.view.adapters.spa;

import android.app.Activity;
import android.app.DatePickerDialog;
import android.content.Context;
import android.graphics.Color;
import android.os.Handler;
import androidx.recyclerview.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.DatePicker;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.TimePicker;

import com.arowana.houdini.byod.R;
import com.arowana.houdini.byod.interfaces.listeners.FragmentNavigationHandler;
import com.arowana.houdini.byod.model.spa.SpaAvailabilityTimeObj;
import com.arowana.houdini.byod.model.spa.SpaTimeAndPriceObj;
import com.arowana.houdini.byod.model.spa.SpaTreatmentObj;
import com.arowana.houdini.byod.utils.DateUtil;
import com.arowana.houdini.byod.utils.RealmUtil;
import com.arowana.houdini.byod.view.adapters.ICart;

import java.text.NumberFormat;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.TimeZone;

import io.realm.Realm;
import io.realm.RealmResults;

public class SpaCartAdapter extends RecyclerView.Adapter<SpaCartAdapter.MyViewHolder> implements DatePickerDialog.OnDateSetListener {

private static final String TAG = "SpaCartAdapter";
    private Context context;
    private RealmResults<SpaTreatmentObj> spaTreatmentList;
    private SpaAvailabilityTimeObj spaHourObj;

    private FragmentNavigationHandler mHandler;
    private FrameLayout mHolder;
    private Realm realm;
    private String hotelTimeZone;

    int mYear;
    int mMonth;
    int mDay;


    private DatePickerDialog.OnDateSetListener listener;
    private ICart cartListener;


    public class MyViewHolder extends RecyclerView.ViewHolder {
        TextView treatmentName, treatmentCost, bookingDateTime, bookingDate;
        ImageView btnEdit, btnRemove;
        RelativeLayout bookingLayout;
        LinearLayout layoutDate;
        TimePicker timePicker;
        LinearLayout layoutTreatmentType;

        Spinner spaCartServiceTimepicker;

        public MyViewHolder(View view) {
            super(view);


            treatmentName = view.findViewById(R.id.spa_cart_title);
            treatmentCost = view.findViewById(R.id.spa_cart_price);
            bookingDateTime = view.findViewById(R.id.spa_cart_date_time);

            btnEdit = view.findViewById(R.id.spa_btn_edit);
            layoutDate = view.findViewById(R.id.layout_spa_cart_date);
            bookingDate = view.findViewById(R.id.spa_cart_book_date);
            timePicker = view.findViewById(R.id.spa_cart_time_picker);
            btnRemove = view.findViewById(R.id.btn_remove_treatment);


            bookingLayout = view.findViewById(R.id.spa_cart_booking_layout);
            layoutTreatmentType = view.findViewById(R.id.spa_cart_treatment_type_layout);


            spaCartServiceTimepicker = view.findViewById(R.id.spacart_servicetimepicker);
        }


    }


    public SpaCartAdapter(Context mContext, ICart mlistener, FragmentNavigationHandler handler, FrameLayout holder, SpaAvailabilityTimeObj spaTimeAvailabilityObj, String hotelTimeZone) {
        cartListener = mlistener;
        listener = this;
        this.context = mContext;

        mHandler = handler;
        mHolder = holder;
        realm = RealmUtil.getInstance();
        spaHourObj = spaTimeAvailabilityObj;
        this.hotelTimeZone = hotelTimeZone;

        RealmResults<SpaTreatmentObj> results = realm.where(SpaTreatmentObj.class).equalTo("isSelected", true).findAll();
        spaTreatmentList = results;

        Log.d("realmresults", "result is:" + results.size()+" spa hr.."+spaHourObj.getOpeningTime()+" ,/ "+spaHourObj.getClosingTime());


    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_spa_cart, parent, false);

        return new MyViewHolder(itemView);
    }
    private int OpenTime_24_hr, OpenTime_24_min, CloseTime_24_hr, CloseTime_24_min;
    private String SpaOpeningTime ;
    private String SpaClosingTime ;

    @Override
    public void onBindViewHolder(final MyViewHolder holder, final int position) {
        final SpaTreatmentObj bean = spaTreatmentList.get(position);

        holder.bookingDateTime.setText(bean.getBookingDate().concat(", ").concat(bean.getBookingTime()));
        holder.treatmentName.setText(bean.getTreatmentName());
        try {
            //int price=NumberFormat.getInstance().parse(bean.getPrice()).intValue();
            int price=NumberFormat.getInstance().parse(bean.getBookingTimeAndPrice().getPrice()).intValue();
            Log.d(TAG,"price is..."+price);
            int totalPrice = (bean.getMalePaxCount() + bean.getFemalePaxCount() + bean.getCoupleCount()) * price;
            Log.d(TAG,"total price is..."+totalPrice);
            holder.treatmentCost.setText(RealmUtil.getSpaCurrency() + "\n" + totalPrice);
        } catch (ParseException e) {
            e.printStackTrace();
        }



        holder.layoutTreatmentType.removeAllViews();

        if (bean.getCouplesTreatment().equals("true")) {
            holder.layoutTreatmentType.addView(getCountLayout(context.getResources().getString(R.string.no_of_couples), bean));
        } else if (bean.getGender().equalsIgnoreCase("Male")) {
            holder.layoutTreatmentType.addView(getCountLayout(context.getResources().getString(R.string.male_pax), bean));
        }else if (bean.getGender().equalsIgnoreCase("Female")) {
            holder.layoutTreatmentType.addView(getCountLayout(context.getResources().getString(R.string.female_pax), bean));
        }else{
            holder.layoutTreatmentType.setWeightSum(2);
            holder.layoutTreatmentType.addView(getCountLayout(context.getResources().getString(R.string.female_pax), bean), 0);
            holder.layoutTreatmentType.addView(getCountLayout(context.getResources().getString(R.string.male_pax), bean), 1);

        }
        final TimeZone timeZone=TimeZone.getTimeZone(hotelTimeZone);
        final Calendar c = Calendar.getInstance(timeZone);
        mYear = c.get(Calendar.YEAR);
        mMonth = c.get(Calendar.MONTH);
        mDay = c.get(Calendar.DAY_OF_MONTH);

        SpaOpeningTime =spaHourObj.getOpeningTime();
        SpaClosingTime  =spaHourObj.getClosingTime();
        Log.d(TAG, " SpaCARTAdapter Available time Open Time .."+SpaOpeningTime+" /Close Time, "+SpaClosingTime);

        OpenTime_24_hr = DateUtil.getHr_in24Hr_Timeformat(SpaOpeningTime);
        OpenTime_24_min = DateUtil.getMin_24Hr_Timeformat(SpaOpeningTime);
        CloseTime_24_hr = DateUtil.getHr_in24Hr_Timeformat(SpaClosingTime);
        CloseTime_24_min = DateUtil.getMin_24Hr_Timeformat(SpaClosingTime);



        if (bean.getBookingTime() != null) {
            try {
                holder.timePicker.setCurrentHour(DateUtil.getTimepickerDate(bean.getBookingTime()).getHours());
                holder.timePicker.setCurrentMinute(DateUtil.getTimepickerDate(bean.getBookingTime()).getMinutes());
            } catch (ParseException e) {
                e.printStackTrace();
            }
        } else {
//            Log.d(TAG, " SpaCARTAdapter.getBookingTime!=null else ..."+c.get(Calendar.HOUR_OF_DAY)+": "+c.get(Calendar.MINUTE) );
//            holder.timePicker.setCurrentHour(c.get(Calendar.HOUR_OF_DAY));
//            holder.timePicker.setCurrentMinute(c.get(Calendar.MINUTE));
        }

     /*   if (bean.getBookingTime() == null) {
            holder.timePicker.setCurrentMinute(c.get(Calendar.MINUTE) + 5);
        }*/
        /*if (bean.getBookingDate() != null) {
            try {
                c.setTime(DateUtil.getDate(bean.getBookingDate()));
            } catch (ParseException e) {
                e.printStackTrace();
            }
        }*/


        holder.btnEdit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (holder.bookingLayout.getVisibility() == View.VISIBLE) {
                    holder.bookingLayout.setVisibility(View.GONE);
                } else {
                    holder.bookingLayout.setVisibility(View.VISIBLE);


                }
            }
        });

        holder.btnRemove.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (!realm.isInTransaction())
                    realm.beginTransaction();
                bean.setSelected(false);
                notifyDataSetChanged();
                realm.commitTransaction();
                cartListener.updateCartInfo();
            }
        });


        holder.bookingDate.setText(bean.getBookingDate());


        final DatePickerDialog.OnDateSetListener dates = new DatePickerDialog.OnDateSetListener() {

            @Override
            public void onDateSet(DatePicker view, int year, int monthOfYear,
                                  int dayOfMonth) {
                Log.d(TAG,"OnDateSetListener..");
                c.set(year, monthOfYear, dayOfMonth);
                if (!realm.isInTransaction())
                    realm.beginTransaction();
                bean.setBookingDate(DateUtil.getCurrentDateInSpecificFormat(c));
                notifyItemRangeChanged(position, 1, bean);
                realm.commitTransaction();
            }

        };

        holder.layoutDate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Log.d(TAG,"layoutDate click listener..");
                DatePickerDialog datePickerDialog = new DatePickerDialog(context, dates, mYear, mMonth, mDay);
                Calendar c_calender = Calendar.getInstance();
                mYear = c_calender.get(Calendar.YEAR);
                mMonth = c_calender.get(Calendar.MONTH);
                mDay = c_calender.get(Calendar.DAY_OF_MONTH);
                datePickerDialog.getDatePicker().setMinDate(c_calender.getTimeInMillis());
                if (bean.getBookingDate() != null) {
                    try {
                        c_calender.setTime(DateUtil.getDate(bean.getBookingDate()));
                        mYear = c_calender.get(Calendar.YEAR);
                        mMonth = c_calender.get(Calendar.MONTH);
                        mDay = c_calender.get(Calendar.DAY_OF_MONTH);
                        datePickerDialog.updateDate(mYear,mMonth,mDay);
                    } catch (ParseException e) {
                        e.printStackTrace();
                    }
                }
                datePickerDialog.show();
            }
        });


        //-----------------------------##Bug 127
        final ArrayList<String> timelist = new ArrayList<>(bean.getTimeAndPrice().size());
        for(SpaTimeAndPriceObj timeObj : bean.getTimeAndPrice()) {
            timelist.add(timeObj.getTime());
        }
        ArrayAdapter<String> spinnerAdapter = new ArrayAdapter<String>(context, android.R.layout.simple_spinner_item, timelist){
            //By using this method we will define how
            // the text appears before clicking a spinner
            public View getView(int position, View convertView,
                                ViewGroup parent) {
                View v = super.getView(position, convertView, parent);
                ((TextView) v).setTextColor(Color.parseColor("#8C8179"));
                return v;
            }
            //By using this method we will define
            //how the listview appears after clicking a spinner
            public View getDropDownView(int position, View convertView,
                                        ViewGroup parent) {
                View v = super.getDropDownView(position, convertView,parent);
                ((TextView) v).setTextColor(Color.parseColor("#8C8179"));
                return v;
            }
        };
        spinnerAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        holder.spaCartServiceTimepicker.setAdapter(spinnerAdapter);

        holder.spaCartServiceTimepicker.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
//                priceData = bean.getTimeAndPrice().get(position).getPrice();
//                holder.treatmentCost.setText(priceData);
                try {
                    //int price=NumberFormat.getInstance().parse(bean.getPrice()).intValue();
                    int price=NumberFormat.getInstance().parse(bean.getTimeAndPrice().get(position).getPrice()).intValue();
                    Log.d(TAG," onItemSelected price is..."+price);
                    int totalPrice = (bean.getMalePaxCount() + bean.getFemalePaxCount() + bean.getCoupleCount()) * price;
                    Log.d(TAG,"onItemSelected total price is..."+totalPrice);
                    holder.treatmentCost.setText(RealmUtil.getSpaCurrency() + "\n" + totalPrice);
                } catch (ParseException e) {
                    e.printStackTrace();
                }
                if (!realm.isInTransaction())
                    realm.beginTransaction();
                bean.setDurationSelected(true);
                bean.setSubmit(false);
                bean.setSpaSelectedDuartion(position);
                bean.setBookingTimeAndPrice(bean.getTimeAndPrice().get(position));
                realm.commitTransaction();
                /*notifyDataSetChanged();*/
                cartListener.updateCartInfo();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        if(bean.isDurationSelected()){
            holder.spaCartServiceTimepicker.setSelection(bean.getSpaSelectedDuartion());
        }else
            holder.spaCartServiceTimepicker.setSelection(0);


        //------------------------------


        holder.timePicker.setOnTimeChangedListener(new TimePicker.OnTimeChangedListener() {
            @Override
            public void onTimeChanged(TimePicker timePicker, int hourOfDay, int minute) {
                boolean validTime = true;
                if (hourOfDay < OpenTime_24_hr || (hourOfDay == OpenTime_24_hr && minute < OpenTime_24_min)){
                    //checking if selected time is <than opening hr.
                    validTime = false;
                    timePicker.setCurrentHour(OpenTime_24_hr);
                    timePicker.setCurrentMinute(OpenTime_24_min);
                }
                if (hourOfDay > CloseTime_24_hr || (hourOfDay == CloseTime_24_hr && minute > CloseTime_24_min)){
                    //checking if selected time is > than closing hr.
                    Log.d(TAG,"22onTimeChanged.."+ hourOfDay+" : "+minute);
                    validTime = false;
                    timePicker.setCurrentHour(CloseTime_24_hr);
                    timePicker.setCurrentMinute(CloseTime_24_min);
                }
                if (validTime) {
                    //checking if selected time is within the range.
                    int currentHour = hourOfDay;
                    int currentMinute = minute;
                    timePicker.setEnabled(true);
                    if (!realm.isInTransaction())
                        realm.beginTransaction();
                    bean.setBookingTime(DateUtil.getTimepickerTime(timePicker));
                    realm.commitTransaction();

                    new Handler().post(new Runnable() {
                        @Override
                        public void run() {
                            notifyItemRangeChanged(position, 1, bean);
                        }
                    });
                }
            }
        });
    }
    void spinnerClick(){

    }

    @Override
    public int getItemCount() {
        return spaTreatmentList.size();
    }


    public View getCountLayout(final String type, final SpaTreatmentObj bean) {

        View view = ((Activity) context).getLayoutInflater().inflate(R.layout.item_spa_treatment_type_count_layout, null);
        LinearLayout.LayoutParams param = new LinearLayout.LayoutParams(0, LinearLayout.LayoutParams.WRAP_CONTENT);
        param.weight = 1;
        view.setLayoutParams(param);
        TextView coupleText = view.findViewById(R.id.type);
        TextView btnIncrease = view.findViewById(R.id.btn_increase);
        final TextView countTxt = view.findViewById(R.id.item_count);

        TextView btnDecrease = view.findViewById(R.id.btn_decrease);
        final TextView btnAdd = view.findViewById(R.id.btn_spa_add);
        final LinearLayout countLayout = view.findViewById(R.id.count_layout);
        switch (type) {
            case "Male pax":

                coupleText.setText(type);
                countTxt.setText(String.valueOf(bean.getMalePaxCount()));
                btnAdd.setVisibility(bean.getMalePaxCount() <= 0 ? View.VISIBLE : View.GONE);
                countLayout.setVisibility(bean.getMalePaxCount() < 1 ? View.GONE : View.VISIBLE);

                break;

            case "Female pax":

                coupleText.setText(type);
                countTxt.setText(String.valueOf(bean.getFemalePaxCount()));
                btnAdd.setVisibility(bean.getFemalePaxCount() <= 0 ? View.VISIBLE : View.GONE);
                countLayout.setVisibility(bean.getFemalePaxCount() < 1 ? View.GONE : View.VISIBLE);

                break;

            case "No. of couples":
                coupleText.setText(type);
                countTxt.setText(String.valueOf(bean.getCoupleCount()));
                btnAdd.setVisibility(bean.getCoupleCount() <= 0 ? View.VISIBLE : View.GONE);
                countLayout.setVisibility(bean.getCoupleCount() < 1 ? View.GONE : View.VISIBLE);

                break;
        }


        btnAdd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                btnAdd.setVisibility(View.GONE);
                countLayout.setVisibility(View.VISIBLE);

                realm.executeTransaction(new Realm.Transaction() {
                    @Override
                    public void execute(Realm realm) {
                        if (type.equals(context.getResources().getString(R.string.male_pax))) {
                            bean.setMalePaxCount(1);
                        } else if (type.equals(context.getResources().getString(R.string.female_pax))) {
                            bean.setFemalePaxCount(1);
                        } else if (type.equals(context.getResources().getString(R.string.no_of_couples))) {
                            bean.setCoupleCount(1);
                        }
                        notifyDataSetChanged();
                        cartListener.updateCartInfo();
                    }
                });

            }
        });


        btnIncrease.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                realm.executeTransaction(new Realm.Transaction() {
                    @Override
                    public void execute(Realm realm) {

                        if (type.equals(context.getResources().getString(R.string.male_pax))) {
                            int count = bean.getMalePaxCount() + 1;
                            bean.setMalePaxCount(count > 0 ? count : 0);
                        } else if (type.equals(context.getResources().getString(R.string.female_pax))) {
                            int count = bean.getFemalePaxCount() + 1;
                            bean.setFemalePaxCount(count > 0 ? count : 0);
                        } else if (type.equals(context.getResources().getString(R.string.no_of_couples))) {
                            int count = bean.getCoupleCount() + 1;
                            bean.setCoupleCount(count > 0 ? count : 0);
                        }

                        notifyDataSetChanged();
                        cartListener.updateCartInfo();
                    }
                });


            }
        });

        btnDecrease.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {


                realm.executeTransaction(new Realm.Transaction() {
                    @Override
                    public void execute(Realm realm) {
                        if (type.equals(context.getResources().getString(R.string.male_pax))) {
                            int count = bean.getMalePaxCount() - 1;
                            if(bean.getGender().equalsIgnoreCase("All") && bean.getFemalePaxCount()>0){
                                bean.setMalePaxCount(count > 0 ? count : 0);
                            }else
                                bean.setMalePaxCount(count > 0 ? count : bean.getMalePaxCount());
                        } else if (type.equals(context.getResources().getString(R.string.female_pax))) {
                            int count = bean.getFemalePaxCount() - 1;
                            if(bean.getGender().equalsIgnoreCase("All") && bean.getMalePaxCount()>0){
                                bean.setFemalePaxCount(count > 0 ? count : 0);
                            }else
                                bean.setFemalePaxCount(count > 0 ? count : bean.getFemalePaxCount());
                        } else if (type.equals(context.getResources().getString(R.string.no_of_couples))) {
                            int count = bean.getCoupleCount() - 1;
                            bean.setCoupleCount(count > 0 ? count : bean.getCoupleCount());
                        }
                        notifyDataSetChanged();
                        cartListener.updateCartInfo();
                    }

                });


            }
        });
        return view;
    }

    @Override
    public void onDateSet(DatePicker datePicker, int i, int i1, int i2) {

    }
}
