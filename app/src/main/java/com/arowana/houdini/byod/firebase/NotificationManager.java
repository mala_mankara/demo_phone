package com.arowana.houdini.byod.firebase;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.util.Log;

import androidx.localbroadcastmanager.content.LocalBroadcastManager;

import com.arowana.houdini.byod.model.sharedpreference.SharedPreferenceUtil;
import com.arowana.houdini.byod.model.sharedpreference.StoreKeys;
import com.arowana.houdini.byod.view.activity.BaseActivity;
import com.google.firebase.messaging.FirebaseMessaging;

import org.json.JSONObject;


public class NotificationManager {
    private static String TAG = "NotificationManager";
    private static NotificationManager INSTANCE = null;

    private Context mContext;

    private NotificationManager(Context context) {
        this.mContext = context;
    }

    public static synchronized  NotificationManager getInstance(Context context) {
        if (INSTANCE == null) {
            INSTANCE = new NotificationManager(context);
        }
        return INSTANCE;

    }

    private BroadcastReceiver mRegistrationBroadcastReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            try {
                Log.d(TAG, "----------mRegistrationBroadcastReceiver--------------" + intent.getAction());
                // checking for type intent filter
                if (intent.getAction().equals(NotificationConfig.REGISTRATION_COMPLETE)) {
                    // gcm successfully registered
                    // now subscribe to `global` topic to receive app wide notifications
                    FirebaseMessaging.getInstance().subscribeToTopic(NotificationConfig.TOPIC_GLOBAL);
                    System.out.println("" + SharedPreferenceUtil.getFromStore(StoreKeys.E_PALACE_PUSH_NOTIFICATION_ID));
                    if (mContext instanceof BaseActivity) {
                        ((BaseActivity) mContext).sendNotificationDeviceId("");
                    }
                } else if (intent.getAction().equals(NotificationConfig.PUSH_NOTIFICATION)) {
                    // new push notification is received
                    Log.d(TAG, "PUSH_NOTIFICATION data>>>>" + intent.toString());
                    //JSONObject data = new JSONObject(intent.getStringExtra("message"));

                    String title;
                    String timestamp = "";
                    JSONObject data  = null;
                    String moduleType = null;
                   /* String message = data.getString("message");
                    String title = data.getString("title");
                    String timestamp = data.getString("timestamp");*/
                    String message = intent.getStringExtra("body");
                    if (intent.getStringExtra("title") != null) {
                        title = intent.getStringExtra("title");
                    } else {
                        title = "Emirates Palace";
                    }
                    if (intent.getStringExtra("timestamp") != null) {
                        timestamp = intent.getStringExtra("timestamp");
                    }
                    if (intent.getStringExtra("data") != null) {
                        data = new JSONObject(intent.getStringExtra("data"));
                        moduleType = data.getString("Module");
                    }
//                    String image =  data.getString("image");

                    //Toast.makeText(mContext, "Push notification: " + message, Toast.LENGTH_LONG).show();

                    NotificationUtils notificationUtils = new NotificationUtils(mContext);
                 //   notificationUtils.playNotificationSound();
                    Log.d(TAG, "-------"+moduleType);
                    Intent intentObj = new Intent(mContext, BaseActivity.class);
                    if(moduleType!= null){
                        intentObj.putExtra("Module", moduleType);
                    }


                    /*if(message.contains("Houdini Confirmation Code")){
                        Log.d("Notitest12345", "-------"+message);
                        Intent in = new Intent(mContext,MainActivity.class);
                        in.putExtra("menuFragment", "feedback");
                        intent.putExtra("menuFragment", "feedback");
                        notificationUtils.showNotificationMessage(title, message, timestamp,in);
                        // intent.setFlags(Intent.FLAG_A | Intent.FLAG_ACTIVITY_SINGLE_TOP);
                    }else{*/
                        notificationUtils.showNotificationMessage(title, message, timestamp,intentObj);


                    //}

                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    };

    public void registerNotificationService() {
        pushNotificationRegisterService();
    }

    public void unRegisterNotificationBroadCastReceiver() {
        LocalBroadcastManager.getInstance(mContext).unregisterReceiver(mRegistrationBroadcastReceiver);
    }

    private void pushNotificationRegisterService() {

        Log.d(TAG, "-----pushNotificationRegisterService------------" + SharedPreferenceUtil.getFromStore(StoreKeys.E_PALACE_PUSH_NOTIFICATION_ID));



        LocalBroadcastManager.getInstance(mContext).registerReceiver(mRegistrationBroadcastReceiver,
                new IntentFilter(NotificationConfig.REGISTRATION_COMPLETE));

        // register new push message receiver
        // by doing this, the activity will be notified each time a new message arrives
        LocalBroadcastManager.getInstance(mContext).registerReceiver(mRegistrationBroadcastReceiver,
                new IntentFilter(NotificationConfig.PUSH_NOTIFICATION));
        // clear the notification area when the app is opened
        NotificationUtils.clearNotifications(mContext);
    }
}
