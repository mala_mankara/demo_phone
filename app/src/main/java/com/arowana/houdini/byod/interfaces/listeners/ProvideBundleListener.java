package com.arowana.houdini.byod.interfaces.listeners;


public interface ProvideBundleListener {
    void registerBundleListener(UpdatePreviousScreenListener listener);
}
