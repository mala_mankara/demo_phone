package com.arowana.houdini.byod.model.spa;

import io.realm.RealmList;
import io.realm.RealmObject;

public class SpaTreatmentTypeObj extends RealmObject {

    private String TypeId;

    private String SpaId;

    private String TypeName;

    private RealmList<SpaTreatmentObj> Treatment;


    public String getTypeId() {
        return TypeId;
    }

    public void setTypeId(String typeId) {
        TypeId = typeId;
    }

    public String getSpaId() {
        return SpaId;
    }

    public void setSpaId(String spaId) {
        SpaId = spaId;
    }

    public String getTypeName() {
        return TypeName;
    }

    public void setTypeName(String typeName) {
        TypeName = typeName;
    }

    public RealmList<SpaTreatmentObj> getTreatment() {
        return Treatment;
    }

    public void setTreatment(RealmList<SpaTreatmentObj> treatment) {
        Treatment = treatment;
    }
}
