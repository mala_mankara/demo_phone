package com.arowana.houdini.byod.model.restaurant;

import io.realm.RealmObject;

public class RestaurantReviewsObj extends RealmObject {

    private String Name;

    private String Url;

    public String getName() {
        return Name;
    }

    public void setName(String name) {
        Name = name;
    }

    public String getUrl() {
        return Url;
    }

    public void setUrl(String url) {
        Url = url;
    }
}
