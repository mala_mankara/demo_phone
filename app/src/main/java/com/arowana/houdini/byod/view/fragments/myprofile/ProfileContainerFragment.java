package com.arowana.houdini.byod.view.fragments.myprofile;


import android.app.ProgressDialog;
import android.content.Context;
import android.os.Bundle;
import androidx.annotation.Nullable;
import android.util.Log;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.TextView;

import com.arowana.houdini.byod.R;
import com.arowana.houdini.byod.interfaces.ApiClient;
import com.arowana.houdini.byod.interfaces.api_interface.MyProfileApiInterface;
import com.arowana.houdini.byod.model.login.LoginResponseDataObject;
import com.arowana.houdini.byod.model.myprofile.MyProfileUpdateConfigObj;
import com.arowana.houdini.byod.utils.ViewUtils;
import com.arowana.houdini.byod.view.fragments.BaseFragment;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;

import io.realm.Realm;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class ProfileContainerFragment extends BaseFragment implements UpdateProfileDataListener {


    private View mRootView;
    private Context mContext;
    private FrameLayout layout;
    private LoginResponseDataObject myProfileObject;
    private Realm mRealm;
    private int type;
    private int dataType = 0;
    private Bundle mUpdatedData = null;
    private String mTitle;
    private ProgressDialog mDialog;


    public ProfileContainerFragment() {

    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        // getActivity().getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_VISIBLE | WindowManager.LayoutParams.SOFT_INPUT_ADJUST_RESIZE);
    }

    @Override
    protected int getFragmentLayout() {
        return R.layout.fragment_profile_container;
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        mRootView = view;
        mContext = getActivity();
        init();
    }


    private void init() {
        layout = mRootView.findViewById(R.id.inflaterView);
        layout.removeAllViews();
        loadUI(type);
        initDialog();

    }

    private void initDialog() {
        mDialog = new ProgressDialog(mContext);
        mDialog.setMessage(getString(R.string.dialog_message));
        mDialog.setCancelable(false);
    }

                /*
                1=Edit Username
                2=Edit Password
                3=Add email
                4=EditEmailAddress
                5=Add name address
                6=Edit name address
                7=Add Phone number
                8=Edit Phone Number


                */

    private void loadUI(int type) {
        Log.d("test", "---loadUI----" + type);

        switch (type) {

            case 2:
                mTitle = "Edit Password";
                dataType = 0;
                new EditPassword(mRootView, mContext,mDialog, this);
                break;

            case 3:
                mTitle = "Add Email Id";
                dataType = 1;
                new EditEmailAddress(mRootView, mContext, myProfileObject, this);
                break;
            case 4:
                mTitle = "Edit Email Id";
                dataType = 1;
                new EditEmailAddress(mRootView, mContext, myProfileObject, this);
                break;

            case 5:
                mTitle = "Add Address";
                dataType = 2;
                new EditNameAddress(mRootView, mContext, myProfileObject, this);
                break;

            case 6:
                mTitle = "Edit Address";
                dataType = 2;
                new EditNameAddress(mRootView, mContext, myProfileObject, this);
                break;

            case 7:
                mTitle = "Add Phone Number";
                dataType = 3;
                new EditPhoneNumber(mRootView, mContext, myProfileObject, this);
                break;

            case 8:
                mTitle = "Edit Phone Number";
                dataType = 3;
                new EditPhoneNumber(mRootView, mContext, myProfileObject, this);
                break;


        }
        setTitle(mTitle);

    }

    public void setObject(LoginResponseDataObject object, Realm realm, int type) {
        this.mRealm = realm;
        this.myProfileObject = object;
        this.type = type;

    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();

        Log.d("4444", "onDestroyView");

        layout.removeAllViews();
    }

    @Override
    public void onUpdateProfileData(Bundle data) {
        Log.d("test", "onUpdateProfileData>>>" + data);
        mUpdatedData = data;
        populateSnackbar();
    }

    public void populateSnackbar() {
        final FrameLayout snackLayout = getActivity().findViewById(R.id.snacbar_layout);
        snackLayout.setVisibility(View.VISIBLE);
        View view = getActivity().getLayoutInflater().inflate(R.layout.custom_confirm_snackbar_layout, null);
        TextView mOkMessage = view.findViewById(R.id.snack_ok_message);
        TextView mCancelMessage = view.findViewById(R.id.snack_cancel_message);

        mOkMessage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                snackLayout.setVisibility(View.GONE);
                snackLayout.removeAllViews();
                onSubmitData();


            }
        });

        mCancelMessage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                snackLayout.setVisibility(View.GONE);
                snackLayout.removeAllViews();

            }
        });

        snackLayout.addView(view);
    }

    private void onSubmitData() {

        try {
            JsonObject submitJson = new JsonObject();
            submitJson.addProperty("MembershipId", myProfileObject.getMembershipId());
            if (dataType == 0) {
                submitJson.addProperty("OldPassword", mUpdatedData.getString("oldPassword"));
                submitJson.addProperty("NewPassword", mUpdatedData.getString("newPassword"));
                Log.d("test", "onSubmitData>>>>>" + submitJson.toString());
                callSubmitAPI(submitJson, true);
            } else {

                if (myProfileObject.getMobileNumber() != null && !myProfileObject.getMobileNumber().isEmpty()) {
                    JsonObject phoneObject = new JsonObject();
                    phoneObject.addProperty("CountryCode", myProfileObject.getCountryCode());
                    phoneObject.addProperty("MobileNumber", myProfileObject.getMobileNumber());
                    JsonArray phoneArray = new JsonArray();
                    phoneArray.add(phoneObject);
                    submitJson.add("MobileNumber", phoneArray);
                }
                if (dataType == 3 && mUpdatedData.getString("phoneNumber") != null) {
                    JsonObject phoneObject = new JsonObject();
                    phoneObject.addProperty("CountryCode", mUpdatedData.getString("phoneCode"));
                    phoneObject.addProperty("MobileNumber", mUpdatedData.getString("phoneNumber"));
                    JsonArray phoneArray = new JsonArray();
                    phoneArray.add(phoneObject);
                    submitJson.add("MobileNumber", phoneArray);
                }

                if (myProfileObject.getEmailId() != null && !myProfileObject.getEmailId().isEmpty()) {
                    JsonObject emailObject = new JsonObject();
                    emailObject.addProperty("PrimaryEmail", myProfileObject.getEmailId());
                    submitJson.add("EmailId", emailObject);
                }
                if (dataType == 1 && mUpdatedData.getString("email") != null) {
                    JsonObject emailObject = new JsonObject();
                    emailObject.addProperty("PrimaryEmail", mUpdatedData.getString("email"));
                    submitJson.add("EmailId", emailObject);
                }
                if (myProfileObject.getAddress() != null && !myProfileObject.getAddress().isEmpty()) {
                    JsonObject addressObject = new JsonObject();
                    addressObject.addProperty("Address", myProfileObject.getAddress());
                    if (myProfileObject.getCity() != null && !myProfileObject.getCity().isEmpty()) {
                        addressObject.addProperty("City", myProfileObject.getCity());
                        addressObject.addProperty("CityCode", "");
                    } else {
                        addressObject.addProperty("City", "");
                        addressObject.addProperty("CityCode", "");
                    }
                    if (myProfileObject.getCountry() != null && !myProfileObject.getCountry().isEmpty()) {
                        addressObject.addProperty("Country", myProfileObject.getCountry());
                    } else {
                        addressObject.addProperty("Country", "");
                    }
                    if (myProfileObject.getPinCode() != null && !myProfileObject.getPinCode().isEmpty()) {
                        addressObject.addProperty("PinCode", myProfileObject.getPinCode());
                    } else {
                        addressObject.addProperty("PinCode", "");
                    }
                    JsonArray addressArray = new JsonArray();
                    addressArray.add(addressObject);
                    submitJson.add("Address", addressArray);
                }
                if (dataType == 2 && mUpdatedData.getString("guestAddress") != null) {
                    JsonObject addressObject = new JsonObject();
                    addressObject.addProperty("Address", mUpdatedData.getString("guestAddress"));
                    addressObject.addProperty("City", mUpdatedData.getString("guestCity"));
                    addressObject.addProperty("CityCode", mUpdatedData.getString("guestCity"));
                    addressObject.addProperty("Country", mUpdatedData.getString("guestCountry"));
                    addressObject.addProperty("PinCode", mUpdatedData.getString("guestPincode"));

                    JsonArray addressArray = new JsonArray();
                    addressArray.add(addressObject);
                    submitJson.add("Address", addressArray);
                }

                Log.d("test", "onSubmitData>>>>>" + submitJson.toString());

                callSubmitAPI(submitJson, false);
            }
//            mDialog.show();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    public void callSubmitAPI(JsonObject json, boolean isPassword) {
        Call<MyProfileUpdateConfigObj> responseObj;
        if (mDialog.isShowing())
            mDialog.dismiss();


        if (isPassword)
            responseObj = ApiClient.getApiClient(MyProfileApiInterface.class).updatePassword(json);
        else {
            responseObj = ApiClient.getApiClient(MyProfileApiInterface.class).updateGuestInfo(json);
        }

        responseObj.enqueue(new Callback<MyProfileUpdateConfigObj>() {
            @Override
            public void onResponse(Call<MyProfileUpdateConfigObj> call, Response<MyProfileUpdateConfigObj> response) {


                String flag = response.body().getResponseStatus().getResponseFlag();
                final String message = response.body().getResponseStatus().getResponseMessage();
                if (flag.equals("SUCCESS")) {
                    processData();
                    ViewUtils.showSnackBarMessage(message);

                } else ViewUtils.showSnackBarMessage(message);
            }

            @Override
            public void onFailure(Call<MyProfileUpdateConfigObj> call, Throwable t) {

            }
        });

    }


    private void processData() {
        if (!mRealm.isInTransaction())
            mRealm.beginTransaction();

        if (dataType == 1) {
            myProfileObject.setEmailId(mUpdatedData.getString("email"));
        } else if (dataType == 2) {
            myProfileObject.setAddress(mUpdatedData.getString("guestAddress"));
            myProfileObject.setCity(mUpdatedData.getString("guestCity"));
            myProfileObject.setCountry(mUpdatedData.getString("guestCountry"));
            myProfileObject.setPinCode(mUpdatedData.getString("guestPincode"));
        } else if (dataType == 3) {
            myProfileObject.setCountryCode(mUpdatedData.getString("phoneCode"));
            myProfileObject.setMobileNumber(mUpdatedData.getString("phoneNumber"));
        }
        mRealm.commitTransaction();
        getActivity().onBackPressed();
        if (mDialog.isShowing()) {
            mDialog.dismiss();
        }
    }


}


