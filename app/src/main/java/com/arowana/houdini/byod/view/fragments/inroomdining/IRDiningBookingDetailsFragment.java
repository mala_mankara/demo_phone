package com.arowana.houdini.byod.view.fragments.inroomdining;

import android.os.Bundle;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.arowana.houdini.byod.R;
import com.arowana.houdini.byod.model.inroomdining.IRDiningAddonsObj;
import com.arowana.houdini.byod.model.inroomdining.IRDiningBookingObj;
import com.arowana.houdini.byod.model.inroomdining.IRDiningOrderLineItemsObj;
import com.arowana.houdini.byod.utils.RealmUtil;
import com.arowana.houdini.byod.view.adapters.inroomdining.InRoomDiningBookingDetailsAdapter;
import com.arowana.houdini.byod.view.fragments.BaseFragment;

import java.text.DecimalFormat;


public class IRDiningBookingDetailsFragment extends BaseFragment {

    public IRDiningBookingDetailsFragment() {}
    private View mRootView;
    private TextView mCost;
    private RecyclerView mRecyclerView;
    private IRDiningBookingObj mObj;
    private TextView mMsg, instructiondata;
    private String confirmMessage;
    private LinearLayout mInstructionLayout;

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        mRootView = view;
        setTitle(getString(R.string.order_summary));
        initcomponents();
        populateData();
    }

    public void initcomponents() {
        mRecyclerView = mRootView.findViewById(R.id.ir_dining_booking_details_recyclerview);
        mCost = mRootView.findViewById(R.id.ir_dining_total_cost);
        Button mBtnOk = mRootView.findViewById(R.id.ir_dining_booking_btn_ok);
        mMsg = mRootView.findViewById(R.id.confirm_msg);

        mInstructionLayout =mRootView.findViewById(R.id.ird_instruction_layout);
        instructiondata = mRootView.findViewById(R.id.ird_booking_details_instruction_data);
        instructiondata.setHorizontallyScrolling(false);
        instructiondata.setScrollContainer(true);
        instructiondata.setMaxLines(3);
        mInstructionLayout.setVisibility(mObj.getComments()!= null && ! mObj.getComments().isEmpty()? View.VISIBLE : View.GONE);


        ImageView mBtnBack = getActivity().findViewById(R.id.btn_back);
        mBtnBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                getActivity().finish();
                RealmUtil.deleteDiningRealm();
            }
        });

        mBtnOk.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                getActivity().finish();
                RealmUtil.deleteDiningRealm();
            }
        });

    }

    public void populateData() {
        mMsg.setText(confirmMessage);
        mCost.setText(RealmUtil.getDiningCurrency().concat(" ").concat(getTotalCost()));
        instructiondata.setText(mObj.getComments());
        LinearLayoutManager linearVertical = new LinearLayoutManager(getContext(), RecyclerView.VERTICAL, false);
        mRecyclerView.setLayoutManager(linearVertical);

        InRoomDiningBookingDetailsAdapter adapter = new InRoomDiningBookingDetailsAdapter(getContext(), mObj.getOrderLineItems());
        mRecyclerView.setAdapter(adapter);
    }

    @Override
    protected int getFragmentLayout() {
        return R.layout.fragment_irdining_booking_details;
    }

    public void setBookingObj(IRDiningBookingObj obj,String msg) {
        this.mObj = obj;
        confirmMessage=msg;
    }

    public String getTotalCost() {
        DecimalFormat format = new DecimalFormat();
        format.setDecimalSeparatorAlwaysShown(false);
        Double price = 0d;
        for (IRDiningOrderLineItemsObj obj : mObj.getOrderLineItems()) {
            price = price + Double.parseDouble(obj.getOrderedQuantity()) * Double.parseDouble(obj.getPrice());
            if (obj.getAddOns().size() > 0) {
                for (IRDiningAddonsObj addon : obj.getAddOns()) {
                    price = price + Double.parseDouble(addon.getPrice())* Double.parseDouble(obj.getOrderedQuantity());
                }
            }
        }
        return String.valueOf(format.format(price));
    }
}
