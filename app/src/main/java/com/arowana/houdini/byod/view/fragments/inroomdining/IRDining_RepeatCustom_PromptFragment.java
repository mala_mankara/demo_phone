package com.arowana.houdini.byod.view.fragments.inroomdining;

import android.graphics.Paint;
import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.arowana.houdini.byod.R;
import com.arowana.houdini.byod.model.inroomdining.IRDCartObj;
import com.arowana.houdini.byod.model.inroomdining.IRDiningAddonsObj;
import com.arowana.houdini.byod.model.inroomdining.IRDiningImagesObj;
import com.arowana.houdini.byod.model.inroomdining.IRDiningMenuItemsObj;
import com.arowana.houdini.byod.utils.RealmUtil;
import com.arowana.houdini.byod.view.adapters.inroomdining.IRD_SelectedAddOnListAdapter;
import com.arowana.houdini.byod.view.adapters.inroomdining.IRD_Selected_CustomizationListAdapter;
import com.arowana.houdini.byod.view.fragments.BaseFragment;
import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;

import java.util.Calendar;

import io.realm.Realm;

import static com.arowana.houdini.byod.view.activity.InRoomDiningActivity.fragmentHolder;

public class IRDining_RepeatCustom_PromptFragment extends BaseFragment {


    private IRDiningMenuItemsObj menuItemObj;
    private RecyclerView recyclerView, addOnsValuesView;
    private View mRootView;
    Realm realm;
    private  ImageView isVegNonveg;
    private TextView mDescription;
    private TextView mPrice;
    private TextView strikePrice;
    private TextView addonName;

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        mRootView = view;
        realm = RealmUtil.getInstance();
        // getActivity().findViewById(R.id.toolbar).setVisibility(View.GONE);
        setTitle(menuItemObj.getName());
        initComponents();
        initData();
    }

    public void initComponents() {
        ImageView menuImg = mRootView.findViewById(R.id.ird_repeatprompt_menu_img);
        isVegNonveg = mRootView.findViewById(R.id.ird_repeatprompt_is_veg);

        TextView title = mRootView.findViewById(R.id.ird_repeatprompt_menu_title);
        mDescription = mRootView.findViewById(R.id.ird_repeatprompt_menu_description);

        mPrice      = mRootView.findViewById(R.id.price);
        strikePrice = mRootView.findViewById(R.id.ir_dining_menu_price);

        addonName    = mRootView.findViewById(R.id.ird_prompt_addon_name);
        addOnsValuesView = mRootView.findViewById(R.id.ird_prompt_addonsvalue_recycler);

        recyclerView = mRootView.findViewById(R.id.ird_repeatprompt_recycler);

        TextView btnAddNew = mRootView.findViewById(R.id.btn_ird_repeatprompt_addnew);
        TextView btnRepeatLast = mRootView.findViewById(R.id.btn_ird_repeatprompt_repeatlast);
        ImageView closePopup = mRootView.findViewById(R.id.popup_close);

        btnAddNew.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                IRDiningCustomizationFragment fragment = new IRDiningCustomizationFragment();
                fragment.setMenuItemObj( RealmUtil.createTmpCartObj(menuItemObj), true);
                if (getFragmentManager() != null) {
                    getFragmentManager().popBackStack();
                }
                mHandler.showFragment(fragmentHolder.getId(), fragment);



            }
        });
        btnRepeatLast.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                realm.executeTransaction(new Realm.Transaction() {
                    @Override
                    public void execute(@NonNull Realm realm) {
                        cartObj.setItemCount(cartObj.getItemCount()+1);
                        cartObj.setTimeStamp(Calendar.getInstance().getTimeInMillis());
                        realm.copyToRealmOrUpdate(cartObj);
                        if (getFragmentManager() != null) {
                            getFragmentManager().popBackStack();

                        }

                    }
                });
            }
        });

        closePopup.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getFragmentManager().popBackStack();
            }
        });




        title.setText(menuItemObj.getName());
        if (menuItemObj.getImages().size() > 0) {
            IRDiningImagesObj img = menuItemObj.getImages().get(0);
            Glide.with(getContext()).load(img.getImageUrl()).apply(new RequestOptions().optionalFitCenter()).into(menuImg);
        }
        else menuImg.setVisibility(View.GONE);

    }

    public void initData() {
        isVegNonveg.setBackgroundResource(menuItemObj.getDietType().toLowerCase().equals("veg") ? R.drawable.veg_symbol : R.drawable.non_veg_symbol);
        mDescription.setText(menuItemObj.getDescription());

        if (menuItemObj.getDiscount().size() > 0) {
            strikePrice.setText(RealmUtil.getDiningCurrency().concat(" ").concat(menuItemObj.getPrice()));
            strikePrice.setPaintFlags(strikePrice.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);
            mPrice.setText(RealmUtil.getDiningCurrency().concat(" ").concat(menuItemObj.getDiscount().get(0).getDiscountPrice()));
        } else {
            strikePrice.setVisibility(View.GONE);
            mPrice.setText(RealmUtil.getDiningCurrency().concat(" ").concat(menuItemObj.getPrice()));
        }
        populateRecyclerView(cartObj);
    }
    public void populateRecyclerView(IRDCartObj cartObj) {
        boolean isAddon =false;
        for(IRDiningAddonsObj addOn: cartObj.getAddons())
            if(addOn.isSelected())
                isAddon=true;
        addonName.setVisibility(isAddon ? View.VISIBLE : View.GONE);

        addonName.setText("Addons  : ");
        LinearLayoutManager linearHorizontal = new LinearLayoutManager(getContext(), LinearLayoutManager.HORIZONTAL, false);
        addOnsValuesView.setLayoutManager(linearHorizontal);
        IRD_SelectedAddOnListAdapter addon_adapter = new IRD_SelectedAddOnListAdapter(cartObj.getAddons(), false);
        addOnsValuesView.setAdapter(addon_adapter);

        LinearLayoutManager linearVertical = new LinearLayoutManager(getContext(), RecyclerView.VERTICAL, false);
        recyclerView.setLayoutManager(linearVertical);
        IRD_Selected_CustomizationListAdapter adapter = new IRD_Selected_CustomizationListAdapter(cartObj.getCustomization());
        recyclerView.setAdapter(adapter);
    }

    @Override
    protected int getFragmentLayout() {
        return R.layout.fragment_ird_repeat_customprompt;
    }
    IRDCartObj cartObj;

    public void setMenuItemObj(IRDCartObj Obj, boolean b) {
        cartObj = Obj;
        this.menuItemObj = cartObj.getMenuItemObj();


    }


}
