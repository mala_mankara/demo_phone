package com.arowana.houdini.byod.view.adapters;

import android.app.Activity;
import android.content.Context;
import android.os.Handler;
import android.os.Looper;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.view.GravityCompat;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.recyclerview.widget.RecyclerView;

import com.arowana.houdini.byod.Consts;
import com.arowana.houdini.byod.MainApplication;
import com.arowana.houdini.byod.R;
import com.arowana.houdini.byod.interfaces.ApiClient;
import com.arowana.houdini.byod.interfaces.api_interface.DeviceActivationApiInterface;
import com.arowana.houdini.byod.interfaces.listeners.AppstateChangeListener;
import com.arowana.houdini.byod.model.deviceactivation_QR.QR_DeviceRegisterConfigObj;
import com.arowana.houdini.byod.model.home.HomeMenuObject;
import com.arowana.houdini.byod.utils.BasicApiResponse;
import com.arowana.houdini.byod.utils.JSONUtil;
import com.arowana.houdini.byod.utils.NavigationManager;
import com.arowana.houdini.byod.utils.RealmUtil;
import com.arowana.houdini.byod.utils.ViewUtils;
import com.arowana.houdini.byod.view.activity.HomeActivity;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;

import io.realm.RealmList;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class HamburgerMenuAdapter extends RecyclerView.Adapter<HamburgerMenuAdapter.MyViewHolder> {


    public Context context;
    private RealmList<HomeMenuObject> mGridObj;
    DrawerLayout mDrawerLayout;
    private AppstateChangeListener mListener;


    public class MyViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        public TextView title;
        public ImageView thumbnail;


        public MyViewHolder(View view) {
            super(view);
            title = view.findViewById(R.id.menu_title);
            thumbnail = view.findViewById(R.id.menu_icon);

            view.setOnClickListener(this);

        }

        public void checkoutGuest() {

            JsonObject jsonObject = new JsonObject();
            jsonObject.addProperty("RoomNumber", RealmUtil.getRoomNo());
            jsonObject.addProperty("MembershipId", RealmUtil.getMembershipId());
            jsonObject.addProperty("BookingId", RealmUtil.getBookingId());
            String message;
            Call<BasicApiResponse> responseObj = ApiClient.getApiClient(DeviceActivationApiInterface.class).checkoutGuest(jsonObject);

            responseObj.enqueue(new Callback<BasicApiResponse>() {
                @Override
                public void onResponse(@NonNull Call<BasicApiResponse> call, @NonNull Response<BasicApiResponse> response) {

                    BasicApiResponse qrResponse = response.body();
                    if (qrResponse != null && RealmUtil.responseIsSuccess(qrResponse)) {
                        final String message = qrResponse.getResponseStatus().getResponseMessage();

                        mListener.changeAppState(Consts.APP_LOGIN_STATE);


                        if (!(MainApplication.getCurrentActivity() instanceof HomeActivity))
                            MainApplication.getCurrentActivity().finish();
                        //if(RealmUtil.isPrimaryCheckin()) {
                        if(Consts.IS_PRIMARYUSER_CHECKIN){
                            NavigationManager.loadView("feedback", context, mListener);
                        }

                        RealmUtil.clearDataOnCheckout();
                        Handler handler = new Handler();
                        handler.postDelayed(new Runnable() {
                            @Override
                            public void run() {
                                ViewUtils.showSnackBarMessage(message);
                            }
                        }, 200);

                    } else {
                        final String message = RealmUtil.getResponseMessage(qrResponse);
                        ViewUtils.showSnackBarMessage(message != null ? message : "A server error occurred");
                    }
                }

                @Override
                public void onFailure(@Nullable Call<BasicApiResponse> call, @Nullable Throwable t) {
                    ViewUtils.showSnackBarMessage(t != null ? t.getMessage() : "Error in fetching data");
                }
            });
        }

        private void deactivateUser() {
            JsonObject request = new JsonObject();
            request.addProperty("RoomNo", RealmUtil.getRoomNo());
            request.addProperty("MembershipId", RealmUtil.getMembershipId());

            JsonObject additionalDevices = new JsonObject();
            QR_DeviceRegisterConfigObj results = RealmUtil.getInstance().where(QR_DeviceRegisterConfigObj.class).findFirst();
            if (results != null) {
                additionalDevices.addProperty("Id", results.getResponseData().getId());
            }
            additionalDevices.addProperty("MacId", Consts.DEVICE_MAC_ID);
            JsonArray jsonArray = new JsonArray();
            jsonArray.add(additionalDevices);

            request.add("UserAdditionalDevices", jsonArray);

            Call<BasicApiResponse> responseObj = ApiClient.getApiClient(DeviceActivationApiInterface.class).deactivateDevice(request);

            responseObj.enqueue(new Callback<BasicApiResponse>() {
                @Override
                public void onResponse(@NonNull Call<BasicApiResponse> call, @NonNull Response<BasicApiResponse> response) {

                    BasicApiResponse responseBody = response.body();

                    if (responseBody != null && RealmUtil.responseIsSuccess(responseBody)) {
                        mListener.changeAppState(Consts.APP_LOGIN_STATE);

                        RealmUtil.clearDataOnCheckout();
                        final String message = RealmUtil.getResponseMessage(responseBody);
                        ViewUtils.showSnackBarMessage(message);

                    } else {
                        final String message = RealmUtil.getResponseMessage(responseBody);
                        ViewUtils.showSnackBarMessage(message != null ? message : "A server error occurred");
                    }
                }

                @Override
                public void onFailure(@Nullable Call<BasicApiResponse> call, @Nullable Throwable t) {
                    ViewUtils.showSnackBarMessage(t != null ? t.getMessage() : "Error in fetching data");
                }
            });
        }



        @Override
        public void onClick(View view) {

            if (mGridObj.get(getAdapterPosition()).getKey().equalsIgnoreCase("check_out")) {


                if (mDrawerLayout.isDrawerOpen(GravityCompat.END))
                    mDrawerLayout.closeDrawer(GravityCompat.END);

                //Toast.makeText(mContext,""+message,Toast.LENGTH_SHORT).show();
                showCheckOutMessage("Initiate the checkout now ?");

              // checkoutGuest();
            } else if (mGridObj.get(getAdapterPosition()).getKey().equalsIgnoreCase("deactivate")) {

                if (mDrawerLayout.isDrawerOpen(GravityCompat.END))
                    mDrawerLayout.closeDrawer(GravityCompat.END);

                deactivateUser();
            } else {
                NavigationManager.loadView(mGridObj.get(getAdapterPosition()).getKey(), context, mListener);
            }

        }

        private void showCheckOutMessage(final String s) {


            Handler handler = new Handler(Looper.getMainLooper());
            handler.post(new Runnable() {
                @Override
                public void run() {
                    Activity activity = MainApplication.getCurrentActivity();
                    final FrameLayout layout = activity.findViewById(R.id.snacbar_layout);
                    layout.setVisibility(View.VISIBLE);
                    View view = activity.getLayoutInflater().inflate(R.layout.custom_confirm_snackbar_layout, null);
                    TextView message = view.findViewById(R.id.dineMessage);
                    TextView mOkMessage = view.findViewById(R.id.snack_ok_message);
                    TextView mCancelMessage = view.findViewById(R.id.snack_cancel_message);
                    message.setText(s);
                    mOkMessage.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            checkoutGuest();
                            layout.setVisibility(View.GONE);
                            layout.removeAllViews();

                        }
                    });

                    mCancelMessage.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            layout.setVisibility(View.GONE);
                            layout.removeAllViews();

                        }
                    });

                    layout.addView(view);
                }
            });
        }
    }






    public HamburgerMenuAdapter(Context mContext, RealmList<HomeMenuObject> obj, DrawerLayout drawerLayout, AppstateChangeListener listener) {
        this.context = mContext;
        this.mGridObj = obj;

        this.mDrawerLayout = drawerLayout;
        this.mListener = listener;
        //    this.mGridObj.addAll(results);
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_hamburger_menu, parent, false);


        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, int position) {
        HomeMenuObject obj = mGridObj.get(position);

        holder.title.setText(obj.getTitle());
        Log.d("menugenerator", "" + obj.getImage());
        if (obj.getImage() != null)
            holder.thumbnail.setImageDrawable(JSONUtil.GetDrawableByName(obj.getImage()));


    }


    @Override
    public int getItemCount() {
        return mGridObj.size();
    }


}
