package com.arowana.houdini.byod.model.inroomdining;

import io.realm.RealmList;
import io.realm.RealmObject;

public class IRDiningBookingObj extends RealmObject{

    private String BookingID;

    private IRDiningDeviceObj Device;

    private String ActivityId;

    private String IsResident;

    private String KotRefId;

    private String RoomNumber;

    private String DateTime;

    private IRDiningHotelPropertyObj HotelProperty;

    private RealmList<IRDiningOrderLineItemsObj> OrderLineItems;

    private String MessageCode;

    private String Version;

    private String Tenant;

    private String Comments;

    private String ModifiedBy;

    private String Response;

    private String IsNonChargeable;

    private String UserId;

    private String ModifiedDate;

    private String Status;

    private String CreatedBy;

    private String CreatedDate;

    private String Id;

    private String AppKey;

    private String OperationType;

    private String EmailCheck;

    private IRDiningGuestProfileObject GuestProfile;

    public String getBookingID() {
        return BookingID;
    }

    public void setBookingID(String bookingID) {
        BookingID = bookingID;
    }

    public IRDiningDeviceObj getDevice() {
        return Device;
    }

    public void setDevice(IRDiningDeviceObj device) {
        Device = device;
    }

    public String getActivityId() {
        return ActivityId;
    }

    public void setActivityId(String activityId) {
        ActivityId = activityId;
    }

    public String getIsResident() {
        return IsResident;
    }

    public void setIsResident(String isResident) {
        IsResident = isResident;
    }

    public String getKotRefId() {
        return KotRefId;
    }

    public void setKotRefId(String kotRefId) {
        KotRefId = kotRefId;
    }

    public String getRoomNumber() {
        return RoomNumber;
    }

    public void setRoomNumber(String roomNumber) {
        RoomNumber = roomNumber;
    }

    public String getDateTime() {
        return DateTime;
    }

    public void setDateTime(String dateTime) {
        DateTime = dateTime;
    }

    public IRDiningHotelPropertyObj getHotelProperty() {
        return HotelProperty;
    }

    public void setHotelProperty(IRDiningHotelPropertyObj hotelProperty) {
        HotelProperty = hotelProperty;
    }

    public RealmList<IRDiningOrderLineItemsObj> getOrderLineItems() {
        return OrderLineItems;
    }

    public void setOrderLineItems(RealmList<IRDiningOrderLineItemsObj> orderLineItems) {
        OrderLineItems = orderLineItems;
    }

    public String getMessageCode() {
        return MessageCode;
    }

    public void setMessageCode(String messageCode) {
        MessageCode = messageCode;
    }

    public String getVersion() {
        return Version;
    }

    public void setVersion(String version) {
        Version = version;
    }

    public String getTenant() {
        return Tenant;
    }

    public void setTenant(String tenant) {
        Tenant = tenant;
    }

    public String getComments() {
        return Comments;
    }

    public void setComments(String comments) {
        Comments = comments;
    }

    public String getModifiedBy() {
        return ModifiedBy;
    }

    public void setModifiedBy(String modifiedBy) {
        ModifiedBy = modifiedBy;
    }

    public String getResponse() {
        return Response;
    }

    public void setResponse(String response) {
        Response = response;
    }

    public String getIsNonChargeable() {
        return IsNonChargeable;
    }

    public void setIsNonChargeable(String isNonChargeable) {
        IsNonChargeable = isNonChargeable;
    }

    public String getUserId() {
        return UserId;
    }

    public void setUserId(String userId) {
        UserId = userId;
    }

    public String getModifiedDate() {
        return ModifiedDate;
    }

    public void setModifiedDate(String modifiedDate) {
        ModifiedDate = modifiedDate;
    }

    public String getStatus() {
        return Status;
    }

    public void setStatus(String status) {
        Status = status;
    }

    public String getCreatedBy() {
        return CreatedBy;
    }

    public void setCreatedBy(String createdBy) {
        CreatedBy = createdBy;
    }

    public String getCreatedDate() {
        return CreatedDate;
    }

    public void setCreatedDate(String createdDate) {
        CreatedDate = createdDate;
    }

    public String getId() {
        return Id;
    }

    public void setId(String id) {
        Id = id;
    }

    public String getAppKey() {
        return AppKey;
    }

    public void setAppKey(String appKey) {
        AppKey = appKey;
    }

    public String getOperationType() {
        return OperationType;
    }

    public void setOperationType(String operationType) {
        OperationType = operationType;
    }

    public String getEmailCheck() {
        return EmailCheck;
    }

    public void setEmailCheck(String emailCheck) {
        EmailCheck = emailCheck;
    }

    public IRDiningGuestProfileObject getGuestProfile() {
        return GuestProfile;
    }

    public void setGuestProfile(IRDiningGuestProfileObject guestProfile) {
        GuestProfile = guestProfile;
    }

    @Override
    public String toString()
    {
        return "ClassPojo [BookingID = "+BookingID+", Device = "+Device+", ActivityId = "+ActivityId+", IsResident = "+IsResident+", KotRefId = "+KotRefId+", RoomNumber = "+RoomNumber+", DateTime = "+DateTime+", HotelProperty = "+HotelProperty+", OrderLineItems = "+OrderLineItems+", MessageCode = "+MessageCode+", Version = "+Version+", Tenant = "+Tenant+", Comments = "+Comments+", ModifiedBy = "+ModifiedBy+", Response = "+Response+", IsNonChargeable = "+IsNonChargeable+", UserId = "+UserId+", ModifiedDate = "+ModifiedDate+", Status = "+Status+", CreatedBy = "+CreatedBy+", CreatedDate = "+CreatedDate+", Id = "+Id+", AppKey = "+AppKey+", OperationType = "+OperationType+", EmailCheck = "+EmailCheck+", GuestProfile = "+GuestProfile+"]";
    }

}
