package com.arowana.houdini.byod.model.inroomdining;

import io.realm.RealmObject;

public class IRDiningAddonsObj extends RealmObject {

    private String Description;

    private String AddOnName;

    private String Price;

    private String Id;

    private boolean isSelected;

    public String getDescription() {
        return Description;
    }

    public void setDescription(String description) {
        Description = description;
    }

    public String getAddOnName() {
        return AddOnName;
    }

    public void setAddOnName(String addOnName) {
        AddOnName = addOnName;
    }

    public String getPrice() {
        return Price;
    }

    public void setPrice(String price) {
        Price = price;
    }

    public String getId() {
        return Id;
    }

    public void setId(String id) {
        Id = id;
    }

    public boolean isSelected() {
        return isSelected;
    }

    public void setSelected(boolean selected) {
        isSelected = selected;
    }

    public IRDiningAddonsObj duplicateObj(){
       IRDiningAddonsObj  newObj= new IRDiningAddonsObj();
        newObj.Description=this.Description;
        newObj.AddOnName = this.AddOnName;
        newObj.Price = this.Price;
        newObj.Id = this.Id;
        newObj.isSelected = this.isSelected;


        return newObj;
    }
}
