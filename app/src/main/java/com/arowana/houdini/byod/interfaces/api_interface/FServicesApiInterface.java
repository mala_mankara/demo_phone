package com.arowana.houdini.byod.interfaces.api_interface;

import com.arowana.houdini.byod.model.facilitiesAndServices.FacilityServiceConfigObj;

import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.POST;

public interface FServicesApiInterface {
    @FormUrlEncoded
    @POST("Amenities/GetAll")
    Call<FacilityServiceConfigObj> getAllFacilities(@Field("ImageSize") String imageSize);
}
