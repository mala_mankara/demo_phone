package com.arowana.houdini.byod.view.activity;

import android.os.Bundle;
import android.util.Log;
import android.widget.FrameLayout;

import com.arowana.houdini.byod.MainApplication;
import com.arowana.houdini.byod.R;
import com.arowana.houdini.byod.view.fragments.telephone.PhoneCallFragment;
import com.arowana.houdini.byod.view.fragments.telephone.TelephoneFragment;

public class TelephoneActivity extends BaseActivity {

    public static final String TAG = "TelephoneActivity";

    public static FrameLayout fragmentHolder;
    Object value;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //setContentView(R.layout.activity_telephone);
        setContentView(R.layout.activity_module_layout);
        MainApplication.setCurrentActivity(this);
        initComponents();
        if (getIntent().getExtras() != null) {
            for (String key : getIntent().getExtras().keySet()) {
                value = getIntent().getExtras().get(key);
                Log.d(TAG, "Extras" +
                        " Key: " + key + " Value: " + value);

            }
        }

    }

    public void initComponents(){

        fragmentHolder=findViewById(R.id.fragment_holder);
        loadFragments();

    }

    public void loadFragments(){
        if(value != null){
            PhoneCallFragment phonecallFrag= new PhoneCallFragment();
            Bundle bundle = new Bundle();
//            bundle.putString("phoneNo", callNo);
//            bundle.putString("name", speedDialName.getText().toString());
            bundle.putBoolean("isInComingCall",true);
            phonecallFrag.setArguments(bundle);
            showFragment(fragmentHolder.getId(),new PhoneCallFragment());
        }else
            showFragment(fragmentHolder.getId(),new TelephoneFragment());

    }

}
