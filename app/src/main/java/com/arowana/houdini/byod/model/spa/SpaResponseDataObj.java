package com.arowana.houdini.byod.model.spa;

import io.realm.RealmList;
import io.realm.RealmObject;

public class SpaResponseDataObj extends RealmObject {

    private String Symbol;

    private RealmList<SpaObject> SpaInfo;

    private String Currency;

    private String HotelTimeZone;

    public String getHotelTimeZone ()
    {
        return HotelTimeZone;
    }

    public void setHotelTimeZone (String HotelTimeZone)
    {
        this.HotelTimeZone = HotelTimeZone;
    }

    public String getSymbol() {
        return Symbol;
    }

    public void setSymbol(String symbol) {
        Symbol = symbol;
    }

    public RealmList<SpaObject> getSpaInfo() {
        return SpaInfo;
    }

    public void setSpaInfo(RealmList<SpaObject> spaInfo) {
        SpaInfo = spaInfo;
    }

    public String getCurrency() {
        return Currency;
    }

    public void setCurrency(String currency) {
        Currency = currency;
    }
}
