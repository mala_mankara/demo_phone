package com.arowana.houdini.byod.model.inroomdining;

import io.realm.RealmList;
import io.realm.RealmObject;

public class IRDiningMenuItemsObj extends RealmObject {

    private String ItemCode;

    private boolean isSelected;

    private String Ingredients;

    private String CategoryCode;

    private String Description;

    private RealmList<IRDiningUpsellingItemObj> UpsellingItems;

    private RealmList<IRDiningImagesObj> Images;

    private String MenuCode;

    private String Name;

    private String Price;

    private String DietType;

    private RealmList<IRDiningCustomizationObj> Customization;

    private RealmList<IRDiningDiscountObj> Discount;

    private String Id;

    private RealmList<IRDiningAddonsObj> Addons;

    private int ItemCount;
    private boolean isUpsellingItem;

    public String getItemCode() {
        return ItemCode;
    }

    public void setItemCode(String itemCode) {
        ItemCode = itemCode;
    }

    public String getIngredients() {
        return Ingredients;
    }

    public void setIngredients(String ingredients) {
        Ingredients = ingredients;
    }

    public String getCategoryCode() {
        return CategoryCode;
    }

    public void setCategoryCode(String categoryCode) {
        CategoryCode = categoryCode;
    }

    public String getDescription() {
        return Description;
    }

    public void setDescription(String description) {
        Description = description;
    }

    public RealmList<IRDiningUpsellingItemObj> getUpsellingItems() {
        return UpsellingItems;
    }

    public void setUpsellingItems(RealmList<IRDiningUpsellingItemObj> upsellingItems) {
        UpsellingItems = upsellingItems;
    }

    public RealmList<IRDiningImagesObj> getImages() {
        return Images;
    }

    public void setImages(RealmList<IRDiningImagesObj> images) {
        Images = images;
    }

    public String getMenuCode() {
        return MenuCode;
    }

    public void setMenuCode(String menuCode) {
        MenuCode = menuCode;
    }

    public String getName() {
        return Name;
    }

    public void setName(String name) {
        Name = name;
    }

    public String getPrice() {
        return Price;
    }

    public void setPrice(String price) {
        Price = price;
    }

    public String getDietType() {
        return DietType;
    }

    public void setDietType(String dietType) {
        DietType = dietType;
    }

    public RealmList<IRDiningCustomizationObj> getCustomization() {
        return Customization;
    }

    public void setCustomization(RealmList<IRDiningCustomizationObj> customization) {
        Customization = customization;
    }

    public RealmList<IRDiningDiscountObj> getDiscount() {
        return Discount;
    }

    public void setDiscount(RealmList<IRDiningDiscountObj> discount) {
        Discount = discount;
    }

    public String getId() {
        return Id;
    }

    public void setId(String id) {
        Id = id;
    }

    public RealmList<IRDiningAddonsObj> getAddons() {
        return Addons;
    }

    public void setAddons(RealmList<IRDiningAddonsObj> addons) {
        Addons = addons;
    }

    public int getItemCount() {
        return ItemCount;
    }

    public void setItemCount(int itemCount) {
        ItemCount = itemCount;
    }

    public boolean isSelected() {
        return isSelected;
    }

    public void setSelected(boolean selected) {
        isSelected = selected;
    }

    public boolean isUpsellingItem() {
        return isUpsellingItem;
    }

    public void setUpsellingItem(boolean upsellingItem) {
        isUpsellingItem = upsellingItem;
    }
}
