package com.arowana.houdini.byod.model.palaceInformation;

import io.realm.RealmObject;

public class PalaceInfoImages extends RealmObject {

    private String ImageSize;

    private String ImageName;

    private String ImageUrl;

    private String ImageWidth;

    private String ImageHeight;

    public String getImageSize ()
    {
        return ImageSize;
    }

    public void setImageSize (String ImageSize)
    {
        this.ImageSize = ImageSize;
    }

    public String getImageName ()
    {
        return ImageName;
    }

    public void setImageName (String ImageName)
    {
        this.ImageName = ImageName;
    }

    public String getImageUrl ()
    {
        return ImageUrl;
    }

    public void setImageUrl (String ImageUrl)
    {
        this.ImageUrl = ImageUrl;
    }

    public String getImageWidth ()
    {
        return ImageWidth;
    }

    public void setImageWidth (String ImageWidth)
    {
        this.ImageWidth = ImageWidth;
    }

    public String getImageHeight ()
    {
        return ImageHeight;
    }

    public void setImageHeight (String ImageHeight)
    {
        this.ImageHeight = ImageHeight;
    }

    @Override
    public String toString()
    {
        return "ClassPojo [ImageSize = "+ImageSize+", ImageName = "+ImageName+", ImageUrl = "+ImageUrl+", ImageWidth = "+ImageWidth+", ImageHeight = "+ImageHeight+"]";
    }
}
