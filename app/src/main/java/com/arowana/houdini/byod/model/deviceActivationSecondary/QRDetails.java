package com.arowana.houdini.byod.model.deviceActivationSecondary;

import io.realm.RealmObject;

public class QRDetails extends RealmObject {
    private String PropertyId;
    private String MGSIP;
    private String MGSID;
    private String EncryptedData;

    public String getPropertyId() {
        return PropertyId;
    }

    public void setPropertyId(String propertyId) {
        PropertyId = propertyId;
    }

    public String getMGSIP() {
        return MGSIP;
    }

    public void setMGSIP(String MGSIP) {
        this.MGSIP = MGSIP;
    }

    public String getMGSID() {
        return MGSID;
    }

    public void setMGSID(String MGSID) {
        this.MGSID = MGSID;
    }

    public String getEncryptedData() {
        return EncryptedData;
    }

    public void setEncryptedData(String encryptedData) {
        EncryptedData = encryptedData;
    }
}
