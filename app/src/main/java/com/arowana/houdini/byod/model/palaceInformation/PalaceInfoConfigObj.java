package com.arowana.houdini.byod.model.palaceInformation;

import io.realm.RealmList;
import io.realm.RealmObject;

public class PalaceInfoConfigObj extends RealmObject {

    private String RequestData;

    private PalaceInfoResponseStatus ResponseStatus;

    private RealmList<PalaceInfoResponseData> ResponseData;

    public String getRequestData ()
    {
        return RequestData;
    }

    public void setRequestData (String RequestData)
    {
        this.RequestData = RequestData;
    }

    public PalaceInfoResponseStatus getResponseStatus ()
    {
        return ResponseStatus;
    }

    public void setResponseStatus (PalaceInfoResponseStatus ResponseStatus)
    {
        this.ResponseStatus = ResponseStatus;
    }

    public RealmList<PalaceInfoResponseData> getResponseData ()
    {
        return ResponseData;
    }

    public void setResponseData (RealmList<PalaceInfoResponseData> ResponseData)
    {
        this.ResponseData = ResponseData;
    }

    @Override
    public String toString()
    {
        return "ClassPojo [RequestData = "+RequestData+", ResponseStatus = "+ResponseStatus+", ResponseData = "+ResponseData+"]";
    }
}
