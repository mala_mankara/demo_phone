package com.arowana.houdini.byod.view.fragments.myprofile;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.Bundle;
import com.google.android.material.textfield.TextInputLayout;
import android.text.Html;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.TextView;

import com.arowana.houdini.byod.R;
import com.arowana.houdini.byod.utils.TextUtil;
import com.arowana.houdini.byod.utils.ViewUtils;


public class EditPassword implements View.OnClickListener {

    private  ProgressDialog mDialog;
    View mRootView;

    private TextInputLayout mOldPassword, mNewPassword, mRetypePassword;
    private TextView mConfirm;
    private Context mContext;
    private FrameLayout layout;
    private UpdateProfileDataListener mListener;


    public EditPassword(View mRootView, Context mContext, ProgressDialog mDialog, UpdateProfileDataListener listener) {
        this.mRootView = mRootView;
        this.mContext = mContext;
        this.mListener = listener;
        this.mDialog = mDialog;
        init();
    }

    private void init() {

        View view = LayoutInflater.from(mContext).inflate(R.layout.layout_edit_password, null);

        layout = mRootView.findViewById(R.id.inflaterView);
        layout.addView(view);

        mOldPassword = view.findViewById(R.id.old_password_txt);
        mNewPassword = view.findViewById(R.id.new_password_txt);
        mRetypePassword = view.findViewById(R.id.confirm_password_txt);

        mOldPassword.getEditText().setHint(TextUtils.concat(mContext.getString(R.string.old_password), Html.fromHtml(mContext.getString(R.string.required_asterisk))));
        mNewPassword.getEditText().setHint(TextUtils.concat(mContext.getString(R.string.new_password), Html.fromHtml(mContext.getString(R.string.required_asterisk))));
        mRetypePassword.getEditText().setHint(TextUtils.concat(mContext.getString(R.string.retype_password), Html.fromHtml(mContext.getString(R.string.required_asterisk))));

        mConfirm = view.findViewById(R.id.confirmButton);
        mConfirm.setOnClickListener(this);

    }

    @Override
    public void onClick(View v) {
        int id = v.getId();

        switch (id) {
            case R.id.confirmButton:

                ViewUtils.hideSoftKeyboard(v);
                validateField();
                break;
        }


    }

    private void validateField() {
        if(mDialog != null && mDialog.isShowing())
            mDialog.dismiss();
        if (!TextUtil.isEmpty(mOldPassword, "Enter valid password")) {
            if (TextUtil.validatePassword(mOldPassword, "Password too short")) {
                if (!TextUtil.isEmpty(mNewPassword, "Enter valid password")) {
                    if (TextUtil.validatePassword(mNewPassword, "Password too short")) {
                        if (!TextUtil.isEmpty(mRetypePassword, "Enter valid password")) {
                            if (TextUtil.validatePassword(mRetypePassword, "Password too short")) {

                                if (!mNewPassword.getEditText().getText().toString().equals(mRetypePassword.getEditText().getText().toString())) {
                                    mRetypePassword.setError("Password does not match");
                                    mRetypePassword.requestFocus();
                                    return;
                                }

                                if (mListener != null) {
                                    Bundle data = new Bundle();
                                    data.putString("oldPassword", mOldPassword.getEditText().getText().toString());
                                    data.putString("newPassword", mNewPassword.getEditText().getText().toString());
                                    mListener.onUpdateProfileData(data);
                                }


                            }
                        }
                    }
                }


            }

        }
    }

}
