package com.arowana.houdini.byod.view.activity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.FrameLayout;

import com.arowana.houdini.byod.MainApplication;
import com.arowana.houdini.byod.R;
import com.arowana.houdini.byod.view.fragments.login.LoginFragment;
import com.arowana.houdini.byod.view.fragments.login.SignupFragment;

public class LoginActivity extends BaseActivity {


    public static FrameLayout fragmentHolder;
    Bundle bundle;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_module_layout);
        MainApplication.setCurrentActivity(this);
        bundle = getIntent().getExtras();
        initComponents();

    }

    public void initComponents() {
        findViewById(R.id.navigation_toggle).setVisibility(View.GONE);

        fragmentHolder = findViewById(R.id.fragment_holder);
        loadFragments();

    }

    public void loadFragments() {
        if (bundle != null && bundle.getString("fragment") != null) {
            String type = bundle.getString("fragment");
            switch (type) {
                case "login":
                    showFragment(fragmentHolder.getId(), new LoginFragment());
                    break;

                case "signup":
                    showFragment(fragmentHolder.getId(), new SignupFragment());
                    break;
            }
        } else
            showFragment(fragmentHolder.getId(), new LoginFragment());

    }


    @Override
    protected void onDestroy() {
        super.onDestroy();
        findViewById(R.id.navigation_toggle).setVisibility(View.VISIBLE);
    }

    public void refreshUI() {
        //Code to refresh Activity
        Intent intent = getIntent();
        intent.putExtra("fragment","login");
        finish();
        startActivity(intent);

    }
}
