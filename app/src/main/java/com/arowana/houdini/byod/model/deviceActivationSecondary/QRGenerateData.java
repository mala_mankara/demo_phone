package com.arowana.houdini.byod.model.deviceActivationSecondary;

import io.realm.RealmObject;


/*"ResponseData": {
    "FirstName": "Rohit",
    "LastName": "Kumar",
    "EmailId": "rohit.kumar@gmail.com",
    "QRDetails": {
        "PropertyId": "5ab1119223d1621460512e90",
        "MGSIP": "xx",
        "MGSID": "977",
        "EncryptedData": "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAABHQAAAR0CAYAAAAQDEG2AAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAAJcEhZcwAADsMAAA7DAcdvqGQAAP+lSURBVHhe7P3Rku3KsSuJ6v9/WvdJ+5p16cgyFpwjQYa3Wb8lQcARyaqKPZfOv/7t/ycBCUhAAhKQgAQkIAEJSEACEpCABCTwKg"
    }
}*/

public class QRGenerateData extends RealmObject {
    private String FirstName;
    private String LastName;
    private String EmailId;

    private QRDetails QRDetails;

    public String getFirstName() {
        return FirstName;
    }

    public void setFirstName(String firstName) {
        FirstName = firstName;
    }

    public String getLastName() {
        return LastName;
    }

    public void setLastName(String lastName) {
        LastName = lastName;
    }

    public String getEmailId() {
        return EmailId;
    }

    public void setEmailId(String emailId) {
        EmailId = emailId;
    }

    public QRDetails getQRDetails() {
        return QRDetails;
    }

    public void setQRDetails(QRDetails QRDetails) {
        this.QRDetails = QRDetails;
    }
}
