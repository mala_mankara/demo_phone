package com.arowana.houdini.byod.utils;

import android.content.Context;
import android.os.Handler;
import android.text.Html;
import android.text.TextUtils;
import android.widget.AutoCompleteTextView;

import com.google.android.material.textfield.TextInputLayout;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class TextUtil {


    public static void setRequired(Context context, TextInputLayout layout) {

        layout.setHint(
                TextUtils.concat(layout.getHint(),
                        Html.fromHtml("<font color=\"#cc0029\">" + "* " + "</font>")));
    }

    public static boolean isEmpty(final TextInputLayout layout, String message) {

        if (layout.getEditText().getText().toString().trim().isEmpty()) {
            layout.setErrorEnabled(true);
            layout.setError(message);
            Handler handler = new Handler();
            handler.postDelayed(new Runnable() {
                @Override
                public void run() {
                    layout.setError(null);
                }
            }, 2000);
            return true;
        } else return false;

    }

    public static boolean isEmpty(TextInputLayout editText) {
        String value = editText.getEditText().getText().toString();
        return value.length() != 0;
    }

    public static boolean isEmpty(final AutoCompleteTextView tv, String message) {

        if (tv.getText().toString().trim().isEmpty()) {
            tv.setError(message);
            Handler handler = new Handler();
            handler.postDelayed(new Runnable() {
                @Override
                public void run() {
                    tv.setError(null);
                }
            }, 2000);
            return true;
        } else return false;

    }


    public static boolean validatePassword(final TextInputLayout layout, String Message) {

        String editPass = layout.getEditText().getText().toString();

        if (editPass.length() < 6) {
            layout.setError(Message);
            Handler handler = new Handler();
            handler.postDelayed(new Runnable() {
                @Override
                public void run() {
                    layout.setError(null);
                }
            }, 2000);
            layout.requestFocus();
            return false;
        } else if (editPass.length() > 15) {
            Handler handler = new Handler();
            handler.postDelayed(new Runnable() {
                @Override
                public void run() {
                    layout.setError(null);
                }
            }, 2000);
            layout.setError("Password too long");
            layout.requestFocus();
            return false;
        } else {
            layout.setError(null);
            return true;
        }
    }


    public static boolean isEmailValid(final TextInputLayout layout, String message) {
        String editEmail = layout.getEditText().getText().toString();
        Pattern pattern1 = Pattern.compile("^([a-zA-Z0-9_.-])+@([a-zA-Z0-9_.-])+\\.([a-zA-Z])+([a-zA-Z])+");
        Matcher matcher1 = pattern1.matcher(editEmail);
        if (!matcher1.matches()) {

            layout.setError(message);
            Handler handler = new Handler();
            handler.postDelayed(new Runnable() {
                @Override
                public void run() {
                    layout.setError(null);
                }
            }, 2000);
            layout.requestFocus();
            return false;
        } else {
            layout.setError(null);
            return true;
        }
    }


    public static boolean validatePhoneNo(final TextInputLayout editText, String ErrorMessage) {
        String value = editText.getEditText().getText().toString();
        boolean flag = (value.equalsIgnoreCase("0000000000") || (value.length() < 4) || (value.length() > 15));
        if (flag) {
            editText.setError(ErrorMessage);
            Handler handler = new Handler();
            handler.postDelayed(new Runnable() {
                @Override
                public void run() {
                    editText.setError(null);
                }
            }, 2000);
            editText.requestFocus();
            return false;
        }

        else{
            editText.setError(null);
            return true;
        }

    }

    public static boolean validatePincode(TextInputLayout editText, String ErrorMessage) {
        String value = editText.getEditText().getText().toString();
        boolean flag = !(value.equalsIgnoreCase("0000000000") || (value.length() < 6));
        editText.setFocusableInTouchMode(true);
        editText.setFocusable(true);
        editText.setError(flag ? null : ErrorMessage);
        editText.requestFocus();
        System.out.println("Flag : TYTYTYTYTYT : " + flag);
        return flag;
    }
}
