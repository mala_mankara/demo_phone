package com.arowana.houdini.byod.interfaces.api_interface;

import com.arowana.houdini.byod.model.servicerequest.ServiceLocationConfigObject;
import com.arowana.houdini.byod.model.servicerequest.ServiceRequestConfigObject;
import com.google.gson.JsonObject;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.POST;

public interface ServiceRequestApiInterface {

    @POST("RoomServices/GetServiceRequests")
    Call<ServiceRequestConfigObject> get_serviceRequest(@Body JsonObject body);

    @POST("ServiceRequest/GetAllServiceLocations")
    Call<ServiceLocationConfigObject> get_serviceLocations();

    @POST("ServiceRequest/AddServiceRequests")
    Call<ServiceRequestConfigObject> addedServiceRequest(@Body JsonObject body);


}
