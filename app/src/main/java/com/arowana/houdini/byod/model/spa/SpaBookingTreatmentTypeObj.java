package com.arowana.houdini.byod.model.spa;

import io.realm.RealmList;
import io.realm.RealmObject;

public class SpaBookingTreatmentTypeObj extends RealmObject {

    private String TreatmentTypeId;

    private String TreatmentType;

    private RealmList<SpaBookingTreatmentObj> Treatment;

    public String getTreatmentTypeId() {
        return TreatmentTypeId;
    }

    public void setTreatmentTypeId(String treatmentTypeId) {
        TreatmentTypeId = treatmentTypeId;
    }

    public String getTreatmentType() {
        return TreatmentType;
    }

    public void setTreatmentType(String treatmentType) {
        TreatmentType = treatmentType;
    }

    public RealmList<SpaBookingTreatmentObj> getTreatment() {
        return Treatment;
    }

    public void setTreatment(RealmList<SpaBookingTreatmentObj> treatment) {
        Treatment = treatment;
    }
}
