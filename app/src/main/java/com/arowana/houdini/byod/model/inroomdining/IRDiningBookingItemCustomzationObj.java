package com.arowana.houdini.byod.model.inroomdining;

import io.realm.RealmList;
import io.realm.RealmObject;

public class IRDiningBookingItemCustomzationObj extends RealmObject {

    private RealmList<IRBookingCustomType> CustomType;

    private String CustomizeItemId;

    private String CustomizeItemName;

    public RealmList<IRBookingCustomType> getCustomType ()
    {
        return CustomType;
    }

    public void setCustomType (RealmList<IRBookingCustomType> CustomType)
    {
        this.CustomType = CustomType;
    }

    public String getCustomizeItemId ()
    {
        return CustomizeItemId;
    }

    public void setCustomizeItemId (String CustomizeItemId)
    {
        this.CustomizeItemId = CustomizeItemId;
    }

    public String getCustomizeItemName ()
    {
        return CustomizeItemName;
    }

    public void setCustomizeItemName (String CustomizeItemName)
    {
        this.CustomizeItemName = CustomizeItemName;
    }

    @Override
    public String toString()
    {
        return "ClassPojo [CustomType = "+CustomType+", CustomizeItemId = "+CustomizeItemId+", CustomizeItemName = "+CustomizeItemName+"]";
    }
}
