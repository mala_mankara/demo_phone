package com.arowana.houdini.byod.model.palaceInformation;

import io.realm.RealmList;
import io.realm.RealmObject;

public class PalaceInfoHotels extends RealmObject {

    private String Description;

    private RealmList<String> Modules;

    private String Location;

    private RealmList<PalaceInfoImages> Images;

    private String BrandCode;

    private String Country;

    private String CheckInTime;

    private String City;

    private RealmList<PalaceInfoInformation> Information;

    private String Name;

    private String HotelCode;

    private String GroupCode;

    private String Address;

    private String Latitude;

    private String Longitude;

    private String Id;

    private String CountryCode;

    private String CheckOutTime;

    public String getDescription ()
    {
        return Description;
    }

    public void setDescription (String Description)
    {
        this.Description = Description;
    }

    public RealmList<String> getModules ()
    {
        return Modules;
    }

    public void setModules (RealmList<String> Modules)
    {
        this.Modules = Modules;
    }

    public String getLocation ()
    {
        return Location;
    }

    public void setLocation (String Location)
    {
        this.Location = Location;
    }

    public RealmList<PalaceInfoImages> getImages ()
    {
        return Images;
    }

    public void setImages (RealmList<PalaceInfoImages> Images)
    {
        this.Images = Images;
    }

    public String getBrandCode ()
    {
        return BrandCode;
    }

    public void setBrandCode (String BrandCode)
    {
        this.BrandCode = BrandCode;
    }

    public String getCountry ()
    {
        return Country;
    }

    public void setCountry (String Country)
    {
        this.Country = Country;
    }

    public String getCheckInTime ()
    {
        return CheckInTime;
    }

    public void setCheckInTime (String CheckInTime)
    {
        this.CheckInTime = CheckInTime;
    }

    public String getCity ()
    {
        return City;
    }

    public void setCity (String City)
    {
        this.City = City;
    }

    public RealmList<PalaceInfoInformation> getInformation ()
    {
        return Information;
    }

    public void setInformation (RealmList<PalaceInfoInformation> Information)
    {
        this.Information = Information;
    }

    public String getName ()
    {
        return Name;
    }

    public void setName (String Name)
    {
        this.Name = Name;
    }

    public String getHotelCode ()
    {
        return HotelCode;
    }

    public void setHotelCode (String HotelCode)
    {
        this.HotelCode = HotelCode;
    }

    public String getGroupCode ()
    {
        return GroupCode;
    }

    public void setGroupCode (String GroupCode)
    {
        this.GroupCode = GroupCode;
    }

    public String getAddress ()
    {
        return Address;
    }

    public void setAddress (String Address)
    {
        this.Address = Address;
    }

    public String getLatitude ()
    {
        return Latitude;
    }

    public void setLatitude (String Latitude)
    {
        this.Latitude = Latitude;
    }

    public String getLongitude ()
    {
        return Longitude;
    }

    public void setLongitude (String Longitude)
    {
        this.Longitude = Longitude;
    }

    public String getId ()
    {
        return Id;
    }

    public void setId (String Id)
    {
        this.Id = Id;
    }

    public String getCountryCode ()
    {
        return CountryCode;
    }

    public void setCountryCode (String CountryCode)
    {
        this.CountryCode = CountryCode;
    }

    public String getCheckOutTime ()
    {
        return CheckOutTime;
    }

    public void setCheckOutTime (String CheckOutTime)
    {
        this.CheckOutTime = CheckOutTime;
    }

    @Override
    public String toString()
    {
        return "ClassPojo [Description = "+Description+", Modules = "+Modules+", Location = "+Location+", Images = "+Images+", BrandCode = "+BrandCode+", Country = "+Country+", CheckInTime = "+CheckInTime+", City = "+City+", Information = "+Information+", Name = "+Name+", HotelCode = "+HotelCode+", GroupCode = "+GroupCode+", Address = "+Address+", Latitude = "+Latitude+", Longitude = "+Longitude+", Id = "+Id+", CountryCode = "+CountryCode+", CheckOutTime = "+CheckOutTime+"]";
    }
}
