package com.arowana.houdini.byod.model.inroomdining;

import io.realm.RealmObject;

public class IRDiningDeviceObj extends RealmObject {

    private String MacId;

    private String DeviceType;

    public String getMacId() {
        return MacId;
    }

    public void setMacId(String macId) {
        MacId = macId;
    }

    public String getDeviceType() {
        return DeviceType;
    }

    public void setDeviceType(String deviceType) {
        DeviceType = deviceType;
    }
}
