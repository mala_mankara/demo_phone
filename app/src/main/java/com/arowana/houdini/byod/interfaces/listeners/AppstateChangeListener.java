package com.arowana.houdini.byod.interfaces.listeners;

public interface AppstateChangeListener {

     void changeAppState(int state);
}
