package com.arowana.houdini.byod.model.redirct;

import io.realm.RealmList;
import io.realm.RealmObject;

public class RedirectResponseData extends RealmObject {

    private String Key;

    private String ActivityId;

    private String HotelId;

    private String ModifiedBy;

    private String BrandCode;

    private String GroupId;

    private String Module;

    private String Title;

    private String BrandId;

    private String ModifiedDate;

    private String HotelCode;

    private String GroupCode;

    private String Image;

    private String CreatedDate;

    private String CreatedBy;

    private String Id;

    private String MessageCode;

    private RealmList<RedirectSubModule> SubModule;

    private String Version;

    private String ModuleUrl;

    private String Tenant;

    public String getKey ()
    {
        return Key;
    }

    public void setKey (String Key)
    {
        this.Key = Key;
    }

    public String getActivityId ()
    {
        return ActivityId;
    }

    public void setActivityId (String ActivityId)
    {
        this.ActivityId = ActivityId;
    }

    public String getHotelId ()
    {
        return HotelId;
    }

    public void setHotelId (String HotelId)
    {
        this.HotelId = HotelId;
    }

    public String getModifiedBy ()
    {
        return ModifiedBy;
    }

    public void setModifiedBy (String ModifiedBy)
    {
        this.ModifiedBy = ModifiedBy;
    }

    public String getBrandCode ()
    {
        return BrandCode;
    }

    public void setBrandCode (String BrandCode)
    {
        this.BrandCode = BrandCode;
    }

    public String getGroupId ()
    {
        return GroupId;
    }

    public void setGroupId (String GroupId)
    {
        this.GroupId = GroupId;
    }

    public String getModule ()
    {
        return Module;
    }

    public void setModule (String Module)
    {
        this.Module = Module;
    }

    public String getTitle ()
    {
        return Title;
    }

    public void setTitle (String Title)
    {
        this.Title = Title;
    }

    public String getBrandId ()
    {
        return BrandId;
    }

    public void setBrandId (String BrandId)
    {
        this.BrandId = BrandId;
    }

    public String getModifiedDate ()
    {
        return ModifiedDate;
    }

    public void setModifiedDate (String ModifiedDate)
    {
        this.ModifiedDate = ModifiedDate;
    }

    public String getHotelCode ()
    {
        return HotelCode;
    }

    public void setHotelCode (String HotelCode)
    {
        this.HotelCode = HotelCode;
    }

    public String getGroupCode ()
    {
        return GroupCode;
    }

    public void setGroupCode (String GroupCode)
    {
        this.GroupCode = GroupCode;
    }

    public String getImage ()
    {
        return Image;
    }

    public void setImage (String Image)
    {
        this.Image = Image;
    }

    public String getCreatedDate ()
    {
        return CreatedDate;
    }

    public void setCreatedDate (String CreatedDate)
    {
        this.CreatedDate = CreatedDate;
    }

    public String getCreatedBy ()
    {
        return CreatedBy;
    }

    public void setCreatedBy (String CreatedBy)
    {
        this.CreatedBy = CreatedBy;
    }

    public String getId ()
    {
        return Id;
    }

    public void setId (String Id)
    {
        this.Id = Id;
    }

    public String getMessageCode ()
    {
        return MessageCode;
    }

    public void setMessageCode (String MessageCode)
    {
        this.MessageCode = MessageCode;
    }

    public RealmList<RedirectSubModule> getSubModule ()
    {
        return SubModule;
    }

    public void setSubModule (RealmList<RedirectSubModule> SubModule)
    {
        this.SubModule = SubModule;
    }

    public String getVersion ()
    {
        return Version;
    }

    public void setVersion (String Version)
    {
        this.Version = Version;
    }

    public String getModuleUrl ()
    {
        return ModuleUrl;
    }

    public void setModuleUrl (String ModuleUrl)
    {
        this.ModuleUrl = ModuleUrl;
    }

    public String getTenant ()
    {
        return Tenant;
    }

    public void setTenant (String Tenant)
    {
        this.Tenant = Tenant;
    }

    @Override
    public String toString()
    {
        return "ClassPojo [Key = "+Key+", ActivityId = "+ActivityId+", HotelId = "+HotelId+", ModifiedBy = "+ModifiedBy+", BrandCode = "+BrandCode+", GroupId = "+GroupId+", Module = "+Module+", Title = "+Title+", BrandId = "+BrandId+", ModifiedDate = "+ModifiedDate+", HotelCode = "+HotelCode+", GroupCode = "+GroupCode+", Images = "+Image+", CreatedDate = "+CreatedDate+", CreatedBy = "+CreatedBy+", Id = "+Id+", MessageCode = "+MessageCode+", RedirectSubModule = "+SubModule+", Version = "+Version+", ModuleUrl = "+ModuleUrl+", Tenant = "+Tenant+"]";
    }
}
