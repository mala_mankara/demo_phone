package com.arowana.houdini.byod.view.activity;

import android.os.Bundle;
import android.view.View;
import android.widget.FrameLayout;

import com.arowana.houdini.byod.MainApplication;
import com.arowana.houdini.byod.R;
import com.arowana.houdini.byod.view.fragments.DeviceActivationFragment;

public class DeviceActivationActivity extends BaseActivity {

    @Override
    protected void onDestroy() {
        super.onDestroy();
        findViewById(R.id.navigation_toggle).setVisibility(View.VISIBLE);
    }


    public static FrameLayout fragmentHolder;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        MainApplication.setCurrentActivity(this);
        setContentView(R.layout.activity_module_layout);

        initComponents();

    }

    public void initComponents(){
     //   findViewById(R.id.navigation_toggle).setVisibility(View.GONE);
        fragmentHolder=findViewById(R.id.fragment_holder);
        loadFragments();

    }

    public void loadFragments(){
        showFragment(fragmentHolder.getId(),new DeviceActivationFragment());

    }
}
