package com.arowana.houdini.byod.model.restaurant;

import io.realm.RealmObject;

public class RestaurantInformationObj extends RealmObject {

    private String Value;

    private String Field;

    private String Sequence;

    public String getValue() {
        return Value;
    }

    public void setValue(String value) {
        Value = value;
    }

    public String getField() {
        return Field;
    }

    public void setField(String field) {
        Field = field;
    }

    public String getSequence() {
        return Sequence;
    }

    public void setSequence(String sequence) {
        Sequence = sequence;
    }
}
