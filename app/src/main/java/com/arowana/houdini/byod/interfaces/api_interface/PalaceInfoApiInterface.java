package com.arowana.houdini.byod.interfaces.api_interface;

import com.arowana.houdini.byod.model.palaceInformation.PalaceInfoConfigObj;

import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.POST;

public interface PalaceInfoApiInterface {
    @FormUrlEncoded
    @POST("Hotel/GetAll")
    Call<PalaceInfoConfigObj> fetchAll(@Field("ImageSize") String imageSize);
}
