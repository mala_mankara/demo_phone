package com.arowana.houdini.byod.model.spa;

import io.realm.RealmList;
import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

public class SpaTreatmentObj extends RealmObject {

    private int FemalePaxCount;
    private int MalePaxCount;
    private int CoupleCount;
    private int Count;


    private String TreatmentTypeId;

    private String Description;

    @PrimaryKey
    private String TreatmentId;

    private String Time;

    private String Gender;

    private String Price;

    private String Disclaimer;

    private String CouplesTreatment;

    private String TreatmentName;

    private RealmList<SpaImagesObj> Images;



    private boolean isSelected;
    private String bookingTime;
    private String bookingDate;
    private boolean isSubmit;
    private boolean isDurationSelected;
    private SpaTimeAndPriceObj bookingTimeAndPrice;

    private RealmList<SpaTimeAndPriceObj> TimeAndPrice;
    private int spaSelectedDuartion =-1;

    public RealmList<SpaTimeAndPriceObj> getTimeAndPrice() {
        return TimeAndPrice;
    }

    public void setTimeAndPrice(RealmList<SpaTimeAndPriceObj> timeAndPrice) {
        TimeAndPrice = timeAndPrice;
    }

    public String getTreatmentTypeId() {
        return TreatmentTypeId;
    }

    public void setTreatmentTypeId(String treatmentTypeId) {
        TreatmentTypeId = treatmentTypeId;
    }

    public String getDescription() {
        return Description;
    }

    public void setDescription(String description) {
        Description = description;
    }

    public String getTreatmentId() {
        return TreatmentId;
    }

    public void setTreatmentId(String treatmentId) {
        TreatmentId = treatmentId;
    }

    public String getTime() {
        return Time;
    }

    public void setTime(String time) {
        Time = time;
    }

    public String getGender() {
        return Gender;
    }

    public void setGender(String gender) {
        Gender = gender;
    }

    public String getPrice() {
        return Price;
    }

    public void setPrice(String price) {
        Price = price;
    }

    public String getDisclaimer() {
        return Disclaimer;
    }

    public void setDisclaimer(String disclaimer) {
        Disclaimer = disclaimer;
    }

    public String getCouplesTreatment() {
        return CouplesTreatment;
    }

    public void setCouplesTreatment(String couplesTreatment) {
        CouplesTreatment = couplesTreatment;
    }

    public String getTreatmentName() {
        return TreatmentName;
    }

    public void setTreatmentName(String treatmentName) {
        TreatmentName = treatmentName;
    }



    public int getFemalePaxCount() {
        return FemalePaxCount;
    }

    public void setFemalePaxCount(int femalePaxCount) {
        FemalePaxCount = femalePaxCount;
    }

    public int getMalePaxCount() {
        return MalePaxCount;
    }

    public void setMalePaxCount(int malePaxCount) {
        MalePaxCount = malePaxCount;
    }

    public int getCoupleCount() {
        return CoupleCount;
    }

    public void setCoupleCount(int coupleCount) {
        CoupleCount = coupleCount;
    }

    public boolean isSelected() {
        return isSelected;
    }

    public void setSelected(boolean selected) {
        isSelected = selected;
    }

    public int getCount() {
        return Count;
    }

    public void setCount(int count) {
        Count = count;
    }

    public RealmList<SpaImagesObj> getImages() {
        return Images;
    }

    public void setImages(RealmList<SpaImagesObj> images) {
        Images = images;
    }

    public String getBookingTime() {
        return bookingTime;
    }

    public void setBookingTime(String bookingTime) {
        this.bookingTime = bookingTime;
    }

    public String getBookingDate() {
        return bookingDate;
    }

    public void setBookingDate(String bookingDate) {
        this.bookingDate = bookingDate;
    }

    public boolean isSubmit() {
        return isSubmit;
    }
    public void setSubmit(boolean submit) {
        isSubmit = submit;
    }

   public SpaTimeAndPriceObj getBookingTimeAndPrice() {
        return bookingTimeAndPrice;
    }

    public void setBookingTimeAndPrice(SpaTimeAndPriceObj bookingTimeAndPrice) {
        this.bookingTimeAndPrice = bookingTimeAndPrice;
    }

     public boolean isDurationSelected() {
        return isDurationSelected;
    }

    public void setDurationSelected(boolean durationSelected) {
        isDurationSelected = durationSelected;
    }

    public int getSpaSelectedDuartion() {
        return spaSelectedDuartion;
    }

    public void setSpaSelectedDuartion(int spaSelectedDuartion) {
        this.spaSelectedDuartion = spaSelectedDuartion;
    }
}
