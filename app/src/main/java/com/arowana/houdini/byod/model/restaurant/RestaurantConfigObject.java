package com.arowana.houdini.byod.model.restaurant;

import io.realm.RealmObject;

public class RestaurantConfigObject extends RealmObject {


    private RestaurantResponseStatusObj ResponseStatus;

    private RestaurantResponseDataObj ResponseData;

    public RestaurantResponseStatusObj getResponseStatus() {
        return ResponseStatus;
    }

    public void setResponseStatus(RestaurantResponseStatusObj responseStatus) {
        ResponseStatus = responseStatus;
    }

    public RestaurantResponseDataObj getResponseData() {
        return ResponseData;
    }

    public void setResponseData(RestaurantResponseDataObj responseData) {
        ResponseData = responseData;
    }
}
