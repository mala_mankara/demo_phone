package com.arowana.houdini.byod.view.fragments.inroomdining;


import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.TextView;

import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.arowana.houdini.byod.R;
import com.arowana.houdini.byod.model.inroomdining.IRDCartObj;
import com.arowana.houdini.byod.model.inroomdining.IRDiningMenuItemsObj;
import com.arowana.houdini.byod.utils.RealmUtil;
import com.arowana.houdini.byod.view.adapters.inroomdining.InRoomDiningAddonsAdapter;
import com.arowana.houdini.byod.view.adapters.inroomdining.InRoomDiningCustomizationAdapter;
import com.arowana.houdini.byod.view.fragments.BaseFragment;

import java.util.Calendar;
import java.util.Objects;

import io.realm.Realm;

import static android.content.Context.INPUT_METHOD_SERVICE;


public class IRDiningCustomizationFragment extends BaseFragment {


    boolean isTemporary = false;

    public IRDiningCustomizationFragment() {
        // Required empty public constructor
    }

    private View mRootView;
    private RecyclerView addonRecyclerview, customisationRecyclerview;
    private IRDCartObj cartObj;
    private IRDiningMenuItemsObj menuObj;
    private TextView item_name;

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        mRootView = view;
        //  getActivity().findViewById(R.id.toolbar).setVisibility(View.GONE);
        Log.d("IRD_CustomizationFrag","onViewCreated..");
        setTitle(getString(R.string.ir_dining_customized_title));
        initComponents();
        populateData();
    }

    public void initComponents() {
        addonRecyclerview = mRootView.findViewById(R.id.addon_recyclerview);
        customisationRecyclerview = mRootView.findViewById(R.id.customize_recyclerview);
        TextView txtAddon = mRootView.findViewById(R.id.txt_addon);
        txtAddon.setVisibility(menuObj.getAddons().size() > 0 ? View.VISIBLE : View.GONE);

        // new Changes.. Mala
        item_name                = mRootView.findViewById(R.id.ir_customise_menuItem);
        final TextView cancel = mRootView.findViewById(R.id.btn_ir_customized_cancel);
        TextView addCustomisationTocart = mRootView.findViewById(R.id.btn_ir_customized_add);
        if(buttonUpdateText != null) {
            addCustomisationTocart.setTag("UPDATE");
            addCustomisationTocart.setText(buttonUpdateText);
        }else {
            addCustomisationTocart.setTag("ADD");
            addCustomisationTocart.setText("ADD");
        }
        addCustomisationTocart.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(final View v) {
                Log.d("IRDCusFrag","clicked");
                Realm realmObj = Realm.getDefaultInstance();
                if (v.getTag() == "ADD") {
                    if (isTemporary) {
                        IRDCartObj existingCartObj = RealmUtil.isThisCartAlreadyExist(cartObj, cartObj.getMenuItemObj().getId());
                        if (existingCartObj != null) {
                            Log.d("IRDCusFrag", "isThisCartAlreadyExist");
                            realmObj.beginTransaction();
                            existingCartObj.setItemCount(existingCartObj.getItemCount() + 1);
                            realmObj.copyToRealmOrUpdate(existingCartObj);
                            existingCartObj.setTimeStamp(Calendar.getInstance().getTimeInMillis());
                            realmObj.commitTransaction();
                        } else {
                            int key = 0;
                            realmObj.beginTransaction();
                            try {
                                key = Objects.requireNonNull(realmObj.where(IRDCartObj.class).max("RealmId")).intValue() + 1;
                            } catch (NullPointerException ex) {
                                ex.printStackTrace();
                            }

                            cartObj.setRealmId(key);
                            cartObj.setTimeStamp(Calendar.getInstance().getTimeInMillis());
                            realmObj.copyToRealm(cartObj);
                            realmObj.commitTransaction();
                        }
                    } else {
                        IRDCartObj existingCartObj = RealmUtil.isThisCartAlreadyExist(cartObj, cartObj.getMenuItemObj().getId());
                        realmObj.beginTransaction();
                        if (existingCartObj != null) {
                            existingCartObj.setItemCount(existingCartObj.getItemCount() + 1);
                            existingCartObj.setTimeStamp(Calendar.getInstance().getTimeInMillis());
                            realmObj.copyToRealmOrUpdate(existingCartObj);
                        } else
                            realmObj.copyToRealmOrUpdate(cartObj);
                        realmObj.commitTransaction();
                    }
                } else if (v.getTag() == "UPDATE") {
                    Log.d("IRDCusFrag","UPDATE ");
                    IRDCartObj existingCartObj = RealmUtil.isThisCartAlreadyExist(cartObj, cartObj.getMenuItemObj().getId());
                    realmObj.beginTransaction();
                    if(existingCartObj != null && existingCartObj != cartObj && existingCartObj.getRealmId()!=cartObj.getRealmId()){
                        cartObj.setItemCount(existingCartObj.getItemCount() + cartObj.getItemCount());
                        cartObj.setTimeStamp(Calendar.getInstance().getTimeInMillis());
                        realmObj.copyToRealmOrUpdate(cartObj);
                        existingCartObj.deleteFromRealm();
                    }else {
                        cartObj.setTimeStamp(Calendar.getInstance().getTimeInMillis());
                        realmObj.copyToRealmOrUpdate(cartObj);
                    }
                    realmObj.commitTransaction();
                }

                if (getFragmentManager() != null) {
                    getFragmentManager().popBackStack();
                }
            }
        });

        cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (getFragmentManager() != null)
                    getFragmentManager().popBackStack();
                InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(INPUT_METHOD_SERVICE);
                imm.hideSoftInputFromWindow(item_name.getWindowToken(), 0);
            }
        });
    }

    public void populateData() {
        item_name.setText(menuObj.getName());
        LinearLayoutManager linearVertical = new LinearLayoutManager(getContext(), RecyclerView.VERTICAL, false);
        addonRecyclerview.setLayoutManager(linearVertical);
        InRoomDiningAddonsAdapter adapter = new InRoomDiningAddonsAdapter(cartObj.getAddons());
        addonRecyclerview.setAdapter(adapter);


        LinearLayoutManager linearVertical1 = new LinearLayoutManager(getContext(), RecyclerView.VERTICAL, false);
        customisationRecyclerview.setLayoutManager(linearVertical1);
        InRoomDiningCustomizationAdapter adapter1 = new InRoomDiningCustomizationAdapter(getContext(), cartObj.getCustomization());
        customisationRecyclerview.setAdapter(adapter1);

    }

    @Override
    protected int getFragmentLayout() {
        return R.layout.fragment_irdining_customization;
    }

    public void setMenuItemObj(IRDCartObj obj, boolean isTmp) {
        if (isTmp) {
            cartObj = obj;
        }
        else {
            Realm realm=RealmUtil.getInstance();
            cartObj = realm.copyFromRealm(obj);


        }
        isTemporary = isTmp;
        menuObj= obj.getMenuItemObj();
    }


    @Override
    public void onDestroy() {
        super.onDestroy();
        if(getActivity()!= null)
            getActivity().findViewById(R.id.toolbar).setVisibility(View.VISIBLE);
    }
    String buttonUpdateText ;

    public void setButtonUpdates(String buttonUpdates) {
        buttonUpdateText = buttonUpdates;
    }
}
