package com.arowana.houdini.byod.view.fragments;


import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import com.arowana.houdini.byod.R;
import com.arowana.houdini.byod.interfaces.listeners.AppstateChangeListener;
import com.arowana.houdini.byod.interfaces.listeners.FragmentNavigationHandler;
import com.arowana.houdini.byod.utils.RealmUtil;
import com.arowana.houdini.byod.view.activity.BaseActivity;
import com.google.android.material.appbar.AppBarLayout;


/**
 * A simple {@link Fragment} subclass.
 */
public abstract class BaseFragment extends Fragment {


    public BaseFragment() {

    }


    public FragmentNavigationHandler mHandler;
    public AppstateChangeListener mListener = null;
    public AppBarLayout mToolBar;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        return inflater.inflate(getFragmentLayout(), container, false);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
    }

    protected abstract int getFragmentLayout();


    public void setTitle(String str) {
        TextView title = getActivity().findViewById(R.id.fragment_title);
        try {
            title.setText(str);
        }catch (Exception ex){
            ex.printStackTrace();
        }

    }


    @Override
    public void onAttach(Context context) {

        super.onAttach(context);
        if (getActivity() instanceof AppstateChangeListener) {
            //   (getActivity()).hideActionBar(this instanceof LandingFragment || (this instanceof HomeActivity));
            mHandler=(FragmentNavigationHandler)getActivity();
            mListener = (AppstateChangeListener) getActivity();


        }
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();

    }

    @Override
    public void onResume() {
        super.onResume();
        if (RealmUtil.IsUserCheckedIn())
        ((BaseActivity)getActivity()).IsUserCheckedIn();

    }



}
