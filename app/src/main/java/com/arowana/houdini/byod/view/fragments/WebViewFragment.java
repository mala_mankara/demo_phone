package com.arowana.houdini.byod.view.fragments;

import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.webkit.WebView;

import com.arowana.houdini.byod.R;
import com.arowana.houdini.byod.utils.CustomWebViewClient;

public class WebViewFragment extends BaseFragment implements View.OnClickListener{

    private static final String TAG = "WebViewFragment";
    private String url;
    WebView webView;
    Context mContext;
    private View root;

    public WebViewFragment() { }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mContext = getActivity();


    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        root=view;
        if (getArguments() != null) {
            setTitle(getArguments().getString("title"));
            Log.d(TAG, "The web title is.............."+getArguments().getString("title"));
            url = getArguments().getString("url");
            Log.d(TAG, "The web url is.............."+url);
        }
        initComponents();
    }

    private void initComponents() {
       /* final ProgressDialog pDialog = new ProgressDialog(getContext());
        pDialog.setTitle(getContext().getString(R.string.app_name));
        pDialog.setMessage("Loading...");
        pDialog.setIndeterminate(false);
        pDialog.setCancelable(false);*/
        webView=root.findViewById(R.id.web_view);
        webView.getSettings().setJavaScriptEnabled(true);
        webView.getSettings().setBuiltInZoomControls(true);
        webView.getSettings().setLoadWithOverviewMode(true);
        webView.getSettings().setUseWideViewPort(true);
        webView.setWebViewClient(new CustomWebViewClient(getContext()));
       // pDialog.show();
        Uri webpage = Uri.parse(url);
        if (!url.startsWith("http://") && !url.startsWith("https://")) {
            webpage = Uri.parse("http://" + url);
        }
        webView.loadUrl(webpage.toString());


        /*final ProgressDialog pDialog = new ProgressDialog(getContext());
        pDialog.setTitle(getContext().getString(R.string.app_name));
        pDialog.setMessage("Loading...");
        pDialog.setIndeterminate(false);
        pDialog.setCancelable(false);

        webView=root.findViewById(R.id.web_view);
        webView.getSettings().setJavaScriptEnabled(true);
        webView.getSettings().setLoadWithOverviewMode(true);
        webView.getSettings().setUseWideViewPort(true);
        //---you need this to prevent the webview from
        // launching another browser when a url
        // redirection occurs---
       *//* webView.setWebViewClient(new Callback());
        String myPdfUrl = "https://sherlock-holm.es/stories/pdf/a4/1-sided/advs.pdf";
        webView.loadUrl("http://docs.google.com/viewer?embedded=true&url="+myPdfUrl);*//*
       // webView.loadUrl("http://drive.google.com/viewerng/viewer?embedded=true&url="+url1);
        //webView.loadUrl("https://docs.google.com/viewer?url="+url1+"&embedded=true");
       // webView.loadUrl("https://docs.google.com/gview?embedded=true&url="+url1);
        String finalUrl = "http://docs.google.com/viewer?embedded=true&url="+url1;


        pDialog.show();
        webView.loadUrl(finalUrl);
       // webView.setWebViewClient(new CustomWebViewClient(getContext()));
        webView.setWebViewClient(new WebViewClient() {

            @Override
            public void onPageFinished(WebView view, String url) {
                view.getSettings().setLoadsImagesAutomatically(true);
               pDialog.dismiss();
                Log.v("after load", view.getUrl());
            }

            @Override
            public void onPageStarted(WebView view, String url, Bitmap favicon) {

            }

            @Override
            public void onReceivedError(WebView view, int errorCode, String description, String failingUrl) {
                Toast.makeText(getApplicationContext(), description, Toast.LENGTH_SHORT).show();
                Log.e("error", description);

            }
        });



       *//* final ProgressDialog progressDialog = new ProgressDialog(getContext());
        progressDialog.setMessage("Loading Data...");
        progressDialog.setCancelable(false);
        webView=root.findViewById(R.id.web_view);
        webView.requestFocus();
        webView.getSettings().setJavaScriptEnabled(true);
        webView.getSettings().setLoadWithOverviewMode(true);
        webView.getSettings().setUseWideViewPort(true);
        String myPdfUrl = "https://app.emiratespalace.ae//Image/Menu/Menu_7036178.pdf";
        String url = "http://docs.google.com/viewer?embedded=true&url="+myPdfUrl;
        webView.loadUrl(url);
        webView.setWebViewClient(new WebViewClient() {
            @Override
            public boolean shouldOverrideUrlLoading(WebView view, String url) {
                view.loadUrl(url);
                return true;
            }
        });
        webView.setWebChromeClient(new WebChromeClient() {
            public void onProgressChanged(WebView view, int progress) {
                if (progress < 100) {
                    progressDialog.show();
                }
                if (progress == 100) {
                    progressDialog.dismiss();
                }
            }
        });*//*
*/
    }

    /*private class Callback extends WebViewClient {
        @Override
        public boolean shouldOverrideUrlLoading(
                WebView view, String url) {
            return (false);
        }
    }*/

    @Override
    protected int getFragmentLayout() {
        return  R.layout.flight_webfragment;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        Log.d(TAG,"onAttach");
    }

    @Override
    public void onDetach() {
        super.onDetach();
        Log.d(TAG,"onDetach");
    }

    @Override
    public void onClick(View v) {

    }
}
