package com.arowana.houdini.byod.model.inroomdining;

import io.realm.RealmList;
import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

public class IRDCartObj extends RealmObject {

    @PrimaryKey
    private int RealmId ;

    IRDiningMenuItemsObj MenuItemObj;
    private RealmList<IRDiningAddonsObj> Addons;
    private RealmList<IRDiningCustomizationObj> Customization;

    private IRDCartObj lastCartObj;
    private int ItemCount;
    private long timeStamp;

    public IRDiningMenuItemsObj getMenuItemObj() {
        return MenuItemObj;
    }

    public void setMenuItemObj(IRDiningMenuItemsObj menuItemObj) {
        MenuItemObj = menuItemObj;
    }

    public RealmList<IRDiningAddonsObj> getAddons() {
        return Addons;
    }

    public void setAddons(RealmList<IRDiningAddonsObj> addons) {
        Addons = addons;
    }

    public RealmList<IRDiningCustomizationObj> getCustomization() {
        return Customization;
    }

    public void setCustomization(RealmList<IRDiningCustomizationObj> customization) {
        Customization = customization;
    }

    public int getItemCount() {
        return ItemCount;
    }

    public void setItemCount(int itemCount) {
        ItemCount = itemCount;
    }

    public IRDCartObj getLastCartObj() {
        return lastCartObj;
    }

    public void setLastCartObj(IRDCartObj lastCartObj) {
        this.lastCartObj = lastCartObj;
    }

    public int getRealmId() {
        return RealmId;
    }

    public void setRealmId(int realmId) {
        RealmId = realmId;
    }

    public long getTimeStamp() {
        return timeStamp;
    }

    public void setTimeStamp(long timeStamp) {
        this.timeStamp = timeStamp;
    }
}
