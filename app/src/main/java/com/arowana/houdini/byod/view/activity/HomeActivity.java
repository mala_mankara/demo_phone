package com.arowana.houdini.byod.view.activity;


import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.Gravity;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.alcatel.servpro.byod.api.ISqualeService;
import com.arowana.houdini.byod.Consts;
import com.arowana.houdini.byod.MainApplication;
import com.arowana.houdini.byod.R;
import com.arowana.houdini.byod.firebase.NotificationManager;
import com.arowana.houdini.byod.interfaces.listeners.AppstateChangeListener;
import com.arowana.houdini.byod.model.home.HomeGridViewObject;
import com.arowana.houdini.byod.model.login.LoginConfigObject;
import com.arowana.houdini.byod.model.sharedpreference.SharedPreferenceUtil;
import com.arowana.houdini.byod.model.sharedpreference.StoreKeys;
import com.arowana.houdini.byod.model.speeddial.SpeedDialConfigObj;
import com.arowana.houdini.byod.model.speeddial.SpeedDialResponseData;
import com.arowana.houdini.byod.utils.DisplayUtils;
import com.arowana.houdini.byod.utils.ItemOffsetDecoration;
import com.arowana.houdini.byod.utils.JSONUtil;
import com.arowana.houdini.byod.utils.RealmUtil;
import com.arowana.houdini.byod.utils.ViewUtils;
import com.arowana.houdini.byod.view.adapters.HomeGridAdapter;

import org.json.JSONArray;
import org.json.JSONObject;

import alcatel.model.ByodManager;
import alcatel.services.AlcatelService;
import io.realm.Case;
import io.realm.Realm;
import io.realm.RealmChangeListener;
import io.realm.RealmList;
import io.realm.RealmResults;

public class HomeActivity extends BaseActivity implements View.OnTouchListener, AppstateChangeListener, View.OnClickListener, RealmChangeListener<LoginConfigObject> {


    public HomeActivity() {
        // Required empty public constructor
    }

    public static final String TAG = "HomeActivity";
    private View mRootView;
    private RecyclerView mGridLayout;
    private ImageView mBtnScroll;
    private ImageView mBtnMenu;
    private TextView mBtnLogin, mUserName;
    private DrawerLayout mDrawerLayout;
    private Bundle bundle;
    private RelativeLayout homelayout;

    private ScrollView mScrollview;
    private LinearLayout dummyView;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        setTheme(R.style.AppTheme);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);
        MainApplication.setCurrentActivity(this);
        ByodManager.getInstance().setContext(this.getApplicationContext());
        initComponents();
        initListeners();
        MainApplication.fetchRedirectData();
        if (RealmUtil.IsUserCheckedIn())
            changeAppState(Consts.APP_CHECKIN_STATE);
        else if (RealmUtil.IsUserLoggedin())
            changeAppState(Consts.APP_LOGIN_STATE);
        else if (Consts.APP_CURRENT_STATE > 0) {
            changeAppState(Consts.APP_DEFAULT__STATE);
        }
        bundle = getIntent().getExtras();
        if (Consts.APP_CURRENT_STATE == -1) {
            Intent intent = new Intent(this, LandingActivity.class);
            startActivity(intent);
        }

        if (bundle != null && bundle.getString("login_message") != null) {
            Handler handler = new Handler();
            handler.postDelayed(new Runnable() {
                @Override
                public void run() {
                    ViewUtils.showSnackBarMessage(bundle.getString("login_message"));
                }
            }, 1000);
        }

        if (getIntent().getExtras() != null) {
            for (String key : getIntent().getExtras().keySet()) {
                Object value = getIntent().getExtras().get(key);
                Log.d(TAG, "Extras" +
                        " Key: " + key + " Value: " + value);
            }
        }
        // initNotification();

    }


    Intent intent;

    public void registerDeviceToAlcatel() {
        //*****************
        Log.d(TAG, "registerDeviceToAlcatel get sip Profile Data");
        Handler mainHandler = new Handler(Looper.getMainLooper());
        Runnable myRunnable = new Runnable() {
            @Override
            public void run() {
                // progressDialog.show();
                ByodManager.getInstance().setContext(HomeActivity.this.getApplicationContext());
                if (!ByodManager.getInstance().isSipConnected()) {
                    ByodManager.getInstance().init();
                    //Start service:
                    intent = new Intent(HomeActivity.this, AlcatelService.class);
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                        startForegroundService(intent);
                    } else {
                        //startService(new Intent(this, YourService.class));
                        HomeActivity.this.getApplicationContext().startService(intent);
                    }
                } else {
                }
                ISqualeService squaleService = ByodManager.getInstance().getSqualeService();
                if (squaleService != null)
                    squaleService.registerUser();
                Log.d(TAG, "registerDeviceToAlcatel registerUser........." + ByodManager.getInstance().isRegistered);
            }
        };
        mainHandler.post(myRunnable);

    }

    Realm mRealm;
    RealmList<SpeedDialResponseData> responseDataList;

    private void fetchSpeedDialModuleList() {
        if (MainApplication.getSpeedDialKeyValueList().isEmpty()) {
            mRealm = RealmUtil.getInstance();
            if (mRealm.where(SpeedDialConfigObj.class).count() != 0) {
                responseDataList = mRealm.where(SpeedDialConfigObj.class).findFirst().getResponseData();
                for (int i = 0; i < responseDataList.size(); i++) {
                    SpeedDialResponseData data = responseDataList.get(i);
                    MainApplication.setSpeedDialKeyValueList(data.getName().toLowerCase(), data.getNumber());
                }
            }
        }

    }


    public void updateGrid() {
        populateHamburgerMenu();
        String currState = String.valueOf(Consts.APP_CURRENT_STATE);
        String response = null;
        if (Consts.APP_CURRENT_STATE < Consts.APP_LOGIN_STATE)
            response = JSONUtil.ReadFromfile(this, "json/home_tiles.json");
        else if(Consts.APP_CURRENT_STATE == Consts.APP_LOGIN_STATE)
            response = JSONUtil.ReadFromfile(this, "json/home_tiles_afterLogin.json");
        else if(Consts.APP_CURRENT_STATE == Consts.APP_CHECKIN_STATE)
            response = JSONUtil.ReadFromfile(this, "json/home_tiles_aftercheckin.json");
        try {
            JSONObject obj = new JSONObject(response);
            final JSONArray array = obj.getJSONArray("modules");
            Realm realm = RealmUtil.getInstance();
            realm.executeTransaction(new Realm.Transaction() {
                @Override
                public void execute(Realm realm) {
                    realm.delete(HomeGridViewObject.class);
                    realm.createAllFromJson(HomeGridViewObject.class, array);
                }
            });


            RealmResults<HomeGridViewObject> results = realm.where(HomeGridViewObject.class).contains("appState", currState, Case.INSENSITIVE)
                    .findAll();

            RealmList<HomeGridViewObject> mList = new RealmList<>();


            RecyclerView.LayoutManager mLayoutManager = new GridLayoutManager(this, 2);

            mGridLayout.setLayoutManager(mLayoutManager);
            ItemOffsetDecoration itemDecoration = new ItemOffsetDecoration(this, R.dimen.home_grid_spacing);
            if(mGridLayout.getItemDecorationCount()<1)
                mGridLayout.addItemDecoration(itemDecoration);
            mGridLayout.setItemAnimator(new DefaultItemAnimator());
            mList.clear();
            mList.addAll(results);
            HomeGridAdapter adapter = new HomeGridAdapter(this, mList, currState);
            mGridLayout.setAdapter(adapter);
            adapter.notifyDataSetChanged();


            mGridLayout.setNestedScrollingEnabled(false);
           // Log.d("getHeight", "height is:     !!!!!!!!!!!!!!!  " + mScrollview.getHeight());

            DisplayMetrics displayMetrics = new DisplayMetrics();
            getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
            Double height = displayMetrics.heightPixels * 0.30;
            DisplayUtils.getInstance(this).dp2px(height.intValue());


            int width = displayMetrics.widthPixels;


            dummyView.setLayoutParams(new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, Integer.valueOf(height.intValue())));

            mScrollview.getViewTreeObserver().addOnScrollChangedListener(new ViewTreeObserver.OnScrollChangedListener() {
                @Override
                public void onScrollChanged() {
                    float scrollY = mScrollview.getScrollY();
                    mBtnScroll.setAlpha(100 - scrollY);

                }
            });
            if (Integer.parseInt(currState) > 0) {
                mBtnLogin.setVisibility(View.GONE);
                mUserName.setVisibility(View.VISIBLE);
                mUserName.setText("Hi " + RealmUtil.getUserName());
                mBtnMenu.setVisibility(View.VISIBLE);
                mDrawerLayout.setEnabled(true);

            } else {
                mBtnLogin.setVisibility(View.VISIBLE);
                mUserName.setVisibility(View.GONE);
                mBtnMenu.setVisibility(View.GONE);
                mDrawerLayout.setEnabled(false);
            }


        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    public void initComponents() {

        homelayout = findViewById(R.id.home_layout);
        mGridLayout = findViewById(R.id.home_grid_layout);
        FrameLayout mediaView = findViewById(R.id.videoView);
        mBtnScroll = findViewById(R.id.home_btn_scroll);
        mBtnMenu = findViewById(R.id.btn_home_menu);
        mDrawerLayout = findViewById(R.id.drawer_layout);

        mBtnLogin = findViewById(R.id.btn_login_home);
        mUserName = findViewById(R.id.home_username);
        mScrollview = findViewById(R.id.scrollView);
        dummyView = findViewById(R.id.dummyView);

    }

    private String imageUrl = null;

    public void initListeners() {
        mBtnScroll.setOnClickListener(this);
        mBtnMenu.setOnClickListener(this);
        mBtnLogin.setOnClickListener(this);
       // homelayout.setOnTouchListener(this);
    }

    @Override
    public void onClick(View view) {
        int id = view.getId();
        switch (id) {

            case R.id.btn_login_home:
                mHandler = this;
                mListener = this;
                Intent intent = new Intent(this, LoginActivity.class);
                startActivity(intent);
                break;

            case R.id.home_btn_scroll:
                mScrollview.fullScroll(View.FOCUS_DOWN);
                break;

            case R.id.btn_home_menu:

                if (!mDrawerLayout.isDrawerOpen(Gravity.RIGHT))
                    mDrawerLayout.openDrawer(Gravity.RIGHT);
                else
                    mDrawerLayout.closeDrawer(Gravity.RIGHT);
                break;
        }
    }

    @Override
    public void changeAppState(int state) {

        SharedPreferenceUtil.putIntegerIntoStore(StoreKeys.APPLICATION_STATE, state);
        Consts.APP_CURRENT_STATE = state;
//        Log.d("Homeactivity", "APP_CURRENT_STATE>>>>" + Consts.APP_CURRENT_STATE);
//        Log.d("Homeactivity", "details are>>>>" + RealmUtil.getFullName());
        updateGrid();
    }

    @Override
    public void onResume() {
        super.onResume();
        MainApplication.setCurrentActivity(this);
        //     initNotification();
        this.hideActionBar(true);
        updateGrid();
        populateHamburgerMenu();
        if (RealmUtil.IsUserLoggedin()) {
            IsUserCheckedIn();
        }
        if (Consts.APP_CURRENT_STATE > 1 || RealmUtil.IsUserCheckedIn()) {

            if (!ByodManager.getInstance().isSipConnected()) {
                fetchSpeedDialModuleList();
                registerDeviceToAlcatel();
            }
        }
    }

    @Override
    protected void onPause() {
        super.onPause();
        this.hideActionBar(false);
    }

    @Override
    public void onBackPressed() {
        finish();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        NotificationManager.getInstance(this).unRegisterNotificationBroadCastReceiver();
        //RealmUtil.getInstance().close();
    }

    @Override
    public void onChange(LoginConfigObject o) {
        ViewUtils.showSnackBarMessage(o.getResponseStatus().getResponseMessage());
    }

    @Override
    protected void onStop() {
        super.onStop();
        // mGridLayout.removeItemDecoration(itemDecoration);
    }

    /*..Toggle between DemoAPI and Production API...*/
    private int count = 0;
    private long startMillis=0;
    private boolean isToggledDone; ;

    @Override
    public boolean onTouch(View v, MotionEvent event) {


        int eventaction = event.getAction();
        Log.d(TAG, "on touch.."+eventaction);
       // Toast.makeText(this,"touched count.."+eventaction+"  /count....."+count,Toast.LENGTH_SHORT).show();


        if (eventaction == MotionEvent.ACTION_DOWN) {
            //get system current milliseconds
            long time= System.currentTimeMillis();
            //if it is the first time, or if it has been more than 3
            // seconds since the first tap ( so it is like a new try), we reset everything
            if (startMillis==0 || (time-startMillis> 3000) ) {
                startMillis=time;
                count=1;
            }
            //it is not the first, and it has been  less than 3 seconds since the first
            else{ //  time-startMillis< 3000
                count++;
            }
          //  Toast.makeText(this,"touched count.."+count,Toast.LENGTH_SHORT).show();
            if(count==5 && ! isToggledDone) {
                //ServerConfig.DOMAIN_URL2 = ServerConfig.RELEASE_URL;

            }else {
                //ServerConfig.DOMAIN_URL = ServerConfig.STAGING_URL;
                isToggledDone=false;
            }
            return true;
        }
        return false;
    }

}
