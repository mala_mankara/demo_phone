package com.arowana.houdini.byod.model.login;

public class ForgotPasswordResponseObject {

    private ResponseStatus ResponseStatus;

    private ResponseData ResponseData;

    public ResponseStatus getResponseStatus() {
        return ResponseStatus;
    }

    public void setResponseStatus(ResponseStatus responseStatus) {
        ResponseStatus = responseStatus;
    }

    public ResponseData getResponseData() {
        return ResponseData;
    }

    public void setResponseData(ResponseData responseData) {
        ResponseData = responseData;
    }


    public class ResponseStatus {
        private String ResponseFlag;

        private String ResponseId;

        private String ResponseCode;

        private String ResponseMessage;

        public String getResponseFlag() {
            return ResponseFlag;
        }

        public void setResponseFlag(String ResponseFlag) {
            this.ResponseFlag = ResponseFlag;
        }

        public String getResponseId() {
            return ResponseId;
        }

        public void setResponseId(String ResponseId) {
            this.ResponseId = ResponseId;
        }

        public String getResponseCode() {
            return ResponseCode;
        }

        public void setResponseCode(String ResponseCode) {
            this.ResponseCode = ResponseCode;
        }

        public String getResponseMessage() {
            return ResponseMessage;
        }

        public void setResponseMessage(String ResponseMessage) {
            this.ResponseMessage = ResponseMessage;
        }
    }


    public class ResponseData {
        private String DeviceId;

        private String ConfirmationCode;

        private String IsLogin;

        private String UserId;

        private String EmailId;

        private String MobileNumber;

        public String getDeviceId() {
            return DeviceId;
        }

        public void setDeviceId(String DeviceId) {
            this.DeviceId = DeviceId;
        }

        public String getConfirmationCode() {
            return ConfirmationCode;
        }

        public void setConfirmationCode(String ConfirmationCode) {
            this.ConfirmationCode = ConfirmationCode;
        }

        public String getIsLogin() {
            return IsLogin;
        }

        public void setIsLogin(String IsLogin) {
            this.IsLogin = IsLogin;
        }

        public String getUserId() {
            return UserId;
        }

        public void setUserId(String UserId) {
            this.UserId = UserId;
        }

        public String getEmailId() {
            return EmailId;
        }

        public void setEmailId(String EmailId) {
            this.EmailId = EmailId;
        }

        public String getMobileNumber() {
            return MobileNumber;
        }

        public void setMobileNumber(String MobileNumber) {
            this.MobileNumber = MobileNumber;
        }


    }
}
