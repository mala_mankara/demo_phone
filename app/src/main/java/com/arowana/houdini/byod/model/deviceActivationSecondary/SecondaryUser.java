package com.arowana.houdini.byod.model.deviceActivationSecondary;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

public class SecondaryUser extends RealmObject {

    @PrimaryKey
    private String Id;
    private String FirstName;
    private String LastName;
    private String MembershipId;
    private String MacId;
    private Boolean IsDeleted;

    public String getId() {
        return Id;
    }

    public void setId(String id) {
        Id = id;
    }

    public String getFirstName() {
        return FirstName;
    }

    public void setFirstName(String firstName) {
        FirstName = firstName;
    }

    public String getLastName() {
        return LastName;
    }

    public void setLastName(String lastName) {
        LastName = lastName;
    }

    public String getMembershipId() {
        return MembershipId;
    }

    public void setMembershipId(String membershipId) {
        MembershipId = membershipId;
    }

    public String getMacId() {
        return MacId;
    }

    public void setMacId(String macId) {
        MacId = macId;
    }

    public Boolean getDeleted() {
        return IsDeleted;
    }

    public void setDeleted(Boolean deleted) {
        IsDeleted = deleted;
    }
}
