package com.arowana.houdini.byod.view.adapters.inroomdining;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.arowana.houdini.byod.R;
import com.arowana.houdini.byod.model.inroomdining.IRDiningAddonsObj;
import com.arowana.houdini.byod.utils.RealmUtil;

import io.realm.RealmList;

public class InRoomDiningAddonsAdapter extends RecyclerView.Adapter<InRoomDiningAddonsAdapter.MyViewHolder> {


    private RealmList<IRDiningAddonsObj> addonObj;


    public class MyViewHolder extends RecyclerView.ViewHolder {
        private TextView price;
        private CheckBox checkBox;


        public MyViewHolder(View view) {
            super(view);
            checkBox = view.findViewById(R.id.addon_checkbox);
            price = view.findViewById(R.id.addon_price);

        }
    }


    public InRoomDiningAddonsAdapter(RealmList<IRDiningAddonsObj> obj) {
        this.addonObj = obj;
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_custom_checkbox, parent, false);
        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull final MyViewHolder holder, final int position) {
        final IRDiningAddonsObj bean = addonObj.get(position);

        assert bean != null;
        holder.checkBox.setText(bean.getAddOnName());
        holder.checkBox.setChecked(bean.isSelected());
        holder.price.setText(RealmUtil.getDiningCurrency().concat(" ").concat(bean.getPrice()));

        holder.checkBox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {

            @Override
            public void onCheckedChanged(final CompoundButton compoundButton,final boolean b) {
                if(compoundButton.isChecked()) {
                    compoundButton.setChecked(true);
                    bean.setSelected(true);
                }   else {
                    compoundButton.setChecked(false);
                    bean.setSelected(false);
                }
            }
        });
    }

    @Override
    public int getItemCount() {
        if (addonObj!=null)
            return addonObj.size();
        else
            return 0;
    }


}
