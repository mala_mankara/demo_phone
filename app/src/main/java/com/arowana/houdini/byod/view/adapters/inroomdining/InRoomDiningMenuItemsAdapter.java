package com.arowana.houdini.byod.view.adapters.inroomdining;

import android.app.Activity;
import android.content.Context;
import android.graphics.Paint;
import androidx.recyclerview.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.arowana.houdini.byod.MainApplication;
import com.arowana.houdini.byod.R;
import com.arowana.houdini.byod.interfaces.listeners.FragmentNavigationHandler;
import com.arowana.houdini.byod.model.inroomdining.IRDCartObj;
import com.arowana.houdini.byod.model.inroomdining.IRDiningImagesObj;
import com.arowana.houdini.byod.model.inroomdining.IRDiningMenuItemsObj;
import com.arowana.houdini.byod.utils.RealmUtil;
import com.arowana.houdini.byod.view.adapters.ICart;
import com.arowana.houdini.byod.view.fragments.inroomdining.IRDiningCartFragment;
import com.arowana.houdini.byod.view.fragments.inroomdining.IRDiningCustomizationFragment;
import com.arowana.houdini.byod.view.fragments.inroomdining.IRDiningItemDetailsFragment;
import com.arowana.houdini.byod.view.fragments.inroomdining.IRDining_RepeatCustom_PromptFragment;
import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;

import io.realm.Realm;
import io.realm.RealmList;
import io.realm.RealmResults;

import static com.arowana.houdini.byod.view.activity.InRoomDiningActivity.fragmentHolder;

public class InRoomDiningMenuItemsAdapter extends RecyclerView.Adapter<InRoomDiningMenuItemsAdapter.MyViewHolder> {


    private Context context;
    private RealmList<IRDiningMenuItemsObj> mMenuItemsObj;

    FragmentNavigationHandler mHandler;
    FrameLayout mHolder;
    Realm realm;
    private ICart cartListener;


    public class MyViewHolder extends RecyclerView.ViewHolder {
        private TextView title, description, strikePrice, price, btnAdd, btnIncrease, btnDecrease, countTxt;
        private ImageView isVegImg, menuImg;
        private LinearLayout customiseLayout, countLayout;
        private RelativeLayout itemDetailslayout;
        private View divider;


        public MyViewHolder(View view) {
            super(view);
            itemDetailslayout = view.findViewById(R.id.ir_item_details_layout);
            isVegImg = view.findViewById(R.id.ir_dining_is_veg);
            menuImg = view.findViewById(R.id.ir_dining_menu_img);

            title = view.findViewById(R.id.ir_dining_menu_title);
            description = view.findViewById(R.id.ir_dining_menu_description);
            strikePrice = view.findViewById(R.id.ir_dining_menu_price);
            price = view.findViewById(R.id.price);

            customiseLayout = view.findViewById(R.id.layout_customize);
            countLayout = view.findViewById(R.id.ir_dining_menu_count_layout);

            btnAdd = view.findViewById(R.id.btn_ir_dining_menu_add);
            divider = view.findViewById(R.id.ir_dining_menu_detail_divider);

            btnIncrease = view.findViewById(R.id.btn_increase);
            btnDecrease = view.findViewById(R.id.btn_decrease);
            countTxt = view.findViewById(R.id.item_count);

            itemDetailslayout.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    IRDiningItemDetailsFragment fragment = new IRDiningItemDetailsFragment();

                    fragment.setMenuObj(mMenuItemsObj.get(getAdapterPosition()));
                    mHandler.showFragment(mHolder.getId(), fragment);
                }
            });
        }


    }


    public InRoomDiningMenuItemsAdapter(Context mContext, ICart mlistener, RealmList<IRDiningMenuItemsObj> obj, FragmentNavigationHandler handler, FrameLayout holder) {
        this.context = mContext;
        this.mMenuItemsObj = obj;
        this.mHandler = handler;
        this.mHolder = holder;
        realm = RealmUtil.getInstance();
        cartListener = mlistener;


    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_ir_dining_menu_items_component, parent, false);


        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, final int position) {
        final IRDiningMenuItemsObj bean = mMenuItemsObj.get(position);

        holder.isVegImg.setBackgroundResource(bean.getDietType().toLowerCase().equals("veg") ? R.drawable.veg_symbol : R.drawable.non_veg_symbol);
        if (bean.getImages().size() > 0) {
            IRDiningImagesObj img = bean.getImages().get(0);

            // Picasso.get().load(img.getImageUrl()).fit().into(holder.menuImg);

            Glide.with(context).load(img.getImageUrl()).apply(new RequestOptions()

                    .centerCrop()).into(holder.menuImg);
        } else holder.menuImg.setVisibility(View.GONE);


        holder.title.setText(bean.getName());
        holder.description.setText(bean.getDescription());
        if (bean.getDiscount().size() > 0) {
            holder.strikePrice.setText(RealmUtil.getDiningCurrency().concat(" ").concat(bean.getPrice()));
            holder.strikePrice.setPaintFlags(holder.strikePrice.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);
            holder.price.setText(RealmUtil.getDiningCurrency().concat(" ").concat(bean.getDiscount().get(0).getDiscountPrice()));
        } else {
            holder.strikePrice.setVisibility(View.GONE);
            holder.price.setText(RealmUtil.getDiningCurrency().concat(" ").concat(bean.getPrice()));
        }

        realm.executeTransaction(new Realm.Transaction() {
            @Override
            public void execute(Realm realm) {
                bean.setSelected(bean.getItemCount() > 0 ? true : false);

            }
        });

        int itemCountInCart = RealmUtil.getMenuItemCountInCart(bean.getId());

        holder.countTxt.setText(String.valueOf(itemCountInCart));

        holder.btnAdd.setVisibility(itemCountInCart <= 0 ? View.VISIBLE : View.GONE);
        holder.countLayout.setVisibility(itemCountInCart < 1 ? View.GONE : View.VISIBLE);

        holder.customiseLayout.setVisibility(bean.getCustomization().size() > 0 || bean.getAddons().size()>0 ? View.VISIBLE : View.GONE);
        if (position == mMenuItemsObj.size()) {
            holder.divider.setVisibility(View.GONE);
        }

//        holder.customiseLayout.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                if (bean.getItemCount() > 0) {
//
//
//                    IRDiningCustomizationFragment fragment = new IRDiningCustomizationFragment();
//
//                    fragment.setMenuItemObj(mMenuItemsObj.get(position));
//                    mHandler.showFragment(fragmentHolder.getId(), fragment);
//                } else {
//                    Toast.makeText(context, "Please add item to customize", Toast.LENGTH_SHORT).show();
//                }
//            }
//        });

        holder.btnAdd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                holder.btnAdd.setVisibility(View.GONE);
                holder.countLayout.setVisibility(View.VISIBLE);


                if(bean.getCustomization().size() > 0 || bean.getAddons().size()>0) {
                    IRDiningCustomizationFragment fragment = new IRDiningCustomizationFragment();
                    fragment.setMenuItemObj(RealmUtil.createTmpCartObj(bean), true);
                    mHandler.showFragment(fragmentHolder.getId(), fragment);
                }else {
                    RealmUtil.createCartObj(bean);
                    cartListener.updateCartInfo();
                    notifyItemRangeChanged(position, 1, bean);
                }

            }
        });

        holder.btnIncrease.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                realm.executeTransaction(new Realm.Transaction() {
                    @Override
                    public void execute(Realm realm) {
                        if(bean.getCustomization().size() > 0 || bean.getAddons().size()>0) {
                            IRDining_RepeatCustom_PromptFragment fragment = new IRDining_RepeatCustom_PromptFragment();
                            // IRDCartObj cartObj=realm.where(IRDCartObj.class).equalTo("MenuItemObj.Id", bean.getId()).findAll().last();
                            IRDCartObj cartObj=realm.where(IRDCartObj.class).equalTo("MenuItemObj.Id", bean.getId()).findAll().sort("timeStamp").last();
                            fragment.setMenuItemObj(cartObj,true);
                            mHandler.addFragment(fragmentHolder.getId(), fragment,"PromptFragment");

                        }else {
                            IRDCartObj cartObj=realm.where(IRDCartObj.class).equalTo("MenuItemObj.Id", bean.getId()).findFirst();
                            cartObj.setItemCount(cartObj.getItemCount()+1);
                            realm.copyToRealmOrUpdate(cartObj);

                        }

                    }
                });
                cartListener.updateCartInfo();
                notifyItemRangeChanged(position, 1, bean);


            }
        });

        holder.btnDecrease.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                realm.executeTransaction(new Realm.Transaction() {
                    @Override
                    public void execute(Realm realm) {
                        IRDCartObj cartObj;
                        int cartCount=0;
                        if(bean.getCustomization().size() > 0 || bean.getAddons().size()>0) {
                            RealmResults<IRDCartObj> cartObjList= realm.where(IRDCartObj.class).equalTo("MenuItemObj.Id", bean.getId()).findAll();
                           cartObj=realm.where(IRDCartObj.class).equalTo("MenuItemObj.Id", bean.getId()).findAll().last();
                           Log.d("IRDMenuITEMADpt","size of cartObj.."+cartObj.getItemCount()+"/cartList... "+cartObjList.size());
                            cartCount=cartObjList.size();
                            if(cartCount>1){
                                populateSnackbar(bean.getId());
                            }else if(cartCount == 1 && cartObj.getItemCount()>1) {
                                cartObj.setItemCount(cartObj.getItemCount() - 1);
                                realm.copyToRealmOrUpdate(cartObj);
                            }
                            else{
                                cartObj.deleteFromRealm();
                            }
                        }else {
                            cartObj=realm.where(IRDCartObj.class).equalTo("MenuItemObj.Id", bean.getId()).findFirst();
                            cartCount=cartObj.getItemCount();
                            if(cartCount > 1) {
                                cartObj.setItemCount(cartObj.getItemCount() - 1);
                                realm.copyToRealmOrUpdate(cartObj);
                            }else{
                                cartObj.deleteFromRealm();
                            }
                        }



//                        int count = bean.getItemCount() - 1;
//                        bean.setItemCount(count > 0 ? count : 0);
                    }
                });
                cartListener.updateCartInfo();
                notifyItemRangeChanged(position, 1, bean);

            }
        });


    }

    private void populateSnackbar(final String menuID) {
        Activity activity = MainApplication.getCurrentActivity();
        final FrameLayout layout = activity.findViewById(R.id.snacbar_layout);
        layout.setVisibility(View.VISIBLE);
        View view = activity.getLayoutInflater().inflate(R.layout.custom_confirm_snackbar_layout, null);
        TextView message= view.findViewById(R.id.dineMessage);
        message.setText("This item has multiple customizations added.Proceed to cart to edit");
        TextView mOkMessage = view.findViewById(R.id.snack_ok_message);
        mOkMessage.setTextSize(18);
        mOkMessage.setText("GO TO CART");
        TextView mCancelMessage = view.findViewById(R.id.snack_cancel_message);
        mCancelMessage.setTextSize(18);

        mOkMessage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                IRDiningCartFragment fragment = new IRDiningCartFragment();
                mHandler.showFragment(fragmentHolder.getId(), fragment);
                layout.setVisibility(View.GONE);
                layout.removeAllViews();

            }
        });

        mCancelMessage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                layout.setVisibility(View.GONE);
                layout.removeAllViews();

            }
        });

        layout.addView(view);
    }


    @Override
    public int getItemCount() {
        return mMenuItemsObj.size();
    }


}
