package com.arowana.houdini.byod.model.inroomdining;

import io.realm.RealmList;
import io.realm.RealmObject;

public class RoomDiningInfoObj extends RealmObject{


    private RealmList<IRDiningAvailabilityTimeObj> AvailabilityTime;

    private String BrandId;

    private String CategoryCode;

    private String HotelCode;

    private String CategoryName;

    private String HotelId;

    private String GroupCode;

    private String Id;

    private String BrandCode;

    private String Sequence;

    private RealmList<IRDiningMenusObj> Menus;

    private String GroupId;

    public RealmList<IRDiningAvailabilityTimeObj> getAvailabilityTime() {
        return AvailabilityTime;
    }

    public void setAvailabilityTime(RealmList<IRDiningAvailabilityTimeObj> availabilityTime) {
        AvailabilityTime = availabilityTime;
    }

    public String getBrandId() {
        return BrandId;
    }

    public void setBrandId(String brandId) {
        BrandId = brandId;
    }

    public String getCategoryCode() {
        return CategoryCode;
    }

    public void setCategoryCode(String categoryCode) {
        CategoryCode = categoryCode;
    }

    public String getHotelCode() {
        return HotelCode;
    }

    public void setHotelCode(String hotelCode) {
        HotelCode = hotelCode;
    }

    public String getCategoryName() {
        return CategoryName;
    }

    public void setCategoryName(String categoryName) {
        CategoryName = categoryName;
    }

    public String getHotelId() {
        return HotelId;
    }

    public void setHotelId(String hotelId) {
        HotelId = hotelId;
    }

    public String getGroupCode() {
        return GroupCode;
    }

    public void setGroupCode(String groupCode) {
        GroupCode = groupCode;
    }

    public String getId() {
        return Id;
    }

    public void setId(String id) {
        Id = id;
    }

    public String getBrandCode() {
        return BrandCode;
    }

    public void setBrandCode(String brandCode) {
        BrandCode = brandCode;
    }

    public String getSequence() {
        return Sequence;
    }

    public void setSequence(String sequence) {
        Sequence = sequence;
    }

    public RealmList<IRDiningMenusObj> getMenus() {
        return Menus;
    }

    public void setMenus(RealmList<IRDiningMenusObj> menus) {
        Menus = menus;
    }

    public String getGroupId() {
        return GroupId;
    }

    public void setGroupId(String groupId) {
        GroupId = groupId;
    }
}
