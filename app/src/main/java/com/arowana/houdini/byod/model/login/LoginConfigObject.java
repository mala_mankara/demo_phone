package com.arowana.houdini.byod.model.login;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

public class LoginConfigObject extends RealmObject {
    @PrimaryKey
    private String RealmId = "1";

    private LoginResponseStatusObject ResponseStatus;

    private LoginResponseDataObject ResponseData;

    public LoginResponseStatusObject getResponseStatus() {
        return ResponseStatus;
    }

    public void setResponseStatus(LoginResponseStatusObject responseStatus) {
        ResponseStatus = responseStatus;
    }

    public LoginResponseDataObject getResponseData() {
        return ResponseData;
    }

    public void setResponseData(LoginResponseDataObject responseData) {
        ResponseData = responseData;
    }
}
