package com.arowana.houdini.byod.view.activity;

import android.os.Bundle;
import android.widget.FrameLayout;

import com.arowana.houdini.byod.MainApplication;
import com.arowana.houdini.byod.R;
import com.arowana.houdini.byod.view.fragments.FeedBackFragment;

public class FeedBackActivity extends BaseActivity {

    public static FrameLayout fragmentHolder;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_module_layout);
        MainApplication.setCurrentActivity(this);
        initComponenets();

    }

    private void initComponenets() {
        fragmentHolder = findViewById(R.id.fragment_holder);
        loadFragments();
    }

    private void loadFragments() {

        showFragment(fragmentHolder.getId(), new FeedBackFragment());
    }


}
