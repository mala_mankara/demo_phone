package com.arowana.houdini.byod.view.fragments.serviceRequest;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.res.ColorStateList;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;

import com.arowana.houdini.byod.Consts;
import com.arowana.houdini.byod.MainApplication;
import com.arowana.houdini.byod.R;
import com.arowana.houdini.byod.interfaces.listeners.ProvideBundleListener;
import com.arowana.houdini.byod.interfaces.listeners.UpdateBundleListener;
import com.arowana.houdini.byod.interfaces.listeners.UpdatePreviousScreenListener;
import com.arowana.houdini.byod.model.servicerequest.AvailTime;
import com.arowana.houdini.byod.model.servicerequest.ServiceRequestResponseData;
import com.arowana.houdini.byod.utils.DialogUtil;
import com.arowana.houdini.byod.utils.NavigationConsts;
import com.arowana.houdini.byod.utils.RealmUtil;
import com.arowana.houdini.byod.view.fragments.BaseFragment;
import com.arowana.houdini.byod.view.fragments.telephone.PhoneCallFragment;

import java.util.Objects;

import alcatel.model.ByodManager;
import io.realm.Realm;

import static com.arowana.houdini.byod.view.activity.ServiceRequestActivity.fragmentHolder;

/**
 * A simple {@link Fragment} subclass.
 */
public class MakeMyRoomFragment extends BaseFragment implements RadioGroup.OnCheckedChangeListener, View.OnClickListener, ProvideBundleListener {

    public static final String TAG = "MakeMyRoomFragment";
    private ServiceRequestResponseData mServiceObject;
    private String speedDialNo;

    public MakeMyRoomFragment() {
        /* Required empty public constructor */
    }

    @Override
    protected int getFragmentLayout() {
        return R.layout.fragment_make_myroom;
    }

    private View mRootView;
    ProgressDialog mDialog;
    TextView next;
    private Context mContext;
    private Realm mRealm;
    private RadioGroup mAvailTimings;

    private String mSelectedTime;
    Fragment mFragment;
    private View call_conceirge;

    private int selectedID;
    private boolean isSelected=false;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mContext = getActivity();
        setTitle(mServiceObject.getServiceItem());
        mRealm = RealmUtil.getInstance();
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        mRootView = view;
        initComponents();
        mDialog = DialogUtil.showProgressDialog(getActivity(), "");

    }

    private void initComponents() {

        mAvailTimings   = mRootView.findViewById(R.id.scheduleTiming);
        next            = mRootView.findViewById(R.id.btnNext);
        call_conceirge  = mRootView.findViewById(R.id.btn_conceirge_makeroom);

       // call_conceirge.setVisibility(Consts.APP_CURRENT_STATE > 1 ? View.VISIBLE : View.GONE);
        if(MainApplication.isContainedSpeedDial("housekeeping")&& Consts.APP_CURRENT_STATE > 1 ) {
            call_conceirge.setVisibility(View.VISIBLE);
            speedDialNo = MainApplication.getValueforKey();

        }else
            call_conceirge.setVisibility(View.GONE);
        call_conceirge.setOnClickListener(this);
        initListeners();
        initData();
    }

    private void initListeners() {
        mAvailTimings.setOnCheckedChangeListener(this);
        next.setOnClickListener(this);
    }

    private void initData() {
        Typeface myTypeface = Typeface.createFromAsset(mContext.getAssets(), "fonts/SF-UI-Text-Regular.otf");
        mAvailTimings.removeAllViews();
        //  mAvailTimings.setWeightSum(mServiceObject.getAvailTime().size());
        for (int i = 0; i < mServiceObject.getAvailTime().size(); i++) {


            AvailTime timeSlots = mServiceObject.getAvailTime().get(i);
            RadioButton radioButton = new RadioButton(mContext);
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP)
                radioButton.setButtonTintList(ColorStateList.valueOf(ContextCompat.getColor(mContext, R.color.action_bar_color)));
            radioButton.setHighlightColor(getResources().getColor(R.color.action_bar_color));
            radioButton.setBackgroundColor(Color.TRANSPARENT);
            radioButton.setId(i);
            radioButton.setPadding(0,80,0,80);
            radioButton.setTextColor(Color.parseColor("#666564"));
            radioButton.setLayoutParams(new RadioGroup.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT, .25f));
            if ( timeSlots!=null && timeSlots.getTime() != null)
                radioButton.setText(timeSlots.getTime());
            radioButton.setTypeface(myTypeface);
          /*  if (mServiceObject.getAvailTime().size() == i) {
                radioButton.setChecked(true);
            }*/

            if (mServiceObject.getPos() == i) {
                radioButton.setChecked(true);
            }
            mAvailTimings.addView(radioButton);
        }
    }


    public void setServiceObject(ServiceRequestResponseData service) {
        mServiceObject = service;

    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.btnNext:
              /*  if (!mRealm.isInTransaction())
                    mRealm.beginTransaction();
                mRealm.commitTransaction();*/
                updateData();
                break;
            case R.id.btn_conceirge_makeroom:
                if(!ByodManager.getInstance().isSipConnected()) {
                    Toast.makeText(getActivity(),"Call service not working. Please try later..",Toast.LENGTH_SHORT).show();
                }else {
                    if (!speedDialNo.isEmpty()) {
                        PhoneCallFragment fragment = new PhoneCallFragment();
                        Bundle bundle = new Bundle();
                        bundle.putString("phoneNo", speedDialNo);
                        bundle.putString("name", "housekeeping");
                        bundle.putBoolean("isMakingCall",true);
                        fragment.setArguments(bundle);

                        mHandler.showFragment(fragmentHolder.getId(), fragment);
                    }
                }
                break;
        }
    }

    private void updateData() {
        if (isSelected){
            if (!mRealm.isInTransaction())
                mRealm.beginTransaction();
            mServiceObject.setItemCount(1);
            AvailTime timeChecked = mServiceObject.getAvailTime().get(selectedID);
            if (timeChecked != null) {
                timeChecked.setSelected(true);
            }
            mServiceObject.setPos(selectedID);
            mRealm.commitTransaction();
        }
        Bundle bundle = new Bundle();
        bundle.putString("data_type", NavigationConsts.MAKE_MY_ROOM);
        bundle.putString("time_request", mSelectedTime);

        FragmentManager.BackStackEntry backEntry;
        if (getFragmentManager() != null) {
            backEntry = getFragmentManager().getBackStackEntryAt(0);
            String tag = backEntry.getName();
            mFragment = getFragmentManager().findFragmentByTag(tag);
        }

        if (mFragment instanceof UpdateBundleListener) {
            ((UpdateBundleListener) mFragment).onBundleUpdate(bundle);
        }
        Objects.requireNonNull(getActivity()).onBackPressed();
    }

    @Override
    public void onCheckedChanged(RadioGroup group, int radioId) {

        RadioButton radioButton = group.findViewById(radioId);
        if (radioButton != null && radioButton.isPressed()) {
            isSelected=true;
            selectedID = radioId;
            mSelectedTime = radioButton.getText().toString().trim();
             /*  if (!mRealm.isInTransaction())
                mRealm.beginTransaction();
            mServiceObject.setItemCount(1);
            AvailTime timeChecked = mServiceObject.getAvailTime().get(radioId);
            if (timeChecked != null) {
                timeChecked.setSelected(true);
            }
            mServiceObject.setPos(radioId);
            mSelectedTime = radioButton.getText().toString().trim();
            // AvailTime availTimeObj = mServiceObject.getAvailTime().get(radioId);
            mRealm.commitTransaction();*/
        }else {
            isSelected = false;

        }

    }

    @Override
    public void registerBundleListener(UpdatePreviousScreenListener listener) {
        Log.d("MakemyRoom..","registerBundleListener");
    }
}
