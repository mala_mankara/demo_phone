package com.arowana.houdini.byod.view.fragments.palaceInfo;


import android.content.Context;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.arowana.houdini.byod.R;
import com.arowana.houdini.byod.model.palaceInformation.PalaceInfoHotels;
import com.arowana.houdini.byod.view.adapters.PalaceInfoAdapter;
import com.arowana.houdini.byod.view.fragments.BaseFragment;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.MapsInitializer;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;

/**
 * A simple {@link Fragment} subclass.
 */
public class PalaceMapFragment extends BaseFragment implements OnMapReadyCallback {

    float zoomLevel = 15.0f;

    private double mLocationLat = 0.00;
    private double mLocationLng = 0.00;

    Bundle bundle;

    String name, mAddress, lat, lng;
    private View mRootView;
    MapView mMapView;
    private GoogleMap googleMap;
    //private ImageView mBtnNavigation;
    private TextView checkinvalue;
    private TextView title;
    private TextView checkoutValue;
    private RecyclerView infoListView;


    private Context mContext;
    PalaceInfoHotels hotelObj;


    @Override
    public void onCreate(Bundle savedInstance) {
        super.onCreate(savedInstance);
        mContext = getActivity();

        if (hotelObj.isValid()) {
            name = hotelObj.getName();
            lat = hotelObj.getLatitude();
            lng = hotelObj.getLongitude();
        }
        try {
            if (lat != null) {
                mLocationLat = Double.parseDouble(lat);
            }
            if (lng != null) {
                mLocationLng = Double.parseDouble(lng);
            }
        } catch (Exception e) {

        }
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        mRootView = view;
        setTitle(getString(R.string.location));
        initComponents();
        if (hotelObj != null) {
            title.setText(hotelObj.getName() + ", " + hotelObj.getCity());
            checkinvalue.setText(hotelObj.getCheckInTime());
            checkoutValue.setText(hotelObj.getCheckOutTime());
        }
        mMapView.onCreate(savedInstanceState);

        mMapView.onResume(); // needed to get the map to display immediately

        try {
            MapsInitializer.initialize(getActivity().getApplicationContext());
        } catch (Exception e) {
            e.printStackTrace();
        }

        mMapView.getMapAsync(this);
        infoListView.setAdapter(new PalaceInfoAdapter(mContext, hotelObj, mHandler, true));
    }

    @Override
    protected int getFragmentLayout() {
        return R.layout.facility_mapview;
    }


    public void initComponents() {
        mMapView = mRootView.findViewById(R.id.mapView);
        title = mRootView.findViewById(R.id.map_title);
        checkinvalue = mRootView.findViewById(R.id.map_checkin_time);
        checkoutValue = mRootView.findViewById(R.id.map_checkout_time);
        infoListView = mRootView.findViewById(R.id.mapinfo_list);

        infoListView.setLayoutManager(new LinearLayoutManager(mContext));
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        MarkerOptions opts = new MarkerOptions();
        LatLng latLng = new LatLng(mLocationLat, mLocationLng);
        opts.position(latLng);

        googleMap.addMarker(opts);
        googleMap.moveCamera(CameraUpdateFactory.newLatLngZoom(latLng, zoomLevel));
    }

    public void setData(Bundle bundle) {
        this.bundle = bundle;
    }


    @Override
    public void onResume() {
        super.onResume();
        mMapView.onResume();
    }

    @Override
    public void onPause() {
        super.onPause();
        mMapView.onPause();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        mMapView.onDestroy();
    }

    @Override
    public void onLowMemory() {
        super.onLowMemory();
        mMapView.onLowMemory();
    }

    public void setObj(PalaceInfoHotels dataObj) {
        hotelObj = dataObj;
    }


}

