package com.arowana.houdini.byod.view.adapters.inroomdining;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.TextView;

import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.arowana.houdini.byod.R;
import com.arowana.houdini.byod.interfaces.listeners.FragmentNavigationHandler;
import com.arowana.houdini.byod.model.inroomdining.IRDiningMenusObj;
import com.arowana.houdini.byod.view.adapters.ICart;

import io.realm.RealmList;

public class InRoomDiningMenuAdapter extends RecyclerView.Adapter<InRoomDiningMenuAdapter.MyViewHolder> {


    private Context context;
    private RealmList<IRDiningMenusObj> mMenuObj;

    FragmentNavigationHandler mHandler;
    FrameLayout mHolder;
    private ICart cartListener;


    public class MyViewHolder extends RecyclerView.ViewHolder {
        private TextView title;
        private RecyclerView mRecyclerView;


        public MyViewHolder(View view) {
            super(view);
            title = view.findViewById(R.id.ir_dining_menu_main_title);
            mRecyclerView = view.findViewById(R.id.ir_dining_menu_detail_recyclerview);
        }


    }


    public InRoomDiningMenuAdapter(Context mContext, ICart mlistener, RealmList<IRDiningMenusObj> obj, FragmentNavigationHandler handler, FrameLayout holder) {
        this.context = mContext;
        this.mMenuObj = obj;
        this.mHandler = handler;
        this.mHolder = holder;
        cartListener = mlistener;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_ir_dining_menu_main_component, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, int position) {
        IRDiningMenusObj obj = mMenuObj.get(position);

        holder.title.setText(obj.getMenuName());
        LinearLayoutManager linearVertical = new LinearLayoutManager(context, RecyclerView.VERTICAL, false);
        holder.mRecyclerView.setLayoutManager(linearVertical);
        InRoomDiningMenuItemsAdapter adapter = new InRoomDiningMenuItemsAdapter(context,cartListener, obj.getMenuItems(), mHandler, mHolder);
        holder.mRecyclerView.setAdapter(adapter);

    }


    @Override
    public int getItemCount() {
        return mMenuObj.size();
    }

}
