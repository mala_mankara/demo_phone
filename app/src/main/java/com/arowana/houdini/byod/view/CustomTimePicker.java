package com.arowana.houdini.byod.view;

import android.content.Context;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Typeface;
import android.graphics.drawable.Drawable;
import androidx.core.content.res.ResourcesCompat;
import android.util.AttributeSet;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.NumberPicker;
import android.widget.TimePicker;


import com.arowana.houdini.byod.R;

import java.lang.reflect.Field;

/**
 * Created by biplab.tripathy on 30-08-2016.
 */
public class CustomTimePicker extends TimePicker {
    static final String TAG = "MyCustomTimePicker";
    private NumberPicker hourPicker;
    private NumberPicker minutePicker;
    private NumberPicker ampmPicker;

    private void commonInit() {
        try {
            Field delegateField = TimePicker.class.getDeclaredField("mDelegate");
            delegateField.setAccessible(true);
            Object mDelegate = delegateField.get(this);

            Field hourPickerField = mDelegate.getClass().getDeclaredField("mHourSpinner");
            hourPickerField.setAccessible(true);

            Field minutePickerField = mDelegate.getClass().getDeclaredField("mMinuteSpinner");
            minutePickerField.setAccessible(true);

            Field ampmPickerField = mDelegate.getClass().getDeclaredField("mAmPmSpinner");
            ampmPickerField.setAccessible(true);

            // setBackgroundResource(R.drawable.calendar_bg);
            hourPicker = (NumberPicker) hourPickerField.get(mDelegate);
            minutePicker = (NumberPicker) minutePickerField.get(mDelegate);
            ampmPicker = (NumberPicker) ampmPickerField.get(mDelegate);



            Drawable divider = ResourcesCompat.getDrawable(getResources(), R.drawable.item_spa_time_picker_divider, null);
            customize(hourPicker, Color.parseColor("#A6713C"), divider);
            customize(minutePicker, Color.parseColor("#A6713C"), divider);
            customize(ampmPicker,  Color.parseColor("#A6713C"), divider);

        } catch (Exception ex) {
            Log.v(TAG, "Unable to change date dialog style." + ex.getMessage());
        }
    }

    public CustomTimePicker(Context context) {
        super(context);
        commonInit();
    }

    public CustomTimePicker(Context context, AttributeSet attrs) {
        super(context, attrs);
        commonInit();
    }

    public CustomTimePicker(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        commonInit();
    }

        public CustomTimePicker(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
            super(context, attrs, defStyleAttr);
            commonInit();
        }

    public boolean customize(NumberPicker numberPicker, int textColor, Drawable divider)
    {
        final int count = numberPicker.getChildCount();
        for(int i = 0; i < count; i++){
            View child = numberPicker.getChildAt(i);
            if(child instanceof EditText){
                try{
                    Typeface type = Typeface.createFromAsset(getContext().getAssets(),"Roboto-Light.ttf");
                    //txtyour.setTypeface(type);
                    EditText editText = ((EditText)child);
                    editText.setTypeface(type);
                    editText.setTextSize(24);
                    editText.setTextColor(textColor);
                    editText.setEnabled(false);


                    //Field mIncre  = NumberFormat.class.getDeclaredField()()

                    Field mSelectionDividerField = NumberPicker.class.getDeclaredField("mSelectionDivider");
                    mSelectionDividerField.setAccessible(true);
                    divider.setAlpha(200);
                    mSelectionDividerField.set(numberPicker, divider);


                    Field selectorWheelPaintField = numberPicker.getClass().getDeclaredField("mSelectorWheelPaint");
                    selectorWheelPaintField.setAccessible(true);
                    Paint mSelectorWheelPaint = ((Paint) selectorWheelPaintField.get(numberPicker));
                    mSelectorWheelPaint.setColor(textColor);
                    mSelectorWheelPaint.setTypeface(editText.getTypeface());
                    mSelectorWheelPaint.setTextSize(editText.getTextSize());

                    //LinearLayout lin = new LinearLayout(numberPicker.getContext());
                    //ImageView im = new CallButtonView()
                   // numberPicker.
                    numberPicker.invalidate();
                    return true;
                }
                catch(IllegalArgumentException e){
                    Log.w(TAG, e);
                }
                catch(NoSuchFieldException e){
                    Log.w(TAG, e);
                }
                catch(IllegalAccessException e) {
                    Log.w(TAG, e);
                }
            }
        }

        return false;
    }



}
