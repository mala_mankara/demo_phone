package com.arowana.houdini.byod.view.adapters.inroomdining;

import android.content.Context;
import android.graphics.Paint;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.arowana.houdini.byod.R;
import com.arowana.houdini.byod.interfaces.listeners.FragmentNavigationHandler;
import com.arowana.houdini.byod.model.inroomdining.IRDCartObj;
import com.arowana.houdini.byod.model.inroomdining.IRDiningAddonsObj;
import com.arowana.houdini.byod.model.inroomdining.IRDiningCustomizationObj;
import com.arowana.houdini.byod.model.inroomdining.IRDiningImagesObj;
import com.arowana.houdini.byod.model.inroomdining.IRDiningMenuItemsObj;
import com.arowana.houdini.byod.model.inroomdining.IRDiningUpsellingItemObj;
import com.arowana.houdini.byod.utils.RealmUtil;
import com.arowana.houdini.byod.view.adapters.ICart;
import com.arowana.houdini.byod.view.fragments.inroomdining.IRDiningCustomizationFragment;
import com.arowana.houdini.byod.view.fragments.inroomdining.IRDining_RepeatCustom_PromptFragment;
import com.squareup.picasso.Picasso;

import java.text.DecimalFormat;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import io.realm.Realm;
import io.realm.RealmList;
import io.realm.RealmResults;

import static com.arowana.houdini.byod.view.activity.InRoomDiningActivity.fragmentHolder;

public class InRoomDiningCartAdapter extends RecyclerView.Adapter<InRoomDiningCartAdapter.MyViewHolder> {

    private Context context;
    private RealmResults<IRDCartObj> menuItemObj;

    private FragmentNavigationHandler mHandler;
    Realm realm;
    private ICart cartListener;
    private InRoomDiningUpsellingAdapter upsellingAdapter;
    private RecyclerView upsellingRecylerview;
    private LinearLayout upsellingLayout;

    public class MyViewHolder extends RecyclerView.ViewHolder {
        private TextView title, strikePrice, price, btnCustomize, isCustomized, btnAdd, btnIncrease, btnDecrease, count;
        private LinearLayout countLayout;
        private ImageView btnRemove;
        private View mDivider;

        public MyViewHolder(View view) {
            super(view);

            title = view.findViewById(R.id.item_name);
            strikePrice = view.findViewById(R.id.ir_dining_menu_price);
            price = view.findViewById(R.id.price);

            btnCustomize = view.findViewById(R.id.btn_customize);
            isCustomized = view.findViewById(R.id.txt_customized);

            btnAdd = view.findViewById(R.id.btn_ir_dining_cart_add);
            btnIncrease = view.findViewById(R.id.btn_increase);
            btnDecrease = view.findViewById(R.id.btn_decrease);
            count = view.findViewById(R.id.item_count);
            countLayout = view.findViewById(R.id.ir_dining_cart_count_layout);

            btnRemove = view.findViewById(R.id.btn_remove_item);
            mDivider = view.findViewById(R.id.ir_dining_cart_divider);
        }
    }


    public InRoomDiningCartAdapter(Context mContext, ICart listener, FragmentNavigationHandler handler,
                                   InRoomDiningUpsellingAdapter upsellingAdapter, RecyclerView upsellingRecylerview, LinearLayout upsellingLayout) {
        this.context = mContext;

        this.mHandler = handler;

        realm = RealmUtil.getInstance();
        this.cartListener = listener;
        this.upsellingAdapter = upsellingAdapter;
        this.upsellingRecylerview = upsellingRecylerview;
        this.upsellingLayout = upsellingLayout;

        this.menuItemObj = RealmUtil.getCartItems();
        cartListener = listener;
        getUpsellingItems();
    }


    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_ir_dining_cart_component, parent, false);


        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull final MyViewHolder holder, final int position) {
       /* DecimalFormat format = new DecimalFormat();
        format.setDecimalSeparatorAlwaysShown(false);*/
        final IRDCartObj cartObj = menuItemObj.get(position);
        if(cartObj != null) {
            final IRDiningMenuItemsObj bean = cartObj.getMenuItemObj();
            holder.title.setText(bean.getName());
            if (bean.getDiscount().size() > 0) {
                String price1 = getPrice(cartObj, bean.getPrice());
                holder.strikePrice.setText(RealmUtil.getDiningCurrency().concat(" ").concat(price1));
                holder.strikePrice.setPaintFlags(holder.strikePrice.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);

                String price = getPrice(cartObj, bean.getDiscount().get(0).getDiscountPrice());
                holder.price.setText(RealmUtil.getDiningCurrency().concat(" ").concat(price));
            } else {
                holder.strikePrice.setVisibility(View.GONE);
                String price = getPrice(cartObj, bean.getPrice());
                holder.price.setText(RealmUtil.getDiningCurrency().concat(" ").concat(price));
            }
            holder.count.setText(String.valueOf(cartObj.getItemCount()));
            holder.btnAdd.setVisibility(cartObj.getItemCount() <= 0 ? View.VISIBLE : View.GONE);
            holder.countLayout.setVisibility(cartObj.getItemCount() < 1 ? View.GONE : View.VISIBLE);

            holder.btnCustomize.setVisibility(cartObj.getCustomization().size() > 0 ||cartObj.getAddons().size()>0 ? View.VISIBLE : View.GONE);
            boolean isCustomized =isThisCustomized(cartObj);
            holder.isCustomized.setVisibility(isCustomized ? View.VISIBLE : View.GONE);

            if (position == menuItemObj.size()) {
                holder.mDivider.setVisibility(View.GONE);
            }

            holder.btnAdd.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    holder.btnAdd.setVisibility(View.GONE);
                    holder.countLayout.setVisibility(View.VISIBLE);

                    realm.executeTransaction(new Realm.Transaction() {
                        @Override
                        public void execute(Realm realm) {
                            cartObj.setItemCount(1);
                        }
                    });
                    cartListener.updateCartInfo();
                    notifyItemRangeChanged(position, 1, bean);
                }
            });

            holder.btnIncrease.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    realm.executeTransaction(new Realm.Transaction() {
                        @Override
                        public void execute(Realm realm) {
                            if(isThisCustomized(cartObj)){
                                IRDining_RepeatCustom_PromptFragment fragment = new IRDining_RepeatCustom_PromptFragment();
                                fragment.setMenuItemObj(cartObj,true);
                                mHandler.showFragment(fragmentHolder.getId(), fragment,"PromptFragment");
                            }else {
                                int count = cartObj.getItemCount() + 1;
                                cartObj.setItemCount(count > 0 ? count : 0);
                            }
                        }
                    });
                    cartListener.updateCartInfo();
                    notifyItemRangeChanged(position, 1, bean);
                }
            });

            holder.btnDecrease.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    realm.executeTransaction(new Realm.Transaction() {
                        @Override
                        public void execute(Realm realm) {
                            int count = cartObj.getItemCount() - 1;
                            if (count > 0) {
                                cartObj.setItemCount(count);
                            } else {
                                cartObj.deleteFromRealm();
                                bean.setUpsellingItem(false);
                                notifyDataSetChanged();
                                getUpsellingItems();
                            }
                            //cartObj.setItemCount(count > 0 ? count : 0);
                        }
                    });
                    cartListener.updateCartInfo();
                    notifyItemRangeChanged(position, 1, bean);
                }
            });

            holder.btnRemove.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    realm.executeTransaction(new Realm.Transaction() {
                        @Override
                        public void execute(Realm realm) {
                            cartObj.deleteFromRealm();
                            // bean.setSelected(cartObj.getItemCount() > 0 ? true : false);
                            bean.setUpsellingItem(false);
                        }
                    });

                    notifyDataSetChanged();
                    getUpsellingItems();
//                upsellingAdapter.notifyDataSetChanged();

                    cartListener.updateCartInfo();
                }
            });

            holder.btnCustomize.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (cartObj.getItemCount() > 0) {
                        IRDiningCustomizationFragment fragment = new IRDiningCustomizationFragment();
                        fragment.setMenuItemObj(cartObj, false);
                        fragment.setButtonUpdates( "UPDATE");
                        mHandler.showFragment(fragmentHolder.getId(), fragment);
                    } else {
                        Toast.makeText(context, "Please add item to customize", Toast.LENGTH_SHORT).show();
                    }
                }
            });
        }
    }

    private boolean isThisCustomized(IRDCartObj cartObj) {
        if(cartObj.getAddons().size()>0  ){
            for(IRDiningAddonsObj addonsObj: cartObj.getAddons()){
                if(addonsObj.isSelected()) {
                    return addonsObj.isSelected();
                }
            }
        }
        if(cartObj.getCustomization().size()>0 ){
            for(IRDiningCustomizationObj customObj: cartObj.getCustomization()){
                if(customObj.isSelected()){
                    return customObj.isSelected();
                }
            }
        }
        return false;
    }

    private String getPrice(IRDCartObj bean, String price) {
        DecimalFormat format = new DecimalFormat();
        format.setDecimalSeparatorAlwaysShown(false);
        Double totalPrice = 0d;
        totalPrice += bean.getItemCount() * Double.parseDouble(price);
        if (bean.getAddons().size() > 0) {
            for (IRDiningAddonsObj addon : bean.getAddons()) {
                if (addon.isSelected())
                    totalPrice = totalPrice + bean.getItemCount()*Double.parseDouble(addon.getPrice());
            }
        }
        return String.valueOf(format.format(totalPrice));
    }

    @Override
    public int getItemCount() {
        return menuItemObj.size();
    }


    public class UpsellingViewHolder extends RecyclerView.ViewHolder {
        private TextView title, price, btnAdd;
        private ImageView mImg;

        public UpsellingViewHolder(View view) {
            super(view);
            title = view.findViewById(R.id.upselling_item_name);
            price = view.findViewById(R.id.upselling_price);
            btnAdd = view.findViewById(R.id.upselling_btn_add);
            mImg = view.findViewById(R.id.upselling_img);
        }
    }


    public class InRoomDiningUpsellingAdapter extends RecyclerView.Adapter<UpsellingViewHolder> {

        private List<IRDiningUpsellingItemObj> upsellingObj;
        Realm realm;
        ICart cartListener;
        InRoomDiningCartAdapter adapter;

        public InRoomDiningUpsellingAdapter(Context mContext, ICart listener, List<IRDiningUpsellingItemObj> upsellingList, InRoomDiningCartAdapter adapter) {
            this.upsellingObj = upsellingList;
            cartListener = listener;
            this.adapter = adapter;
            realm = RealmUtil.getInstance();
        }

        @NonNull
        @Override
        public UpsellingViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
            View itemView = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.item_ir_dining_upselling_component, parent, false);
            return new UpsellingViewHolder(itemView);
        }

        @Override
        public void onBindViewHolder(@NonNull final UpsellingViewHolder holder, final int position) {
            final IRDiningUpsellingItemObj bean = upsellingObj.get(position);

            holder.title.setText(bean.getItemName());
            holder.price.setText(RealmUtil.getDiningCurrency().concat(" ").concat(bean.getPrice()));

            if (bean.getImages().size() > 0) {
                IRDiningImagesObj img = bean.getImages().get(0);
                Picasso.get().load(img.getImageUrl()).fit().into(holder.mImg);
            } else holder.mImg.setVisibility(View.GONE);
            holder.btnAdd.setOnClickListener(new View.OnClickListener() {
                IRDiningMenuItemsObj obj;
                @Override
                public void onClick(View view) {
                    realm.executeTransaction(new Realm.Transaction() {
                        @Override
                        public void execute(@NonNull Realm realm) {
                            obj = realm.where(IRDiningMenuItemsObj.class).equalTo("Id", bean.getId()).findAll().first();
                            obj.setItemCount(1);
                            obj.setUpsellingItem(true);
                            obj.setSelected(true);
                            upsellingObj.remove(position);


                        }
                    });


                    if(obj.getCustomization().size() > 0 || obj.getAddons().size()>0) {
                        IRDiningCustomizationFragment fragment = new IRDiningCustomizationFragment();
                        fragment.setMenuItemObj(RealmUtil.createTmpCartObj(obj), true);
                        mHandler.showFragment(fragmentHolder.getId(), fragment);
                    }else {
                        RealmUtil.createCartObj(obj);
                        notifyDataSetChanged();
                        adapter.notifyDataSetChanged();
                        cartListener.updateCartInfo();

                    }
//                    RealmUtil.createCartObj(obj);
//                    notifyDataSetChanged();
//                    adapter.notifyDataSetChanged();
//                    cartListener.updateCartInfo();
                    upsellingLayout.setVisibility(upsellingObj.size() > 0 ? View.VISIBLE : View.GONE);

                }
            });
        }

        @Override
        public int getItemCount() {
            return upsellingObj.size() > 0 ? upsellingObj.size() : 0;
        }
    }


    private void getUpsellingItems() {
        RealmList<IRDiningUpsellingItemObj> upsellingList = new RealmList<>();
        Set<IRDiningUpsellingItemObj> upsellingResultsSet = new HashSet<>();

        for (IRDCartObj menuItem : menuItemObj) {
            List<IRDiningUpsellingItemObj> items = menuItem.getMenuItemObj().getUpsellingItems();
            upsellingList.addAll(items);
        }
        // remove duplicate items from upsellingsList if exist
        for (int i = 0; i < upsellingList.size(); i++) {
            for (int j = i + 1; j < upsellingList.size(); j++) {
                if (upsellingList.get(i).getId().equals(upsellingList.get(j).getId())) {
                    upsellingList.remove(j);
                    j--;
                }
            }
        }
        // remove  items from upselling list if   presents in cart List
        for (IRDCartObj menuItem : menuItemObj) {
            for(int i=0;i<upsellingList.size();i++){
                if(upsellingList.get(i).getId().equals(menuItem.getMenuItemObj().getId())){
                    upsellingList.remove(i);
                    i--;
                }
            }

        }

        this.upsellingLayout.setVisibility(upsellingList.size() > 0 ? View.VISIBLE : View.GONE);
        if (upsellingList.size() > 0) {

            LinearLayoutManager linearhorizontal = new LinearLayoutManager(context, LinearLayoutManager.HORIZONTAL, false);
            this.upsellingRecylerview.setLayoutManager(linearhorizontal);
            this.upsellingAdapter = new InRoomDiningUpsellingAdapter(context, cartListener, upsellingList, this);
            this.upsellingRecylerview.setAdapter(this.upsellingAdapter);
            this.upsellingAdapter.notifyDataSetChanged();
        }
    }
}
