package com.arowana.houdini.byod.utils;

import io.realm.RealmObject;

public class ApiResponseStatus extends RealmObject {
    private String ResponseFlag;

    private String ResponseId;

    private String ResponseCode;

    private String ResponseMessage;

    public String getResponseFlag() {
        return ResponseFlag;
    }

    public void setResponseFlag(String responseFlag) {
        ResponseFlag = responseFlag;
    }

    public String getResponseId() {
        return ResponseId;
    }

    public void setResponseId(String responseId) {
        ResponseId = responseId;
    }

    public String getResponseCode() {
        return ResponseCode;
    }

    public void setResponseCode(String responseCode) {
        ResponseCode = responseCode;
    }

    public String getResponseMessage() {
        return ResponseMessage;
    }

    public void setResponseMessage(String responseMessage) {
        ResponseMessage = responseMessage;
    }

    @Override
    public String toString()
    {
        return "ApiResponseStatus { ResponseFlag = "+ResponseFlag+", ResponseId = "+ResponseId+", ResponseCode = "+ResponseCode+", ResponseMessage = "+ResponseMessage+"]";
    }
}
