package com.arowana.houdini.byod.view.fragments.palaceInfo;


import android.app.ProgressDialog;
import android.content.Context;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.arowana.houdini.byod.R;
import com.arowana.houdini.byod.interfaces.ApiClient;
import com.arowana.houdini.byod.interfaces.api_interface.PalaceInfoApiInterface;
import com.arowana.houdini.byod.model.palaceInformation.PalaceInfoBrands;
import com.arowana.houdini.byod.model.palaceInformation.PalaceInfoConfigObj;
import com.arowana.houdini.byod.model.palaceInformation.PalaceInfoHotels;
import com.arowana.houdini.byod.model.palaceInformation.PalaceInfoResponseData;
import com.arowana.houdini.byod.utils.DialogUtil;
import com.arowana.houdini.byod.utils.ExpandableTextView;
import com.arowana.houdini.byod.utils.FactoryUtil;
import com.arowana.houdini.byod.view.adapters.PalaceInfoAdapter;
import com.arowana.houdini.byod.view.fragments.BaseFragment;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


/**
 * A simple {@link Fragment} subclass.
 */
public class PalaceInfoFragment extends BaseFragment {

    private View mRootView;
    private Context mContext;

    private TextView palaceName;
    private TextView checkinvalue;
    private TextView checkoutValue;
    private ImageView titleImage;
    private ExpandableTextView description;
    private TextView expandBtnTxt;
    private RecyclerView infoListView;
    private PalaceInfoBrands brandObj;
    ProgressDialog mDialog;
    String title;

    public PalaceInfoFragment() {
        // Required empty public constructor
    }

    @Override
    protected int getFragmentLayout() {
        return R.layout.fragment_palace_info;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mContext = getActivity();

    }



    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        mRootView = view;
        mDialog = DialogUtil.showProgressDialog(getActivity(), "");
        mDialog.show();
        setTitle(getString(R.string.title_palaceInfo));
        initView();
        getData();
    }

    private void getData() {


        Call<PalaceInfoConfigObj> obj = ApiClient.getApiClient(PalaceInfoApiInterface.class).fetchAll("2X");
        obj.enqueue(new Callback<PalaceInfoConfigObj>() {
            @Override
            public void onResponse(@NonNull Call<PalaceInfoConfigObj> call, @NonNull Response<PalaceInfoConfigObj> response) {

                PalaceInfoConfigObj body = response.body();
                if (response.isSuccessful() && body != null) {
                    PalaceInfoResponseData responseData = body.getResponseData().first();

                    if (responseData != null) {
                        brandObj = responseData.getBrands().first();
                        if (brandObj != null)
                            populateData(brandObj);
                    }
                } else {
                    Toast.makeText(mContext, "No Data To display..", Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(@NonNull Call<PalaceInfoConfigObj> call, @NonNull Throwable t) {
                if (mDialog.isShowing())
                    mDialog.dismiss();
                t.printStackTrace();
            }
        });

    }

    PalaceInfoHotels hotelObj;

    private void populateData(PalaceInfoBrands obj) {
        hotelObj = obj.getHotels().first();
        if (mDialog.isShowing())
            mDialog.dismiss();
        if (hotelObj != null) {
            palaceName.setText(String.format("%s, %s", hotelObj.getName(), hotelObj.getCity()));
            checkinvalue.setText(hotelObj.getCheckInTime());
            checkoutValue.setText(hotelObj.getCheckOutTime());

            if (hotelObj.getImages() != null &&
                    ! hotelObj.getImages().isEmpty() &&
                    hotelObj.getImages().first().getImageUrl() != null)
                FactoryUtil.loadImage(mContext, hotelObj.getImages().first().getImageUrl(), titleImage);
            if (hotelObj.getDescription() != null) {
                description.setText(hotelObj.getDescription());
                if (hotelObj.getInformation() != null) {
                    infoListView.setAdapter(new PalaceInfoAdapter(mContext, hotelObj, mHandler));
                }
                expandBtnTxt.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        description.toggle();
                        expandBtnTxt.setText(description.isExpanded() ? "Read more" : "Read less");
                    }
                });
            }
        }
    }

    private void initView() {
        titleImage = mRootView.findViewById(R.id.palaceinfo_image);
        palaceName = mRootView.findViewById(R.id.palace_header);
        checkinvalue = mRootView.findViewById(R.id.palace_checkin_time);
        checkoutValue = mRootView.findViewById(R.id.palace_checkout_time);

        description = mRootView.findViewById(R.id.palace_expand_txt);
        expandBtnTxt = mRootView.findViewById(R.id.palace_collapse_txt);
        infoListView = mRootView.findViewById(R.id.palaceinfo_list);

        infoListView.setLayoutManager(new LinearLayoutManager(mContext));
    }


    @Override
    public void onResume() {
        super.onResume();
    }
}