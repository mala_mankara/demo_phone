package com.arowana.houdini.byod.model.offers;

import io.realm.RealmList;
import io.realm.RealmObject;

public class OffersConfigObject extends RealmObject {

    private String RequestData;

    private String IsReadOnly;

    private OffersResponseStatus ResponseStatus;

    private RealmList<OffersResponseData> ResponseData;

    public String getRequestData ()
    {
        return RequestData;
    }

    public void setRequestData (String RequestData)
    {
        this.RequestData = RequestData;
    }

    public String getIsReadOnly ()
    {
        return IsReadOnly;
    }

    public void setIsReadOnly (String IsReadOnly)
    {
        this.IsReadOnly = IsReadOnly;
    }

    public OffersResponseStatus getResponseStatus ()
    {
        return ResponseStatus;
    }

    public void setResponseStatus (OffersResponseStatus ResponseStatus){
        this.ResponseStatus = ResponseStatus;
    }

    public RealmList<OffersResponseData> getResponseData ()
    {
        return ResponseData;
    }

    public void setResponseData (RealmList<OffersResponseData> ResponseData)
    {
        this.ResponseData = ResponseData;
    }

    @Override
    public String toString()
    {
        return "ClassPojo [RequestData = "+RequestData+", IsReadOnly = "+IsReadOnly+", ResponseStatus = "+ResponseStatus+", ResponseData = "+ResponseData+"]";
    }
}
