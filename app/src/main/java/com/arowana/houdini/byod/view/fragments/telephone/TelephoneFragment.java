package com.arowana.houdini.byod.view.fragments.telephone;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.Bundle;
import android.text.InputType;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputConnection;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.arowana.houdini.byod.MainApplication;
import com.arowana.houdini.byod.R;
import com.arowana.houdini.byod.interfaces.ApiClient;
import com.arowana.houdini.byod.interfaces.api_interface.SpeedDialApiInterface;
import com.arowana.houdini.byod.interfaces.listeners.AdapterCallbacks;
import com.arowana.houdini.byod.model.speeddial.SpeedDialConfigObj;
import com.arowana.houdini.byod.model.speeddial.SpeedDialResponseData;
import com.arowana.houdini.byod.utils.DialogUtil;
import com.arowana.houdini.byod.utils.MyKeyboard;
import com.arowana.houdini.byod.utils.NetworkUtils;
import com.arowana.houdini.byod.utils.RealmUtil;
import com.arowana.houdini.byod.view.adapters.SpeedDialAdapter;
import com.arowana.houdini.byod.view.fragments.BaseFragment;

import java.util.Objects;

import alcatel.model.ByodManager;
import io.realm.Realm;
import io.realm.RealmList;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.arowana.houdini.byod.view.activity.TelephoneActivity.fragmentHolder;

/**
 * A simple {@link Fragment} subclass.
 */
public class TelephoneFragment extends BaseFragment implements AdapterCallbacks, View.OnClickListener{

    View mRootView;
    private Context mContext;
    RealmList<SpeedDialResponseData> responseDataList;

    TextView speedDialName;
    EditText speedDialNo;
    ImageView speedDialCall;
    LinearLayout sDDeleteText;
    ImageView speedDialInfo;
    private RecyclerView mGridLayout;
    SpeedDialAdapter adapter;
    /*MyKeyboard keyboard;
    KeyboardView mKeyboardView;*/
    InputConnection inputConnection;
    ProgressDialog mDialog;
    Realm mRealm;

    public TelephoneFragment() {
        // Required empty public constructor
    }

    @Override
    protected int getFragmentLayout() {
        return R.layout.fragment_telephone;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mContext=getActivity();
        setTitle(getResources().getString(R.string.title_activity_telephone));
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        mRootView   = view;
        mDialog = DialogUtil.showProgressDialog(getActivity(), "");
        mDialog.show();
        initComponents();
        //  getActivity().getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
        setKeybrdwithgrid();
        callAPI();


    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
    /*     getActivity().getWindow().setSoftInputMode(
                WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
       final InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(speedDialNo.getWindowToken(), 0);*/
    }

    private void setKeybrdwithgrid() {
        MyKeyboard keyboard = mRootView.findViewById(R.id.keyboard);
        // prevent system keyboard from appearing when EditText is tapped
        speedDialNo.setRawInputType(InputType.TYPE_CLASS_TEXT );
        speedDialNo.setTextIsSelectable(true);

        // pass the InputConnection from the EditText to the keyboard
        inputConnection = speedDialNo.onCreateInputConnection(new EditorInfo());
        keyboard.setInputConnection(inputConnection);

    }

    private void initData(RealmList<SpeedDialResponseData>responseDataList) {


        RecyclerView.LayoutManager mLayoutManager = new GridLayoutManager(mContext, 2);
        mGridLayout.setLayoutManager(mLayoutManager);
        adapter = new SpeedDialAdapter(getContext(), responseDataList,this);
        mGridLayout.setAdapter(adapter);
        adapter.notifyDataSetChanged();
        for(int i=0;i<responseDataList.size();i++ ){
          SpeedDialResponseData data=responseDataList.get(i);
          MainApplication.setSpeedDialKeyValueList(data.getName().toLowerCase(),data.getNumber());
        }
    }

    private void initComponents() {
        speedDialName   = mRootView.findViewById(R.id.speeddial_name);
        speedDialNo     = mRootView.findViewById(R.id.speeddial_no);
        mGridLayout     = mRootView.findViewById(R.id.speeddial_list);
        speedDialCall   = mRootView.findViewById(R.id.speed_call);
        sDDeleteText    = mRootView.findViewById(R.id.speeddial_remove);
        speedDialInfo   = mRootView.findViewById(R.id.speeddial_info);
        speedDialNo.setShowSoftInputOnFocus(false);
        InputMethodManager imm = (InputMethodManager) mContext.getSystemService(Context.INPUT_METHOD_SERVICE);
        Objects.requireNonNull(imm).hideSoftInputFromWindow(speedDialNo.getWindowToken(), InputMethodManager.HIDE_NOT_ALWAYS);
        speedDialCall.setOnClickListener(this);
        sDDeleteText.setOnClickListener(this);
        speedDialInfo.setOnClickListener(this);
        speedDialNo.setInputType(InputType.TYPE_NULL);
    }

    private void callAPI() {
        if (NetworkUtils.checkConnectivityStatus(mContext)) {
            if (mDialog.isShowing())
                mDialog.dismiss();
            Call<SpeedDialConfigObj> speedDialObj = ApiClient.getApiClient(SpeedDialApiInterface.class).getAll();
            speedDialObj.enqueue(new Callback<SpeedDialConfigObj>() {
                @Override
                public void onResponse(@NonNull Call<SpeedDialConfigObj> call,@NonNull Response<SpeedDialConfigObj> response) {
                    SpeedDialConfigObj obj;
                    if (response.isSuccessful() && response.body() != null) {
                        // fetchDetails(response.body().getResponseData());
                        obj=response.body();
                        mRealm = RealmUtil.getInstance();
                        mRealm.beginTransaction();
                        if(mRealm.where(SpeedDialConfigObj.class).equalTo("RealmId","1").count() == 0){
                            mRealm.copyToRealm(obj);
                        }
                        responseDataList = Objects.requireNonNull(mRealm.where(SpeedDialConfigObj.class).findFirst()).getResponseData();
                        mRealm.commitTransaction();
                        initData(responseDataList);
                    } else {
                        Toast.makeText(getContext(), "No Data To display..", Toast.LENGTH_SHORT).show();
                    }
                }

                @Override
                public void onFailure(@NonNull Call<SpeedDialConfigObj> call,@NonNull Throwable t) {
                    t.printStackTrace();
                }
            });
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        Log.d("TELEPHONE.","onResume");
    }

    @Override
    public void onPause() {
        super.onPause();
        Log.d("TELEPHONE.","onPause......");
    }

    @Override
    public void methodCallBacks(SpeedDialResponseData dataObj) {
        speedDialName .setText(dataObj.getName());
        speedDialNo.setText(dataObj.getNumber());
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.speeddial_remove:
                if (inputConnection == null)
                    return;
                CharSequence selectedText = inputConnection.getSelectedText(0);

                if (TextUtils.isEmpty(selectedText)) {
                    inputConnection.deleteSurroundingText(1, 0);
                } else {
                    inputConnection.commitText("", 1);
                }
                if(speedDialNo.getText().length() == 0) {
                    speedDialName.setText("");
                }
                break;

            case R.id.speed_call:
                if(!ByodManager.getInstance().isSipConnected()) {
                    Toast.makeText(mContext,"Call service not working. Please try later..",Toast.LENGTH_SHORT).show();
                }else {
                    String callNo = speedDialNo.getText().toString();
                    if (!callNo.isEmpty()) {
                        PhoneCallFragment fragment = new PhoneCallFragment();
                        Bundle bundle = new Bundle();
                        bundle.putString("phoneNo", callNo);
                        bundle.putString("name", speedDialName.getText().toString());
                        bundle.putBoolean("isMakingCall",true);
                        fragment.setArguments(bundle);

                        mHandler.showFragment(fragmentHolder.getId(), fragment);
                    }
                }
                break;
            case R.id.speeddial_info:
                break;
        }
    }
}

