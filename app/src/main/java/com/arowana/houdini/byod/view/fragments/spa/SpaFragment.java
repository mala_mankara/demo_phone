package com.arowana.houdini.byod.view.fragments.spa;


import android.app.ProgressDialog;
import android.os.Bundle;
import android.view.View;

import androidx.annotation.Nullable;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.arowana.houdini.byod.Consts;
import com.arowana.houdini.byod.R;
import com.arowana.houdini.byod.interfaces.ApiClient;
import com.arowana.houdini.byod.interfaces.api_interface.SpaApiInterface;
import com.arowana.houdini.byod.model.spa.SpaConfigObject;
import com.arowana.houdini.byod.utils.DialogUtil;
import com.arowana.houdini.byod.utils.RealmUtil;
import com.arowana.houdini.byod.utils.ViewUtils;
import com.arowana.houdini.byod.view.adapters.spa.SpaAdapter;
import com.arowana.houdini.byod.view.fragments.BaseFragment;

import io.realm.Realm;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.arowana.houdini.byod.view.activity.SpaActivity.fragmentHolder;

public class SpaFragment extends BaseFragment {


    public SpaFragment() {
    }

    private View mRootView;
    private RecyclerView mRecyclerview;
    private ProgressDialog mDialog;
    private String speedDialData;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        mRootView = view;
        setTitle(getString(R.string.spa));
        mDialog = DialogUtil.showProgressDialog(getActivity(), "");
        mDialog.show();
        initComponents();
        fetchData();
    }

    @Override
    protected int getFragmentLayout() {
        return R.layout.fragment_spa;
    }

    public void initComponents() {
        mRecyclerview = mRootView.findViewById(R.id.spa_main_recyclerview);
    }
    String flag;
    String message;
    SpaConfigObject results;

    public void fetchData() {
        if(results != null){
            if (mDialog.isShowing())
                mDialog.dismiss();
            populateData(results);
        }else {
            Call<SpaConfigObject> responseObj;
            responseObj = ApiClient.getApiClient(SpaApiInterface.class).getAllSpa(Consts.DEVICE_DENSITY);

            responseObj.enqueue(new Callback<SpaConfigObject>() {
                @Override
                public void onResponse(Call<SpaConfigObject> call, Response<SpaConfigObject> response) {
                    if (mDialog.isShowing())
                        mDialog.dismiss();
                    if (response.body() != null) {
                        SpaConfigObject obj = response.body();
                        flag = response.body().getResponseStatus().getResponseFlag();
                        message = response.body().getResponseStatus().getResponseMessage();
                        if (flag.equals("SUCCESS")) {
                            Realm realm = RealmUtil.getInstance();
                            realm.beginTransaction();
                            realm.copyToRealmOrUpdate(obj);
                            realm.commitTransaction();

                            results = realm.where(SpaConfigObject.class).findFirst();
                            if (results != null)
                                populateData(results);
                        }

                    } else ViewUtils.showSnackBarMessage(message);
                }

                @Override
                public void onFailure(Call<SpaConfigObject> call, Throwable t) {
                    if (mDialog.isShowing())
                        mDialog.dismiss();
                    t.printStackTrace();
                }
            });
        }

    }

    public void populateData(SpaConfigObject obj) {

        LinearLayoutManager linearVertical = new LinearLayoutManager(getContext(), RecyclerView.VERTICAL, false);
        mRecyclerview.setLayoutManager(linearVertical);
        SpaAdapter adapter = new SpaAdapter(getContext(), obj.getResponseData(), mHandler, fragmentHolder);
        mRecyclerview.setAdapter(adapter);


    }
}
