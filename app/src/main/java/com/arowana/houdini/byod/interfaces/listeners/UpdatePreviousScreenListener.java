package com.arowana.houdini.byod.interfaces.listeners;

import android.os.Bundle;

public interface UpdatePreviousScreenListener {
    void updatePreviousScreen(Bundle bundle);
}
