package com.arowana.houdini.byod.model.facilitiesAndServices;

import io.realm.RealmObject;

public class FServicesInformation extends RealmObject{

    private String Value;

    private String Field;

    private String Sequence;

    public String getValue ()
    {
        return Value;
    }

    public void setValue (String Value)
    {
        this.Value = Value;
    }

    public String getField ()
    {
        return Field;
    }

    public void setField (String Field)
    {
        this.Field = Field;
    }

    public String getSequence ()
    {
        return Sequence;
    }

    public void setSequence (String Sequence)
    {
        this.Sequence = Sequence;
    }

    @Override
    public String toString()
    {
        return "ClassPojo [Value = "+Value+", Field = "+Field+", Sequence = "+Sequence+"]";
    }
}
