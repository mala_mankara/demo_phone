package com.arowana.houdini.byod.model.redirct;

import io.realm.RealmList;
import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

public class RedirctConfigObj extends RealmObject {

    @PrimaryKey
    private String RealmId = "1";

    private String RequestData;

    private String IsReadOnly;

    private RedirectResponseStatus ResponseStatus;

    private RealmList<RedirectResponseData> ResponseData;

    public String getRequestData ()
    {
        return RequestData;
    }

    public void setRequestData (String RequestData)
    {
        this.RequestData = RequestData;
    }

    public String getIsReadOnly ()
    {
        return IsReadOnly;
    }

    public void setIsReadOnly (String IsReadOnly)
    {
        this.IsReadOnly = IsReadOnly;
    }

    public RedirectResponseStatus getResponseStatus ()
    {
        return ResponseStatus;
    }

    public void setResponseStatus (RedirectResponseStatus ResponseStatus)
    {
        this.ResponseStatus = ResponseStatus;
    }

    public RealmList<RedirectResponseData> getResponseData ()
    {
        return ResponseData;
    }

    public void setResponseData (RealmList<RedirectResponseData> ResponseData)
    {
        this.ResponseData = ResponseData;
    }

    @Override
    public String toString()
    {
        return "ClassPojo [RequestData = "+RequestData+", IsReadOnly = "+IsReadOnly+", ResponseStatus = "+ResponseStatus+", ResponseData = "+ResponseData+"]";
    }
}
