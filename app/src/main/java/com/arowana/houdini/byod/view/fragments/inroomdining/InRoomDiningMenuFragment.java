package com.arowana.houdini.byod.view.fragments.inroomdining;


import android.os.Bundle;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.arowana.houdini.byod.Consts;
import com.arowana.houdini.byod.MainApplication;
import com.arowana.houdini.byod.R;
import com.arowana.houdini.byod.model.inroomdining.IRDiningMenusObj;
import com.arowana.houdini.byod.utils.RealmUtil;
import com.arowana.houdini.byod.view.adapters.ICart;
import com.arowana.houdini.byod.view.adapters.inroomdining.InRoomDiningMenuAdapter;
import com.arowana.houdini.byod.view.fragments.BaseFragment;
import com.arowana.houdini.byod.view.fragments.telephone.PhoneCallFragment;

import alcatel.model.ByodManager;
import io.realm.RealmList;

import static com.arowana.houdini.byod.view.activity.InRoomDiningActivity.fragmentHolder;


public class InRoomDiningMenuFragment extends BaseFragment implements ICart {


    public InRoomDiningMenuFragment() {
        // Required empty public constructor
    }

    private View mRootView;
    private RecyclerView mRecylerView;
    private RealmList<IRDiningMenusObj> mObj;
    private TextView mCartCount;
    private String title;
    private TextView mTotalPrice;
    private LinearLayout mBtnSubmit;
    private View call_conceirge;
    private String speedDialNo;


    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        mRootView = view;

        setTitle(title);
        initComponents();
    }

    @Override
    protected int getFragmentLayout() {
        return R.layout.fragment_in_room_dining_menu;
    }


    public void initComponents() {
        mRecylerView = mRootView.findViewById(R.id.ir_dining_menu_recyclerview);
        mCartCount = mRootView.findViewById(R.id.ir_dining_cart_count);
        mTotalPrice=mRootView.findViewById(R.id.ir_dining_cart_total_price);
        mBtnSubmit=mRootView.findViewById(R.id.btn_ir_dining_submit_menu);

        call_conceirge = mRootView.findViewById(R.id.btn_call_ir_dining);
       // call_conceirge.setVisibility(Consts.APP_CURRENT_STATE > 1 ? View.VISIBLE : View.GONE);
        if(MainApplication.isContainedSpeedDial("Palace Ceremony") && Consts.APP_CURRENT_STATE > 1){
            call_conceirge.setVisibility(View.VISIBLE);
            speedDialNo = MainApplication.getValueforKey();
        }else
            call_conceirge.setVisibility(View.GONE);

        call_conceirge.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(!ByodManager.getInstance().isSipConnected()) {
                    Toast.makeText(getActivity(),"Call service not working. Please try later..",Toast.LENGTH_SHORT).show();
                }else {

                    if (!speedDialNo.isEmpty()) {
                        PhoneCallFragment fragment = new PhoneCallFragment();
                        Bundle bundle = new Bundle();
                        bundle.putString("phoneNo", speedDialNo);
                        bundle.putString("name", "Palace Ceremony");
                        bundle.putBoolean("isMakingCall",true);
                        fragment.setArguments(bundle);
                        mHandler.showFragment(fragmentHolder.getId(), fragment);
                    }
                }
            }
        });


        mBtnSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if(RealmUtil.getDiningCartCount()>0) {
                    IRDiningCartFragment fragment = new IRDiningCartFragment();
                    // fragment.setMenuObj(mMenuItemsObj.get(getAdapterPosition()));
                    mHandler.showFragment(fragmentHolder.getId(), fragment);
                }
                else{
                    Toast.makeText(getContext(), "Please Add items to cart!", Toast.LENGTH_SHORT).show();
                }
            }
        });

        mCartCount.setText(String.valueOf(RealmUtil.getDiningCartCount()));
        mTotalPrice.setText(RealmUtil.getDiningCurrency().concat(" ").concat(String.valueOf(RealmUtil.getDiningTotalPrice())));


        populateData(mObj);


    }

    public void populateData(RealmList<IRDiningMenusObj> obj) {

        LinearLayoutManager linearVertical = new LinearLayoutManager(getContext(), RecyclerView.VERTICAL, false);
        mRecylerView.setLayoutManager(linearVertical);
        InRoomDiningMenuAdapter adapter = new InRoomDiningMenuAdapter(getContext(), this, obj, mHandler, fragmentHolder);
        mRecylerView.setAdapter(adapter);


    }

    public void setMenuObj(RealmList<IRDiningMenusObj> obj,String name) {
        this.mObj = obj;
        this.title=name;

    }

    @Override
    public void updateCartInfo() {
        mCartCount.setText(String.valueOf(RealmUtil.getDiningCartCount()));
        mTotalPrice.setText(RealmUtil.getDiningCurrency().concat(" ").concat(String.valueOf(RealmUtil.getDiningTotalPrice())));
    }


}
