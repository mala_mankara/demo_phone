package com.arowana.houdini.byod.view.adapters;


import android.content.Context;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.TextView;

import com.arowana.houdini.byod.R;
import com.arowana.houdini.byod.interfaces.listeners.FragmentNavigationHandler;
import com.arowana.houdini.byod.model.deviceActivationSecondary.SecondaryUser;
import com.arowana.houdini.byod.model.deviceActivationSecondary.SecondaryUserListData;

import io.realm.Realm;
import io.realm.RealmList;

public class SecondaryUserListAdapter extends RecyclerView.Adapter<SecondaryUserListAdapter.MyViewHolder> {
    private Context context;

    FragmentNavigationHandler mHandler;
    FrameLayout mHolder;
    Realm realm;
    private RealmList<SecondaryUser> userList;
    private Listner mListner;

    public interface Listner {
        void deleteUser(SecondaryUser user);
    }

    public SecondaryUserListAdapter(Listner listner, SecondaryUserListData data, FragmentNavigationHandler handler, FrameLayout holder) {
        userList  = data.getUserAdditionalDevices();
        mListner = listner;
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        TextView mName;
        TextView mMembershipId;
        View mDeleteBtn;

        public MyViewHolder(View view) {
            super(view);
            mName = view.findViewById(R.id.name_text);
            mMembershipId = view.findViewById(R.id.membership_id_text);
            mDeleteBtn = view.findViewById(R.id.delete_button);
        }
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_secondary_user, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull final MyViewHolder holder, int position) {
        final SecondaryUser user = userList.get(position);

        if (user != null) {
            holder.mName.setText(String.format("%s %s", user.getFirstName(), user.getLastName()));
            holder.mMembershipId.setText(user.getMembershipId());

            holder.mDeleteBtn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (mListner != null)
                        mListner.deleteUser(user);
                }
            });
        }
    }

    @Override
    public int getItemCount() {
        return userList.size();
    }
}
