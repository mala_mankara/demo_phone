package com.arowana.houdini.byod.view.fragments.inroomdining;


import android.app.ProgressDialog;
import android.os.Bundle;
import android.view.View;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.arowana.houdini.byod.Consts;
import com.arowana.houdini.byod.R;
import com.arowana.houdini.byod.interfaces.ApiClient;
import com.arowana.houdini.byod.interfaces.api_interface.InRoomDiningApiInterface;
import com.arowana.houdini.byod.model.inroomdining.InRoomDiningConfigObject;
import com.arowana.houdini.byod.utils.DialogUtil;
import com.arowana.houdini.byod.utils.RealmUtil;
import com.arowana.houdini.byod.utils.ViewUtils;
import com.arowana.houdini.byod.view.adapters.inroomdining.InRoomDiningMainAdapter;
import com.arowana.houdini.byod.view.fragments.BaseFragment;

import io.realm.Realm;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.arowana.houdini.byod.view.activity.InRoomDiningActivity.fragmentHolder;


public class InRoomDiningMainFragment extends BaseFragment {


    public InRoomDiningMainFragment() {

    }

    private View mRootView;
    private RecyclerView mRecylerView;
    private ProgressDialog mDialog;
    InRoomDiningConfigObject results;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        mRootView = view;
        mDialog = DialogUtil.showProgressDialog(getActivity(), "");
        mDialog.show();
        setTitle(getString(R.string.title_in_room_dining));
        initComponents();
        fetchData();
    }

    @Override
    protected int getFragmentLayout() {
        return R.layout.fragment_in_room_dining_main;
    }


    public void initComponents() {
        mRecylerView = mRootView.findViewById(R.id.ir_dining_main_recyclerview);
    }

    public void fetchData() {

        if(results != null){
            if (mDialog.isShowing())
                mDialog.dismiss();
            populateData(results);
        }else {
            Call<InRoomDiningConfigObject> responseObj;


            responseObj = ApiClient.getApiClient(InRoomDiningApiInterface.class).getAllDining(Consts.DEVICE_DENSITY);

            responseObj.enqueue(new Callback<InRoomDiningConfigObject>() {
                @Override
                public void onResponse(@NonNull Call<InRoomDiningConfigObject> call, @NonNull Response<InRoomDiningConfigObject> response) {
                    if (mDialog.isShowing())
                        mDialog.dismiss();
                    InRoomDiningConfigObject bodyObj = response.body();
                    if (bodyObj != null) {
                        String flag = bodyObj.getResponseStatus().getResponseFlag();
                        final String message = bodyObj.getResponseStatus().getResponseMessage();
                        if (flag.equals("SUCCESS")) {
                            Realm realm = RealmUtil.getInstance();
                            realm.beginTransaction();
                            realm.copyToRealmOrUpdate(bodyObj);
                            realm.commitTransaction();

                            results = realm.where(InRoomDiningConfigObject.class).findFirst();

                            if (results != null) {
                                populateData(results);
                            }
                        } else ViewUtils.showSnackBarMessage(message);
                    }
                }
                @Override
                public void onFailure(@NonNull Call<InRoomDiningConfigObject> call, @NonNull Throwable t) {
                    if (mDialog.isShowing())
                        mDialog.dismiss();
                    t.printStackTrace();
                }
            });
        }

    }

    public void populateData(InRoomDiningConfigObject obj) {

        LinearLayoutManager linearVertical = new LinearLayoutManager(getContext(), RecyclerView.VERTICAL, false);
        mRecylerView.setLayoutManager(linearVertical);
        InRoomDiningMainAdapter adapter = new InRoomDiningMainAdapter(getContext(), obj.getResponseData().get(0).getRoomDiningInfo(), mHandler, fragmentHolder);
        mRecylerView.setAdapter(adapter);


    }


}
