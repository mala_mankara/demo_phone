package com.arowana.houdini.byod.view.fragments.login;


import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.text.Html;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.arowana.houdini.byod.Consts;
import com.arowana.houdini.byod.R;
import com.arowana.houdini.byod.interfaces.ApiClient;
import com.arowana.houdini.byod.interfaces.api_interface.LoginApiInterface;
import com.arowana.houdini.byod.model.login.LoginConfigObject;
import com.arowana.houdini.byod.utils.JSONUtil;
import com.arowana.houdini.byod.utils.NetworkUtils;
import com.arowana.houdini.byod.utils.RealmUtil;
import com.arowana.houdini.byod.utils.TextUtil;
import com.arowana.houdini.byod.utils.UserInputValidations;
import com.arowana.houdini.byod.utils.ViewUtils;
import com.arowana.houdini.byod.view.activity.HomeActivity;
import com.arowana.houdini.byod.view.fragments.BaseFragment;
import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.FacebookSdk;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.facebook.login.LoginManager;
import com.facebook.login.LoginResult;
import com.google.android.gms.auth.api.Auth;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.auth.api.signin.GoogleSignInResult;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.common.api.Status;
import com.google.android.material.textfield.TextInputLayout;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.Arrays;

import io.realm.Realm;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static android.content.Context.INPUT_METHOD_SERVICE;
import static com.arowana.houdini.byod.view.activity.LoginActivity.fragmentHolder;


public class LoginFragment extends BaseFragment implements View.OnClickListener, GoogleApiClient.OnConnectionFailedListener {


    private static final int RC_SIGN_IN = 007;

    private View mRootView;
    private TextInputLayout mEmail, mPassword;
    private TextView mForgotPassword, mBtnLogin, mSignup;
    private LinearLayout mBtnFacebook, mBtnGoogle;
    private CallbackManager mCallbackManager;
    private GoogleApiClient mGoogleApiClient;
    private Dialog mUserInputDialog;
    private TextView txtUserName;
    private EditText editTextPhoneNumber, editTextEmailId;
    private Button btnSubmit;
    private View lineDialogPhoneNumber, lineDialogEmail;
    private Context mContext;
    private ProgressDialog mDialog;


    private ImageView imgFacebookProfilePic, imgDialogWarningEmailId, imgDialogWarningPhoneNumber;


    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mContext = getActivity();
        initFacebook();
        initGooglePlus();
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        mRootView = view;
        setTitle(getResources().getString(R.string.login));
        initComponents();
        initDialog();
        initListeners();
    }

    public void initComponents() {
        mEmail = mRootView.findViewById(R.id.login_email);
        mPassword = mRootView.findViewById(R.id.login_password);

        mForgotPassword = mRootView.findViewById(R.id.forgot_password);
        mBtnLogin = mRootView.findViewById(R.id.btn_login);

        mBtnFacebook = mRootView.findViewById(R.id.btn_facebook);
        mBtnGoogle = mRootView.findViewById(R.id.btn_google);
        mSignup = mRootView.findViewById(R.id.signup_navigate_txt);

        mEmail.getEditText().setHint(TextUtils.concat(getString(R.string.email), Html.fromHtml(getString(R.string.required_asterisk))));
        mPassword.getEditText().setHint(TextUtils.concat(getString(R.string.password), Html.fromHtml(getString(R.string.required_asterisk))));

    }

    public void initListeners() {
        mForgotPassword.setOnClickListener(this);
        mBtnLogin.setOnClickListener(this);
        mSignup.setOnClickListener(this);

        mBtnFacebook.setOnClickListener(this);
        mBtnGoogle.setOnClickListener(this);

    }

    private void initDialog() {
        mDialog = new ProgressDialog(mContext);
        mDialog.setMessage(getString(R.string.dialog_message));
        mDialog.setCancelable(false);
    }

    private void initFacebook() {
        try {
            FacebookSdk.sdkInitialize(getActivity());
            mCallbackManager = CallbackManager.Factory.create();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void initGooglePlus() {
        try {
            GoogleSignInOptions gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                    .requestEmail()
                    .build();

            mGoogleApiClient = new GoogleApiClient.Builder(getActivity())
                    .enableAutoManage(getActivity(), this)
                    .addApi(Auth.GOOGLE_SIGN_IN_API, gso)
                    .build();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    @Override
    public void onClick(View view) {
        int id = view.getId();
        InputMethodManager imm = (InputMethodManager)getActivity(). getSystemService(INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(mEmail.getWindowToken(), 0);
        switch (id) {
            case R.id.forgot_password:
                mHandler.showFragment(fragmentHolder.getId(), new ForgotPasswordFragment());
                break;

            case R.id.btn_login:
                validate();

                break;

            case R.id.btn_facebook:
                LoginManager.getInstance().logInWithReadPermissions(this, Arrays.asList("public_profile,email"));
                LoginManager.getInstance().registerCallback(mCallbackManager,
                        new FacebookCallback<LoginResult>() {
                            @Override
                            public void onSuccess(LoginResult loginResult) {
                                Log.d("Success", "Login");
                                fetchFacebookUserInfo(loginResult);
                            }

                            @Override
                            public void onCancel() {
                            }

                            @Override
                            public void onError(FacebookException exception) {
                                Log.d("onError", "-- " + exception.getMessage());
                                Toast.makeText(getActivity(), exception.getMessage(), Toast.LENGTH_LONG).show();
                            }
                        });
                break;

            case R.id.btn_google:
                Intent signInIntent = Auth.GoogleSignInApi.getSignInIntent(mGoogleApiClient);
                startActivityForResult(signInIntent, RC_SIGN_IN);
                mGoogleApiClient.connect();
                break;

            case R.id.signup_navigate_txt:
                mHandler.showFragment(fragmentHolder.getId(), new SignupFragment());
                break;


        }

    }

    public void validate() {
        if (!TextUtil.isEmpty(mEmail, "Email is required")) {
            if (!TextUtil.isEmpty(mPassword, "Password is required")) {
                if (TextUtil.isEmailValid(mEmail, "Enter Valid Email")) {
                    if (TextUtil.validatePassword(mPassword, "Enter valid password")) {
                        login();
                    }
                }
            }
        }

    }

    @Override
    public void onResume() {
        super.onResume();
    }

    public void login() {
        JsonObject gsonObject = new JsonObject();

        try {
            JSONObject jsonObject = new JSONObject();
            jsonObject.put("UserId", mEmail.getEditText().getText().toString().trim());
            jsonObject.put("Password", mPassword.getEditText().getText().toString().trim());
            jsonObject.put("MacId", Consts.DEVICE_MAC_ID);

            JsonParser jsonParser = new JsonParser();
            gsonObject = (JsonObject) jsonParser.parse(jsonObject.toString());


            callAPILogin(gsonObject, true);

        } catch (JSONException e) {
            e.printStackTrace();
        }

    }

    private void fetchFacebookUserInfo(LoginResult loginResult) {
        try {
            GraphRequest request = GraphRequest.newMeRequest(
                    loginResult.getAccessToken(),
                    new GraphRequest.GraphJSONObjectCallback() {
                        @Override
                        public void onCompleted(JSONObject graphResponse, GraphResponse response) {
                            try {


                                JSONObject jsonObject = new JSONObject(graphResponse.toString());
                                JSONObject prepareFbReqJson = new JSONObject();
                                String names[] = jsonObject.getString("name").split("\\s+");
                                prepareFbReqJson.put("id", "" + jsonObject.getString("id"));
                                // prepareFbReqJson.put("MacId", Consts.DEVICE_MAC_ID);
                                prepareFbReqJson.put("first_name", "" + names[0]);
                                prepareFbReqJson.put("last_name", names.length > 1 ? "" + names[1] : "Houdini");
                                prepareFbReqJson.put("image_url", "" + "https://graph.facebook.com/" + jsonObject.getString("id") + "/picture?type=large");
                                if (!jsonObject.has("email")) {
                                    showDialog(prepareFbReqJson.toString());
                                } else {

                                    prepareFbReqJson.put("email", "" + jsonObject.getString("email"));
                                    if (NetworkUtils.checkConnectivityStatus(mContext)) {
                                        mDialog.show();
                                        JSONObject facebookRequestJson = requestSocialLogin(prepareFbReqJson, "Facebook");
                                        callAPILogin(JSONUtil.getGsonfromJSON(facebookRequestJson), false);
                                    }


                                }
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                            logOutFacebook();
                        }
                    });
            Bundle parameters = new Bundle();
            parameters.putString("fields", "id,name,email");
            request.setParameters(parameters);
            request.executeAsync();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void fetchGoogleUserInfo(GoogleSignInResult result) {
        try {
            System.out.println("Signin Current Code Status : " + result.getStatus().getStatusCode() + " " + result.getStatus().getStatusMessage());
            if (result.isSuccess()) {
                GoogleSignInAccount acct = result.getSignInAccount();
                System.out.println("ffffffffffffff : " + "Id = " + acct.getId() + "\nEmail = " + acct.getEmail() +
                        "\n First Name = " + acct.getDisplayName() + " ");
                JSONObject prepareFbReqJson = new JSONObject();
                String names[] = acct.getDisplayName().split("\\s+");
                prepareFbReqJson.put("id", "" + acct.getId());
                prepareFbReqJson.put("first_name", "" + names[0]);
                prepareFbReqJson.put("last_name", names.length > 1 ? "" + names[1] : "Houdini");
                if (acct.getPhotoUrl() == null)
                    prepareFbReqJson.put("image_url", "");
                else
                    prepareFbReqJson.put("image_url", acct.getPhotoUrl().toString());
                prepareFbReqJson.put("email", "" + acct.getEmail());

                if (NetworkUtils.checkConnectivityStatus(mContext)) {
                    mDialog.show();
                    JSONObject googleRequestJson = requestSocialLogin(prepareFbReqJson, "Google");
                    callAPILogin(JSONUtil.getGsonfromJSON(googleRequestJson), false);
                }


                logOutGoogle();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private JSONObject requestSocialLogin(JSONObject requestString, String loginType) {
        JSONObject jsonReqObject = requestString;
        try {
            JSONObject jsonPh = new JSONObject();
            jsonPh.put("Type", "Work");
            // Email Object
            JSONObject jsonEmail = new JSONObject();
            jsonEmail.put("PrimaryEmail", "" + jsonReqObject.getString("email"));

            JSONObject jsonObject = new JSONObject();
            jsonObject.put("FirstName", jsonReqObject.getString("first_name"));
            jsonObject.put("LastName", jsonReqObject.getString("last_name"));
            jsonObject.put("LoginType", "" + loginType);
            jsonObject.put("MacId", Consts.DEVICE_MAC_ID);
            jsonObject.put("UserId", "" + jsonReqObject.getString("email"));

            if (!jsonReqObject.getString("email").isEmpty())
                jsonObject.put("EmailId", jsonEmail);
            jsonReqObject = jsonObject;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return jsonReqObject;
    }


    public void callAPILogin(JsonObject jsonObject, final boolean isLogin) {


        Log.d("LoginFragment", "User login json is:" + jsonObject);
        Call<LoginConfigObject> responseObj;

        if (isLogin) {
            responseObj = ApiClient.getApiClient(LoginApiInterface.class).login(jsonObject);
        } else {
            responseObj = ApiClient.getApiClient(LoginApiInterface.class).signup(jsonObject);
        }

        responseObj.enqueue(new Callback<LoginConfigObject>() {
            @Override
            public void onResponse(Call<LoginConfigObject> call, Response<LoginConfigObject> response) {
                if (mDialog.isShowing())
                    mDialog.dismiss();

                String flag = response.body().getResponseStatus().getResponseFlag();
                final String message = response.body().getResponseStatus().getResponseMessage();
                if (flag.equals("SUCCESS")) {

                    LoginConfigObject obj = response.body();
                    if(isLogin) {
                        obj.getResponseData().setPassword(mPassword.getEditText().getText().toString());
                    }
                    Realm realm = RealmUtil.getInstance();
                    realm.beginTransaction();
                    if (realm.where(LoginConfigObject.class).equalTo("RealmId", "1").count() == 0) {
                        realm.copyToRealm(obj);
                    }
                    realm.commitTransaction();

                    System.out.println("requestCode = " + RealmUtil.getUserName());
                    mListener.changeAppState(Consts.APP_LOGIN_STATE);



                    Intent intent = new Intent(getContext(), HomeActivity.class);
                    intent.putExtra("login_message",message);
                    getActivity().finishAffinity();
                    startActivity(intent);


                } else ViewUtils.showSnackBarMessage(message);
            }

            @Override
            public void onFailure(Call<LoginConfigObject> call, Throwable t) {
                t.printStackTrace();
            }
        });
    }


    @Override
    public void onActivityResult(final int requestCode, final int resultCode, final Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        mCallbackManager.onActivityResult(requestCode, resultCode, data);

        if (requestCode == RC_SIGN_IN) {
            GoogleSignInResult result = Auth.GoogleSignInApi.getSignInResultFromIntent(data);
            fetchGoogleUserInfo(result);
        }
    }

    private void logOutGoogle() {
        try {
            Auth.GoogleSignInApi.signOut(mGoogleApiClient).setResultCallback(
                    new ResultCallback<Status>() {
                        @Override
                        public void onResult(Status status) {
                        }
                    });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void logOutFacebook() {
        try {
            LoginManager.getInstance().logOut();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    private void showDialog(String response) {
        mUserInputDialog = new Dialog(getActivity(), android.R.style.Theme_Dialog);
        mUserInputDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        mUserInputDialog.setContentView(R.layout.dialog_phone_no_request);
        mUserInputDialog.setCanceledOnTouchOutside(false);
        txtUserName = mUserInputDialog.findViewById(R.id.txtUserName);
        imgFacebookProfilePic = mUserInputDialog.findViewById(R.id.imgFacebookProfilePic);
        editTextEmailId = mUserInputDialog.findViewById(R.id.editTextEmailId);
        editTextPhoneNumber = mUserInputDialog.findViewById(R.id.editTextPhoneNumber);
        imgDialogWarningEmailId = mUserInputDialog.findViewById(R.id.imgDialogWarningEmailId);
        imgDialogWarningPhoneNumber = mUserInputDialog.findViewById(R.id.imgDialogWarningPhoneNumber);
        btnSubmit = mUserInputDialog.findViewById(R.id.btnSubmit);
        lineDialogEmail = mUserInputDialog.findViewById(R.id.lineDialogEmail);
        lineDialogPhoneNumber = mUserInputDialog.findViewById(R.id.lineDialogPhoneNumber);
        try {
            final JSONObject jsonObject = new JSONObject(response);
            String userName = jsonObject.getString("first_name");
            txtUserName.setText("" + userName);
            Glide.with(mContext).load(jsonObject.getString("image_url")).apply(new RequestOptions()
                    .placeholder(R.drawable.ic_default_profile_pic)
                    .fitCenter());

            btnSubmit.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    try {
                        imgDialogWarningEmailId.setVisibility(editTextEmailId.getText().toString().isEmpty() ||
                                !UserInputValidations.isEmailValid(editTextEmailId) ? View.VISIBLE : View.GONE);

                        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                            lineDialogPhoneNumber.setBackgroundColor(getResources().getColor(
                                    !editTextPhoneNumber.getText().toString().isEmpty() || UserInputValidations.isValidMobile(editTextPhoneNumber.getText().toString().trim()) ?
                                            R.color.zuri_text_input_line : R.color.error_line_red,
                                    getActivity().getTheme()));

                            lineDialogEmail.setBackgroundColor(getResources().getColor(
                                    !editTextEmailId.getText().toString().isEmpty() || UserInputValidations.isEmailValid(editTextEmailId) ?
                                            R.color.zuri_text_input_line : R.color.error_line_red,
                                    getActivity().getTheme()));

                        } else {
                            lineDialogPhoneNumber.setBackgroundColor(getResources().getColor(
                                    !editTextPhoneNumber.getText().toString().isEmpty() || UserInputValidations.isValidMobile(editTextPhoneNumber.getText().toString().trim()) ?
                                            R.color.zuri_text_input_line : R.color.error_line_red));

                            lineDialogEmail.setBackgroundColor(getResources().getColor(
                                    !editTextEmailId.getText().toString().isEmpty() || UserInputValidations.isEmailValid(editTextEmailId) ?
                                            R.color.zuri_text_input_line : R.color.error_line_red));
                        }

                        if ((!editTextEmailId.getText().toString().isEmpty() &&
                                UserInputValidations.isEmailValid(editTextEmailId) ||
                                (!editTextPhoneNumber.getText().toString().isEmpty() &&
                                        UserInputValidations.isValidMobile(editTextPhoneNumber.getText().toString().trim())))) {
                            JSONObject prepareFbReqJson = new JSONObject();
                            prepareFbReqJson.put("id", "" + jsonObject.getString("id"));
                            prepareFbReqJson.put("first_name", "" + jsonObject.getString("first_name"));
                            prepareFbReqJson.put("last_name", "" + jsonObject.getString("last_name"));
                            prepareFbReqJson.put("image_url", "" + "https://graph.facebook.com/" + jsonObject.getString("id") + "/picture?type=large");
                            prepareFbReqJson.put("email", "" + editTextEmailId.getText().toString());
                            prepareFbReqJson.put("phone_number", "" + editTextPhoneNumber.getText().toString());
                            JSONObject facebookRequestJson = requestSocialLogin(prepareFbReqJson, "Facebook");
                            System.out.println("facebookRequestJson" + facebookRequestJson);
                        } else {

                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }

            });
        } catch (Exception e) {
            e.printStackTrace();
        }
        mUserInputDialog.show();
    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {

    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        mGoogleApiClient.stopAutoManage(getActivity());
        mGoogleApiClient.disconnect();
    }


    @Override
    protected int getFragmentLayout() {
        return R.layout.fragment_login;
    }
}
