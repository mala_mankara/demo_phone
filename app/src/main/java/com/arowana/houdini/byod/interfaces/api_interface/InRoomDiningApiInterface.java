package com.arowana.houdini.byod.interfaces.api_interface;

import com.arowana.houdini.byod.model.inroomdining.IRDiningBookingConfigObj;
import com.arowana.houdini.byod.model.inroomdining.InRoomDiningConfigObject;
import com.google.gson.JsonObject;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.POST;

public interface InRoomDiningApiInterface {

    @FormUrlEncoded
    @POST("RoomDining/GetAllRoomDiningMenu")
    Call<InRoomDiningConfigObject> getAllDining(@Field("ImageSize") String imageSize);

    @POST("InRoomDiningKOT/CreateKOT")
    Call<IRDiningBookingConfigObj> submitOrder(@Body JsonObject submitJson);






}
