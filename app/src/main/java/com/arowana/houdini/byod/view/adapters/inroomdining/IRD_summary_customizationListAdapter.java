package com.arowana.houdini.byod.view.adapters.inroomdining;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.arowana.houdini.byod.R;
import com.arowana.houdini.byod.model.inroomdining.IRDiningBookingItemCustomzationObj;

import io.realm.RealmList;

public class IRD_summary_customizationListAdapter extends RecyclerView.Adapter<IRD_summary_customizationListAdapter.MyViewHolder>{

    private RealmList<IRDiningBookingItemCustomzationObj> listObj;

    IRD_summary_customizationListAdapter( RealmList<IRDiningBookingItemCustomzationObj> obj) {
        listObj = obj;

    }
    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.summary_itemvalue_layout, parent, false);
        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, int position) {

        IRDiningBookingItemCustomzationObj customobj = listObj.get(position);
        if(customobj != null) {
            holder.name.setText(customobj.getCustomizeItemName());
            holder.value.setText(customobj.getCustomType().first().getCustomTypeName());
        }
    }

    @Override
    public int getItemCount() {
        return listObj.size() ;
    }


    class MyViewHolder extends RecyclerView.ViewHolder{
        TextView name, value;

        MyViewHolder(View itemView) {
            super(itemView);
            name = itemView.findViewById(R.id.ird_summary_customize_name);
            value = itemView.findViewById(R.id.ird_summary_customize_value);
        }
    }
}
