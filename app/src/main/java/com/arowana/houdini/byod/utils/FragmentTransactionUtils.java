package com.arowana.houdini.byod.utils;

import android.content.Context;
import androidx.fragment.app.FragmentTransaction;


public class FragmentTransactionUtils {
    private static FragmentTransactionUtils INSTANCE;

    public static final String DEFAULT = "default";
    public static final String TYPE1 = "type1";
    public static final String TYPE2 = "type2";
    public static final String TYPE3 = "type3";
    public static final String TYPE4 = "type4";

    public static final String TYPE5 = "type5";

    private Context mContext;

    private int mEnterAnimation;
    private int mExitAnimation;
    private int mPopEnterAnimation;
    private int mPopExitAnimation;


    private FragmentTransactionUtils() {

    }


    public static FragmentTransactionUtils getInstance() {
        if (INSTANCE == null) {
            INSTANCE = new FragmentTransactionUtils();
        }
        return INSTANCE;
    }

    public void setContext(Context context) {
        mContext = context;
    }

    public void setTransaction(FragmentTransaction transaction) {
        setTransaction(transaction, DEFAULT);
    }

    public void setTransaction(FragmentTransaction transaction, String type) {


        switch (type) {
            case TYPE1:
                // break;
            case TYPE2:
                // break;

            case TYPE3:
                // break;
            case TYPE4:
//                mEnterAnimation = R.anim.enter_from_right;
//                mExitAnimation =0;
//                mPopEnterAnimation =  R.anim.enter_from_left;
//                mPopExitAnimation = 0;
                break;

        }
        transaction.setCustomAnimations(mEnterAnimation,mExitAnimation,mPopEnterAnimation,mPopExitAnimation);



    }


}
