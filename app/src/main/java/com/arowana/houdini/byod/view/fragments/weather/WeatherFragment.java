package com.arowana.houdini.byod.view.fragments.weather;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.arowana.houdini.byod.Consts;
import com.arowana.houdini.byod.R;
import com.arowana.houdini.byod.interfaces.ApiClient;
import com.arowana.houdini.byod.interfaces.api_interface.WeatherApiInterface;
import com.arowana.houdini.byod.model.weather.Forecast;
import com.arowana.houdini.byod.model.weather.Location;
import com.arowana.houdini.byod.model.weather.Weather;
import com.arowana.houdini.byod.model.weather.WeatherConfigObject;
import com.arowana.houdini.byod.model.weather.WeatherResponseDataObject;
import com.arowana.houdini.byod.utils.DialogUtil;
import com.arowana.houdini.byod.utils.FactoryUtil;
import com.arowana.houdini.byod.view.adapters.WeatherAdapter;
import com.arowana.houdini.byod.view.fragments.BaseFragment;

import java.util.ArrayList;

import io.realm.RealmList;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * A simple {@link Fragment} subclass.
 */
public class WeatherFragment extends BaseFragment implements View.OnClickListener {

    public static final String TAG = "WeatherFragment";
    private View mRootView;
    private RealmList<Forecast> forecastDataObj;
    private Weather.Current currentDataObj;
    private Location locationDataObj;

    private TextView placeName;
    private ImageView weather_state;
    private TextView cTemp;
    private TextView minTemp;
    private TextView maxTemp;
    private TextView cState;
    ProgressDialog mDialog;

    RecyclerView recyclerView;
    public String unit;
    ArrayList<String>dayOfTheWeek;

    ImageView switchImage;
    Context mContext;
    public static boolean isCelUnit=false;

    public WeatherFragment() {
        // Required empty public constructor
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        mRootView=view;
        mDialog = DialogUtil.showProgressDialog(getActivity(), "");
        mDialog.show();
        initView();
        getData();



    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mContext = getActivity();
        setTitle(getResources().getString(R.string.weather));
    }

    @Override
    protected int getFragmentLayout() {
        return R.layout.weatherlayout;
    }

    private void initView() {
        placeName=mRootView.findViewById(R.id.placename);
        weather_state=mRootView.findViewById(R.id.state);
        cTemp=mRootView.findViewById(R.id.c_temp);
        minTemp=mRootView.findViewById(R.id.mintemp);
        maxTemp=mRootView.findViewById(R.id.maxtemp);
        cState=mRootView.findViewById(R.id.status);
        switchImage=mRootView.findViewById(R.id.switchbet);
        recyclerView = mRootView.findViewById(R.id.recycleview);
    }

    public void getData()  {
        final Call<WeatherConfigObject> weatherObject = ApiClient.getApiClient(WeatherApiInterface.class).weatherData();
        weatherObject.enqueue(new Callback<WeatherConfigObject>() {
            @Override
            public void onResponse(@NonNull Call<WeatherConfigObject> call, @NonNull Response<WeatherConfigObject> response) {
                if (mDialog.isShowing())
                    mDialog.dismiss();
                if(response.isSuccessful() && response.body() != null) {
                    if (response.body() == null) throw new AssertionError();
                    if(response.body().getResponseData()!=null)
                    fetchData(response.body().getResponseData());
                    else
                        Toast.makeText(getContext(),"No Data To display..",Toast.LENGTH_SHORT).show();
                }
                else{
                    Toast.makeText(getContext(),"No Data To display..",Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(@NonNull Call<WeatherConfigObject> call, @NonNull Throwable t) {
                if (mDialog.isShowing())
                    mDialog.dismiss();
                t.printStackTrace();
            }
        });
    }

    private void inflateData() {
        dayOfTheWeek = new ArrayList<>();
        placeName.setText(locationDataObj.getCity());
        cTemp.setText(currentDataObj.getTemperature());
        minTemp.setText(currentDataObj.getMinimum());
        maxTemp.setText(currentDataObj.getMaximum());
        FactoryUtil.loadImage(getContext(),currentDataObj.getIconImage(),weather_state);

        cState.setText(currentDataObj.getTitle());
        if(!unit.isEmpty() && unit.equalsIgnoreCase(Consts.WEATHER_TEMP_CELUNIT)) {
            isCelUnit=true;
            switchImage.setBackgroundResource(R.drawable.celcious_icon);
        }else {
            isCelUnit=false;
            switchImage.setBackgroundResource(R.drawable.farade_icon);
        }
        switchImage.setOnClickListener(this);


        LinearLayoutManager layoutManager = new LinearLayoutManager(getContext());
        layoutManager.setOrientation(LinearLayoutManager.HORIZONTAL);
        recyclerView.setLayoutManager(layoutManager);

        WeatherAdapter adapter = new WeatherAdapter(getContext(),forecastDataObj);
        recyclerView.setAdapter(adapter);

    }

    private void fetchData(WeatherResponseDataObject responseData) {
        if(responseData.getUnit()!=null && responseData.getLocation()!= null && responseData.getWeather()!=null ) {
            unit = responseData.getUnit();
            locationDataObj = responseData.getLocation();
            Weather weatherDataObj = responseData.getWeather();
            if (weatherDataObj.getCurrent() != null) {
                currentDataObj = weatherDataObj.getCurrent();
                for (int i = 0; i < weatherDataObj.getForecast().size(); i++)
                    forecastDataObj = weatherDataObj.getForecast();
                inflateData();
            }
        }
        Toast.makeText(getContext(),"No Data To display..",Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onClick(View v) {
        if(v.getId()==R.id.switchbet){
            if(isCelUnit){
                switchImage.setBackgroundResource(R.drawable.farade_icon);
                callAPI(Consts.WEATHER_TEMP_FAHRENUNIT);
                isCelUnit=false;
            }else{
                switchImage.setBackgroundResource(R.drawable.celcious_icon);
                callAPI(Consts.WEATHER_TEMP_CELUNIT);
                isCelUnit=true;
            }
        }
    }

    private void callAPI(String weatherTempCelunit) {
        Call<WeatherConfigObject> weatherObj=ApiClient.getApiClient(WeatherApiInterface.class).weatherData(weatherTempCelunit);
        weatherObj.enqueue(new Callback<WeatherConfigObject>() {
            @Override
            public void onResponse(@NonNull Call<WeatherConfigObject> call, @NonNull Response<WeatherConfigObject> response) {
                Log.d(TAG,"response Code"+response.code() );
                if(response.isSuccessful() && response.body() != null) {
                    if (response.body() == null) throw new AssertionError();
                    fetchData(response.body().getResponseData());
                }
                else{
                    Toast.makeText(getContext(),"No Data To display..",Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(@NonNull Call<WeatherConfigObject> call, @NonNull Throwable t) {
                t.printStackTrace();
            }
        });
    }
}



