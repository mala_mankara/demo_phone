package com.arowana.houdini.byod.view.fragments;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.fragment.app.DialogFragment;
import androidx.viewpager.widget.PagerAdapter;
import androidx.viewpager.widget.ViewPager;

import com.arowana.houdini.byod.R;
import com.google.android.material.tabs.TabLayout;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;


public class OnBoardingFragment extends DialogFragment implements View.OnClickListener {

    private View mRootView;
    private ViewPager pagerCityGuide;
    private TabLayout tabDots;
    private ArrayList<Integer> listBoardingImages;
    private ArrayList<String> mTitle;
    private ArrayList<String> mDesc;

    private ImageView imgCloseBtn;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        listBoardingImages = new ArrayList<>();

      //  listBoardingImages.add(R.drawable.onboarding_mobile_check);
        listBoardingImages.add(R.drawable.onboarding_inroom_dining);
        listBoardingImages.add(R.drawable.onboarding_house_keeping);

        listBoardingImages.add(R.drawable.onboarding_checkout);

        mDesc = new ArrayList<>();

      //  mDesc.add("Select your stay and food preferences \nbefore checking in");
        mDesc.add("View Add to Cart and Confirm.\nThat's all it takes to place an order.");
        mDesc.add("Extra pillows, phone charger, MMR or \nany other hotel services can be easily \nrequested");
        mDesc.add("View bill and quickly initiate check out \nprocess through your device");

        mTitle = new ArrayList<>();

    //    mTitle.add("My Preferences");
        mTitle.add("In-Room Dining");
        mTitle.add("Service Request");
        mTitle.add("Initiate Check out");

    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        Dialog dialog = super.onCreateDialog(savedInstanceState);
        dialog.getWindow().requestFeature(Window.FEATURE_NO_TITLE);
        return dialog;
    }

    @Override
    public void onStart() {
        super.onStart();
        Dialog dialog = getDialog();
        if (dialog != null) {
            dialog.getWindow().setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);
            dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup viewGroup, Bundle savedBundle) {
        mRootView = inflater.inflate(R.layout.fragment_onboarding_pager, null);
        initComponents();
        return mRootView;
    }

    private void initComponents() {
        imgCloseBtn = mRootView.findViewById(R.id.imgCloseBtn);
        pagerCityGuide = mRootView.findViewById(R.id.pagerCityGuide);
        tabDots = mRootView.findViewById(R.id.tabDots);
        initListeners();
    }

    private void initListeners() {
        imgCloseBtn.setOnClickListener(this);
        pagerCityGuide.setAdapter(new ImageAdapter(getActivity(), listBoardingImages));
        tabDots.setupWithViewPager(pagerCityGuide, true);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.imgCloseBtn:
                getActivity().finish();
                break;
        }
    }

    class ImageAdapter extends PagerAdapter {

        private Context mContext;
        private LayoutInflater mLayoutInflator;
        private ArrayList<Integer> mImages;

        public ImageAdapter(Context context, ArrayList<Integer> mImages) {
            this.mContext = context;
            this.mImages = mImages;
            mLayoutInflator = ((Activity) mContext).getLayoutInflater();
        }

        @Override
        public int getCount() {
            return mImages.size();
        }

        @Override
        public boolean isViewFromObject(View view, Object object) {
            return view == object;
        }

        @Override
        public Object instantiateItem(ViewGroup container, int position) {
            View view = getActivity().getLayoutInflater().inflate(R.layout.onboarding_item_slider, null);
            ImageView image = view.findViewById(R.id.placeImage);
            TextView title = view.findViewById(R.id.onboarding_title);
            TextView desc = view.findViewById(R.id.onboarding_description);

            Picasso.get().load(mImages.get(position)).fit().into(image);

            /*Glide.with(mContext).load(mImages.get(position)).into(image);*/

            title.setText(mTitle.get(position));
            desc.setText(mDesc.get(position));
            container.addView(view);
            return view;
        }

        @Override
        public void destroyItem(ViewGroup container, int position, Object object) {
            container.removeView((FrameLayout) object);
        }

    }

}