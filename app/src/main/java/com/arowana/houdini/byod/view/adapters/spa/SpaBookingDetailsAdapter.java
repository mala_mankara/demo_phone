package com.arowana.houdini.byod.view.adapters.spa;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.arowana.houdini.byod.R;
import com.arowana.houdini.byod.model.spa.SpaBookingTreatmentTypeObj;

import io.realm.RealmList;

public class SpaBookingDetailsAdapter extends RecyclerView.Adapter<SpaBookingDetailsAdapter.MyViewHolder> {


    private Context context;
    private RealmList<SpaBookingTreatmentTypeObj> mObj;


    public class MyViewHolder extends RecyclerView.ViewHolder {
        TextView treatmentType;
        RecyclerView mTreatmentList;


        public MyViewHolder(View view) {
            super(view);
            treatmentType = view.findViewById(R.id.spa_booking_treatment_type_name);
            mTreatmentList=view.findViewById(R.id.spa_booking_treatment_list);
        }


    }

    public SpaBookingDetailsAdapter(Context mContext, RealmList<SpaBookingTreatmentTypeObj> obj) {
        this.context = mContext;
        this.mObj=obj;
        Log.d("SpaBD_Adapter","obj size"+obj.size());
    }



    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_spa_booking_detail, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, int position) {
       final SpaBookingTreatmentTypeObj bean = mObj.get(position);
        holder.treatmentType.setText(bean.getTreatmentType());


        LinearLayoutManager linearVertical = new LinearLayoutManager(context, RecyclerView.VERTICAL, false);
        holder.mTreatmentList.setLayoutManager(linearVertical);

        SpaBookingTreatmentsAdapter adapter = new SpaBookingTreatmentsAdapter(context,bean.getTreatment());
        holder.mTreatmentList.setAdapter(adapter);


    }


    @Override
    public int getItemCount() {
        return mObj.size();
    }


}
