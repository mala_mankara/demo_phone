package com.arowana.houdini.byod.model.cityguide;

import io.realm.RealmList;
import io.realm.RealmObject;

public class CityGuideConfigObj extends RealmObject {

    private String RequestData;

    private String IsReadOnly;

    private CityguideResponseStatusObj ResponseStatus;

    private RealmList<CityguideResponseDataObj> ResponseData;


    public String getRequestData() {
        return RequestData;
    }

    public void setRequestData(String requestData) {
        RequestData = requestData;
    }

    public String getIsReadOnly() {
        return IsReadOnly;
    }

    public void setIsReadOnly(String isReadOnly) {
        IsReadOnly = isReadOnly;
    }

    public CityguideResponseStatusObj getResponseStatus() {
        return ResponseStatus;
    }

    public void setResponseStatus(CityguideResponseStatusObj responseStatus) {
        ResponseStatus = responseStatus;
    }

    public RealmList<CityguideResponseDataObj> getResponseData() {
        return ResponseData;
    }

    public void setResponseData(RealmList<CityguideResponseDataObj> responseData) {
        ResponseData = responseData;
    }
}
