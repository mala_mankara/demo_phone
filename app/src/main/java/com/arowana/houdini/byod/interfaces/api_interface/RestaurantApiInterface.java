package com.arowana.houdini.byod.interfaces.api_interface;

import com.arowana.houdini.byod.model.restaurant.RestaurantConfigObject;

import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.POST;

public interface RestaurantApiInterface {

    @FormUrlEncoded
    @POST("Restaurant/GetAll")
    Call<RestaurantConfigObject> getAllRestaurant(@Field("ImageSize") String imageSize, @Field("MenuSize") String menuSize);




}
