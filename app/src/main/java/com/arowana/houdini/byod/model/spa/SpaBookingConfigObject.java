package com.arowana.houdini.byod.model.spa;

import io.realm.RealmObject;

public class SpaBookingConfigObject extends RealmObject{

    private SpaResponseStatusObj ResponseStatus;

    private SpaBookingObject ResponseData;

    public SpaResponseStatusObj getResponseStatus() {
        return ResponseStatus;
    }

    public void setResponseStatus(SpaResponseStatusObj responseStatus) {
        ResponseStatus = responseStatus;
    }

    public SpaBookingObject getResponseData() {
        return ResponseData;
    }

    public void setResponseData(SpaBookingObject responseData) {
        ResponseData = responseData;
    }
}
