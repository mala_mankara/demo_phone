package com.arowana.houdini.byod.model.inroomdining;

import io.realm.RealmList;
import io.realm.RealmObject;

public class IRDiningOrderLineItemsObj extends RealmObject{


    private String IsUpSellItem;

    private String ItemCode;

    private String Description;

    private String CategoryCode;

    private String OrderStatus;

    private String CourseNumber;

    private String KotComment;

    private String MenuCode;

    private String OrderedQuantity;

    private RealmList<IRDiningBookingItemCustomzationObj> ItemCustomzation;

    private RealmList<IRDiningAddonsObj> AddOns;

    private String MenuName;

    private String IsCrossSellItem;

    private String ItemName;

    private String Price;

    private String CoverNumber;

    private String DietType;

    public String getIsUpSellItem ()
    {
        return IsUpSellItem;
    }

    public void setIsUpSellItem (String IsUpSellItem)
    {
        this.IsUpSellItem = IsUpSellItem;
    }

    public String getItemCode ()
    {
        return ItemCode;
    }

    public void setItemCode (String ItemCode)
    {
        this.ItemCode = ItemCode;
    }

    public String getDescription ()
    {
        return Description;
    }

    public void setDescription (String Description)
    {
        this.Description = Description;
    }

    public String getCategoryCode ()
    {
        return CategoryCode;
    }

    public void setCategoryCode (String CategoryCode)
    {
        this.CategoryCode = CategoryCode;
    }

    public String getOrderStatus ()
    {
        return OrderStatus;
    }

    public void setOrderStatus (String OrderStatus)
    {
        this.OrderStatus = OrderStatus;
    }

    public String getCourseNumber ()
    {
        return CourseNumber;
    }

    public void setCourseNumber (String CourseNumber)
    {
        this.CourseNumber = CourseNumber;
    }

    public String getKotComment ()
    {
        return KotComment;
    }

    public void setKotComment (String KotComment)
    {
        this.KotComment = KotComment;
    }

    public String getMenuCode ()
    {
        return MenuCode;
    }

    public void setMenuCode (String MenuCode)
    {
        this.MenuCode = MenuCode;
    }

    public String getOrderedQuantity ()
    {
        return OrderedQuantity;
    }

    public void setOrderedQuantity (String OrderedQuantity)
    {
        this.OrderedQuantity = OrderedQuantity;
    }

    public RealmList<IRDiningBookingItemCustomzationObj> getItemCustomzation ()
    {
        return ItemCustomzation;
    }

    public void setItemCustomzation (RealmList<IRDiningBookingItemCustomzationObj> ItemCustomzation)
    {
        this.ItemCustomzation = ItemCustomzation;
    }

    public RealmList<IRDiningAddonsObj> getAddOns ()
    {
        return AddOns;
    }

    public void setAddOns (RealmList<IRDiningAddonsObj> AddOns)
    {
        this.AddOns = AddOns;
    }

    public String getMenuName ()
    {
        return MenuName;
    }

    public void setMenuName (String MenuName)
    {
        this.MenuName = MenuName;
    }

    public String getIsCrossSellItem ()
    {
        return IsCrossSellItem;
    }

    public void setIsCrossSellItem (String IsCrossSellItem)
    {
        this.IsCrossSellItem = IsCrossSellItem;
    }

    public String getItemName ()
    {
        return ItemName;
    }

    public void setItemName (String ItemName)
    {
        this.ItemName = ItemName;
    }

    public String getPrice ()
    {
        return Price;
    }

    public void setPrice (String Price)
    {
        this.Price = Price;
    }

    public String getCoverNumber ()
    {
        return CoverNumber;
    }

    public void setCoverNumber (String CoverNumber)
    {
        this.CoverNumber = CoverNumber;
    }

    public String getDietType ()
    {
        return DietType;
    }

    public void setDietType (String DietType)
    {
        this.DietType = DietType;
    }

    @Override
    public String toString()
    {
        return "ClassPojo [IsUpSellItem = "+IsUpSellItem+", ItemCode = "+ItemCode+", Description = "+Description+", CategoryCode = "+CategoryCode+", OrderStatus = "+OrderStatus+", CourseNumber = "+CourseNumber+", KotComment = "+KotComment+", MenuCode = "+MenuCode+", OrderedQuantity = "+OrderedQuantity+", ItemCustomzation = "+ItemCustomzation+", AddOns = "+AddOns+", MenuName = "+MenuName+", IsCrossSellItem = "+IsCrossSellItem+", ItemName = "+ItemName+", Price = "+Price+", CoverNumber = "+CoverNumber+", DietType = "+DietType+"]";
    }
}

