package com.arowana.houdini.byod.view.fragments.cityguide;


import android.app.ProgressDialog;
import android.os.Bundle;
import android.view.View;

import androidx.viewpager.widget.ViewPager;

import com.arowana.houdini.byod.Consts;
import com.arowana.houdini.byod.R;
import com.arowana.houdini.byod.interfaces.ApiClient;
import com.arowana.houdini.byod.interfaces.api_interface.CityGuideApiInterface;
import com.arowana.houdini.byod.model.cityguide.CityGuideConfigObj;
import com.arowana.houdini.byod.model.cityguide.CityguideResponseDataObj;
import com.arowana.houdini.byod.utils.DialogUtil;
import com.arowana.houdini.byod.utils.ViewUtils;
import com.arowana.houdini.byod.view.adapters.cityguide.CityGuidePagerAdapter;
import com.arowana.houdini.byod.view.fragments.BaseFragment;
import com.google.android.material.tabs.TabLayout;

import io.realm.RealmList;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class CityGuideViewPagerFragment extends BaseFragment {


    private View mRootView;



    CityGuidePagerAdapter mAdapter;

    ViewPager mViewPager;
    TabLayout tabLayout;
    private ProgressDialog mDialog;


    public static CityGuideViewPagerFragment newInstance() {
        return new CityGuideViewPagerFragment();
    }


    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        mRootView = view;
        mDialog = DialogUtil.showProgressDialog(getActivity(), "");
        mDialog.show();
        setTitle(getString(R.string.cityguide));
        initComponents();

        fetchData();
    }


    private void initComponents() {


        tabLayout = mRootView.findViewById(R.id.cityguide_tab_layout);
        mViewPager = mRootView.findViewById(R.id.cityguide_viewpager);

    }


    @Override
    protected int getFragmentLayout() {
        return R.layout.fragment_city_guide_view_pager;
    }

    public void fetchData(){
        Call<CityGuideConfigObj> responseObj;



        responseObj = ApiClient.getApiClient(CityGuideApiInterface.class).getAllCityGuide(Consts.DEVICE_DENSITY);

        responseObj.enqueue(new Callback<CityGuideConfigObj>() {
            @Override
            public void onResponse(Call<CityGuideConfigObj> call, Response<CityGuideConfigObj> response) {
                if (mDialog.isShowing())
                 mDialog.dismiss();

                String flag = response.body().getResponseStatus().getResponseFlag();
                final String message = response.body().getResponseStatus().getResponseMessage();
                if (flag.equals("SUCCESS")) {
                    CityGuideConfigObj obj = response.body();
                   try {
                       populateData(obj);
                   }catch (Exception ex){
                       ex.printStackTrace();
                   }
                } else ViewUtils.showSnackBarMessage(message);
            }

            @Override
            public void onFailure(Call<CityGuideConfigObj> call, Throwable t) {
                if (mDialog.isShowing())
                    mDialog.dismiss();
            }
        });


    }

    public void populateData(CityGuideConfigObj obj){
        RealmList<CityguideResponseDataObj> cityguideObj=obj.getResponseData();
        mAdapter = new CityGuidePagerAdapter(getChildFragmentManager(),cityguideObj);
        mViewPager.setAdapter(mAdapter);
        tabLayout.setupWithViewPager(mViewPager);
    }


}
