package com.arowana.houdini.byod.model.spa;

import io.realm.RealmList;
import io.realm.RealmObject;

public class SpaBookingObject extends RealmObject {

    private String AdditionalNote;

    private String BookingId;

    private String ActivityId;

    private String Device;

    private String RoomNo;

    private String ModifiedBy;

    private String SpaName;

    private RealmList<SpaBookingTreatmentTypeObj> TreatmentType;

    private String ModifiedDate;

    private String SpaId;

    private String HotelProperty;

    private String CreatedDate;

    private String CreatedBy;

    private String Id;

    private String MessageCode;

    private String Version;

    private String Tenant;

    private String ReferenceId;

    private SpaGuestProfileObject GuestProfile;

    public String getAdditionalNote() {
        return AdditionalNote;
    }

    public void setAdditionalNote(String additionalNote) {
        AdditionalNote = additionalNote;
    }

    public String getBookingId() {
        return BookingId;
    }

    public void setBookingId(String bookingId) {
        BookingId = bookingId;
    }

    public String getActivityId() {
        return ActivityId;
    }

    public void setActivityId(String activityId) {
        ActivityId = activityId;
    }

    public String getDevice() {
        return Device;
    }

    public void setDevice(String device) {
        Device = device;
    }

    public String getRoomNo() {
        return RoomNo;
    }

    public void setRoomNo(String roomNo) {
        RoomNo = roomNo;
    }

    public String getModifiedBy() {
        return ModifiedBy;
    }

    public void setModifiedBy(String modifiedBy) {
        ModifiedBy = modifiedBy;
    }

    public String getSpaName() {
        return SpaName;
    }

    public void setSpaName(String spaName) {
        SpaName = spaName;
    }

    public RealmList<SpaBookingTreatmentTypeObj> getTreatmentType() {
        return TreatmentType;
    }

    public void setTreatmentType(RealmList<SpaBookingTreatmentTypeObj> treatmentType) {
        TreatmentType = treatmentType;
    }

    public String getModifiedDate() {
        return ModifiedDate;
    }

    public void setModifiedDate(String modifiedDate) {
        ModifiedDate = modifiedDate;
    }

    public String getSpaId() {
        return SpaId;
    }

    public void setSpaId(String spaId) {
        SpaId = spaId;
    }

    public String getHotelProperty() {
        return HotelProperty;
    }

    public void setHotelProperty(String hotelProperty) {
        HotelProperty = hotelProperty;
    }

    public String getCreatedDate() {
        return CreatedDate;
    }

    public void setCreatedDate(String createdDate) {
        CreatedDate = createdDate;
    }

    public String getCreatedBy() {
        return CreatedBy;
    }

    public void setCreatedBy(String createdBy) {
        CreatedBy = createdBy;
    }

    public String getId() {
        return Id;
    }

    public void setId(String id) {
        Id = id;
    }

    public String getMessageCode() {
        return MessageCode;
    }

    public void setMessageCode(String messageCode) {
        MessageCode = messageCode;
    }

    public String getVersion() {
        return Version;
    }

    public void setVersion(String version) {
        Version = version;
    }

    public String getTenant() {
        return Tenant;
    }

    public void setTenant(String tenant) {
        Tenant = tenant;
    }

    public String getReferenceId() {
        return ReferenceId;
    }

    public void setReferenceId(String referenceId) {
        ReferenceId = referenceId;
    }

    public SpaGuestProfileObject getGuestProfile() {
        return GuestProfile;
    }

    public void setGuestProfile(SpaGuestProfileObject guestProfile) {
        GuestProfile = guestProfile;
    }
}
