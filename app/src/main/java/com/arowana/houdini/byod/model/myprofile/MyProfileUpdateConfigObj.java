package com.arowana.houdini.byod.model.myprofile;

import io.realm.RealmObject;

public class MyProfileUpdateConfigObj extends RealmObject {

    private MyProfileResponseStatusObj ResponseStatus;

    public MyProfileResponseStatusObj getResponseStatus() {
        return ResponseStatus;
    }

    public void setResponseStatus(MyProfileResponseStatusObj responseStatus) {
        ResponseStatus = responseStatus;
    }
}
