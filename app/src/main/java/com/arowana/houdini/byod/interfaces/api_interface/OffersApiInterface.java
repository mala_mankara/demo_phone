package com.arowana.houdini.byod.interfaces.api_interface;

import com.arowana.houdini.byod.model.offers.OffersConfigObject;

import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.POST;

public interface OffersApiInterface {

    @FormUrlEncoded
    @POST("Offers/GetAll")
    Call<OffersConfigObject> getOfferDetails(@Field("ImageSize") String imageSize);
}
