package com.arowana.houdini.byod.model.home;

import io.realm.RealmObject;

public class HomeGridViewObject extends RealmObject {


    private String Title;
    private String Image;
    private String Key;
    private String appState;

    public String getTitle() {
        return Title;
    }

    public void setTitle(String title) {
        Title = title;
    }

    public String getImage() {
        return Image;
    }

    public void setImage(String image) {
        Image = image;
    }

    public String getKey() {
        return Key;
    }

    public void setKey(String Key) {
        this.Key = Key;
    }


    public String getAppState() {
        return appState;
    }

    public void setAppState(String appState) {
        this.appState = appState;
    }
}
