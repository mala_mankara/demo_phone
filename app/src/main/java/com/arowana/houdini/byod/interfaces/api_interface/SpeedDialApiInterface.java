package com.arowana.houdini.byod.interfaces.api_interface;

import com.arowana.houdini.byod.model.speeddial.SpeedDialConfigObj;

import retrofit2.Call;
import retrofit2.http.GET;

public interface SpeedDialApiInterface {

    @GET("SpeedDials/GetAll")
    Call<SpeedDialConfigObj> getAll();

}
