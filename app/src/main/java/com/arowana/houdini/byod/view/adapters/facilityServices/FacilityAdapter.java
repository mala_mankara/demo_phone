package com.arowana.houdini.byod.view.adapters.facilityServices;

import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.RecyclerView;

import com.arowana.houdini.byod.MainApplication;
import com.arowana.houdini.byod.R;
import com.arowana.houdini.byod.interfaces.listeners.AppstateChangeListener;
import com.arowana.houdini.byod.interfaces.listeners.FragmentNavigationHandler;
import com.arowana.houdini.byod.model.facilitiesAndServices.FServiceAmenities;
import com.arowana.houdini.byod.model.facilitiesAndServices.FServiceResponseData;
import com.arowana.houdini.byod.utils.FactoryUtil;
import com.arowana.houdini.byod.utils.NavigationManager;
import com.arowana.houdini.byod.view.fragments.WebViewFragment;
import com.arowana.houdini.byod.view.fragments.facilityservice.FacilityDetailsFragment;

import java.util.Objects;

import io.realm.RealmList;

public class FacilityAdapter extends RecyclerView.Adapter<FacilityAdapter.CustomViewHolder> {

    private Context mContext;
    private RealmList<FServiceResponseData> dataList;
    private FragmentNavigationHandler mHandler;
    private FrameLayout mHolder;
    private AppstateChangeListener mListener;


    public FacilityAdapter(Context context, RealmList<FServiceResponseData> obj, FragmentNavigationHandler handler, FrameLayout fragmentHolder, AppstateChangeListener listener) {
        super();
        mContext = context;
        dataList = obj;
        mHandler = handler;
        mHolder  = fragmentHolder;
        mListener = listener;
    }

    @NonNull
    @Override
    public CustomViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.image_item, parent, false);
        return new CustomViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull CustomViewHolder holder, int position) {

        FServiceResponseData data=dataList.get(position);
        if(data != null) {
            holder.title.setText(data.getType());
            holder.imageBG.setTag(position);
            if (data.getImages().size() > 0) {

                String image = Objects.requireNonNull(data.getImages().first()).getImageUrl();
                FactoryUtil.loadImage(mContext, image, holder.image);
            }
        }
    }

    @Override
    public int getItemCount() {
        return dataList.size();
    }

    public class CustomViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener{
        RelativeLayout imageBG;
        TextView title;
        ImageView image;

        CustomViewHolder(View itemView) {
            super(itemView);
            imageBG = itemView.findViewById(R.id.image_bgview);
            image   = itemView.findViewById(R.id.image);
            title   = itemView.findViewById(R.id.title);
            imageBG.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            onItemClick(v, getAdapterPosition());
        }

        private void onItemClick(View v, int adapterPosition) {
            FServiceResponseData rowData = dataList.get(adapterPosition);
            if(Objects.requireNonNull(rowData).getUrl()== null|| rowData.getUrl().isEmpty() || ! FactoryUtil.isValidURL(rowData.getUrl())){
                if(MainApplication.getModuleList().contains(rowData.getModule())){
                   String str= Objects.requireNonNull(dataList.get(adapterPosition)).getModule();
                    redirectToModule(str);
                }else {

                    RealmList<FServiceAmenities> amenities = Objects.requireNonNull(dataList.get((Integer) v.getTag())).getAmenities();
                    String detailTitle = Objects.requireNonNull(dataList.get((Integer) v.getTag())).getType();
                    FacilityDetailsFragment fragment = new FacilityDetailsFragment();
                    fragment.setDetailsObj(amenities,detailTitle);
                    mHandler.showFragment(mHolder.getId(), fragment);
                }
            }else{
                loadWebView(rowData.getModule(),rowData.getUrl());
            }

        }
    }

    private void loadWebView(String module, String url) {
        Fragment webView = new WebViewFragment();
        Bundle bundle = new Bundle();
        bundle.putString("title",module);
        bundle.putString("url",url);
        webView.setArguments(bundle);
        mHandler.showFragment(mHolder.getId(),webView);
    }

    private void redirectToModule(String key) {
        NavigationManager.loadView(key, mContext,mListener);
    }
}
