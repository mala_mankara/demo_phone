package com.arowana.houdini.byod.view.activity;

import android.os.Bundle;
import android.widget.FrameLayout;

import com.arowana.houdini.byod.MainApplication;
import com.arowana.houdini.byod.R;
import com.arowana.houdini.byod.view.fragments.LandingFragment;

public class LandingActivity extends BaseActivity {

    public static FrameLayout fragmentHolder;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        setTheme(R.style.AppTheme);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_module_layout);
        MainApplication.setCurrentActivity(this);
        hideActionBar(true);
        initComponents();

    }


    public void initComponents() {

        fragmentHolder = findViewById(R.id.fragment_holder);
        loadFragments();

    }

    public void loadFragments() {
        showFragment(fragmentHolder.getId(), new LandingFragment());

    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finishAffinity();
    }
}
