package com.arowana.houdini.byod.view.fragments.login;


import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.arowana.houdini.byod.R;
import com.arowana.houdini.byod.view.activity.LoginActivity;
import com.arowana.houdini.byod.view.fragments.BaseFragment;


public class SignupConfirmationFragment extends BaseFragment {


    public SignupConfirmationFragment() {
        // Required empty public constructor
    }


    View mRootView;
    TextView mMsg, mMembershipId;
    Button mBtnOk;
    String mMessage, membershipid;


    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        mRootView = view;
        setTitle(getString(R.string.confirmation));
        initComponents();
        initListeners();

    }

    @Override
    protected int getFragmentLayout() {
        return
                R.layout.fragment_signup_confirmation;
    }

    public void setMsg(String msg, String membershipid) {
        this.mMessage = msg;
        this.membershipid = membershipid;
    }

    public void initComponents() {
        mMsg = mRootView.findViewById(R.id.signup_msg);
        mMembershipId = mRootView.findViewById(R.id.txtMembershipId);
        mBtnOk = mRootView.findViewById(R.id.signup_btn_ok);

        mMembershipId.setText(membershipid);
        mMsg.setText(mMessage);

    }

    public void initListeners() {
        mBtnOk.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ((LoginActivity)getActivity()).refreshUI();
            }
        });
    }


}
