package com.arowana.houdini.byod.view.adapters.restaurant;

import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.viewpager.widget.ViewPager;

import com.arowana.houdini.byod.Consts;
import com.arowana.houdini.byod.MainApplication;
import com.arowana.houdini.byod.R;
import com.arowana.houdini.byod.interfaces.listeners.FragmentNavigationHandler;
import com.arowana.houdini.byod.model.restaurant.RestaurantBarObject;
import com.arowana.houdini.byod.model.restaurant.RestaurantConfigObject;
import com.arowana.houdini.byod.model.restaurant.RestaurantImagesObj;
import com.arowana.houdini.byod.model.restaurant.RestaurantInformationObj;
import com.arowana.houdini.byod.model.restaurant.RestaurantReviewsObj;
import com.arowana.houdini.byod.utils.ExpandableTextView;
import com.arowana.houdini.byod.utils.FactoryUtil;
import com.arowana.houdini.byod.utils.ViewUtils;
import com.arowana.houdini.byod.view.activity.BaseActivity;
import com.arowana.houdini.byod.view.activity.OffersActivity;
import com.arowana.houdini.byod.view.adapters.MultiImageAdapter;
import com.arowana.houdini.byod.view.fragments.WebViewFragment;

import java.util.ArrayList;

import io.realm.RealmList;

public class RestaurantAdapter extends RecyclerView.Adapter<RestaurantAdapter.MyViewHolder> {

    private Context mContext;
    private LayoutInflater mInflator;
    private RealmList<RestaurantBarObject> mRestaurant;
    private FragmentNavigationHandler mhandler;
    private FrameLayout mholder;
    private int offers;


    public RestaurantAdapter(Context mContext, RestaurantConfigObject mConfigObject, FragmentNavigationHandler handler, FrameLayout holder, int offerSize) {
        this.mContext = mContext;
        mRestaurant = mConfigObject.getResponseData().getRestauarantBar();
        /*mInflator = ((Activity) mContext).getLayoutInflater();*/
        mhandler = handler;
        mholder = holder;
        offers = offerSize;

    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        ViewPager imgHolder;
        Button book_a_slot;
        TextView rest_menu_layout, rest_offers_layout;
        private RecyclerView mDetailsContainer;
        private ImageView trip_advisor_icon, zomato_icon;

        TextView reviews_caption;
        private LinearLayout ll_dots;

        ExpandableTextView txtHotelDescription;
        TextView txtHotelName, txtHotelType, txtReadMore;


        public MyViewHolder(View view) {
            super(view);

            imgHolder = view.findViewById(R.id.restaurantImageHolder);
            txtHotelName = view.findViewById(R.id.txtHotelName);
            txtHotelType = view.findViewById(R.id.txtHotelType);
            book_a_slot = view.findViewById(R.id.spa_book_a_slot);
            txtHotelDescription = view.findViewById(R.id.txtHotelDescription);
            mDetailsContainer = view.findViewById(R.id.restaurant_table_layout);
            rest_menu_layout = view.findViewById(R.id.rest_menu_layout);

            rest_offers_layout = view.findViewById(R.id.rest_offers_layout);
            zomato_icon = view.findViewById(R.id.zomato_icon);
            trip_advisor_icon = view.findViewById(R.id.trip_advisor_icon);
            reviews_caption = view.findViewById(R.id.reviews_caption);

            txtReadMore = view.findViewById(R.id.spa_read_more);

            ll_dots = view.findViewById(R.id.ll_dots);
            mDetailsContainer.setLayoutManager(new LinearLayoutManager(mContext));

        }
    }


    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_restaurant_main, parent, false);
        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull final MyViewHolder holder, final int position) {
        final RestaurantBarObject bean = mRestaurant.get(position);


        holder.mDetailsContainer.removeAllViews();
        if (bean.getInformation() != null) {
            holder.mDetailsContainer.setAdapter(new InfoDetailsAdapter(mContext, bean, mhandler));
        }
        holder.txtHotelName.setText("" + bean.getName());
        holder.txtHotelType.setText("" + bean.getType());
        holder.txtHotelDescription.setText("" + bean.getDescription());

        int count = holder.txtHotelDescription.getLineCount();

        holder.txtHotelDescription.post(new Runnable() {
            @Override
            public void run() {
                // Perform any actions you want based on the line count here.
                int count = holder.txtHotelDescription.getLineCount();
                holder.txtReadMore.setVisibility(count < 4 ? View.GONE : View.VISIBLE);
            }
        });


        holder.txtReadMore.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                holder.txtHotelDescription.toggle();
                holder.txtReadMore.setText(holder.txtHotelDescription.isExpanded() ? R.string.read_more : R.string.read_less);

            }
        });

        //if(Consts.APP_CURRENT_STATE>0 && b)
        holder.book_a_slot.setVisibility(Consts.APP_CURRENT_STATE > 0 && !(bean.getAppLink()== null || bean.getAppLink().isEmpty()) ? View.VISIBLE : View.GONE);



        holder.book_a_slot.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                WebViewFragment webview = new WebViewFragment();
                Bundle bundle = new Bundle();

                bundle.putString("title", mContext.getResources().getString(R.string.restaurant));
                bundle.putString("url", bean.getAppLink());
                webview.setArguments(bundle);
                mhandler.showFragment(mholder.getId(), webview);

            }
        });
        holder.rest_menu_layout.setVisibility((bean.getMenu()!= null &&bean.getMenu().size() > 0 ) ? View.VISIBLE : View.GONE);
        holder.rest_menu_layout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (mRestaurant.get(position).getMenu() != null && bean.getMenu().size() > 0) {
//                    WebViewFragment webview = new WebViewFragment();
//                    Bundle bundle = new Bundle();
//                    bundle.putString("title", "Menu");
                    String url = bean.getMenu().get(0).getImageUrl();
//                    try {
//                        url = URLEncoder.encode(url, "utf-8");
//                    } catch (UnsupportedEncodingException e) {
//                        e.printStackTrace();
//                    }
//                    bundle.putString("url", "http://docs.google.com/gview?embedded=true&url="+url);
//                    webview.setArguments(bundle);


//                   String googleDocs = "https://docs.google.com/viewer?url=";
//                    String pdf_url = "http://www.somedomain.com/new.pdf";
//                    bundle.putString("url", googleDocs + pdf_url);
//                    webview.setArguments(bundle);
//                    mhandler.showFragment(mholder.getId(), webview);


                    //    String pdfLink= "http://192.168.0.244:9710//Image/Menu/Menu_3324469.pdf";
                    Intent intent = new Intent(Intent.ACTION_VIEW);
                    intent.setDataAndType(Uri.parse(url), "application/pdf");
                    try{
                        view.getContext().startActivity(intent);
                    } catch (ActivityNotFoundException e) {
                        //user does not have a pdf viewer installed
                    }
                }

            }
        });
        holder.zomato_icon.setVisibility(View.GONE);
        holder.trip_advisor_icon.setVisibility(View.GONE);


        if ((bean.getReviews().size()) > 0  ) {
            for(final RestaurantReviewsObj reviews: bean.getReviews())
                if(reviews.getUrl()!= null && ! reviews.getUrl().isEmpty()) {
                    holder.reviews_caption.setVisibility(View.VISIBLE);
                    /* for (final RestaurantReviewsObj reviews : bean.getReviews()) {*/
                    String name = reviews.getName();
                    if (name.toLowerCase().equals("zomato")) {
                        holder.zomato_icon.setVisibility(View.VISIBLE);
                        holder.zomato_icon.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                WebViewFragment webview = new WebViewFragment();
                                Bundle bundle = new Bundle();

                                bundle.putString("title", mContext.getResources().getString(R.string.restaurant));
                                bundle.putString("url", reviews.getUrl());
                                webview.setArguments(bundle);
                                mhandler.showFragment(mholder.getId(), webview);
                            }
                        });
                    }

                    if (name.toLowerCase().equals("tripadvisor")) {
                        holder.trip_advisor_icon.setVisibility(View.VISIBLE);
                        holder.trip_advisor_icon.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                WebViewFragment webview = new WebViewFragment();
                                Bundle bundle = new Bundle();
                                bundle.putString("title", mContext.getResources().getString(R.string.restaurant));
                                bundle.putString("url", reviews.getUrl());
                                webview.setArguments(bundle);
                                mhandler.showFragment(mholder.getId(), webview);
                            }
                        });
                    }

                }else
                holder.reviews_caption.setVisibility(View.GONE);
        }else
           holder.reviews_caption.setVisibility(View.GONE);

        holder.rest_offers_layout.setVisibility(offers > 0  ? View.VISIBLE : View.GONE);
        holder.rest_offers_layout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent offersIntent = new Intent(mContext, OffersActivity.class);
                mContext.startActivity(offersIntent);
            }
        });


        if (bean.getImages().size() > 0) {
            ArrayList<String> imageList = new ArrayList<>();
            for (RestaurantImagesObj image : bean.getImages()) {
                String imgPath = image.getImageUrl();
                imageList.add(imgPath);

            }


            holder.imgHolder.setVisibility(View.VISIBLE);
            holder.imgHolder.setAdapter(new MultiImageAdapter(mContext, imageList));

            if (imageList.size() > 1) {
                holder.ll_dots.removeAllViews();
                final ImageView[] ivArrayDotsPager = new ImageView[imageList.size()];
                for (int i = 0; i < ivArrayDotsPager.length; i++) {
                    ivArrayDotsPager[i] = new ImageView(mContext);
                    LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
                    params.setMargins(5, 0, 5, 0);
                    ivArrayDotsPager[i].setLayoutParams(params);
                    ivArrayDotsPager[i].setImageResource(R.drawable.slider_inactive);

                    ivArrayDotsPager[i].setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            view.setAlpha(1);
                        }
                    });
                    holder.ll_dots.addView(ivArrayDotsPager[i]);
                    holder.ll_dots.bringToFront();
                }


                ivArrayDotsPager[0].setImageResource(R.drawable.slider_active);

                holder.imgHolder.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
                    @Override
                    public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

                    }

                    @Override
                    public void onPageSelected(int position) {

                        Log.d("test", "--imgPath---" + position);
                        for (int i = 0; i < ivArrayDotsPager.length; i++) {
                            ivArrayDotsPager[i].setImageResource(R.drawable.slider_inactive);
                        }
                        ivArrayDotsPager[position].setImageResource(R.drawable.slider_active);
                    }

                    @Override
                    public void onPageScrollStateChanged(int state) {

                    }
                });
            } else
                holder.ll_dots.setVisibility(View.GONE);

        } else {
            holder.imgHolder.setVisibility(View.GONE);
        }
    }




    @Override
    public int getItemCount() {
        return mRestaurant.size();
    }


    private class InfoDetailsAdapter  extends RecyclerView.Adapter<InfoDetailsAdapter.CustomViewHolder> {
        private RestaurantBarObject hotelObj;
        private RealmList<RestaurantInformationObj> infoList;
        private Context cntxt;

        InfoDetailsAdapter(Context mContext, RestaurantBarObject bean, FragmentNavigationHandler mhandler) {
            cntxt          = mContext;
            hotelObj       = bean;
            infoList       = hotelObj.getInformation();
        }


        @NonNull
        @Override
        public CustomViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
            View view = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.palaceinfo_item, parent, false);
            return new InfoDetailsAdapter.CustomViewHolder(view);
        }

        @Override
        public void onBindViewHolder(@NonNull CustomViewHolder holder, int position) {

            RestaurantInformationObj infoData = infoList.get(position);
            holder.info_field.setText(infoData.getField());
            holder.info_value.setTag(position);

            if (infoData.getField().equalsIgnoreCase("address")) {
                holder.info_value.setTextColor(cntxt.getResources().getColor(R.color.color_charcoal));
            }else if(infoData.getField().equalsIgnoreCase("Phone")||infoData.getField().equalsIgnoreCase("Telephone")
                    ||infoData.getField().equalsIgnoreCase("Email Id")
                    ||infoData.getField().equalsIgnoreCase("Email")||infoData.getField().equalsIgnoreCase("Whatsapp")){
                holder.info_value.setLinkTextColor(cntxt.getResources().getColor(R.color.autolink_text_color));
                holder.info_value.setTextColor(cntxt.getResources().getColor(R.color.autolink_text_color));
              //  holder.info_value.setAutoLinkMask(position);
            }

            holder.info_value.setText(infoData.getValue());

        }
        @Override
        public int getItemCount() {
            return infoList.size();
        }

        class CustomViewHolder extends RecyclerView.ViewHolder  implements  View.OnClickListener {
            private TextView info_field;
            private TextView info_value;
            private ImageView navigation;

            CustomViewHolder(View itemView) {
                super(itemView);
                info_field    = itemView.findViewById(R.id.field);
                info_value    = itemView.findViewById(R.id.value);
                navigation    = itemView.findViewById(R.id.palace_location_image);
                navigation.setVisibility(View.GONE);
                info_value.setOnClickListener(this);
            }

            @Override
            public void onClick(View v) {
                RestaurantInformationObj rowData = infoList.get((Integer) v.getTag());

                String value     = rowData.getValue();
                String fieldName = rowData.getField();
                Intent actionIntent ;
                Log.d("RESTAURANT&BAR","field/value.."+fieldName+" /:"+value);
                if(fieldName.equalsIgnoreCase("Phone")|| fieldName.equalsIgnoreCase("TelePhone")){

                    actionIntent = new Intent(Intent.ACTION_DIAL);
                    String p = "tel:" + (value);
                    actionIntent.setData(Uri.parse(p));
                    cntxt.startActivity(actionIntent);
                }if(fieldName.equalsIgnoreCase("Email")){

                    actionIntent = new Intent(Intent.ACTION_SENDTO);
                    actionIntent.setType("text/plain");
                    actionIntent.setData(Uri.fromParts("mailto",value, null));
                    cntxt.startActivity(Intent.createChooser(actionIntent, "Choose an Email client :"));

                }if(fieldName.equalsIgnoreCase("Whatsapp")){

                    PackageManager pm = cntxt.getPackageManager();

                    if (!MainApplication.appInstalledOrNot("com.whatsapp")) {
                        ViewUtils.showSnackBarMessage("Feature unavailable. WhatsApp needs to be installed",(BaseActivity)cntxt);
                    } else {
                        String whatsAppNo = value;
                        if (!(whatsAppNo == null && whatsAppNo.isEmpty())) {
                            FactoryUtil.openWhatsApp(cntxt, whatsAppNo);
                        } else
                            ViewUtils.showSnackBarMessage("WhatsApp no. is not valid.");
                    }
                }
            }
        }
    }
}
