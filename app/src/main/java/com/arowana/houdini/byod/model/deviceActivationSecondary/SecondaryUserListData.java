package com.arowana.houdini.byod.model.deviceActivationSecondary;

import io.realm.RealmList;
import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

public class SecondaryUserListData extends RealmObject {
    @PrimaryKey
    private String RealmId = "1";
    private RealmList<SecondaryUser> UserAdditionalDevices;

    public String getRealmId() {
        return RealmId;
    }

    public void setRealmId(String realmId) {
        RealmId = realmId;
    }

    public RealmList<SecondaryUser> getUserAdditionalDevices() {
        return UserAdditionalDevices;
    }

    public void setUserAdditionalDevices(RealmList<SecondaryUser> userAdditionalDevices) {
        UserAdditionalDevices = userAdditionalDevices;
    }

}
