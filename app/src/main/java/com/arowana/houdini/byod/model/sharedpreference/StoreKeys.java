package com.arowana.houdini.byod.model.sharedpreference;

public class StoreKeys {

    // Application state
    public static String APPLICATION_STATE = "app_state";

    public static String SHOWING_FRAGMENT = "showingFragment";

    public static String IS_DEVICE_CONTROL_ACTIVATED = "device_activation_status";

    public static final String E_PALACE_PUSH_NOTIFICATION_ID = "push_notification_id";

    public static String SIPPROFILE_STATE = "sip_profile";
}
