package com.arowana.houdini.byod.view.fragments.spa;


import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.arowana.houdini.byod.Consts;
import com.arowana.houdini.byod.MainApplication;
import com.arowana.houdini.byod.R;
import com.arowana.houdini.byod.interfaces.listeners.AdapterCallbacks;
import com.arowana.houdini.byod.model.spa.SpaAvailabilityTimeObj;
import com.arowana.houdini.byod.model.spa.SpaTreatmentTypeObj;
import com.arowana.houdini.byod.model.speeddial.SpeedDialResponseData;
import com.arowana.houdini.byod.utils.RealmUtil;
import com.arowana.houdini.byod.view.adapters.ICart;
import com.arowana.houdini.byod.view.adapters.spa.SpaDetailsAdapter;
import com.arowana.houdini.byod.view.fragments.BaseFragment;
import com.arowana.houdini.byod.view.fragments.telephone.PhoneCallFragment;

import alcatel.model.ByodManager;
import io.realm.Realm;

import static com.arowana.houdini.byod.view.activity.SpaActivity.fragmentHolder;


public class SpaDetailsFragment extends BaseFragment implements ICart, AdapterCallbacks {


    public SpaDetailsFragment() {

    }


    private View mRootView;
    private SpaTreatmentTypeObj mTreatmentTypeObj;
    private SpaAvailabilityTimeObj mTimeAvailabilityObj;
    private TextView mTitle, mCartCount;
    private RecyclerView mRecyclerView;
    private RelativeLayout btnSubmit, bottomLayout;
    private View call_conceirge;
    private String speedDialNo;
    private String spaHotelTimeZone;

Realm mRealm;
    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        mRootView = view;
        setTitle(getString(R.string.spa_details));

        initComponents();
        populateData(mTreatmentTypeObj, mTimeAvailabilityObj);



    }


    @Override
    protected int getFragmentLayout() {
        return R.layout.fragment_spa_details;
    }

    public void setTreatmenttypeObj(String hotelTimeZone, SpaTreatmentTypeObj obj, SpaAvailabilityTimeObj timeAvailabilityObj) {
        mTreatmentTypeObj = obj;
        mTimeAvailabilityObj = timeAvailabilityObj;
        spaHotelTimeZone = hotelTimeZone;
    }

    public void initComponents() {


        mTitle = mRootView.findViewById(R.id.spa_treatment_title);
        mRecyclerView = mRootView.findViewById(R.id.spa_treatment_recyclerview);
        btnSubmit = mRootView.findViewById(R.id.btn_spa_submit);
        mCartCount = mRootView.findViewById(R.id.spa_cart_count);
        bottomLayout = mRootView.findViewById(R.id.bottom_btn_layout);
        call_conceirge = mRootView.findViewById(R.id.btn_spa_conceirge);
        bottomLayout.setVisibility(Consts.APP_CURRENT_STATE > 0 ? View.VISIBLE : View.GONE);

        //call_conceirge.setVisibility(Consts.APP_CURRENT_STATE > 1 ? View.VISIBLE : View.GONE);
        if(MainApplication.isContainedSpeedDial("spa") && Consts.APP_CURRENT_STATE>1){
            call_conceirge.setVisibility(View.VISIBLE);
            speedDialNo = MainApplication.getValueforKey();
        }else
            call_conceirge.setVisibility(View.GONE);

        call_conceirge.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(!ByodManager.getInstance().isSipConnected()) {
                    Toast.makeText(getActivity(),"Call service not working. Please try later..",Toast.LENGTH_SHORT).show();
                }else {

                    if (!speedDialNo.isEmpty()) {
                        PhoneCallFragment fragment = new PhoneCallFragment();
                        Bundle bundle = new Bundle();
                        bundle.putString("phoneNo", speedDialNo);
                        bundle.putString("name", "spa");
                        bundle.putBoolean("isMakingCall",true);
                        fragment.setArguments(bundle);
                        mHandler.showFragment(fragmentHolder.getId(), fragment);
                    }
                }
            }
        });

        btnSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Log.d("spaDetailsFrag","######..");
                if (RealmUtil.getSpaCartCount() > 0 ) {
                    SpaCartFragment fragment = new SpaCartFragment();
                    fragment.setBookingTimeDetails(mTimeAvailabilityObj,spaHotelTimeZone);
                    mHandler.showFragment(fragmentHolder.getId(), fragment);
                }else
                    Toast.makeText(getContext(), "Please select a treatment", Toast.LENGTH_SHORT).show();
            }
        });

        mTitle.setText(mTreatmentTypeObj.getTypeName());
        
        mCartCount.setText(String.valueOf(RealmUtil.getSpaCartCount()));


    }

    public void populateData(SpaTreatmentTypeObj obj, SpaAvailabilityTimeObj mTimeAvailabilityObj) {

        LinearLayoutManager linearVertical = new LinearLayoutManager(getContext(), RecyclerView.VERTICAL, false);
        mRecyclerView.setLayoutManager(linearVertical);

        SpaDetailsAdapter adapter = new SpaDetailsAdapter(getContext(), this,spaHotelTimeZone, obj , mTimeAvailabilityObj);
        mRecyclerView.setAdapter(adapter);


    }


    @Override
    public void updateCartInfo() {
        mCartCount.setText(String.valueOf(RealmUtil.getSpaCartCount()));
    }

    @Override
    public void methodCallBacks(SpeedDialResponseData dataObj) {

    }
}
