package com.arowana.houdini.byod.model.restaurant;

import io.realm.RealmList;
import io.realm.RealmObject;

public class RestaurantBarObject extends RealmObject{

    private String HotelId;

    private String ActivityId;

    private String Type;

    private String TypeName;

    private RealmList<RestaurantImagesObj> Images;

    private String GroupId;

    private String AppLink;

    private RealmList<RestaurantMenuObj> Menu;

    private String LanguageCode;

    private String TypeId;

    private String MessageCode;

    private String Version;

    private String Tenant;

    private String Description;

    private String ModifiedBy;

    private String IsReadOnly;

    private String BrandCode;

    private String BrandId;

    private String Name;

    private RealmList<RestaurantInformationObj> Information;

    private String ModifiedDate;

    private String Status;

    private String HotelCode;

    private String GroupCode;

    private RealmList<RestaurantReviewsObj> Reviews;

    private String CreatedDate;

    private String CreatedBy;

    private String Id;


    public String getHotelId() {
        return HotelId;
    }

    public void setHotelId(String hotelId) {
        HotelId = hotelId;
    }

    public String getActivityId() {
        return ActivityId;
    }

    public void setActivityId(String activityId) {
        ActivityId = activityId;
    }

    public String getType() {
        return Type;
    }

    public void setType(String type) {
        Type = type;
    }

    public String getTypeName() {
        return TypeName;
    }

    public void setTypeName(String typeName) {
        TypeName = typeName;
    }

    public RealmList<RestaurantImagesObj> getImages() {
        return Images;
    }

    public void setImages(RealmList<RestaurantImagesObj> images) {
        Images = images;
    }

    public String getGroupId() {
        return GroupId;
    }

    public void setGroupId(String groupId) {
        GroupId = groupId;
    }

    public String getAppLink() {
        return AppLink;
    }

    public void setAppLink(String appLink) {
        AppLink = appLink;
    }

    public RealmList<RestaurantMenuObj> getMenu() {
        return Menu;
    }

    public void setMenu(RealmList<RestaurantMenuObj> menu) {
        Menu = menu;
    }

    public String getLanguageCode() {
        return LanguageCode;
    }

    public void setLanguageCode(String languageCode) {
        LanguageCode = languageCode;
    }

    public String getTypeId() {
        return TypeId;
    }

    public void setTypeId(String typeId) {
        TypeId = typeId;
    }

    public String getMessageCode() {
        return MessageCode;
    }

    public void setMessageCode(String messageCode) {
        MessageCode = messageCode;
    }

    public String getVersion() {
        return Version;
    }

    public void setVersion(String version) {
        Version = version;
    }

    public String getTenant() {
        return Tenant;
    }

    public void setTenant(String tenant) {
        Tenant = tenant;
    }

    public String getDescription() {
        return Description;
    }

    public void setDescription(String description) {
        Description = description;
    }

    public String getModifiedBy() {
        return ModifiedBy;
    }

    public void setModifiedBy(String modifiedBy) {
        ModifiedBy = modifiedBy;
    }

    public String getIsReadOnly() {
        return IsReadOnly;
    }

    public void setIsReadOnly(String isReadOnly) {
        IsReadOnly = isReadOnly;
    }

    public String getBrandCode() {
        return BrandCode;
    }

    public void setBrandCode(String brandCode) {
        BrandCode = brandCode;
    }

    public String getBrandId() {
        return BrandId;
    }

    public void setBrandId(String brandId) {
        BrandId = brandId;
    }

    public String getName() {
        return Name;
    }

    public void setName(String name) {
        Name = name;
    }

    public RealmList<RestaurantInformationObj> getInformation() {
        return Information;
    }

    public void setInformation(RealmList<RestaurantInformationObj> information) {
        Information = information;
    }

    public String getModifiedDate() {
        return ModifiedDate;
    }

    public void setModifiedDate(String modifiedDate) {
        ModifiedDate = modifiedDate;
    }

    public String getStatus() {
        return Status;
    }

    public void setStatus(String status) {
        Status = status;
    }

    public String getHotelCode() {
        return HotelCode;
    }

    public void setHotelCode(String hotelCode) {
        HotelCode = hotelCode;
    }

    public String getGroupCode() {
        return GroupCode;
    }

    public void setGroupCode(String groupCode) {
        GroupCode = groupCode;
    }

    public RealmList<RestaurantReviewsObj> getReviews() {
        return Reviews;
    }

    public void setReviews(RealmList<RestaurantReviewsObj> reviews) {
        Reviews = reviews;
    }

    public String getCreatedDate() {
        return CreatedDate;
    }

    public void setCreatedDate(String createdDate) {
        CreatedDate = createdDate;
    }

    public String getCreatedBy() {
        return CreatedBy;
    }

    public void setCreatedBy(String createdBy) {
        CreatedBy = createdBy;
    }

    public String getId() {
        return Id;
    }

    public void setId(String id) {
        Id = id;
    }
}
