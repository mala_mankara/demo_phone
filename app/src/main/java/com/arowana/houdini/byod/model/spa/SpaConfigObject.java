package com.arowana.houdini.byod.model.spa;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

public class SpaConfigObject extends RealmObject {
    @PrimaryKey
    private String RealmId = "1";

    private String RequestData;

    private String IsReadOnly;

    private SpaResponseStatusObj ResponseStatus;

    private SpaResponseDataObj ResponseData;

    public String getRequestData() {
        return RequestData;
    }

    public void setRequestData(String requestData) {
        RequestData = requestData;
    }

    public String getIsReadOnly() {
        return IsReadOnly;
    }

    public void setIsReadOnly(String isReadOnly) {
        IsReadOnly = isReadOnly;
    }

    public SpaResponseStatusObj getResponseStatus() {
        return ResponseStatus;
    }

    public void setResponseStatus(SpaResponseStatusObj responseStatus) {
        ResponseStatus = responseStatus;
    }

    public SpaResponseDataObj getResponseData() {
        return ResponseData;
    }

    public void setResponseData(SpaResponseDataObj responseData) {
        ResponseData = responseData;
    }

    public String getRealmId() {
        return RealmId;
    }

    public void setRealmId(String realmId) {
        RealmId = realmId;
    }
}
