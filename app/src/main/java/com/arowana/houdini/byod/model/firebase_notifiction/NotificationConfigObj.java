package com.arowana.houdini.byod.model.firebase_notifiction;

import io.realm.RealmObject;

/*{
        "ResponseStatus": {
        "ResponseCode": "M1001",
        "ResponseFlag": "SUCCESS",
        "ResponseMessage": "Device successfully registered",
        "ResponseId": null
        }
        }*/
public class NotificationConfigObj extends RealmObject {

    private String ResponseStatus;
    private String ResponseCode;
    private String ResponseFlag;
    private String ResponseMessage;
    private String ResponseId;

    public String getResponseStatus() {
        return ResponseStatus;
    }

    public void setResponseStatus(String responseStatus) {
        ResponseStatus = responseStatus;
    }

    public String getResponseCode() {
        return ResponseCode;
    }

    public void setResponseCode(String responseCode) {
        ResponseCode = responseCode;
    }

    public String getResponseFlag() {
        return ResponseFlag;
    }

    public void setResponseFlag(String responseFlag) {
        ResponseFlag = responseFlag;
    }

    public String getResponseMessage() {
        return ResponseMessage;
    }

    public void setResponseMessage(String responseMessage) {
        ResponseMessage = responseMessage;
    }

    public String getResponseId() {
        return ResponseId;
    }

    public void setResponseId(String responseId) {
        ResponseId = responseId;
    }
}
