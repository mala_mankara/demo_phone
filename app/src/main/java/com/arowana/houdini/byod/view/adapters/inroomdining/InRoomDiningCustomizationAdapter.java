package com.arowana.houdini.byod.view.adapters.inroomdining;

import android.content.Context;
import android.content.res.ColorStateList;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.Build;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;

import com.arowana.houdini.byod.R;
import com.arowana.houdini.byod.model.inroomdining.IRDiningCustomizationObj;
import com.arowana.houdini.byod.utils.RealmUtil;

import io.realm.Realm;
import io.realm.RealmList;


public class InRoomDiningCustomizationAdapter extends RecyclerView.Adapter<InRoomDiningCustomizationAdapter.MyViewHolder> {

    private Context context;
    private RealmList<IRDiningCustomizationObj> mCustomizationObj;
    private Realm realm;

    public class MyViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        private TextView title;
        private RadioGroup rGroup_Options;

        public MyViewHolder(View view) {
            super(view);
            title = view.findViewById(R.id.customization_type);
            rGroup_Options   = view.findViewById(R.id.ir_dining_customize_radiogroup);
            view.setOnClickListener(this);
        }

        @Override
        public void onClick(View view) { }
    }


    public InRoomDiningCustomizationAdapter(Context mContext, RealmList<IRDiningCustomizationObj> obj) {
        this.context = mContext;
        this.mCustomizationObj = obj;
        realm = RealmUtil.getInstance();
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent,int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_customization_main_component, parent, false);
        return new MyViewHolder(itemView);
    }

    private RadioButton radioButton;
    @Override
    public void onBindViewHolder(final MyViewHolder holder,final int position) {
        final IRDiningCustomizationObj obj = mCustomizationObj.get(position);
        if(obj != null) {
            holder.title.setText(obj.getCustomizeItem());

            holder.rGroup_Options.removeAllViews();
            Typeface myTypeface = Typeface.createFromAsset(context.getAssets(), "fonts/SF-UI-Text-Regular.otf");

            //create radio buttons
            for (int i = 0; i < obj.getCustomTypes().size(); i++) {
                radioButton = new RadioButton(context);
                if (obj.getSelectedIndex() == i)
                    radioButton.setChecked(true);

                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP)
                    radioButton.setButtonTintList(ColorStateList.valueOf(ContextCompat.getColor(context, R.color.action_bar_color)));
                radioButton.setHighlightColor(context.getResources().getColor(R.color.action_bar_color));
                radioButton.setBackgroundColor(Color.TRANSPARENT);
                radioButton.setId(position);
                radioButton.setPadding(50, 50, 50, 50);
                radioButton.setTextColor(Color.parseColor("#666564"));
                radioButton.setLayoutParams(new RadioGroup.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT, .25f));
                radioButton.setTypeface(myTypeface);
                radioButton.setText(obj.getCustomTypes().get(i).getCustomTypeName());
                radioButton.setId(i);

                holder.rGroup_Options.addView(radioButton);
            }

            holder.rGroup_Options.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
                @Override
                public void onCheckedChanged(final RadioGroup group, final int checkedId) {
                    radioButton = group.findViewById(checkedId);
                    if (radioButton != null && radioButton.isPressed()) {
                        Log.d("IRDC_ItemAdapter", "checked redio btn pos..." + checkedId);
                        obj.setSelected(true);
                        obj.setSelectedIndex(checkedId);
                        radioButton.setChecked(true);
                    } else {
                        obj.setSelected(false);
                        radioButton.setChecked(false);
                    }
                }
            });
        }
    }

    @Override
    public int getItemCount() {
        if (mCustomizationObj!=null) {
            return mCustomizationObj.size();
        }
        else {
            return 0;
        }
    }
}
