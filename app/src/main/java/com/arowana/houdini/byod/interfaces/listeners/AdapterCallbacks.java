package com.arowana.houdini.byod.interfaces.listeners;

import com.arowana.houdini.byod.model.speeddial.SpeedDialResponseData;

public interface AdapterCallbacks {

    void methodCallBacks(SpeedDialResponseData dataObj);
}
