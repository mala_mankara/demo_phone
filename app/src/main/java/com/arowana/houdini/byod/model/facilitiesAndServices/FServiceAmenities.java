package com.arowana.houdini.byod.model.facilitiesAndServices;

import io.realm.RealmList;
import io.realm.RealmObject;

public class FServiceAmenities extends RealmObject {

    private RealmList<FServicesInformation> Information;

    private String Name;

    private String Description;

    private String TypeId;

    private String Id;

    private RealmList<FServicesImages> Images;

    public RealmList<FServicesInformation> getInformation ()
    {
        return Information;
    }

    public void setInformation (RealmList<FServicesInformation> Information)
    {
        this.Information = Information;
    }

    public String getName ()
    {
        return Name;
    }

    public void setName (String Name)
    {
        this.Name = Name;
    }

    public String getDescription ()
    {
        return Description;
    }

    public void setDescription (String Description)
    {
        this.Description = Description;
    }

    public String getTypeId ()
    {
        return TypeId;
    }

    public void setTypeId (String TypeId)
    {
        this.TypeId = TypeId;
    }

    public String getId ()
    {
        return Id;
    }

    public void setId (String Id)
    {
        this.Id = Id;
    }

    public RealmList<FServicesImages> getImages ()
    {
        return Images;
    }

    public void setImages (RealmList<FServicesImages> Images)
    {
        this.Images = Images;
    }

    @Override
    public String toString()
    {
        return "ClassPojo [Information = "+Information+", Name = "+Name+", Description = "+Description+", TypeId = "+TypeId+", Id = "+Id+", Images = "+Images+"]";
    }
}
