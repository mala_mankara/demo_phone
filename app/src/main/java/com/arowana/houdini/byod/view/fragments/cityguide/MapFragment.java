package com.arowana.houdini.byod.view.fragments.cityguide;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.arowana.houdini.byod.R;
import com.arowana.houdini.byod.view.fragments.BaseFragment;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.MapsInitializer;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;


public class MapFragment extends BaseFragment implements OnMapReadyCallback

{
    float zoomLevel = 15.0f;

    private double mLocationLat =0.0;
    private double mLocationLng = 0.0;

    Bundle bundle;

    String name, mAddress, lat, lng;
    private View mRootView;
    TextView title, address;

    MapView mMapView;
    private GoogleMap googleMap;
    private ImageView mBtnNavigation;


    @Override
    public void onCreate(Bundle savedInstance) {
        super.onCreate(savedInstance);
        bundle = getArguments();

        if (bundle != null) {
            name = bundle.getString("name");
            mAddress = bundle.getString("address");
            lat = bundle.getString("lat");
            lng = bundle.getString("lng");
        }
        try {
            if (lat != null) {
                mLocationLat = Double.parseDouble(lat);
            }
            if (lng != null) {
                mLocationLng = Double.parseDouble(lng);
            }
        } catch (Exception e) {

        }


    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        mRootView = view;
        setTitle(getString(R.string.location));
        initComponents();
        mMapView = mRootView.findViewById(R.id.mapView);
        mBtnNavigation = mRootView.findViewById(R.id.cityguide_navigation_icon);
        mBtnNavigation.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String uri = "http://maps.google.com/maps?daddr=" + mLocationLat + "," + mLocationLng + "(" + name + ")";
                Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(uri));
                intent.setPackage("com.google.android.apps.maps");
                startActivity(intent);
            }
        });
        mMapView.onCreate(savedInstanceState);

        mMapView.onResume(); // needed to get the map to display immediately

        try {
            MapsInitializer.initialize(getActivity().getApplicationContext());
        } catch (Exception e) {
            e.printStackTrace();
        }

        mMapView.getMapAsync(this);
    }

    @Override
    protected int getFragmentLayout() {
        return R.layout.fragment_map;
    }


    public void initComponents() {
        title = mRootView.findViewById(R.id.title);
        title.setText(name);
        address = mRootView.findViewById(R.id.address);
        address.setText(mAddress);
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        /*MarkerOptions opts = new MarkerOptions();
        LatLng latLng = new LatLng(mLocationLat, mLocationLng);
        opts.position(latLng);

        googleMap.addMarker(opts);*/

        LatLng latLng = new LatLng(mLocationLat, mLocationLng);


        googleMap.addMarker(new MarkerOptions()
                .position(latLng));
        googleMap.moveCamera(CameraUpdateFactory.newLatLngZoom(latLng, zoomLevel));

    }

    public void setData(Bundle bundle) {
        this.bundle = bundle;
    }


    @Override
    public void onResume() {
        super.onResume();
        mMapView.onResume();
    }

    @Override
    public void onPause() {
        super.onPause();
        mMapView.onPause();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        mMapView.onDestroy();
    }

    @Override
    public void onLowMemory() {
        super.onLowMemory();
        mMapView.onLowMemory();
    }

}

