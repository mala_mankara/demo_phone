package com.arowana.houdini.byod.model.home;

public class ImagesObject {

    private String ImageSize;

    private String ImageName;

    private String ImageUrl;

    private String ImageWidth;

    private String ImageHeight;


    public ImagesObject(String imageSize, String imageName, String imageUrl, String imageWidth, String imageHeight) {
        ImageSize = imageSize;
        ImageName = imageName;
        ImageUrl = imageUrl;
        ImageWidth = imageWidth;
        ImageHeight = imageHeight;
    }

    public String getImageSize() {
        return ImageSize;
    }

    public void setImageSize(String imageSize) {
        ImageSize = imageSize;
    }

    public String getImageName() {
        return ImageName;
    }

    public void setImageName(String imageName) {
        ImageName = imageName;
    }

    public String getImageUrl() {
        return ImageUrl;
    }

    public void setImageUrl(String imageUrl) {
        ImageUrl = imageUrl;
    }

    public String getImageWidth() {
        return ImageWidth;
    }

    public void setImageWidth(String imageWidth) {
        ImageWidth = imageWidth;
    }

    public String getImageHeight() {
        return ImageHeight;
    }

    public void setImageHeight(String imageHeight) {
        ImageHeight = imageHeight;
    }
}
