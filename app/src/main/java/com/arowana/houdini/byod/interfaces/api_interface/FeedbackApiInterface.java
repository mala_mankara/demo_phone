package com.arowana.houdini.byod.interfaces.api_interface;

import com.arowana.houdini.byod.model.feedback.FeedBackConfigObject;
import com.google.gson.JsonObject;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.POST;

public interface FeedbackApiInterface {


   @FormUrlEncoded
   @POST("feedback/Get")
   Call<FeedBackConfigObject> getFeedback(@Field("MembershipId") String membershipId, @Field("BookingId") String bookingId);


   @POST("feedback/GuestFeedback")
   Call<FeedBackConfigObject> sendFeedback(@Body JsonObject body);
}
