package com.arowana.houdini.byod.model.offers;

import io.realm.RealmList;
import io.realm.RealmObject;

public class OffersResponseData extends RealmObject {

    private String Description;

    private String ActivityId;

    private String HotelId;

    private RealmList<OfferDetails> Details;

    private String ModifiedBy;

    private String BrandCode;

    private RealmList<OfferImages> Images;

    private String GroupId;

    private String Name;

    private String BrandId;

    private String ModifiedDate;

    private String HotelCode;

    private String GroupCode;

    private String CreatedDate;

    private String CreatedBy;

    private String Id;

    private String MessageCode;

    private String Version;

    private String Tenant;

    public String getDescription ()
    {
        return Description;
    }

    public void setDescription (String Description)
    {
        this.Description = Description;
    }

    public String getActivityId ()
    {
        return ActivityId;
    }

    public void setActivityId (String ActivityId)
    {
        this.ActivityId = ActivityId;
    }

    public String getHotelId ()
    {
        return HotelId;
    }

    public void setHotelId (String HotelId)
    {
        this.HotelId = HotelId;
    }

    public RealmList<OfferDetails> getDetails ()
    {
        return Details;
    }

    public void setDetails (RealmList<OfferDetails> Details)
    {
        this.Details = Details;
    }

    public String getModifiedBy ()
    {
        return ModifiedBy;
    }

    public void setModifiedBy (String ModifiedBy)
    {
        this.ModifiedBy = ModifiedBy;
    }

    public String getBrandCode ()
    {
        return BrandCode;
    }

    public void setBrandCode (String BrandCode)
    {
        this.BrandCode = BrandCode;
    }

    public RealmList<OfferImages> getImages ()
    {
        return Images;
    }

    public void setImages (RealmList<OfferImages> Images)
    {
        this.Images = Images;
    }

    public String getGroupId ()
    {
        return GroupId;
    }

    public void setGroupId (String GroupId)
    {
        this.GroupId = GroupId;
    }

    public String getName ()
    {
        return Name;
    }

    public void setName (String Name)
    {
        this.Name = Name;
    }

    public String getBrandId ()
    {
        return BrandId;
    }

    public void setBrandId (String BrandId)
    {
        this.BrandId = BrandId;
    }

    public String getModifiedDate ()
    {
        return ModifiedDate;
    }

    public void setModifiedDate (String ModifiedDate)
    {
        this.ModifiedDate = ModifiedDate;
    }

    public String getHotelCode ()
    {
        return HotelCode;
    }

    public void setHotelCode (String HotelCode)
    {
        this.HotelCode = HotelCode;
    }

    public String getGroupCode ()
    {
        return GroupCode;
    }

    public void setGroupCode (String GroupCode)
    {
        this.GroupCode = GroupCode;
    }

    public String getCreatedDate ()
    {
        return CreatedDate;
    }

    public void setCreatedDate (String CreatedDate)
    {
        this.CreatedDate = CreatedDate;
    }

    public String getCreatedBy ()
    {
        return CreatedBy;
    }

    public void setCreatedBy (String CreatedBy)
    {
        this.CreatedBy = CreatedBy;
    }

    public String getId ()
    {
        return Id;
    }

    public void setId (String Id)
    {
        this.Id = Id;
    }

    public String getMessageCode ()
    {
        return MessageCode;
    }

    public void setMessageCode (String MessageCode)
    {
        this.MessageCode = MessageCode;
    }

    public String getVersion ()
    {
        return Version;
    }

    public void setVersion (String Version)
    {
        this.Version = Version;
    }

    public String getTenant ()
    {
        return Tenant;
    }

    public void setTenant (String Tenant)
    {
        this.Tenant = Tenant;
    }

    @Override
    public String toString()
    {
        return "ClassPojo [Description = "+Description+", ActivityId = "+ActivityId+", HotelId = "+HotelId+", Details = "+Details+", ModifiedBy = "+ModifiedBy+", BrandCode = "+BrandCode+", Images = "+Images+", GroupId = "+GroupId+", Name = "+Name+", BrandId = "+BrandId+", ModifiedDate = "+ModifiedDate+", HotelCode = "+HotelCode+", GroupCode = "+GroupCode+", CreatedDate = "+CreatedDate+", CreatedBy = "+CreatedBy+", Id = "+Id+", MessageCode = "+MessageCode+", Version = "+Version+", Tenant = "+Tenant+"]";
    }

}
