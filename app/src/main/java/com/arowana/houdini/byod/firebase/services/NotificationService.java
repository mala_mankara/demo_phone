package com.arowana.houdini.byod.firebase.services;

import android.content.Context;
import android.content.Intent;
import android.text.TextUtils;
import android.util.Log;

import androidx.localbroadcastmanager.content.LocalBroadcastManager;

import com.arowana.houdini.byod.firebase.NotificationConfig;
import com.arowana.houdini.byod.firebase.NotificationUtils;
import com.arowana.houdini.byod.model.sharedpreference.SharedPreferenceUtil;
import com.arowana.houdini.byod.model.sharedpreference.StoreKeys;
import com.arowana.houdini.byod.view.activity.BaseActivity;
import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.Map;


public class NotificationService extends FirebaseMessagingService {

    //private static final String TAG = NotificationService.class.getSimpleName();
    private static final String TAG = "NotificationService";

    private NotificationUtils notificationUtils;

    private void sendRegistrationToServer(final String token) {
        Log.e(TAG, "sendRegistrationToServer: " + token);
    }

    private void storeRegIdInPref(String token) {
        SharedPreferenceUtil.putIntoStore(StoreKeys.E_PALACE_PUSH_NOTIFICATION_ID, token);
    }

    @Override
    public void onNewToken(String s) {
        String refreshedToken = s;
        Log.d("TAG","token.."+refreshedToken);
        // Saving reg id to shared preferences
        storeRegIdInPref(refreshedToken);
        // sending reg id to your server
        sendRegistrationToServer(refreshedToken);
        // Notify UI that registration has completed, so the progress indicator can be hidden.
        Intent registrationComplete = new Intent(NotificationConfig.REGISTRATION_COMPLETE);
        registrationComplete.putExtra("token", refreshedToken);
        Log.d("firebase","token is:  "+refreshedToken);
        LocalBroadcastManager.getInstance(this).sendBroadcast(registrationComplete);
    }

    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {
        Log.d(TAG, "onMessageReceived----: " + remoteMessage.getData().toString());

        Log.d(TAG, "onMessageReceived----: " + remoteMessage.getNotification().getTitle());
        Log.d(TAG, "onMessageReceived----: " + remoteMessage.getSentTime());
        if (remoteMessage == null)
            return;

        // Check if message contains a notification payload.
        if (remoteMessage.getNotification() != null) {

            // Log.e("biplab", "Notification Body: " + remoteMessage.getData());

            Log.e(TAG, "Notification Body: " + remoteMessage.getNotification().getBody() + "----");
            handleNotification(remoteMessage);
        }

        if (remoteMessage.getData().size() > 0) {
            Log.e(TAG, "Data Payload: " + remoteMessage.getData().toString());

            try {
                JSONObject json = new JSONObject(remoteMessage.getData().toString());
                handleDataMessage(json);
            } catch (Exception e) {
                Log.e(TAG, "Exception: " + e.getMessage());
            }
        }



    }

    private void handleNotification(RemoteMessage remoteMessage) {
        if (!NotificationUtils.isAppIsInBackground(getApplicationContext())) {
            // app is in foreground, broadcast the push message
            Intent pushNotification = new Intent(NotificationConfig.PUSH_NOTIFICATION);
            pushNotification.putExtra("title", remoteMessage.getNotification().getTitle());
            pushNotification.putExtra("body", remoteMessage.getNotification().getBody());
            /*pushNotification.putExtra("timestamp", remoteMessage.getNotification().getBody());*/

            if (remoteMessage.getData().size() > 0) {
                JSONObject json = new JSONObject();
                Map<String, String> map = remoteMessage.getData();
                try {
                    for (Map.Entry<String, String> entry : map.entrySet()) {
                        json.put(entry.getKey(), entry.getValue());
                    }
                } catch (Exception e) {
                    Log.e(TAG, "Exception: " + e.getMessage());
                }
                pushNotification.putExtra("data", json.toString());
            }

            LocalBroadcastManager.getInstance(this).sendBroadcast(pushNotification);

            // play notification sound
        } else {
            // If the app is in background, firebase itself handles the notification
        }
    }

    private void handleDataMessage(JSONObject json) {
        Log.d(TAG, "push json: " + json.toString());

        try {
            // JSONObject data = json.getJSONObject("data");
            JSONObject data = json.getJSONObject("Param");

            String title = data.optString("title");
            String message = data.optString("message");
            boolean isBackground = data.optBoolean("is_background");
            String imageUrl = data.optString("image");
            String timestamp = data.optString("timestamp");
            JSONObject payload = data.getJSONObject("payload");

            Log.e(TAG, "title: " + title);
            Log.e(TAG, "message: " + message);
            Log.e(TAG, "isBackground: " + isBackground);
            Log.e(TAG, "payload: " + payload.toString());
            Log.e(TAG, "imageUrl: " + imageUrl);
            Log.e(TAG, "timestamp: " + timestamp);


            if (!NotificationUtils.isAppIsInBackground(getApplicationContext())) {
                // app is in foreground, broadcast the push message
                Intent pushNotification = new Intent(NotificationConfig.PUSH_NOTIFICATION);
                pushNotification.putExtra("message", "" + data);
                LocalBroadcastManager.getInstance(this).sendBroadcast(pushNotification);

                // play notification sound
                NotificationUtils notificationUtils = new NotificationUtils(getApplicationContext());
               // notificationUtils.playNotificationSound();
            } else {
                // app is in background, show the notification in notification tray
                Intent resultIntent = new Intent(getApplicationContext(), BaseActivity.class);
                resultIntent.setAction(NotificationConfig.PUSH_NOTIFICATION);
                resultIntent.putExtra("message", "" + data);

                // check for image attachment
                if (TextUtils.isEmpty(imageUrl)) {
                    showNotificationMessage(getApplicationContext(), title, message, timestamp, resultIntent);
                } else {
                    // image is present, show notification with image
                    showNotificationMessageWithBigImage(getApplicationContext(), title, message, timestamp, resultIntent, imageUrl);
                }
            }
            notificationUtils.showNotificationMessage(title, message, timestamp, new Intent(), imageUrl);
        } catch (JSONException e) {
            Log.e(TAG, "Json Exception12: " + e.getMessage());
        } catch (Exception e) {
            Log.e(TAG, "Exception12: " + e.getMessage());
        }
    }

    /**
     * Showing notification with text only
     */
    private void showNotificationMessage(Context context, String title, String message, String timeStamp, Intent intent) {
        notificationUtils = new NotificationUtils(context);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        notificationUtils.showNotificationMessage(title, message, timeStamp, intent);
    }

    /**
     * Showing notification with text and image
     */
    private void showNotificationMessageWithBigImage(Context context, String title, String message, String timeStamp, Intent intent, String imageUrl) {
        notificationUtils = new NotificationUtils(context);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        notificationUtils.showNotificationMessage(title, message, timeStamp, intent, imageUrl);
    }
}