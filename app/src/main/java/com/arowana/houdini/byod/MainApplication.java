package com.arowana.houdini.byod;

import android.app.Activity;
import android.app.Application;
import android.content.Context;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.util.Log;

import androidx.annotation.NonNull;

import com.arowana.houdini.byod.interfaces.ApiClient;
import com.arowana.houdini.byod.interfaces.api_interface.RedirectApiInterface;
import com.arowana.houdini.byod.model.redirct.RedirctConfigObj;
import com.arowana.houdini.byod.model.redirct.RedirectResponseData;
import com.arowana.houdini.byod.model.sharedpreference.SharedPreferenceUtil;
import com.arowana.houdini.byod.model.sharedpreference.StoreKeys;
import com.arowana.houdini.byod.utils.FragmentTransactionUtils;
import com.arowana.houdini.byod.utils.RealmUtil;
import com.arowana.houdini.byod.view.activity.BaseActivity;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;

import io.realm.Realm;
import io.realm.RealmConfiguration;
import io.realm.RealmList;
import io.realm.RealmResults;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MainApplication extends Application {

    private static RealmList<RedirectResponseData> redirectResponseData;

    class AppLifecycleHandler implements Application.ActivityLifecycleCallbacks {

        @Override
        public void onActivityCreated(Activity activity, Bundle savedInstanceState) {
        }

        @Override
        public void onActivityStarted(Activity activity) {
        }

        @Override
        public void onActivityResumed(Activity activity) {
            //fetchRedirectData();
            Log.d("Application","onActivityResumed");
        }

        @Override
        public void onActivityPaused(Activity activity) {
        }

        @Override
        public void onActivityStopped(Activity activity) {
        }

        @Override
        public void onActivitySaveInstanceState(Activity activity, Bundle outState) {
        }

        @Override
        public void onActivityDestroyed(Activity activity) {
        }


    }

    public static void fetchRedirectData(){
        Log.d("Application","onActivityResumed");

        Call<RedirctConfigObj> obj = ApiClient.getApiClient(RedirectApiInterface.class).fetchAll("2X");
        obj.enqueue(new Callback<RedirctConfigObj>() {
            @Override
            public void onResponse(@NonNull Call<RedirctConfigObj> call, @NonNull Response<RedirctConfigObj> response) {

                if (response.isSuccessful() && response.body() != null) {
                    RedirctConfigObj redirectConfigobj =response.body();
                    Realm mRealm = RealmUtil.getInstance();
                    mRealm.beginTransaction();
                    mRealm.copyToRealmOrUpdate(redirectConfigobj);
                    mRealm.commitTransaction();
                    setRespondsData(redirectConfigobj);
                }
            }

            @Override
            public void onFailure(@NonNull Call<RedirctConfigObj> call, @NonNull Throwable t) {
                t.printStackTrace();
            }
        });

    }

    private static void setRespondsData(RedirctConfigObj redirectConfigobj) {
        redirectResponseData = redirectConfigobj.getResponseData();
    }

    public static RealmList<RedirectResponseData> getResponseData(){
        return redirectResponseData;
    }

    public static final String TAG = "MainApplication";
    private static Context mContext;

    private static MainApplication singleton;

    private static BaseActivity activity;

    private static Set<String> moduleList;
    private static HashMap<String, String> speedDialModuleList = new HashMap<>();
    private static String speedDialValue;
    //  private AppLifecycleHandler callbackHandler = new AppLifecycleHandler();


    public MainApplication() {
    }

    public static MainApplication getInstance() {
        return singleton;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        singleton = this;
        mContext = getApplicationContext();

        initRealm();

        init();
       // registerActivityLifecycleCallbacks(callbackHandler);
    }

    public static Context getmContext() {
        return mContext;
    }

    private void init() {
        StoreHelperContext();
        FragmentTransactionUtils.getInstance().setContext(this);
        Consts.APP_CURRENT_STATE = SharedPreferenceUtil.getIntegerFromStore(StoreKeys.APPLICATION_STATE);
        addModuleList();

    }

    public void StoreHelperContext() {
        SharedPreferenceUtil.setStoreContext(singleton);
    }

    public Context getGlobalContext() {
        return this;
    }

    @Override
    protected void attachBaseContext(Context base) {
        super.attachBaseContext(base);
    }

    private void initRealm() {
        Realm.init(this.getInstance());
        RealmConfiguration config = new RealmConfiguration.Builder()
                .schemaVersion(1)
                .deleteRealmIfMigrationNeeded()
                .build();
        Realm.setDefaultConfiguration(config);
    }

    public static void setCurrentActivity(BaseActivity currentActivity) {
        activity = currentActivity;
    }
    public static BaseActivity getCurrentActivity() {
        return activity;
    }

    public static Set<String> getModuleList() {
        return moduleList;
    }

    public static void addModuleList() {
        moduleList = new HashSet<String>();
        moduleList.add("airLines");
        moduleList.add("spa");
        moduleList.add("restaurantBar");
        moduleList.add("bookARoom");
        moduleList.add("socialMedia");
        moduleList.add("pressreader");
        moduleList.add("subscribe");
        moduleList.add("whatsapp");
    }

    public static boolean isContainedSpeedDial(String key) {
        if (getSpeedDialKeyValueList().containsKey(key)) {
            setvalueFroKey(key);
            return true;
        }
        return false;
        //return getSpeedDialKeyValueList().containsKey(key)? true : false;
    }

    private static void setvalueFroKey(String key) {
        Iterator myVeryOwnIterator = speedDialModuleList.keySet().iterator();
        while (myVeryOwnIterator.hasNext()) {
            if (myVeryOwnIterator.next().equals(key))
                speedDialValue = (String) speedDialModuleList.get(key);
        }
    }

    public static String getValueforKey() {
        return speedDialValue;
    }

    public static HashMap<String, String> getSpeedDialKeyValueList() {
        return speedDialModuleList;
    }

    public static void setSpeedDialKeyValueList(String Name, String No) {
        speedDialModuleList = new HashMap<>();
        Log.d("EmiratePalaceApp", "spped Dial Module Keys.." + Name + " ,/No.. " + No + "/, SIZE.." + speedDialModuleList.size());
        speedDialModuleList.put(Name, No);
    }


    public static boolean appInstalledOrNot(String uri) {
        PackageManager pm = mContext.getPackageManager();
        boolean app_installed;
        try {
            pm.getPackageInfo(uri, PackageManager.GET_ACTIVITIES);
            app_installed = true;
        } catch (PackageManager.NameNotFoundException e) {
            app_installed = false;
        }
        return app_installed;
    }
}
